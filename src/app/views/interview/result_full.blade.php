<dl>
	<dt><span>メタボ前置</span></dt>
	<dd>メタボリック症候群は腹囲を内臓脂肪蓄積の目安とし、糖尿病・高血圧・高脂血症それぞれの予備軍の状況からも、
		重なり合って蓄積することによって動脈硬化性疾患（心筋梗塞・脳卒中など）のリスクが高くなることから誕生した概念です。
		メタボリック症候群あるいは糖尿病・高血圧・高脂血症の各疾患を予防することは最終的に動脈硬化を予防し、
		これによって発生する各種疾患にかからないようにするために最善の方法です。
	</dd>

	<dt><span>基礎情報</span></dt>
	<dd>あなたの　身長<!--{$app.result.tall}--><?php echo $result['tall'] ?>cm　体重<!--{$app.result.weight}--><?php echo $result['weight'] ?>kg　腹囲<!--{$app.result.abdominal_girth}--><?php echo $result['abdominal_girth'] ?>cm　
		から計算すると、あなたのBMIは、<!--{$app.result.output.3}--><?php echo $result['output'][3] ?>です。あなたの理想体重は、<!--{$app.result.output.1}--><?php echo $result['output'][1] ?>kgです。<br><br>
		<!--{if $app.result.output.3 < 13}-->
		<?php if ($result['output'][3] < 13) { ?>
			<!--{assign var="bmi" value="13"}-->
			<?php $bmi = 13 ?>
		<!--{elseif $app.result.output.3 > 38}-->
		<?php }elseif ($result['output'][3] > 38) { ?>	
			<!--{assign var="bmi" value="38"}-->
			<?php $bmi = 38 ?>
		<!--{else}-->
		<?php }else { ?>	
			<!--{assign var="bmi" value=$app.result.output.3}-->
			<?php $bmi = $result['output'][3] ?>
		<!--{/if}-->
		<?php } ?>	
		<img src="{{ URL::to('/') }}/img/graph_bmi_<?php echo $bmi ?>.gif" alt="BMIグラフ">
	</dd>

<!--{if !$app.result.judge_enable}-->
<?php if($result['judge_enable']==1){ ?>
	<dt><span>代替</span></dt>
	<dd>動脈硬化性疾患の既往があるようです。かかりつけの医師の指示に従ってください。</dd>
<?php } ?>
<!--{/if}-->

<!--{if $app.result.output.enable}-->
<?php if($result['output']['enable']){ ?>
	<dt><span>判定値</span></dt>
	<dd>
	<!--{if $app.result.output.anamnesis}-->
	<?php if($result['output']['anamnesis']){ ?>
		あなたは
		<!--{foreach from=$app.result.output.anamnesis key=key item=anamnesis_name}-->
		<?php foreach ($result['output']['anamnesis'] as $key => $anamnesis_name) { ?>
		<!--{$anamnesis_name}-->
		<?php echo $anamnesis_name ?>
		<?php } ?>	
		<!--{/foreach}-->
		を治療中です。
	<?php } ?>	
	<!--{/if}-->
	<!--{if $app.result.output.disease}-->
	<?php if(isset($result['output']['disease'])) { ?>
		<?php if($result['output']['anamnesis']){ ?><!--{if $app.result.output.anamnesis}--><br /><!--{/if}--><?php } ?>
		あなたは
		<!--{foreach from=$app.result.output.disease key=key item=disease_name}-->
		<?php foreach ($result['output']['disease'] as $key => $disease_name) { ?>
		<!--{$disease_name}-->
		<?php echo $disease_name ?>
		<?php } ?>
		<!--{/foreach}-->
		です。
	<?php } ?>	
	<!--{/if}-->
	</dd>
<?php } ?>	
<!--{/if}-->

<!--{if $app.result.judge_enable}-->
<?php if($result['judge_enable']){ ?>
	<dt><span>判定値</span></dt>
	<dd>あなたの今の心血管疾患リスクの程度は <?php echo $result['judge_metabo'][8]*2 ?><!--{$app.result.judge_metabo.8}--> です。<br /><br />
		<img src="{{ URL::to('/') }}/img/graph_risk_<?php echo $result['judge_metabo'][8]*2 ?>.gif" alt="心血管疾患リスクグラフ">
	</dd>

	<!--<dt><span>判定文</span></dt>-->
    <br />
	<dd><?php echo $result['metabo_judge_text'] ?><!--{$app.result.metabo_judge_text|nl2br}--></dd>

	<dt><span>予測値</span></dt>
	<dd>あなたが今後一生涯で何らかの動脈硬化性のイベント(心血管疾患・脳卒中等)を起こすリスクは、
		<!--{if $app.result.judge_risk.16 == 1}-->
		<?php if($result['judge_risk'][16] == 1) { ?>
			<!--{assign var="risk" value="1"}-->
			<?php $risk = 1 ?>
			低い
		<!--{elseif $app.result.judge_risk.16 == 2}-->
		<?php }elseif($result['judge_risk'][16] == 2){ ?>
			<!--{assign var="risk" value="3"}-->
			<?php $risk = 3 ?>
			やや低い
		<!--{elseif $app.result.judge_risk.16 == 3}-->
		<?php }elseif($result['judge_risk'][16] == 3){ ?>
			<!--{assign var="risk" value="5"}-->
			<?php $risk = 5 ?>
			中等度
		<!--{elseif $app.result.judge_risk.16 == 4}-->
		<?php }elseif($result['judge_risk'][16] == 4){ ?>
			<!--{assign var="risk" value="7"}-->
			<?php $risk = 7 ?>
			高い
		<!--{elseif $app.result.judge_risk.16 == 5}-->
		<?php }elseif($result['judge_risk'][16] == 5){ ?>
			<!--{assign var="risk" value="9"}-->
			<?php $risk = 9 ?>
			きわめて高い
		<?php } ?>	
		<!--{/if}-->
		です。<br><br>
		<img src="{{ URL::to('/') }}/img/graph_risk_<?php echo $risk ?>.gif" alt="動脈硬化性リスクグラフ">
	</dd>

	<dt><span>指導運動</span></dt>
	<dd>
		<!--{if $app.result.judge_sports.6 != "E"}-->
		<?php if($result['judge_sports'][6] != 'E') { ?>
			現在の生活の改善を目指す場合、まずは&nbsp;&nbsp;
			<!--{foreach from=$app.result.judge_sports.7 key=key item=judge_sports name=judge_sports}-->
			<?php $last = count($result['judge_sports'][7])-1; ?>
			<?php foreach ($result['judge_sports'][7] as $key => $judge_sports) {?>
				<?php echo $judge_sports ?><!--{$judge_sports}--><?php if($key == $last ){ ?><!--{if !$smarty.foreach.judge_sports.last}-->・<!--{/if}--><?php } ?>
			<?php } ?>	
			<!--{/foreach}-->
			のような運動を、週に　<?php echo $result['sports_viable_count'] ?><!--{$app.result.sports_viable_count}-->　回、
			1回　<?php echo $result['sports_viable_time'] ?> <!--{$app.result.sports_viable_time}-->　時間程度取り組んでみてください。
		<?php }else{ ?>	
		<!--{else}-->
			医師の許可がない場合は運動は禁止です。許可された場合には、必ず医師の指示に従って運動してください。
		<?php } ?>		
		<!--{/if}-->
	</dd>

	<dt><span>指導食事</span></dt>
	<dd>あわせて、食事の総カロリーを<?php echo $result['output'][13] ?><!--{$app.result.output.13}-->kcalにするようにチャレンジしてみてください。</dd>

	<dt><span>補足</span></dt>
	<dd>持病があって医療機関にかかりつけの方は、かかりつけ機関の先生の指示に従ってください。</dd>
<?php } ?>		
<!--{/if}-->

	<!--{if $app.result.show_smoking_leading}-->
	<?php if(isset($result['show_smoking_leading'])) { ?>
	<dt><span>喫煙指導</span></dt>
	<dd>喫煙は動脈硬化をすすめる原因になります。またその他多くの病気(肺気腫、がんなど）のもとになります。
		周囲の方の健康を害するものでもあり、できる限り禁煙しましょう。
		禁煙指導が必要な場合は専門医療機関にご相談ください。</dd>
	<?php } ?>	
	<!--{/if}-->

	<dt><span>メンタル判定</span></dt>
	<dd>
		<!--{foreach from=$app.result.mental_result key=key item=textext}-->
		<?php foreach ($result['mental_result'] as $key => $textext) { ?>
			<?php echo $textext ?><!--{$textext}--><br>
		<?php } ?>		
		<!--{/foreach}-->
	</dd>
	@include('interview.result_food')
	<!--{include file="result_food.tpl"}-->
</dl>

