@include('admin/header')

<hr />
<div id="wrapper">
<div id="accountList" class="contents">
	<h2 id="pageTitle">アカウント一覧</h2>
	<hr />

	
	<div id="main">

		<div class="table-wrap">
			<table summary="アカウント一覧">
				<thead>
					<tr>
						<th></th>
						<th>アカウントID</th>
						<th>ログインID</th>
						<th>氏名</th>
						<th>氏名かな</th>
						<th>権限</th>
						<th>削除</th>
						
					</tr>
				</thead>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th>アカウントID</th>
						<th>ログインID</th>
						<th>氏名</th>
						<th>氏名かな</th>
						<th>権限</th>
						<th>削除</th>
					
					</tr>
				</tfoot>
									<tbody>
					@foreach ($accounts as $account)
											<tr>
							<td class="column1">
									<input type="button" value="編集" onClick="location.href='editaccount/{{$account->account_id}}'" class="input-btn">
														</td>
							<td class="column2">{{ $account->account_id }}</td>
							<td class="column3">{{ $account->login_name }}</td>
							<td class="column4">{{ $account->name }}</td>
							<td class="column5">{{ $account->name_kana }}</td>
							<td class="column6 center">{{ $roles[$account->role_id]->role_name }}</td>
							<td class="column7">@if ($account->valid ==1) 
													 有効
												@else 
												   無効
											    @endif
											</td>
											
						</tr>
					@endforeach
												</table>

			<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>

	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
