
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
<title>生活習慣診断</title>
<?php
$baselink = "http://m-metabo-web.dev.pmjt.net/src/public";
?>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link type="text/css" href="{{ $baselink }}/css/sunny/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/common/format.css" media="all" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/common/base.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/common_style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/print_style.css" media="print" />
<link rel="stylesheet" type="text/css" href="{{ $baselink }}/css/addition.css" rel="stylesheet" />
<script type="text/javascript" src="{{ $baselink }}/js/jquery-1.6.3.min.js"></script>
<script type="text/javascript" src="{{ $baselink }}/js/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="{{ $baselink }}/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="{{ $baselink }}/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="{{ $baselink }}/js/jquery.alerts.js"></script>
<script type="text/javascript" src="{{ $baselink }}/js/style.js"></script>
<script type="text/javascript" src="{{ $baselink }}/js/jquery.format.1.05.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('.slider_input').format({precision: 1, allow_negative:false, autofix:true});
});
</script>
</head>

<body id="interview">
<div id="wrap">

<div id="noscript" style="display:none">
	<p>このページは、Javascriptを使用しています。<br>
	Javaascriptを有効にしてご利用ください。</p>
</div>
<script>
	document.getElementById("noscript").style.display = "none";
</script>

<!-- ↓↓↓ header  ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="header">
<div id="headerInner">
	<h1><!--{$config.service_name}--></h1>
	<ul>
		<li class="btn-faq"><a href="#">FAQ</a>&nbsp;&nbsp;|</li>
		<li class="btn-contact"><a href="#">お問い合わせ</a></li>
		<li class="btn-mypage"><a href="{{ $baselink }}/mypage">マイページ</a></li>
		<!--{assign var="url" value="`$config.interview_absolute_url`?action_interview_logout_do=true"}-->
		<?php
		if ( \Auth::tuser()->check() ) {
		?>
			<li class="btn-logout"><a href="{{ $baselink }}/logout">ログアウト</a></li>
		<?php
		} else {
			// can just use # as the link in this case because currently the login page is the only page that has the 'not logged in' status.
			// but to make sure in the case the system add more pages with 'not logged in' status, should use a link
			if (Session::has('tenant_token'))
				$link = $baselink."/top/".Session::get('tenant_token');
			else
				$link = "#"; 
		?>
			<li class="btn-login"><a href="{{ $link }}">ログイン</a></li>
		<?php
		}
		?>
	</ul>
</div>
</div><!-- /#header -->
								<?php
									 if ( \Auth::tuser()->check() )
										 $tenant_id = \Auth::tuser()->get()->tenant_id;
									 else 
									 if (!isset($tenant_id)) $tenant_id = 'abc'; // $tenant_id was set in TUserController action login   
								?>
 								<script type="text/javascript">
								function Logo()
								{
								   
									$("#header h1").css("background-image","url('{{ $baselink }}/logo/{{ $tenant_id }}')");
								    
								}
 								Logo();
 								</script>
<!-- ↑↑↑ header  ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->

