<?php

namespace Admin;

class IndexController extends \BaseController {

    public $xml_template_file	= array(0=>'../app/data/followup/followup.xml',   
									1=>'../app/data/followup/remind.xml' );  
	public $change_log = '../app/data/change.log';	 
	
	public function loggedin(){
	  if ( \Auth::user()->check() ) {
		  return true;
	  }
	  else return false; 
    }
	
	public function special(){
		$special = ((\Auth::user()->get()->role_id ==0)&&(\Session::has('temp_tenant_id')));
		return $special;
	}
	
	public function getIndex()
	{
		
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}

        $current_year = date("Y");
        $data["years_option"] = "";
        $data["years_option_2"] = "";
        for($start_year = 2013;$start_year <= $current_year;$start_year++){
            if($start_year == 2013){
                $data["years_option"] .= "<option value='{$start_year}' selected='selected'>{$start_year}</option>";
            }elseif($start_year == $current_year){
                $data["years_option_2"] .= "<option value='{$start_year}' selected='selected'>{$start_year}</option>";
            }else{
                $data["years_option"] .= "<option value='{$start_year}'>{$start_year}</option>";
                $data["years_option_2"] .= "<option value='{$start_year}'>{$start_year}</option>";
            }
        }
        $data["months_option"] = "";
        $data["months_option_2"] = "";
        for($i=1;$i<=12;$i++){
            $data["months_option"] .= "<option value='{$i}'>{$i}</option>";
            if($i==date("n")){
                $data["months_option_2"] .= "<option value='{$i}' selected='selected'>{$i}</option>";
            }else{
                $data["months_option_2"] .= "<option value='{$i}'>{$i}</option>";
            }
        }
        $data["days_option"] = "";
        $data["days_option_2"] = "";
        for($i=1;$i<=31;$i++){
            $data["days_option"] .= "<option value='{$i}'>{$i}</option>";
            if($i==date("j")){
                $data["days_option_2"] .= "<option value='{$i}' selected='selected'>{$i}</option>";
            }else{
                $data["days_option_2"] .= "<option value='{$i}'>{$i}</option>";
            }
        }
		
		$view = \View::make('admin.index');
        foreach($data as $key=>$val){
            \View::share($key,$val);
        }
        return $view;
	}

	public function getLogout(){
    	  \Auth::user()->logout();
		  
		  if (\Session::has('temp_tenant_id')){
			  \Session::forget('temp_tenant_id');
		  }
		  
 	      return \Redirect::to("admin/login");
	}

	public function getLogin(){
		if ($this->loggedin()) {
			return \Redirect::to("admin");
		}
		return \View::make('admin.login');
	}

	public function postLogin(){
 	   
 	   $username= \Input::get('login_name');
 	   $password = \Input::get('passward');
	    	  
 	   $var = \Auth::user()->attempt(array('login_name' => $username, 'password' => $password));
   
  	   if ($var) {
		   
  	       return \Redirect::to("admin");
  	   }else
  		   return \Redirect::to("admin/login");
	}
	
	function getHistoryprintlist(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		
		return \View::make('admin.historyprintlist');
	}
	
	function getInterviewlist(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		
		return \View::make('admin.interviewlist');
	}
	
	function maileditfile($name,$tenant_id){
		//if ($tenant_id==0) return $name;
		$t = strrpos($name,"."); 
		$thename = substr($name,0,$t). "_".$tenant_id. substr($name,$t);
		return $thename;
	}
	
	function getMailedit(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		
		if (\Input::old('followup_subject') !== null){
			// followup
			$followup['subject']	    = \Input::old('followup_subject');
			$followup['fromname']		= \Input::old('followup_sender_name');
			$followup['fromaddress']	= \Input::old('followup_sender_address');
			$followup['body']			= \Input::old('followup_body');

			// remind
			$remind['subject']		= \Input::old('remind_subject');
			$remind['fromname']		= \Input::old('remind_sender_name');
			$remind['fromaddress']	= \Input::old('remind_sender_address');
			$remind['body']			= \Input::old('remind_body');
			
		} else {
		
			$xml_template_file = $this->xml_template_file;
								
			for ($i=0; $i<=1; $i++){

				// xmlファイル編集

				$data = $this->xml_parser($xml_template_file[$i],$tenant_id);

				if (empty($data)) {
					//echo 'admin_trouble XMLファイルが開けません';
					//return \Redirect::to("admin");
					$xml_file_o = $xml_template_file[$i];
					$data = $this->xml_parser($xml_file_o,$tenant_id);
					if (empty($data)) {
						if ($i==0) {
							$followup['subject'] = "";	$followup['fromname'] = ""; $followup['fromaddress'] = ""; $followup['body'] =  "";
						} 
						if ($i==1) {
							$remind['subject'] = "";	$remind['fromname'] = ""; $remind['fromaddress'] = ""; $remind['body'] =  "";
						}
					} else {
						if ($i == 0) {
							$followup = $data;
							
						}elseif ($i == 1){
							$remind = $data;
						}
						$file = @fopen($xml_file_o, "w");
						$fwrite = @fwrite($file, file_get_contents($xml_file_o));
						@fclose($file);
					}
				}
				else
				if ($i == 0) {
					$followup = $data;
				}elseif ($i == 1){
					$remind = $data;
				}
			}
		}	
			// ログファイル読み込み 最終行を取得
			$change_log =$this->maileditfile($this->change_log,$tenant_id);
			
			if (file_exists($change_log)){
				$fp = fopen($change_log, "r");
				while (!feof($fp)) {
					$line[] = fgets($fp);
				}
				fclose($fp);
			}

			// ログが存在した場合、表示内容を生成
			if (!empty($line)) {
				$change_last_line = explode(',', end($line));
				
				foreach ($change_last_line as $key => $val) {
					if ($key == 0) {
						$change_last['create_datetime'] = $val;
						
					}elseif ($key == 1){
						$change_last['update_datetime'] = $val;
					}elseif ($key == 2){
						$change_last['update_user_id'] = $val;
					}
				}
			}
			
		
		return \View::make('admin.mailedit',compact('followup','remind','change_last'));
	}

    function postMailedit(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		$form_data = \Input::all();
		$rules = 	array(
			'followup_sender_address' => 'required|email',
			'followup_subject' => 'required',
			'followup_sender_name' => 'required',
			'followup_body' => 'required',
			
			'remind_sender_address' => 'required|email',
			'remind_subject' => 'required',
			'remind_sender_name' => 'required',
			'remind_body' => 'required'
			
		);
  	
		$validator = \Validator::make($form_data, $rules,\Config::get('error_messages'));
		
		if ($validator->fails()) {
			\Input::flash(); 
			return \Redirect::to("admin/mailedit")->withErrors($validator)->withInput();
		}
		
		$xml_template_file = $this->xml_template_file;
		
		for ($i=0; $i<=1 ; $i++){
			if ($i == 0) {
				// followup
				$data['subject']		= $form_data['followup_subject'];
				$data['fromname']		= $form_data['followup_sender_name'];
				$data['fromaddress']	= $form_data['followup_sender_address'];
				$data['body']			= $form_data['followup_body'];

			}elseif ($i == 1){
				// remind
				$data['subject']		= $form_data['remind_subject'];
				$data['fromname']		= $form_data['remind_sender_name'];
				$data['fromaddress']	= $form_data['remind_sender_address'];
				$data['body']			= $form_data['remind_body'];
			}
			
			
			$this->create_xml($data, $this->maileditfile($xml_template_file[$i],$tenant_id));
		}
		
		
		$change_log =$this->maileditfile($this->change_log,$tenant_id);
		$fp = fopen($change_log, "a");
		$fwrite = fwrite($fp, "\r\n" . date("Y-m-d H:i:s", filectime($change_log)) . ',' . date("Y-m-d H:i:s") . ',' . \Auth::user()->get()->account_id);
		if ($fwrite === false) {
			fclose($fp);
			//echo "ログファイルが書き込めません。";
			//return \Redirect::to("admin");
			
		}
		fclose($fp);
		
		return \Redirect::to("admin/mailedit");
		
	}
	
	
	
	function getTenantlist(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		
		$tenants = \Tenant::all();
		
		/*
		for ($i=0;$i<count($roles); $i++)
			echo $roles[$i]->role_id." "; 
		return;*/
		return \View::make('admin.tenantlist',compact('tenants'));
	}
	
	function getSpeciallogin($tenant_id){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		// only system admin can use this special login feature
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		\Session::put('temp_tenant_id',$tenant_id);
	   
	
		return \Redirect::to("admin");
		
	}
	
	function getUploadlogotenant($token){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		
		/*
		// Use token from Session
		if (!\Session::has('tenant_token')) return;
		$token = \Session::get('tenant_token');
		*/
		
		// Get tenant from token
		$tenant = \Tenant::where("token","=",$token)->first();
		if ($tenant==null) return ;
		$id = $tenant->tenant_id;
		
		// Check the existence of current logo
    	 $c = \UFileTenant::where("tenant_id","=",$id)->count();
		 
		 if ($c ==0)  $exist = false; else $exist = true;
		 
		 
    	return \View::make('admin.uploadlogotenant',compact('exist','tenant'));
	}
	
	//admin/showfiletenant/token will generate the picture 
	// put this code in tag img in the html files, i.e. : 
	//														or  <img src="admin/showtenantlogo"  />
	// 												or : <img src="admin/showfiletenant/{{ $tenant->token }}" />
	//												
	function getShowtenantlogo(){
		if (\Session::has('tenant_token'))
		{
		    //
			$token = \Session::get('tenant_token');
			$this->getShowfiletenant($token);
		} else 
		return;
	}
	
    function getShowfiletenant($token){

		
		// Get tenant from token
		$tenant = \Tenant::where("token","=",$token)->first();
		if ($tenant==null) return ;
		$id = $tenant->tenant_id;
		
 	   $file = \UFileTenant::where("tenant_id","=",$id)->first();
 	   if ($file==null) return;
	   echo header("Content-type: ".$file->mime);
 	   echo $file->data;
 	   return;
    }
	
	
    function getShowfiletenant2($tenant_id){

		
		$id = $tenant_id;
		
 	   $file = \UFileTenant::where("tenant_id","=",$id)->first();
	   if ($file==null) {
		   $d = file_get_contents("img/common/header_logo.jpg");
		   $m = "image/jpeg";
	   }
	   else {
	     $d = $file->data;
		 $m = $file->mime;
   	   }
	   echo header("Content-type: ".$m);
 	   echo $d;
 	   return;
    }
	
	/*
	function postUploadlogotenant($token){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		
		
      $file = \Input::file('uploaded_file');
  	  $path = $file->getRealPath();
  	  $name = $file->getClientOriginalName();
  	  $mime = $file->getMimeType();
  	  $size = $file->getSize();

	  // Get tenant from token
	  $tenant = \Tenant::where("token","=",$token)->first();
	  if ($tenant==null) return ;
	  $id = $tenant->tenant_id;
	
  	  $afile = \UFileTenant::where("tenant_id","=",$id)->first();
	  if ($afile==null) $afile = new \UFileTenant;
	  
  	  $afile->name = $name;
  	  $afile->mime = $mime;
  	  $afile->size = $size;
	  $afile->tenant_id = $id;
  	  $afile->data = file_get_contents($path);
  	  $afile->created = date('Y-m-d H:i:s');
  	  $afile->save();
	 
  	  return \Redirect::to("admin/edittenantaccount"."/$token");
	}
	*/
	
	
	function getEdittenantaccount($token){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		
  	   // Get tenant from token
  	    $tenant = \Tenant::where("token","=",$token)->first();
 	    if ($tenant==null) return ;
	  
		$id = $tenant->tenant_id;
		
		// Check the existence of current logo
    	 $c = \UFileTenant::where("tenant_id","=",$id)->first();
		 
		 if ($c == NULL)  {$exist = false; $clientfilepath = ""; } else
			 { $exist = true; $clientfilepath = $c->name; }
		 
		 
  	    if (\Input::old('tenant_name')!==null) {
  		 
		   $tenant->tenant_name = \Input::old('tenant_name');
		   //$tenant->logo_url = \Input::old('logo_url');
		   //$tenant->interview_type = \Input::old('interview_type');
		   $oldtenant_token = \Input::old('token');
		   $tenant->valid = \Input::old('valid'); 
  	   } else $oldtenant_token = $tenant->token;
	  
		return \View::make('admin.edittenantaccount',compact('tenant','oldtenant_token','exist','clientfilepath'));
	}
	
	function postEdittenantaccount($token){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		
    	// Get tenant from token
    	  $tenant = \Tenant::where("token","=",$token)->first();
    	  if ($tenant==null) return ;
		  
  	   $inputs = \Input::all();
	   

	 
	   $rulesedit= array(
	     		'token' => 'alpha_num|required|halfwidth|min:4|max:12|unique:m_tenant,token,'.$tenant->tenant_id.',tenant_id',	
				'tenant_name' =>'required',
				//'interview_type' => 'required|numeric'
	       );
	
  	  
  	   $validator = \Validator::make($inputs, $rulesedit,\Config::get('error_messages'));
	   
  	   if ($validator->passes()){
			   if (\Input::hasFile('uploaded_file')) {	
			          $file = \Input::file('uploaded_file');
				   	  $path = $file->getRealPath();
				   	  $name = $file->getClientOriginalName();
				   	  $mime = $file->getMimeType();
				   	  $size = $file->getSize();
		  
 	  
				 	  $id = $tenant->tenant_id;
	
				   	  $afile = \UFileTenant::where("tenant_id","=",$id)->first();
				 	  if ($afile==null) $afile = new \UFileTenant;
	  
				   	  $afile->name = $name;
				   	  $afile->mime = $mime;
				   	  $afile->size = $size;
				 	  $afile->tenant_id = $id;
				   	  $afile->data = file_get_contents($path);
				   	  $afile->created = date('Y-m-d H:i:s');
				   	  $afile->save();
					  
		   
		   		   	 //$tenant->logo_url = \URL::to("/")."/admin/showfiletenant/".$inputs['token'];
					  $tenant->logo_url = \URL::to("/")."/logo/".$tenant->tenant_id;
			  }
 		   $tenant->tenant_name = $inputs['tenant_name'];
 		   
		   
		   $tenant->token = $inputs['token'];
 		   $tenant->valid = $inputs['valid'];
 		  
  		   $tenant->save();
  		   
  		   return \Redirect::to('admin/tenantlist');
  	   }
  	   //$errors = $validator->messages();
  	   //print_r($errors);
  	   return \Redirect::to('admin/edittenantaccount/'.$token)->withErrors($validator)->withInput();
		
	}
	

	
    function getRegistertenant(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		$tenant = new \Tenant;
  	    if (\Input::old('tenant_name')!==null) {
  		 
		   $tenant->tenant_name = \Input::old('tenant_name');
		   $tenant->logo_url = \Input::old('logo_url');
		   $tenant->interview_type = \Input::old('interview_type');
		   $tenant->token = \Input::old('token');
		   $tenant->valid = \Input::old('valid'); 
  	   }
        return \View::make('admin.registertenant',compact('tenant'));
    }
   
   
    function postRegistertenant(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		if (\Auth::user()->get()->role_id != 0){
		    return \Redirect::to("admin");
		}
		
 	   $inputs = \Input::all();
  	   //$messages=array(
  	   //  'token.max' => 'token max is 12'
  	   //);
 	   $validator = \Validator::make($inputs, \Tenant::$rulesreg,\Config::get('error_messages'));
	   
 	   if ($validator->passes()){
		   $tenant = new \Tenant;
 		   $tenant->tenant_name = $inputs['tenant_name'];
 		   //$tenant->logo_url = $inputs['logo_url'];
 		   $tenant->interview_type= 1;
 		   $tenant->token = $inputs['token'];
 		   $tenant->valid = $inputs['valid'];
 		  
  		   $tenant->save();
		   $tenant_id = $tenant->tenant_id;
	
		   
		   $account = new \User;
		   $account->tenant_id = $tenant_id;
		   $ab = 'tenant_'.$tenant_id;
		   
 		   $account->password =  \Hash::make($ab);
		   $account->login_name = $ab;
		   $account->name = $ab;
		   $account->name_kana = '';
		   $account->role_id = 1;
		   $account->valid = 1;
		   $t = date('Y-m-d H:i:s');
		   $account->create_datetime = $t;
		   $account->update_datetime = $t;
 		   $account->save();
		   
		   
		   
		   if (\Input::hasFile('uploaded_file')) {	
		          $file = \Input::file('uploaded_file');
			   	  $path = $file->getRealPath();
			   	  $name = $file->getClientOriginalName();
			   	  $mime = $file->getMimeType();
			   	  $size = $file->getSize();
		  
 	  
			 	  $id = $tenant->tenant_id;
	
			   	  $afile = \UFileTenant::where("tenant_id","=",$id)->first();
			 	  if ($afile==null) $afile = new \UFileTenant;
	  
			   	  $afile->name = $name;
			   	  $afile->mime = $mime;
			   	  $afile->size = $size;
			 	  $afile->tenant_id = $id;
			   	  $afile->data = file_get_contents($path);
			   	  $afile->created = date('Y-m-d H:i:s');
			   	  $afile->save();
				  
	   		   	  $tenant->logo_url = \URL::to("/")."/logo/".$tenant->tenant_id;
	   		   	  $tenant->save();
		  }
  		   
 		   return \Redirect::to('admin/tenantlist');
 	   }
 	   //$errors = $validator->messages();
 	   //print_r($errors);
 	   return \Redirect::to('admin/registertenant')->withErrors($validator)->withInput();
    }
	
	function getChangeinterviewtype($newvalue){
		//AJAX 
		 
		if (!$this->loggedin()) {
			return;
			//return \Redirect::to("admin/login");
		}

		 if (!is_numeric($newvalue)) return;
		 
 		$special = $this->special();
		
 		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
 		    return;
			//return \Redirect::to("admin");
 		}
		
 		if ($special) $tenant_id = \Session::get('temp_tenant_id');
 		else 
 		$tenant_id = \Auth::user()->get()->tenant_id;
		
		
    	
    	  $tenant = \Tenant::find($tenant_id);
    	  if ($tenant==null) return ;
		 
		  $tenant->interview_type = $newvalue;
		  $tenant->save();
		 return;  
	}

    function getAccountlist(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		$accounts = \User::where('tenant_id','=',$tenant_id)->get();
		$roles = \UserRole::all();
		/*
		for ($i=0;$i<count($roles); $i++)
			echo $roles[$i]->role_id." "; 
		return;*/
		return \View::make('admin.accountlist',compact('accounts','roles'));
	}
	
	
	function getEditaccount($accountid){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
	
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		$account = \User::find($accountid);
		if ($account==null) return;
		
		if ($account->tenant_id !=$tenant_id) return;
		
		$roles = \UserRole::all();
        \View::share('roles',$roles);
		return \View::make('admin.editaccount',compact('account','roles'));
	}
	
	function postEditaccount($accountid){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		
		$user = \User::find($accountid);
		if ($user==null) return;
	
		if ($user->tenant_id !=$tenant_id) return;
		
  	   $inputs = \Input::all();
  	   //$messages=array(
  	   //  'email.unique' => 'The email has already been taken!!'
  	   //);
	   $rulesedit= array(
	 		//"username" => 'required|alpha_num|min:3',
	 		//"email" => "required|email|unique:t_account,email",
	 		'login_name' => "required|alphanum_under|min:4|unique:t_account,login_name,".$accountid.',account_id'	,
            'password' => 'required|confirmed|halfwidth|min:4|max:12',
	 		'name' => 'required|max:45',
	 		'name_kana' => 'required|max:45',
	
	   );
  	   $validator = \Validator::make($inputs, $rulesedit,\Config::get('error_messages'));
	   
  	   if ($validator->passes()){
 		 	
  		   $user->password =  \Hash::make($inputs['password']);
 		   $user->login_name = $inputs['login_name'];
 		   $user->name = $inputs['name'];
 		   $user->name_kana = $inputs['name_kana'];
 		   $user->role_id = $inputs['role_id'];
 		   $user->valid = $inputs['valid'];
 		   $t = date('Y-m-d H:i:s');
 		   $user->update_datetime = $t;
		   $user->update_account_id = \Auth::user()->get()->account_id;
  		   $user->save();
  		   
  		   return \Redirect::to('admin/accountlist');
  	   }
  	   //$errors = $validator->messages();
  	   //print_r($errors);
  	   return \Redirect::to('admin/editaccount/'.$accountid)->withErrors($validator);
	}
	
    function getRegister(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		
        return \View::make('admin.register');
    }
   
   
    function postRegister(){
		if (!$this->loggedin()) {
			
			return \Redirect::to("admin/login");
		}
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("admin");
		}
		
 	   $inputs = \Input::all();
 	   //$messages=array(
 	   //  'email.unique' => 'The email has already been taken!!'
 	   //);
 	   $validator = \Validator::make($inputs, \User::$rules,\Config::get('error_messages'));
	   
 	   if ($validator->passes()){
 		   $user = new \User;
 		   //$user->email = $inputs['email'];
   			if ($special) $tenant_id = \Session::get('temp_tenant_id');
   			else 
   			$tenant_id = \Auth::user()->get()->tenant_id;
		
		   $user->tenant_id = $tenant_id;
 		   $user->password =  \Hash::make($inputs['password']);
		   $user->login_name = $inputs['login_name'];
		   $user->name = $inputs['name'];
		   $user->name_kana = $inputs['name_kana'];
		   $user->role_id = $inputs['role_id'];
		   $user->valid = $inputs['valid'];
		   $t = date('Y-m-d H:i:s');
		   $user->create_datetime = $t;
		   $user->update_datetime = $t;
 		   $user->save();
 		   //Auth::login($user);
 		   return \Redirect::to('admin/accountlist');
 	   }
 	   //$errors = $validator->messages();
 	   //print_r($errors);
 	   return \Redirect::to('admin/register')->withErrors($validator);
    }
   

	function xml_parser($xml_file,$tenant_id){

        $xml_file2 = $this->maileditfile($xml_file,$tenant_id);
        $root_dir = $_SERVER["SCRIPT_FILENAME"];
        $root_dir = explode("/",$root_dir);
        array_pop($root_dir);
        $root_dir = implode('/',$root_dir);
        $data = array();
		if (file_exists($root_dir.'/'.$xml_file2)) {
			// XMLファイルの読込み
			$content	= file_get_contents($xml_file2);
			// XMLデータを配列に格納
			$xml_parser	= xml_parser_create();
			// XMLデータを配列にパース
			// xml_parser：xml_parser_create()のオブジェクト
			// $content：file_get_contentsより読み込んできたXMLファイルの内容
			// $vals：値を格納するための変数）
			xml_parse_into_struct($xml_parser, $content, $vals);
			// XMLパーサを解放
			xml_parser_free($xml_parser);
			foreach ($vals as $key){
				// 配列にvalueが存在するか確認
				if (array_key_exists('value', $key)) {
					if (trim($key['value']) != '') {
						$data[strtolower($key['tag'])] = $key['value'];
					}
				}
			}
            if($data){
    			return $data;
            }
		}
        $content	= file_get_contents($root_dir.'/'.$xml_file);
        $xml_parser	= xml_parser_create();
        xml_parse_into_struct($xml_parser, $content, $vals);
        xml_parser_free($xml_parser);
        foreach ($vals as $key){
            if (array_key_exists('value', $key)) {
                if (trim($key['value']) != '') {
                    $data[strtolower($key['tag'])] = $key['value'];
                }
            }
        }
        return $data;

	}
	
	
	/**
	 * XMLファイル作成処理
	 *
	 * @param	$input_data			:xmlデータ
	 * @param	$xml_template_file	:xmlファイル名
	 * @return String
	 */
	function create_xml($input_data, $xml_template_file){

		// XMLのルートタグとXML宣言
		$root = '<?xml version="1.0" encoding="UTF-8"?><contents></contents>';
		// XML作成開始
		$xml = new \SimpleXMLElement($root);
	    // contentsタグにcontentタグ作成
		$content_tag = $xml->addChild('content');
	    //子要素を追加
	    $content_tag->addChild('subject', $this->_h($input_data['subject']));
	    $content_tag->addChild('fromname', $this->_h($input_data['fromname']));
	    $content_tag->addChild('fromaddress', $this->_h($input_data['fromaddress']));
	    $content_tag->addChild('body', $this->_h($input_data['body']));

		//XML保存
        if(file_exists($xml_template_file)){
    		$file = @fopen($xml_template_file, "w");
		    $fwrite = @fwrite($file, $xml->asXML());
            if ($fwrite === false) {
                fclose($file);
                // XMLファイルが書き込めません。
                return 'admin_trouble';
            }
    		@fclose($file);
        }else{
            $root_dir = $_SERVER["SCRIPT_FILENAME"];
            $root_dir = explode("/",$root_dir);
            array_pop($root_dir);
            $root_dir = implode('/',$root_dir);
            file_put_contents($root_dir.'/'.$xml_template_file,$xml);
        }

	}

	/**
	 * エスケープ処理
	 * XMLの値の中に&が入ってるとエラーで落ちるので
	 *
	 * @param String
	 * @return String
	 */
	function _h($value)
	{
	    return htmlspecialchars($value, ENT_QUOTES);
	}
	
}
