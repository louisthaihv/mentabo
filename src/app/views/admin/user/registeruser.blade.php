@include('admin/header')
<hr />
<div id="wrapper">
<div id="accountEntry" class="contents">
	<h2 id="pageTitle">ユーザー登録</h2>
	<hr />

	<div class="buttonArea">
		<ul>
			<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
		</ul>
	</div>

	
	<div id="main">
		
			<P><span class="required">＊</span>は必須項目です。</P>

			<form  action="registeruser" method="post">
                @if($errors->all('message'))
                <ul class="valid_error">
                    @foreach($errors->all('<li>:message</li>') as $message)
                    {{ $message }}
                    @endforeach
                </ul>
                @endif
				<div class="table-wrap">
					<table summary="ユーザー登録">
												<tr>
							<th><span class="required">＊</span>ログインID<span class="caution"></span></th>
							<td><input type="text" name="login_id" value="{{ $theuser->login_id }}" maxlength="12" /><br />
                            <span style="font-size:0.8em;">*ログインIDは半角英数字4〜12文字で入力してください。</span></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>ニックネーム</th>
							<td><input type="text" name="nick_name" value="{{ $theuser->nick_name }}" maxlength="45" /></td>
						</tr>
				
						<tr>
							<th>姓</th>
							<td><input type="text" name="last_name" value="{{ $theuser->last_name }}" maxlength="45" /></td>
						</tr>
				
						<tr>
							<th>名</th>
							<td><input type="text" name="first_name" value="{{ $theuser->first_name }}" maxlength="45" /></td>
						</tr>
				
						<tr>
							<th>姓カナ</th>
							<td><input type="text" name="last_name_kana" value="{{ $theuser->last_name_kana }}" maxlength="45" /></td>
						</tr>
				
						<tr>
							<th>名カナ</th>
							<td><input type="text" name="first_name_kana" value="{{ $theuser->first_name_kana }}" maxlength="45" autocomplete="off" /></td>
						</tr>
					
						<tr>
							<th><span class="required">＊</span>パスワード<span class="caution"></span></th>
							<td><input type="password" name="password" value="" maxlength="12" autocomplete="off" /><br />
                                <span style="font-size:0.8em;">*パスワードは半角英数字4〜12文字で入力してください。</span></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>パスワード確認<span class="caution"></span></th>
							<td><input type="password" name="password_confirmation" value="" maxlength="12" /></td>
						</tr>
										
						<tr>
							<th><span class="required">＊</span>性別</th>
							<td><select name="gender">
									<option value="1" <?php if ($theuser->gender=="1") echo 'selected="selected"';?> >男性</option>
									<option value="2" <?php if ($theuser->gender=="2") echo 'selected="selected"';?> >女性</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<th><span class="required">＊</span>生年月日</th>
							<td>
								<?php
								$current_year = date("Y");
								$c1 = $current_year - 100;
								$c2 = $current_year - 18;
								
								
								
								?>
								<script type="text/javascript">
								   function resetdays(){
									   // find the number of days in the month
									   year =  document.getElementById("year").value;
									   month =  document.getElementById("month").value - 1;
									   var monthStart = new Date(year, month, 1);
									   var monthEnd = new Date(year, month + 1, 1);
									   var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
									   // change the select box day
									   var select = document.getElementById("day");
									   select.options.length = 0;
									  
									   for(i=1; i<=monthLength; i++) {
									   	    select.options[select.options.length] = new Option(i, i);
									   	}
								   }
								</script>
								<select id="year" name="year" onchange="resetdays();" >
									<?php 
									for ($i=$c1; $i<=$c2; $i++){
									?>
									<option value="{{ $i }}" <?php if ($year==$i) echo 'selected="selected"';?> >{{ $i }}</option>
									<?php
									}
									?>
								</select>年
								<select id="month" name="month"  onchange="resetdays();" >
									<?php 
									for ($i=1; $i<=12; $i++){
									?>
									<option value="{{ $i }}" <?php if ($month==$i) echo 'selected="selected"';?> >{{ $i }}</option>
									<?php
									}
									?>
								</select>月
								<select id="day" name="day">
									<?php 
									// can use cal_days_in_month , but on the server , does not enable calendar , so dont use this way
									//$d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
									?>
								</select>日　
								<script type="text/javascript">
								    resetdays();
									var select = document.getElementById("day");
									select.options[{{ ($day -1) }}].selected = true;
								</script>
								
							</td>
						</tr>
						
						<tr>
							<th><span class="required">＊</span>有効／無効</th>
							<td><select name="invalid">
							<option value="1" <?php if ($theuser->invalid=="1") echo 'selected="selected"';?>>有効</option>
							<option value="0" <?php if ($theuser->invalid=="0") echo 'selected="selected"';?>>無効</option>
							</select></td>
						</tr>

					<br/>
					<tr>
						<th colspan="2"  class="btn"></th>
					</tr>
					
					
						<tr>
							<th colspan="2"  class="btn"><input type="submit" name="action_user_register" value="登録"></th>
						</tr>
					</table>
					<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
				</div>
			</form>

		
	</div><!-- /#main -->

</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')

