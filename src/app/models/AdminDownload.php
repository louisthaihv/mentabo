<?php

class AdminDownload extends Eloquent {

	public static function interviewData($interview_id) {

		//return DB::table('t_interview')->where('interview_id', $interview_id)->first();
		return DB::table('t_interview')->join('t_user', 't_user.user_id', '=', 't_interview.user_id')->first();
	}	

	public static function interviewFood() {

		$m_food_info = DB::table('m_food_info')->orderBy('FoodID', 'asc')->get();
		foreach ($m_food_info as $key => $row) {
			$food[$row['FoodID']] = $row;
		}
		return $food;
	}

	public static function choiceSelection($interview_id, $str_choice_id) {

		$choice_selection_list = array();
		$t_choice_selection = DB::table('t_choice_selection')->where('interview_id', $interview_id)->whereIn('choice_id', $str_choice_id)->get();
		foreach ($t_choice_selection as $key4 => $row4) {
			$choice_selection_list[$row4['choice_id']] = $row4;
		}
		return $choice_selection_list;
	}	

	public static function interviewDataList($interview_f, $interview_t, $csv_max_cnt_interview) {

		return DB::table('t_interview')->where('create_datetime', '>=', $interview_f)->where('create_datetime', '<=', $interview_t)->where('finished_datetime', '!=', 'NULL')->skip(0)->take($csv_max_cnt_interview)->get();
	}

	public static function choiceDataList($section_set_id) {

		return DB::table('m_section_set_contents')
            ->join('m_section', 'm_section_set_contents.section_id', '=', 'm_section.section_id')
            ->join('m_choice', 'm_section.section_id', '=', 'm_choice.section_id')
            ->where('m_section_set_contents.section_set_id', $section_set_id)
            ->select('m_section_set_contents.section_set_id',
					'm_section_set_contents.section_id',
					'm_section_set_contents.page_num', 
					'm_section_set_contents.order_section', 
					'm_section.section_string',
					'm_choice.choice_id',
					'm_choice.choice_type',
					'm_choice.choice_string')
            ->orderBy('m_section_set_contents.page_num', 'asc')
            ->orderBy('m_section_set_contents.order_section', 'asc')
            ->orderBy('m_choice.layer_id', 'asc')
            ->orderBy('m_choice.order_in_layer', 'asc')
            ->get();

	}

	public static function getInterviewChoice($interview_id) {

		$return = DB::table('m_choice')
			->join('m_layer', 'm_choice.layer_id', '=', 'm_layer.layer_id')
			->join('t_choice_selection', 'm_choice.choice_id', '=', 't_choice_selection.choice_id')
			->where('t_choice_selection.interview_id', $interview_id)
			->select('m_choice.section_id',
					'm_choice.choice_id',
					'm_choice.layer_id',
					'm_choice.choice_type',
					'm_choice.choice_string',
					'm_choice.choice_string_unit',
					'm_layer.order_in_section',
					'm_choice.order_in_layer',
					'm_choice.ui_type',
					't_choice_selection.selection',
					't_choice_selection.input_int',
					't_choice_selection.input_float',
					't_choice_selection.input_datetime',
					't_choice_selection.input_string')
			->get();
		
		foreach ($return as $key => $row) {
			$list[$row['section_id']][$row['choice_id']] = $row;
		}
		return $list;
	}
}	
?>