@include('admin/header')

<hr />
<div id="wrapper">
<div id="adminIndex" class="contents">


	<h2 id="pageTitle">管理メニュー</h2>

	
	<div id="main">
        <?php if (Auth::user()->get()->role_id == 0) { ?>
							<div class="section" id="menu6">
						<h3 id="tenant_">テナント管理</h3>
						<ul>
											<li><a href="admin/tenantlist">テナント一覧</a></li>
															<li><a href="admin/registertenant">新規作成</a></li>
										</ul>
						<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
					</div>
		<?php } 
		if ( ! ( (Auth::user()->get()->role_id == 0)  && (!Session::has('temp_tenant_id') ) ) ) {
			?>
				

				<div class="section" id="menu2">
			<h3 id="interview_">問診結果管理</h3>
			<ul>
				<li><a href="admin/interview">問診結果</a></li>
			</ul>
			<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>
		
		<hr />

				<div class="section" id="menu3">
			<h3 id="print_">印刷履歴管理</h3>
			<ul>
				<li><a href="admin/history">印刷履歴</a></li>
			</ul>
			<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>
		
		<hr />
		<?php if ((Auth::user()->get()->role_id == 1) || ((\Auth::user()->get()->role_id ==0)&&(\Session::has('temp_tenant_id')))  ){ ?>
				<div class="section" id="menu4">
			<h3 id="account_">アカウント管理</h3>
			<ul>
								<li><a href="admin/accountlist">アカウント一覧</a></li>
												<li><a href="admin/register">新規登録</a></li>
							</ul>
			<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>
			
		
				<div class="section" id="menu5">
			<h3 id="mail_">メール内容管理</h3>
			<ul>
				<li><a href="admin/mailedit">メール内容管理</a></li>
			</ul>
			<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>
		<?php } ?>
				<div class="section">
			<h3 id="csv_">問診に関する集計データCSV出力機能</h3>
				<form  action="{{ URL::to('/') }}/admin/interview/download/all" method="post">
					<select value="" onChange="changeDateOptionI1(this)" name="interview_y_f">
                        <?php echo $years_option; ?>
                    </select>年
                    <select value="" onChange="changeDateOptionI1(this)" name="interview_m_f">
                        <?php echo $months_option; ?>
                    </select>月
                    <select value="" onChange="changeDateOptionI1(this)" name="interview_d_f">
                        <?php echo $days_option; ?>
                    </select>日
                    ～
                    <select value="" onChange="changeDateOption(this)" name="interview_y_t">
                        <?php echo $years_option_2; ?>
                    </select>年
                    <select value="" onChange="changeDateOptionI2(this)" name="interview_m_t">
                        <?php echo $months_option_2; ?>
                    </select>月
                    <select value="" onChange="changeDateOptionI2(this)" name="interview_d_t">
                        <?php echo $days_option_2; ?>
                    </select>日
                &nbsp;&nbsp;<input type="submit" name="action_admin_interview_csv_interview" value="問診リストダウンロード" class="input-btn">
                </form>
                <div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
            </div>
			<?php if ((Auth::user()->get()->role_id == 1) || ((\Auth::user()->get()->role_id ==0)&&(\Session::has('temp_tenant_id')))  ){ ?>
					<div class="section" id="menu7">
				<h3 id="user_">ユーザー管理</h3>
				<ul>
													<li><a href="user/userlist">ユーザー一覧</a></li>
													<li><a href="user/registeruser">新規登録</a></li>
													<li><a href="user/csvuserreference">CSVファイルユーザー一括登録</a></li>
								</ul>
				<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
			</div>
					<div class="section" id="menu8">
				<h3 id="type_">問診タイプ管理</h3>
				<ul>
													<li>
				   								 <script type="text/javascript">
												 function checkfirst(newvalue){
                                                     var itypes = {		'1':'超簡易版',
                                                         '2':'簡易版',
                                                         '3':'通常版',
                                                         '4':'PRO版',
                                                         '5':'メンタル版'
                                                     };
													 if (confirm('問診タイプを' + itypes[newvalue] + 'に設定します。よろしいですか？')) {
														 
													     //window.location.href='admin/changeinterviewtype/'+newvalue;
														 $.ajax({url:'admin/changeinterviewtype/'+newvalue,success:function(result){
															 document.getElementById('currentinterviewtype').innerHTML = '&nbsp; &nbsp;現在の設定: '+itypes[newvalue];
															 //alert('変更しました。');
														   }});
															
													 } else {
													     // Do nothing!
													 }
												 	
												 }
				   								 	
				   								 </script>
														<br/>
														<?php
														if (Auth::user()->get()->role_id == 0) $tenant_id = \Session::get('temp_tenant_id');
														else 
														$tenant_id = \Auth::user()->get()->tenant_id;
														$tenant = Tenant::find($tenant_id);
														$interview_type = $tenant->interview_type;
														$itypes = array('1'=>'超簡易版',
																		'2'=>'簡易版',
																		'3'=>'通常版',
																		'4'=>'PRO版',
																		'5'=>'メンタル版'
																	);
																	
														$interview_type_str = $itypes[$interview_type];
														?>
												<div id="currentinterviewtype" class="display:inline">&nbsp; &nbsp;問診タイプ: {{ $interview_type_str }}</div>	&nbsp; &nbsp;
																	<select value="{{ $interview_type }}"  name="interview_type" id="interview_type1">
														<?php
														
															for ($i=1 ; $i <=5; $i++) {
														
																echo '<option value="'.$i.'" '; 
																if ($i==$interview_type) echo 'selected="selected" ';
																echo ">".$itypes[$i].'</option>';
															}
															
														?>
												
												</select>
												 <script type="text/javascript">
												 var interviewtype_e = document.getElementById('interview_type1');
									
												 </script>
												
												<button onclick="checkfirst(interviewtype_e.options[interviewtype_e.selectedIndex].value);">変更</button>
													<br/><br/>
												
														
												</li>
													
								</ul>
				<div class="btm"><img src="img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
			</div>
			
			
		
			<?php } ?>
<?php
	}
?>

</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
