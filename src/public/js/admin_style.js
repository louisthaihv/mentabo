
function changeDateOptionI1(o){
	var f = o.form;
	if(!(f.interview_y_f.value == "0") && !(f.interview_m_f.value == "0")) {
		var d = new Date(f.interview_y_f.value, f.interview_m_f.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.interview_d_f.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.interview_d_f.options[i].text = i;
			f.interview_d_f.options[i].value = i;
		}
	}
}

function changeDateOptionI2(o){
	var f = o.form;
	if(!(f.interview_y_t.value == "0") && !(f.interview_m_t.value == "0")) {
		var d = new Date(f.interview_y_t.value, f.interview_m_t.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.interview_d_t.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.interview_d_t.options[i].text = i;
			f.interview_d_t.options[i].value = i;
		}
	}
}

function changeDateOptionB1(o){
	var f = o.form;
	if(!(f.birthday_y_f.value == "0") && !(f.birthday_m_f.value == "0")) {
		var d = new Date(f.birthday_y_f.value, f.birthday_m_f.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.birthday_d_f.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.birthday_d_f.options[i].text = i;
			f.birthday_d_f.options[i].value = i;
		}
	}
}

function changeDateOptionB2(o){
	var f = o.form;
	if(!(f.birthday_y_t.value == "0") && !(f.birthday_m_t.value == "0")) {
		var d = new Date(f.birthday_y_t.value, f.birthday_m_t.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.birthday_d_t.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.birthday_d_t.options[i].text = i;
			f.birthday_d_t.options[i].value = i;
		}
	}
}

function changeDateOptionF1(o){
	var f = o.form;
	if(!(f.followup_y_f.value == "0") && !(f.followup_m_f.value == "0")) {
		var d = new Date(f.followup_y_f.value, f.followup_m_f.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.followup_d_f.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.followup_d_f.options[i].text = i;
			f.followup_d_f.options[i].value = i;
		}
	}
}

function changeDateOptionF2(o){
	var f = o.form;
	if(!(f.followup_y_t.value == "0") && !(f.followup_m_t.value == "0")) {
		var d = new Date(f.followup_y_t.value, f.followup_m_t.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.followup_d_t.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.followup_d_t.options[i].text = i;
			f.followup_d_t.options[i].value = i;
		}
	}
}


$(function(){

	$("#instruction").keyup(function(){
		var count = $(this).val().length;
		var max = 250;
		$("#rest").text(max - count);

	});


	$("#print-btn").click( function() {
		var length = $("#instruction").val().length;
		if (length > 250) {
			jAlert('特記事項は250文字以内で入力してください', '入力エラー');
			return false;
		}
		jConfirm('印刷を開始しますか？', '印刷', function(r) {
			if (r) {
				printHistory();
				return false;
			} else {
				return false;
			}
		});
	});

	$("#followup-print-btn").click( function() {
		jConfirm('フォローアップ結果の印刷を開始しますか？', '印刷', function(r) {
			if (r) {
				printFollowupHistory();
				return false;
			} else {
				return false;
			}
		});
	});

	function printHistory() {
		var data = {'instruction' : $('#instruction').val()};
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: $('#absolute_url').val() + 'history',
			data: {
				'view_type' : $('#view_type').val(),
				'interview_id' : $('#interview_id').val(),
				'instruction' : $('#instruction').val()
			},
			success: function(data, dataType){
				if (data.result == true) {
					print();
				} else {
					jAlert('エラー：印刷できません');
				}
			},
			error: function(){
				jAlert('エラー：印刷できません');
			}
		});
	}

	function printFollowupHistory() {
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: $('#absolute_url').val() + 'history',
			data: {
				'view_type' : $('#view_type').val(),
				'interview_id' : $('#interview_id').val(),
				'name' : $('#name').val()
			},
			success: function(data, dataType){
				if (data.result == true) {
					print();
				} else {
					jAlert('エラー：フォローアップ結果を印刷できません');
				}
			},
			error: function(){
				alert($('#view_type').val());
				alert($('#interview_id').val());
				alert($('#name').val());
				jAlert('エラー：フォローアップ結果を印刷できません');
			}
		});
	}

});



