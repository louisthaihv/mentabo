
<div id="instruction-area">
	<h3>特記事項：<span id="rest"></span></h3>
	<form action="" method="post">
		<input type="hidden" id="absolute_url" value="{{ $absolute_url }}/">
		<input type="hidden" id="view_type" value="admin">
		<input type="hidden" id="interview_id" value="<?php echo $interview_id ?>">
		<textarea name="instruction" cols="50" rows="5" id="instruction"><?php echo $interview_data['instruction'] ?></textarea>

		<p><!--{$smarty.now|date_format:'%Y'}--><?php echo date('Y') ?>年<!--{$smarty.now|date_format:'%m'}--><?php echo date('m') ?>月<!--{$smarty.now|date_format:'%d'}--><?php echo date('d') ?>日</p>
		<!--{if $app.view_type eq 'admin'}-->
		<!--p>入力者：<span><!--{$session.admin_account.name}--></span></p-->
		<!--{elseif $app.view_type eq 'interview'}-->
		<p>入力者：<input type="text" id="name" name="name" /></p>
		<!--{/if}-->

		<p><input id="print-btn" type="button" value="印刷" /></p>
	</form>
</div>
