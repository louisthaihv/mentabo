@include('header')

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2><?php echo $setTitle ?> 結果</h2>
	<div id="contentInner">
	<div id="interviewResult">

		<?php if($sectionSet['file']=='full') { ?> @include('interview.result_full') <?php } ?>
		<?php if($sectionSet['file']=='light') { ?> @include('interview.result_light') <?php } ?>
		<?php if($sectionSet['file']=='basic') { ?> @include('interview.result_basic') <?php } ?>
		<?php if($sectionSet['file']=='simple') { ?> @include('interview.result_simple') <?php } ?>

		<!--{if $app.interview_data.section_set_id != ""}-->
			<!--{include file="instruction.tpl"}-->
			@include('interview.instruction')
		<!--{/if}-->

	</div>

	<div class="btn-area">
		<!--{assign var="url" value="`$config.interview_absolute_url`?section_set_id=`$config.interview_type[$app.interview_data.section_set_id]`"}-->
		<input type="button" value="問診終了" onClick="location.href='{{ URL::to('/') }}/portal'" class="input-btn">
	</div>

	<!--{if $app.is_followup == 1}-->
	<?php if($sectionSet['flg_followup'] == 1) { ?>
	<div class="btn-area">
	<form action="{{ URL::to('/') }}/followup" id="followup" name="followup" method="POST">
		<input type="hidden" name="interview_id" value="<?php echo $selectInterview['interview_id'] ?>">
		<input type="submit" name="action_interview_entry" value="フォローアップ診断に登録する">
	</form>
	</div>
	<?php } ?>
	<!--{/if}-->

	</div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->

@include('footer')
