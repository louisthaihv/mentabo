<?php

class CsvHeaderInterview  {

	public static function globals()
	{
		//CSVダウンロード
		//問診に関する集計データCSV出力機能
		//
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2004', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2005', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2006', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2007', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2008', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2009', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2010', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2011', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2012', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2053', 'choice_id' => '', 'choice_string' => '');
		//$CSV_HEADER_INTERVIEW[] = array('section_id' => '2053', 'choice_id' => '10422', 'choice_string' => ''); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2013', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2014', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2015', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2016', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2017', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2018', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2054', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2055', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2019', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2020', 'choice_id' => '10129', 'choice_string' => '1日本数');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2020', 'choice_id' => '10130', 'choice_string' => '年数');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2021', 'choice_id' => '10131', 'choice_string' => '1日本数');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2021', 'choice_id' => '10132', 'choice_string' => '喫煙していた年数');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2021', 'choice_id' => '10133', 'choice_string' => '禁煙してから年数');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2022', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10311', 'choice_string' => '【朝食の主食】めし');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10312', 'choice_string' => '【朝食の主食】和風めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10313', 'choice_string' => '【朝食の主食】洋風めん/中華めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10314', 'choice_string' => '【朝食の主食】パン（甘い菓子パンは菓子類へ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10315', 'choice_string' => '【朝食の主食】いも類（主食）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10316', 'choice_string' => '【朝食の主菜】肉類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10317', 'choice_string' => '【朝食の主菜】魚介類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10318', 'choice_string' => '【朝食の主菜】卵');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10319', 'choice_string' => '【朝食の主菜】大豆（とうふ・納豆・厚揚げ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10320', 'choice_string' => '【朝食の副菜･汁物】緑黄色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10321', 'choice_string' => '【朝食の副菜･汁物】淡色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10322', 'choice_string' => '【朝食の副菜･汁物】海藻類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10323', 'choice_string' => '【朝食の副菜･汁物】きのこ類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10324', 'choice_string' => '【朝食の副菜･汁物】いも類（副菜）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10424', 'choice_string' => '【朝食の副菜･汁物】汁');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10325', 'choice_string' => '【朝食の果物･乳類･飲料】果実類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10326', 'choice_string' => '【朝食の果物･乳類･飲料】乳類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10327', 'choice_string' => '【朝食の果物･乳類･飲料】茶類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10328', 'choice_string' => '【朝食の果物･乳類･飲料】ジュース類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10432', 'choice_string' => '【朝食の果物･乳類･飲料】酒（ビール）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10433', 'choice_string' => '【朝食の果物･乳類･飲料】酒（日本酒）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10434', 'choice_string' => '【朝食の果物･乳類･飲料】酒（ワイン）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10435', 'choice_string' => '【朝食の果物･乳類･飲料】酒（焼酎、ウィスキー、ブランデー）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10428', 'choice_string' => '【朝食の果物･乳類･飲料】水');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10330', 'choice_string' => '【朝食のおやつ】菓子（水分少、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10331', 'choice_string' => '【朝食のおやつ】菓子（水分少、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10332', 'choice_string' => '【朝食のおやつ】菓子（水分多、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10333', 'choice_string' => '【朝食のおやつ】菓子（水分多、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10334', 'choice_string' => '【昼食の主食】めし');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10335', 'choice_string' => '【昼食の主食】和風めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10336', 'choice_string' => '【昼食の主食】洋風めん/中華めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10337', 'choice_string' => '【昼食の主食】パン（甘い菓子パンは菓子類へ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10338', 'choice_string' => '【昼食の主食】いも類（主食）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10339', 'choice_string' => '【昼食の主菜】肉類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10340', 'choice_string' => '【昼食の主菜】魚介類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10341', 'choice_string' => '【昼食の主菜】卵');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10342', 'choice_string' => '【昼食の主菜】大豆（とうふ・納豆・厚揚げ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10343', 'choice_string' => '【昼食の副菜･汁物】緑黄色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10344', 'choice_string' => '【昼食の副菜･汁物】淡色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10345', 'choice_string' => '【昼食の副菜･汁物】海藻類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10346', 'choice_string' => '【昼食の副菜･汁物】きのこ類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10347', 'choice_string' => '【昼食の副菜･汁物】いも類（副菜）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10425', 'choice_string' => '【昼食の副菜･汁物】汁');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10348', 'choice_string' => '【昼食の果物･乳類･飲料】果実類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10349', 'choice_string' => '【昼食の果物･乳類･飲料】乳類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10350', 'choice_string' => '【昼食の果物･乳類･飲料】茶類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10351', 'choice_string' => '【昼食の果物･乳類･飲料】ジュース類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10436', 'choice_string' => '【昼食の果物･乳類･飲料】酒（ビール）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10437', 'choice_string' => '【昼食の果物･乳類･飲料】酒（日本酒）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10438', 'choice_string' => '【昼食の果物･乳類･飲料】酒（ワイン）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10439', 'choice_string' => '【昼食の果物･乳類･飲料】酒（焼酎、ウィスキー、ブランデー）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10429', 'choice_string' => '【昼食の果物･乳類･飲料】水');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10353', 'choice_string' => '【昼食のおやつ】菓子（水分少、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10354', 'choice_string' => '【昼食のおやつ】菓子（水分少、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10355', 'choice_string' => '【昼食のおやつ】菓子（水分多、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10356', 'choice_string' => '【昼食のおやつ】菓子（水分多、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10357', 'choice_string' => '【夕食の主食】めし');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10358', 'choice_string' => '【夕食の主食】和風めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10359', 'choice_string' => '【夕食の主食】洋風めん/中華めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10360', 'choice_string' => '【夕食の主食】パン（甘い菓子パンは菓子類へ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10361', 'choice_string' => '【夕食の主食】いも類（主食）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10362', 'choice_string' => '【夕食の主菜】肉類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10363', 'choice_string' => '【夕食の主菜】魚介類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10364', 'choice_string' => '【夕食の主菜】卵');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10365', 'choice_string' => '【夕食の主菜】大豆（とうふ・納豆・厚揚げ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10366', 'choice_string' => '【夕食の副菜･汁物】緑黄色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10367', 'choice_string' => '【夕食の副菜･汁物】淡色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10368', 'choice_string' => '【夕食の副菜･汁物】海藻類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10369', 'choice_string' => '【夕食の副菜･汁物】きのこ類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10370', 'choice_string' => '【夕食の副菜･汁物】いも類（副菜）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10426', 'choice_string' => '【夕食の副菜･汁物】汁');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10371', 'choice_string' => '【夕食の果物･乳類･飲料】果実類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10372', 'choice_string' => '【夕食の果物･乳類･飲料】乳類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10373', 'choice_string' => '【夕食の果物･乳類･飲料】茶類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10374', 'choice_string' => '【夕食の果物･乳類･飲料】ジュース類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10440', 'choice_string' => '【夕食の果物･乳類･飲料】酒（ビール）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10441', 'choice_string' => '【夕食の果物･乳類･飲料】酒（日本酒）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10442', 'choice_string' => '【夕食の果物･乳類･飲料】酒（ワイン）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10443', 'choice_string' => '【夕食の果物･乳類･飲料】酒（焼酎、ウィスキー、ブランデー）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10430', 'choice_string' => '【夕食の果物･乳類･飲料】水');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10376', 'choice_string' => '【夕食のおやつ】菓子（水分少、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10377', 'choice_string' => '【夕食のおやつ】菓子（水分少、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10378', 'choice_string' => '【夕食のおやつ】菓子（水分多、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10379', 'choice_string' => '【夕食のおやつ】菓子（水分多、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10380', 'choice_string' => '【間食の主食】めし');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10381', 'choice_string' => '【間食の主食】和風めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10382', 'choice_string' => '【間食の主食】洋風めん/中華めん');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10383', 'choice_string' => '【間食の主食】パン（甘い菓子パンは菓子類へ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10384', 'choice_string' => '【間食の主食】いも類（主食）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10385', 'choice_string' => '【間食の主菜】肉類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10386', 'choice_string' => '【間食の主菜】魚介類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10387', 'choice_string' => '【間食の主菜】卵');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10388', 'choice_string' => '【間食の主菜】大豆（とうふ・納豆・厚揚げ）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10389', 'choice_string' => '【間食の副菜･汁物】緑黄色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10390', 'choice_string' => '【間食の副菜･汁物】淡色野菜');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10391', 'choice_string' => '【間食の副菜･汁物】海藻類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10392', 'choice_string' => '【間食の副菜･汁物】きのこ類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10393', 'choice_string' => '【間食の副菜･汁物】いも類（副菜）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10427', 'choice_string' => '【間食の副菜･汁物】汁');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10394', 'choice_string' => '【間食の果物･乳類･飲料】果実類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10395', 'choice_string' => '【間食の果物･乳類･飲料】乳類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10396', 'choice_string' => '【間食の果物･乳類･飲料】茶類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10397', 'choice_string' => '【間食の果物･乳類･飲料】ジュース類');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10444', 'choice_string' => '【間食の果物･乳類･飲料】酒（ビール）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10445', 'choice_string' => '【間食の果物･乳類･飲料】酒（日本酒）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10446', 'choice_string' => '【間食の果物･乳類･飲料】酒（ワイン）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10447', 'choice_string' => '【間食の果物･乳類･飲料】酒（焼酎、ウィスキー、ブランデー）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10431', 'choice_string' => '【間食の果物･乳類･飲料】水');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10399', 'choice_string' => '【間食のおやつ】菓子（水分少、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10400', 'choice_string' => '【間食のおやつ】菓子（水分少、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10401', 'choice_string' => '【間食のおやつ】菓子（水分多、脂質多）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2023', 'choice_id' => '10402', 'choice_string' => '【間食のおやつ】菓子（水分多、脂質少）');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2121', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2122', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2123', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2124', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2024', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2027', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2028', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2029', 'choice_id' => '10150', 'choice_string' => 'ビール');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2029', 'choice_id' => '10151', 'choice_string' => '日本酒');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2029', 'choice_id' => '10152', 'choice_string' => '焼酎');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2029', 'choice_id' => '10153', 'choice_string' => 'ワイン');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2029', 'choice_id' => '10154', 'choice_string' => 'ウィスキー');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2029', 'choice_id' => '10155', 'choice_string' => 'ブランデー');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2030', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2031', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2056', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2032', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2033', 'choice_id' => '10162', 'choice_string' => '平日');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2033', 'choice_id' => '10163', 'choice_string' => '休日');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2034', 'choice_id' => '10164', 'choice_string' => '歩き');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2034', 'choice_id' => '10165', 'choice_string' => 'バスまたは電車');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2034', 'choice_id' => '10166', 'choice_string' => '車');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2034', 'choice_id' => '10423', 'choice_string' => '自転車'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2034', 'choice_id' => '10448', 'choice_string' => '１日の合計'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10167', 'choice_string' => '【平日】立っている');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10168', 'choice_string' => '【平日】座って（作業して）いる');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10169', 'choice_string' => '【平日】歩いている');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10170', 'choice_string' => '【平日】軽く息切れするくらいに走っている');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10171', 'choice_string' => '【休日】立っている');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10172', 'choice_string' => '【休日】座って（作業して）いる');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10173', 'choice_string' => '【休日】歩いている');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2035', 'choice_id' => '10174', 'choice_string' => '【休日】軽く息切れするくらいに走っている');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2036', 'choice_id' => array('10175','10176'), 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2036', 'choice_id' => '10278', 'choice_string' => '時間');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2036', 'choice_id' => '10279', 'choice_string' => '重さ');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2037', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2099', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2038', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2039', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2040', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2041', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2042', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2043', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2044', 'choice_id' => '10194', 'choice_string' => '回数');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2044', 'choice_id' => '10195', 'choice_string' => '一回の時間');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10196', 'choice_string' => '収縮期血圧(最高血圧)'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10197', 'choice_string' => '拡張期血圧(最低血圧)'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10280', 'choice_string' => '空腹時血糖'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10281', 'choice_string' => '随時血糖'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10282', 'choice_string' => '中性脂肪'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10283', 'choice_string' => '総コレステロール'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10284', 'choice_string' => 'LDLコレステロール'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10285', 'choice_string' => 'HDLコレステロール'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10286', 'choice_string' => '尿酸'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10287', 'choice_string' => 'AST'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10288', 'choice_string' => 'ALT'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10289', 'choice_string' => 'GGTP'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2045', 'choice_id' => '10290', 'choice_string' => 'HbA1c'); //*****
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2101', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2102', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2103', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2104', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2105', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2106', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2114', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2115', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2116', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2117', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2118', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2119', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2120', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2107', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2108', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2109', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2110', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2111', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2112', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '2113', 'choice_id' => '', 'choice_string' => '');
		$CSV_HEADER_INTERVIEW[] = array('section_id' => '', 'choice_id' => '', 'choice_string' => '');
		$GLOBALS['CSV_HEADER_INTERVIEW'] = $CSV_HEADER_INTERVIEW;

	}
	

}	