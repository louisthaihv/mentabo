@include('header')

<?php if(isset($headerScript)) echo $headerScript ?>

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2><?php if(isset($setTitle)) echo $setTitle ?></h2>
	<div id="contentInner">

		<div class="compulsoryInputError" style="display:none"></div>
		<form id="form_main" action="{{ URL::to('/') }}/followup" method="POST">
			<!--{foreach from=$app.followup_sports key=key item=item}-->

			<?php if(isset($followup_sports)) {?>
				<?php foreach ($followup_sports as $key => $item) { ?>
						<input type="hidden" name="<?php echo $key ?>" id="<?php echo $key ?>" value="<?php echo $item ?>"/>
				<?php } ?>
			<?php } ?>			
			
			<!--{/foreach}-->
			<!--{include file="followup/include_display_questions.tpl"}-->
			<?php echo $content ?>

			<?php if(isset($section_list)) {?>
			<div class="btn-area">
				<input type="hidden" name="input_type" value="completion" />
				<input type="submit" name="action_followup_user_completion" value="次へ">
			</div>
			<?php } ?>	

		</form>

	</div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->

@include('footer')