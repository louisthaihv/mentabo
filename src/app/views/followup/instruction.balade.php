
<div id="instruction-area">
	<form  action="" method="post">
		<input type="hidden" id="absolute_url" value="<!--{$app.absolute_url}-->">
		<input type="hidden" id="view_type" value="<!--{$app.view_type}-->">
		<input type="hidden" id="interview_id" value="<!--{$app.followup_data.followup_interview_id}-->">

		<p><!--{$smarty.now|date_format:'%Y'}--><?php echo date('Y') ?>年<!--{$smarty.now|date_format:'%m'}--><?php echo date('m') ?>月<!--{$smarty.now|date_format:'%d'}--><?php echo date('d') ?>日</p>

		<p>入力者：<input type="text" id="name" /></p>

		<p><input id="followup-print-btn" type="button" value="印刷" /></p>
	</form>
</div>
