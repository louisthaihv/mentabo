
<input type="hidden" name="input_type" value="email" />
<div class="input-area">
<!--{form ethna_action="interview_entry_do"}-->
	<p class="coution">フォローアップ診断を受信される方は、メールアドレスをご登録ください。</p>
	<p>フォローアップ通知用メールアドレス：<!--{form_input name="mailaddress"}--> <input class="mailaddress" name="email"  value="<?php echo $select_interview['email'] ?>"></p>
	
	<?php if($status != 1) { ?> メールアドレスを入力してください。<?php } ?>
	<?php if($status == false) { ?> このメールアドレスはすでに登録されています <?php }?>

	<input type="hidden" name="interview_id"  value="<?php echo $select_interview['interview_id'] ?>">
	<p><input type="submit" value="メールアドレスを登録する" /></p>
<!--{/form}-->
</div>

<div class="btn-area">
<form action="{{ URL::to('/') }}">
	<!--{$app.interview_data.section_set_id}-->
	<!--{assign var="url" value="`$config.interview_absolute_url`?section_set_id=`$config.interview_type[$app.section_set_id]`"}-->
	<input type="button" value="登録せずに問診終了" onClick="location.href='{{ URL::to('/') }}/portal'" class="input-btn">
</form>
</div>
