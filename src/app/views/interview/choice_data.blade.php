<?php $input_name = 'i_'.$choice_data['choice_id'] ?>
<?php $input_layer_name = 'il_'.$layer_choice_data['layer_data']['layer_id'] ?>
<?php if($choice_data['choice_type'] == 'radio'){ ?>
	<input type="radio" id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_layer_name?>" value="<?php echo $choice_data['choice_id'] ?>" <?php if(isset($input_data[$input_layer_name]) == $choice_data['choice_id']){ ?>checked="checked"<?php }?> /><label for="c_<?php echo $choice_data['choice_id'] ?>"><?php echo $choice_data['choice_string'] ?></label>
<?php }elseif($choice_data['choice_type'] == 'checkbox'){ ?>
	<input type="checkbox" id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_layer_name ?>[]" value="<?php echo $choice_data['choice_id'] ?>"<?php if(isset($input_data[$input_layer_name]) == $choice_data['choice_id']){?> checked="checked"<?php } ?> />
	<label for="c_<?php echo $choice_data['choice_id'] ?>"><?php echo $choice_data['choice_string'] ?></label>
<?php }elseif($choice_data['choice_type'] == 'date'){ ?>
	<!--{*TODO メタボには無い*}-->
<?php }elseif($choice_data['choice_type'] == 'time'){ ?>
	<!--{*TODO メタボには無い*}-->
<?php }elseif($choice_data['choice_type'] == 'datetime'){ ?>
	<!--{*TODO メタボには無い*}-->
<?php }elseif($choice_data['choice_type'] == 'string'){ ?>
	<?php if($choice_data['ui_type'] == 3) {?>
	<input type="text" id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_name ?>" value="<?php echo $input_data[$input_name] ?>" class="slider_input" autocomplete="off" readonly="readonly" />
	<?php }else {?>
	<input type="text" id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_name ?>" value="<?php echo $input_data[$input_name] ?>" class="text" autocomplete="off"  />
	<?php }?>
<?php }elseif($choice_data['choice_type'] == 'numeric'){ ?>
	<input type="text" id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_name ?>" value="<?php echo $input_data[$input_name] ?>" class="text" onBlur="convert(this)" autocomplete="off" />
<?php }elseif($choice_data['choice_type'] == 'slider' OR $choice_data['choice_type'] == 'slider_hour' OR $choice_data['choice_type'] == 'slider_minute' OR $choice_data['choice_type'] == 'slider_show_by_step'){ ?>
	<div id="c_<?php echo $choice_data['choice_id'] ?>_div" class="slider-wrapper">

		<?php if(isset($choice_data['show_image'])) { ?>
		<div class="sliderImage"><img src="{{ URL::to('/') }}/img/choice/<?php echo $choice_data['choice_id'] ?>.png"></div>
		<?php } ?>
		<div class="sliderMain">
			<label for="c_<?php echo $choice_data['choice_id'] ?>"><?php echo $choice_data['choice_string'] ?></label>
		<?php if($choice_data['lead']){ ?>
			<p class="lead"><?php echo $choice_data['lead'] ?></p>
		<?php } ?>
			<img id="c_<?php echo $choice_data['choice_id'] ?>_minus" class="minus" src="{{ URL::to('/') }}/img/metabo_minus.gif">
			<input type="text" id="c_<?php echo $choice_data['choice_id'] ?>_show" class="slider_input" readonly="readonly"  />
			<input type="hidden" id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_name ?>" /></span>
			<img id="c_<?php echo $choice_data['choice_id'] ?>_plus" class="plus" src="{{ URL::to('/') }}/img/metabo_plus.gif">
			<div id="c_<?php echo $choice_data['choice_id'] ?>_slider"></div>
		</div>
	</div>
<?php }elseif($choice_data['choice_type'] == 'year' OR $choice_data['choice_type'] == 'month' OR $choice_data['choice_type'] == 'day'){ ?>
	<select id="c_<?php echo $choice_data['choice_id'] ?>" name="<?php echo $input_name ?>">
	<!--{assign var="number" value=`$choice_data.value_min`}-->
	<!--{if $app.input_data[$input_name]}-->
		<!--{assign var="selected_number" value=`$app.input_data[$input_name]`}-->
	<!--{else}-->
		<!--{assign var="selected_number" value=`$choice_data.value_default`}-->
	<!--{/if}-->

	<!--{assign var="loopNum" value=$choice_data.value_max+1}-->
	<!--{section name=number_section start=`$choice_data.value_min` loop=$loopNum}-->
		<option value="<!--{$number}-->" <!--{if $selected_number==$number}-->selected="selected"<!--{/if}-->><!--{$number}--></option>
		<!--{assign var="number" value=$number+1}-->
	<!--{/section}-->
	</select>
	<!--{$choice_data.choice_string_unit}-->
<!--{elseif $choice_data.choice_type eq "tab"}-->
<?php }elseif($choice_data['choice_type'] == 'tab'){ ?>
<!--{elseif $choice_data.choice_type eq "accordion"}-->
<?php }elseif($choice_data['choice_type'] == 'accordion'){ ?>
<?php }else{ ?>
<!--{else}-->
<?php } ?>
<!--{/if}-->
