<?php

namespace History;
use View;
use Input;
use History;

class HistoryController extends \BaseController {
	
	public function getIndex()
	{
		$interview_id = Input::get('interview_id');
		$instruction = Input::get('instruction');
		$view_type = Input::get('view_type');

		$input = Input::all();
		if(isset($interview_id)) {

			$admin_account['account_id'] = \Auth::user()->get()->tenant_id;
			$admin_account['name'] = \Auth::user()->get()->name;

			$result = true;
			if ($view_type == 'admin' || $view_type == 'admin_followup') {
				if (empty($admin_account)) {
					$result = false;
				}
				$account_id = $admin_account['account_id'];
				$name = $admin_account['name'];

			} else {
				$account_id = 0;
				$name =  Input::get('name');
			}

			if ($result) {

				//特記事項登録
				History::updateInterview($interview_id, $instruction);
				History::insertPrintHistory($interview_id, $account_id, $name, $view_type);

			}else {
				$result = false;	
			}

			echo json_encode(array('result' => $result, 'name' => $name));

		}
	}

	
}
