<div class="contents-area">
<p>メールアドレスの登録を受け付けました。</p>
<p>登録いただいたメールアドレスに、フォローアップの通知メールを送付します。</p>
</div>
<div class="btn-area">
<form action="" method="POST">
	<input type="button" value="問診終了" onClick="location.href='{{ URL::to('/') }}/mypage'" class="input-btn" />
</form>
</div>