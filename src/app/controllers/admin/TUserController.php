<?php

namespace Admin;
//use View;
//use Input;

class TUserController extends \BaseController {
	
		
	public function loggedin(){
	  if ( \Auth::tuser()->check() ) {
		  return true;
	  }
	  else return false; 
    }
	
	public function loggedin_backend(){
	  if ( \Auth::user()->check() ) {
		  return true;
	  }
	  else return false; 
    }
	
	public function special(){
		$special = ((\Auth::user()->get()->role_id ==0)&&(\Session::has('temp_tenant_id')));
		return $special;
	}
	
	public function getIndex()
	{
		return \Redirect::to("mypage");
	}

	public function getLogout(){

  		 if (!$this->loggedin()) {
  			 if (\Session::has('tenant_token'))
  			   $token = \Session::get('tenant_token');
  			 else $token = "default";
  		 	 return \Redirect::to("top/$token");
  		 }
    	  \Auth::tuser()->logout();
		  $token = \Session::get('tenant_token');
 	      return \Redirect::to("top/".$token);
	}

	public function getLogin($token){
		if ($this->loggedin()) {
			return \Redirect::to("portal");
		}
		// Get tenant from token
		$tenant = \Tenant::where("token","=",$token)->first();
        if (!$tenant->valid){ return \View::make('404');}
		if ($tenant==null){ return \View::make('404');}
		$tenant_id = $tenant->tenant_id;

		\Session::put('tenant_token',$token);
		$login_id = ""; $password = "";
		return \View::make('admin.user.login',compact('tenant_id','token','login_id','password'));
	}

	public function postLogin($token){
 	   
 	   $login_id = \Input::get('login_id');
 	   $password = \Input::get('password');
	   $inputs = \Input::all();
		// Get tenant from token
  		$tenant = \Tenant::where("token","=",$token)->first();
  		if ($tenant==null) return ;
		$tenant_id = $tenant->tenant_id;
		
 	   $var = \Auth::tuser()->attempt(array('login_id' => $login_id, 'password' => $password,'tenant_id' => $tenant_id,'invalid'=>1));
   
  	   if ($var) {

		   // if(\Session::get('followup')){
     //           \Session::keep('followup');
     //           return \Redirect::to("followup/?" . \Session::get('followup'));
     //       }
  	   		$followup_code = \Session::get('followup_code');
			if($followup_code){
				return \Redirect::to($followup_code);
			}
           	
  	       return \Redirect::to("portal");
		   
  	   }else {
		  
		   if (empty($login_id)) $error_message = 'ログインIDを入力してください。'; else
			   if (empty($password)) $error_message = 'パスワードを入力してください。'; else
		   	   {
				   $error_message = 'ログインID又はパスワードが間違っています。';
			   }
		   return \View::make('admin.user.login',compact('tenant_id','token','error_message','login_id','password'));
  		   //return \Redirect::to("top/".$token);
	   }
	}
	
	//backend
    function getUserlist(){
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		
		
		$users = \TUser::where('tenant_id','=',$tenant_id)->get();
		
		return \View::make('admin.user.userlist',compact('users'));
	}
	
	
    function getRegisteruser(){
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		
		$theuser = new \TUser;
		
		$theuser->login_id = "user_".(1+ \TUser::where('tenant_id',$tenant_id)->count() );
		
		//$theuser->years_old = 32;
		//$theuser->months_old = 1;
		
	   
	   	$current_year = date("Y");
		$year = $current_year - 30;
		$month = 1; 
		$day = 1;
		
		$theuser->invalid = 1;
		
  	    if (\Input::old('first_name')!==null) {
			
			
		   $theuser->login_id = \Input::old('login_id');
		  
		   $theuser->nick_name = \Input::old('nick_name');

		   $theuser->first_name = \Input::old('first_name');
		   $theuser->first_name_kana = \Input::old('first_name_kana');
		   $theuser->last_name = \Input::old('last_name');
		   $theuser->last_name_kana = \Input::old('last_name_kana');
		   $theuser->password = \Input::old('password');
		  
		   $theuser->gender = \Input::old('gender');
		   
		   //$theuser->years_old = \Input::old('years_old');
		   //$theuser->months_old = \Input::old('months_old');
		   
		   
		   $year = \Input::old('year');
		   $month = \Input::old('month');
		   $day = \Input::old('day');
		   
		   $theuser->invalid = \Input::old('invalid');

		   /*
		   $theuser->birth_date = \Input::old('birth_date');
		   $theuser->email = \Input::old('email');
		   
		   $theuser->insurance_company_code = \Input::old('insurance_company_code');
		   $theuser->insurance_code = \Input::old('insurance_code');
		   $theuser->insurance_num = \Input::old('insurance_num');
		   
		   
		   $theuser->zip= \Input::old('zip');
		   $theuser->address= \Input::old('address');
		   
		   $theuser->tel= \Input::old('tel');
		   $theuser->business_zip = \Input::old('business_zip');
		   $theuser->business_address = \Input::old('business_address');
		   $theuser->business_tel = \Input::old('business_tel');
		   $theuser->business_name = \Input::old('business_name');
		   $theuser->promotion_code = \Input::old('promotion_code');
		   */
		   
		   //$theuser->create_datetime = time(); 
  	   }
        return \View::make('admin.user.registeruser',compact('theuser','year','month','day'));
    }
	
   
    function postRegisteruser(){
		
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
	    $rulesreg= array(
	   	  	//"email" => "required|email|unique:t_user,email",
	   		'login_id' => "required|alphanum_under|min:4|max:44|unique:t_user,login_id,NULL,user_id,tenant_id,".$tenant_id	,
	   		'nick_name' => "required",
	   		'password' => 'required|confirmed|halfwidth|min:4|max:12',
	   		'first_name_kana' => "hiragana_katakana",
	   		'last_name_kana' => "hiragana_katakana"
	     );
		 
 	   $inputs = \Input::all();
	   
	   $emessages = \Config::get('error_messages');
	   $emessages['login_id.unique'] = '既に '. $inputs['login_id'] . ' のIDが登録されています';
 	   
	   $validator = \Validator::make($inputs, $rulesreg,$emessages);
	   
 	   if ($validator->passes()){
		   $theuser = new \TUser;
		   
		   $theuser->login_id = $inputs['login_id'];
		  
		   $theuser->tenant_id = $tenant_id;
		   
		   $theuser->nick_name = $inputs['nick_name'];

		   $theuser->first_name = $inputs['first_name'];
		   $theuser->first_name_kana = mb_convert_kana($inputs['first_name_kana'],"C");
		   $theuser->last_name = $inputs['last_name'];
		   $theuser->last_name_kana = mb_convert_kana($inputs['last_name_kana'],"C");
		   $theuser->password = \Hash::make($inputs['password']);
		  
		   $theuser->gender = $inputs['gender'];
		   
		   //$theuser->years_old = $inputs['years_old'];
		   //$theuser->months_old = $inputs['months_old'];
		   
		   $date = new \DateTime();
		   $date->setDate($inputs['year'], $inputs['month'], $inputs['day']);
		   
		   $theuser->birth_date = $date->format('Y-m-d');
		   
		   $theuser->invalid = $inputs['invalid'];
			
		   // curently there is no input for email	
		   //$theuser->email = $theuser->login_id."@examplemetaboweb.com";
		   $theuser->email = NULL;
		   
		   /*
		   
		   $theuser->birth_date = $inputs['birth_date'];
		   $theuser->email = $inputs['email']];
		   
		   $theuser->insurance_company_code = $inputs['insurance_company_code'];
		   $theuser->insurance_code = $inputs['insurance_code'];
		   $theuser->insurance_num = $inputs['insurance_num'];
		   
		   
		   $theuser->zip= $inputs['zip'];
		   $theuser->address= $inputs['address'];
		   
		   $theuser->tel= $inputs['tel'];
		   $theuser->business_zip = $inputs['business_zip'];
		   $theuser->business_address = $inputs['business_address'];
		   $theuser->business_tel = $inputs['business_tel'];
		   $theuser->business_name = $inputs['business_name'];
		   $theuser->promotion_code = $inputs['promotion_code'];
		  
		   */
		   
		   $theuser->create_datetime = time(); 
  		   $theuser->save();
  		  
 		   return \Redirect::to('user/userlist');
 	   }
 	   //$errors = $validator->messages();
 	   //print_r($errors);
 	   return \Redirect::to('user/registeruser')->withErrors($validator)->withInput();
    }
	
	function getEdituser($user_id){
	
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;
		
		
			
		 $theuser = \TUser::find($user_id);
		 if ($theuser ==null) return;
		  
		 if ($theuser->tenant_id != $tenant_id) return;
		 
		 $date = $theuser->birth_date;
		 if ($date == "0000-00-00"){
	 	   	 $current_year = date("Y");
	 		 $year = $current_year - 30;
	 		 $month = 1; 
	 		 $day = 1;
		 } else {
			 $time =strtotime($date);
			 $month=date("n",$time);
			 $year =date("Y",$time);
			 $day = date("d",$time);
	 	 }
		 
		 
  	    if (\Input::old('first_name')!==null) {
		   //$theuser = new \TUser;
		   $theuser->login_id = \Input::old('login_id');
		  
		   $theuser->nick_name = \Input::old('nick_name');

		   $theuser->first_name = \Input::old('first_name');
		   $theuser->first_name_kana = \Input::old('first_name_kana');
		   $theuser->last_name = \Input::old('last_name');
		   $theuser->last_name_kana = \Input::old('last_name_kana');
		   $theuser->password = \Input::old('password');
		  
		   $theuser->gender = \Input::old('gender');
		   
		   //$theuser->years_old = \Input::old('years_old');
		   //$theuser->months_old = \Input::old('months_old');
		   $year = \Input::old('year');
		   $month = \Input::old('month');
		   $day = \Input::old('day');
		   
		   $theuser->invalid = \Input::old('invalid');
		   
  	   	} 
        return \View::make('admin.user.edituser',compact('theuser','year','month','day'));
	}
	
	function postEdituser($user_id){
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id; 
		 
  	   $inputs = \Input::all();
   
	   $rulesedit = array(
			
		 // "email" => "required|email|unique:t_user,email,".$user_id.',user_id',
 		'login_id' => "required|alphanum_under|min:4|max:44|unique:t_user,login_id,".$user_id.",user_id,tenant_id,".$tenant_id,
		'nick_name' => "required",
 		'password' => 'required|confirmed|halfwidth|min:4|max:12',
		'first_name_kana' => "hiragana_katakana",
		'last_name_kana' => "hiragana_katakana"
  		);
	  
	  $theuser = \TUser::find($user_id);
	  if ($theuser ==null) return;
	  
	  if ($theuser->tenant_id !=$tenant_id) return;
	  
	   $emessages = \Config::get('error_messages');
	   $emessages['login_id.unique'] = '既に '. $inputs['login_id'] . ' のIDが登録されています';
	   
  	   $validator = \Validator::make($inputs, $rulesedit,$emessages);
	   
  	   if ($validator->passes()){
		   $theuser->login_id = $inputs['login_id'];
		  
		   //$theuser->tenant_id = $tenant_id;
		   
		   $theuser->nick_name = $inputs['nick_name'];

		   $theuser->first_name = $inputs['first_name'];
		   $theuser->first_name_kana = mb_convert_kana($inputs['first_name_kana'],"C");
		   $theuser->last_name = $inputs['last_name'];
		   $theuser->last_name_kana = mb_convert_kana($inputs['last_name_kana'],"C");
		   $theuser->password = \Hash::make($inputs['password']);
		  
		   $theuser->gender = $inputs['gender'];
		   //$theuser->years_old = $inputs['years_old'];
		   //$theuser->months_old = $inputs['months_old'];
		   $date = new \DateTime();
		   $date->setDate($inputs['year'], $inputs['month'], $inputs['day']);
		   
		   $theuser->birth_date = $date->format('Y-m-d');
		   
		   
		   $theuser->invalid = $inputs['invalid'];
		   
 		   // curently there is no input for email	
		   //$theuser->email = $theuser->login_id."@examplemetaboweb.com";
		   $theuser->email = NULL;
		   
		   
   		   $theuser->save();
   		   
  		   return \Redirect::to('user/userlist');
  	   }
  	   //$errors = $validator->messages();
  	   //print_r($errors);
  	   return \Redirect::to('user/edituser/'.$user_id)->withErrors($validator)->withInput();
		
	}
	
	function getCsvuserreference(){
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		return \View::make('admin.user.csvuserreference');
	}
	
	function postCsvuserreference(){
		
		if (!$this->loggedin_backend()) {
			
			return \Redirect::to("user");
		}
		
		$special = $this->special();
		
		if ( (\Auth::user()->get()->role_id != 1) && (!$special) ) {
		    return \Redirect::to("user");
		}
		
		if ($special) $tenant_id = \Session::get('temp_tenant_id');
		else 
		$tenant_id = \Auth::user()->get()->tenant_id;

        //30秒で370件程であったので、deattanode,数千件に対応するために最大1000秒に設定（httpd.confの設定は900）
        set_time_limit(1000);

        $file = \Input::file('uploaded_file');
    	$path = $file->getRealPath();
    	  
		  $name = $file->getClientOriginalName();
    	  $mime = $file->getMimeType();
    	  $size = $file->getSize();
		
		$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
		if(!in_array($mime,$mimes)){  
			return \Redirect::to("user/csvuserreference");
		}
		$file = fopen($path,"r");
		$results = array();
		$i = 0;
		$get_csv_str = file_get_contents($path);
        $get_csv_str = mb_convert_encoding($get_csv_str,'utf-8','sjis-win');
        $get_csv_str = nl2br($get_csv_str);
        if(strpos($get_csv_str,"<br />") !== false){
            $lines = explode("<br />",$get_csv_str);
        }else{
            $lines = array($get_csv_str);
        }


        $cnt = count($lines);
		for ($i=0;$i < $cnt; $i++) {
            $inputstr = $lines[$i];
            $input = explode(',',$inputstr);
            if(!isset($input[23])){
                $results[$i] = "csvフォーマットが一致しません。";
                continue;
            }
            //foreach($input as $culumn_number=>$culumn_str){
                //$input[$culumn_number] = mb_convert_encoding($culumn_str,'utf-8','sjis-win');
            //}
            //echo $inputstr. "<br/>";
            if($input[0] == '*テナントID'){
                continue;
            }
            $inputs = array();
            $inputs['tenant_id'] 				= trim($input[0]);
            $inputs['login_id'] 				= trim($input[1]);
            $inputs['insurance_company_code'] 	= trim($input[2]);
            $inputs['insurance_code'] 			= trim($input[3]);
            $inputs['insurance_num'] 			= trim($input[4]);
            $inputs['gender'] 					= trim($input[5]);
            $inputs['years_old'] 				= trim($input[6]);
            $inputs['months_old'] 				= trim($input[7]);
            $inputs['birth_date']				= date('Y-m-d',strtotime(trim($input[8])));
            $inputs['nick_name'] 				= trim($input[9]);
            $inputs['last_name'] 				= trim($input[10]);
            $inputs['first_name'] 				= trim($input[11]);
            $inputs['last_name_kana'] 			= trim($input[12]);
            $inputs['first_name_kana'] 			= trim($input[13]);
            $inputs['email'] 					= trim($input[14]);

            $inputs['password'] 				= trim($input[15]);
            $inputs['password_confirmation']	= trim($input[15]);

            $inputs['zip'] 						= trim($input[16]);
            $inputs['address'] 					= trim($input[17]);
            $inputs['tel'] 						= trim($input[18]);
            $inputs['business_zip'] 			= trim($input[19]);
            $inputs['business_address'] 		= trim($input[20]);
            $inputs['business_tel'] 			= trim($input[21]);
            $inputs['business_name']			= trim($input[22]);
            $inputs['promotion_code'] 			= trim($input[23]);


           $rules_csv= array(
                "email" => "email|unique:t_user,email",
                'tenant_id' => "required|csvtenantcheck:".$tenant_id,
                'login_id' => "required|alphanum_under|min:4|max:44|unique:t_user,login_id,NULL,user_id,tenant_id,".$inputs['tenant_id'],
                'nick_name' => "required",
                'password' => "required|confirmed|halfwidth|min:4|max:12|csvpasscheck:".$inputs['login_id']
           );

           $emessages = \Config::get('error_messages');
           $emessages['login_id.unique'] = '既に '. $inputs['login_id'] . ' のログインIDが登録されています。';

           $validator = \Validator::make($inputs, $rules_csv,$emessages);

           if ($validator->passes()){
                $theuser = new \TUser;

                $theuser->tenant_id = $inputs['tenant_id'];
                $theuser->login_id = $inputs['login_id'];
                $theuser->insurance_company_code = $inputs['insurance_company_code'];
                $theuser->insurance_code = $inputs['insurance_code'];
                $theuser->insurance_num = $inputs['insurance_num'];
                $theuser->gender = $inputs['gender'];
                $theuser->years_old = $inputs['years_old'];
                $theuser->months_old = $inputs['months_old'];
                $theuser->birth_date = $inputs['birth_date'];
                $theuser->nick_name = $inputs['nick_name'];
                $theuser->last_name = $inputs['last_name'];
                $theuser->first_name = $inputs['first_name'];
                $theuser->last_name_kana = $inputs['last_name_kana'];
                $theuser->first_name_kana = $inputs['first_name_kana'];

                if (empty($inputs['email']))
                    $theuser->email = NULL;
                else $theuser->email = $inputs['email'];

                $theuser->password = \Hash::make($inputs['password']);
                $theuser->zip = $inputs['zip'];
                $theuser->address = $inputs['address'];
                $theuser->tel = $inputs['tel'];
                $theuser->business_zip = $inputs['business_zip'];
                $theuser->business_address = $inputs['business_address'];
                $theuser->business_tel = $inputs['business_tel'];
                $theuser->business_name = $inputs['business_name'];
                $theuser->promotion_code = $inputs['promotion_code'];


                $theuser->create_datetime = date('Y-m-d H:i:s',time());
                $theuser->invalid = 1;
                $theuser->save();
                isset($results[$i])?$results[$i]:$results[$i] = "Record $i : OK !";
          } else {
              $messages = $validator->messages();
              $results[$i] = isset($results[$i])?$results[$i]:"Record $i : Error: ";
              foreach ($messages->all() as $message)
              {
                  $results[$i] .=" ".$message;
              }
          }
      }

		return \View::make('admin.user.csvresults',compact('results'));
	}
	
	
	function getMypage(){
		if (!$this->loggedin()) {
			if (\Session::has('tenant_token'))
			$token = \Session::get('tenant_token');
			else $token = "default";
			return \Redirect::to("top/$token");
		}
		//$id = \Auth::tuser()->get()->user_id;
		//$theuser = \TUser::find($id);
		return \View::make('admin.user.mypage');
	}
	
	function getPortal(){
		if (!$this->loggedin()) {
			if (\Session::has('tenant_token'))
			$token = \Session::get('tenant_token');
			else $token = "default";
			return \Redirect::to("top/$token");
		}
		
		$token = \Session::get('tenant_token');
		// Get tenant from token
		$tenant = \Tenant::where("token","=",$token)->first();
		if ($tenant==null) return ;

        $interview_type = $tenant->interview_type;
        $interview_complete_flag = \Auth::tuser()->get()->interview_complete_flag;
        $saved_basic_info = \Auth::tuser()->get()->saved_basic_info;

        //フォローアップ用のURLを設定
        $followup_url = "followup";
        $id = \Auth::tuser()->get()->user_id;
        $theuser = \TUser::find($id);
        if($interview_complete_flag){
            $interview_data = \DB::table("t_interview")->where("user_id",$id)->first();
            $param = array();
            $now_dt = new \DateTime();
            $param["week"] = floor(($now_dt->diff(new \DateTime($interview_data["finished_datetime"]))->days)/7);
            $param["first_interview_id"] = $interview_data["interview_id"];
            $param["email"] = $interview_data["email"];
            $param["validation_key"] = $interview_data["validation_key"];
            $followup_url = $this->get_followup_url($param);
        }

		
		$view = \View::make('admin.user.portal',compact('interview_type','interview_complete_flag','saved_basic_info'));
        \View::share('followup_url',$followup_url);
        return $view;
	}
	
	function getChangebasicinfo(){
		if (!$this->loggedin()) {
			if (\Session::has('tenant_token'))
			$token = \Session::get('tenant_token');
			else $token = "default";
			return \Redirect::to("top/$token");
		}
		
		$user_id =  \Auth::tuser()->get()->user_id;
		 if (\Input::old('first_name')!==null) {
			 $theuser = new \TUser;
   		   $theuser->nick_name = \Input::old('nick_name');

   		   $theuser->first_name = \Input::old('first_name');
   		   $theuser->first_name_kana = \Input::old('first_name_kana');
   		   $theuser->last_name = \Input::old('last_name');
   		   $theuser->last_name_kana = \Input::old('last_name_kana');
 	
		  
   		   //$theuser->years_old = \Input::old('years_old');
   		   //$theuser->months_old = \Input::old('months_old');
		   $year = \Input::old('year');
		   $month = \Input::old('month');
		   $day = \Input::old('day');
		   
  		   $theuser->gender = \Input::old('gender');
		   
		} 
		else {
  	    	$theuser = \TUser::find($user_id);
			
	   		 $date = $theuser->birth_date;
			 if ($date == "0000-00-00"){
		 	   	 $current_year = date("Y");
		 		 $year = $current_year - 30;
		 		 $month = 1; 
		 		 $day = 1;
			 } else {
		   		 $time =strtotime($date);
		   		 $month=date("n",$time);
		   		 $year =date("Y",$time);
		   		 $day = date("d",$time);
			 }
		}
		
		return \View::make('admin.user.changebasicinfo',compact('theuser','year','month','day'));
	}
	
	function postChangebasicinfo(){
		if (!$this->loggedin()) {
			if (\Session::has('tenant_token'))
			$token = \Session::get('tenant_token');
			else $token = "default";
			return \Redirect::to("top/$token");
		}
		
		$user_id =  \Auth::tuser()->get()->user_id;
  	    $theuser = \TUser::find($user_id);
		
		$inputs = \Input::all();
    	   //$messages=array(
    	   // 
    	   //);
 	   $ruleschange = array(
			
 		'nick_name' => "required",
		'first_name_kana' => "hiragana_katakana",
		'last_name_kana' => "hiragana_katakana"
  		
   		);
		
   	   $validator = \Validator::make($inputs, $ruleschange,\Config::get('error_messages'));
	   
   	   if ($validator->passes()){

 		   $theuser->nick_name = $inputs['nick_name'];

 		   $theuser->first_name = $inputs['first_name'];
 		   $theuser->first_name_kana = mb_convert_kana($inputs['first_name_kana'],"C");
 		   $theuser->last_name = $inputs['last_name'];
 		   $theuser->last_name_kana = mb_convert_kana($inputs['last_name_kana'],"C");
 		   
		  
 		   //$theuser->years_old = $inputs['years_old'];
 		   //$theuser->months_old = $inputs['months_old'];
		   $date = new \DateTime();
		   $date->setDate($inputs['year'], $inputs['month'], $inputs['day']);
		   
		   $theuser->birth_date = $date->format('Y-m-d');
		   
		   
		   $theuser->gender = $inputs['gender'];
		   
		   $theuser->saved_basic_info = 1;
		   
    	   $theuser->save();
   		   
   		   return \Redirect::to('mypage');
   	   }
   	   //$errors = $validator->messages();
   	   //print_r($errors);
   	   return \Redirect::to('basic_info')->withErrors($validator)->withInput();
		
	}
	
	function getChangepassword(){
		if (!$this->loggedin()) {
			if (\Session::has('tenant_token'))
			$token = \Session::get('tenant_token');
			else $token = "default";
			return \Redirect::to("top/$token");
		}
		
		$error = "";
		return \View::make('admin.user.changepassword',compact('error'));
	}
	
	function postChangepassword(){
		
		if (!$this->loggedin()) {
			if (\Session::has('tenant_token'))
			$token = \Session::get('tenant_token');
			else $token = "default";
			return \Redirect::to("top/$token");
		}
		
		$user_id =  \Auth::tuser()->get()->user_id;
  	    $theuser = \TUser::find($user_id);
		
		$inputs = \Input::all();
    	  
 	   $ruleschangep = array(
			
		   'newpassword' => 'required|confirmed|halfwidth|min:4|max:12',
   		);
		
		if (!\Hash::check($inputs['password'],$theuser->password)) {
			$error = 'パスワードが正しくありません';
			
			return \View::make('admin.user.changepassword',compact('error'));
			
		}
		
    
		
   	   $validator = \Validator::make($inputs, $ruleschangep,\Config::get('error_messages'));
	   
   	   if ($validator->passes()){


 		   $theuser->password = \Hash::make($inputs['newpassword']);
		   		   
    	   $theuser->save();
   		   
   		   return \Redirect::to('mypage');
   	   }
   	   //$errors = $validator->messages();
   	   //print_r($errors);
   	   return \Redirect::to('edit_password')->withErrors($validator)->withInput();
	}
}
