<?php

class History extends Eloquent {

	/**
	 * 特記事項更新
	 * Enter description here ...
	 * @param $interview_id
	 * @param $instruction
	 */
	public static function updateInterview($interview_id, $instruction) {

		DB::table('t_interview')->where('interview_id', $interview_id)->update(array('instruction' => $instruction));
		return true;
	}

	/**
	 * 印刷履歴登録
	 * Enter description here ...
	 * @param $interview_id
	 * @param $account_id
	 * @param $name
	 * @param $view_type
	 */
	public static function insertPrintHistory($interview_id, $account_id, $name, $view_type) {

		DB::table('t_print_history')->insert(
		    array('interview_id' => $interview_id, 'account_id' => $account_id, 'name' => $name, 'view_type' => $view_type, 'create_datetime' => date('Y-m-d H:i:s'))
		);

		return true;
	}
}

?>