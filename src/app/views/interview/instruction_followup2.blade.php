
<div id="instruction-area">
	特記事項：
	<form  action="" method="post">
		<input type="hidden" id="absolute_url" value="{{ URL::to('/') }}/">
		<input type="hidden" id="view_type" value="{{ $view_type }}">
		<input type="hidden" id="interview_id" value="{{ $followup_data['followup_interview_id'] }}">
		<textarea name="instruction" cols="50" rows="5" id="instruction">{{ $interview_data['instruction'] }}</textarea>
		<p>{{ date('Y') }}年{{ date('m') }}月{{ date('d') }}日</p>
		<p>入力者：<input type="text" id="name" /></p>
		<p><input id="followup-print-btn" type="button" value="印刷" /></p>
	</form>
</div>
