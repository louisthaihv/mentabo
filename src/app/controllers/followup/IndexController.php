<?php

namespace Followup;

use View;
use Input;
use Auth;
use Followup;
use URL;
use Session;

class IndexController extends \BaseController {

	public function getIndex()
	{
		return $this->postIndex();
	}

	public function postIndex()
	{
		$input = Input::all();
		$base64_decode = $this->getBase64_decode($input);
		$array_keys = array_keys($input);
		$user = Auth::tuser()->get();

		if(Input::get('section_set_id') == 5 OR Input::get('address_mod') == 1 OR Input::get('entry') == 1) {

	        $interview_id = $base64_decode[1];
	        
	        //メールからの場合は、emailとvalidation_keyが存在する前提
	        //ログイン状態からだと、email,validation_keyがない可能性がある
	        // if(input::get('entry') == 1)  {
		       //  $user_register_mail = $base64_decode[3];
		       //  $select_interview = \Interview::selectInterview(array('interview_id' => $interview_id, 'user_register_mail' => $user_register_mail));	
	        // }else{
		       //  $validation_key = $base64_decode[3];
		       //  $select_interview = \Interview::selectInterview(array('interview_id' => $interview_id, 'validation_key' => $validation_key));

	        // }

	        if(input::get('section_set_id') == 5) $select_interview = \Interview::selectInterview(array('interview_id' => $interview_id, 'validation_key' => $base64_decode[3]));
	        if(input::get('address_mod') == 1) $select_interview = \Interview::selectInterview(array('interview_id' => $interview_id, 'validation_key' => $base64_decode[3]));
	        if(input::get('entry') == 1) $select_interview = \Interview::selectInterview(array('interview_id' => $interview_id, 'validation_key' => $base64_decode[3]));
	        if(!$select_interview) return View::make('404');

			if (!$this->loggedin()) {

				$tenant_id = $select_interview['tenant_id'];
				$tenant = \Tenant::where('tenant_id', '=', $tenant_id)->first();
				$token = $tenant['token'];
				Session::put('token', $token);

				if(input::get('section_set_id') == 5) $followup_code = URL::to('/followup/?section_set_id=5&'.$array_keys[1]);
				if(input::get('address_mod') == 1) $followup_code = URL::to('/followup/?address_mod=1&'.$array_keys[1]);
				if(input::get('entry') == 1) $followup_code = URL::to('/followup/?entry=1&'.$array_keys[1]);

				if($select_interview['validation_key'] /*OR $select_interview['user_register_mail']*/) Session::put('followup_code', $followup_code);
				return \Redirect::to('/top/'.$token);

			} else {


	 			if($user->user_id != $select_interview['user_id']) {
	 				
					Auth::tuser()->logout();
					$followup_code = \Session::get('followup_code');
					return \Redirect::to($followup_code);
				}  

				Session::put('followup_code', '');
				$token = Session::get('token');	

				if(input::get('section_set_id') == 5) return $this->getAge($select_interview, $input);
				if(input::get('address_mod') == 1) return $this->getChangemail($select_interview, $input);
				if(input::get('entry') == 1) return $this->getEntry($select_interview, $input, $token);	

			}	

		} else {

			if ($this->loggedin()) {
		       
		        $interview_type = 1;
		        $user_id = $user->user_id;
		        $select_interview = Followup::select_interview($user_id, $interview_type);

			} else {

				return View::make('404');
			}	
		}

		$status = true;
		if(isset($input['input_type'])) {
			$input_type = $input['input_type'];
		} else {
			$input_type = '';
		}

		switch ($input_type) {
		    case 'email':
		        return $this->getSendmail($select_interview, $input);
		    case 'confirm':
		        return $this->postConfirm($select_interview, $input);
		        break;
		    case 'question':
		        return $this->postQuestion($select_interview, $input);
		        break;
		    case 'completion':
		        return $this->postCompletion($select_interview, $input);
		        break;
		    case 'judge':
		        return $this->postJudge($select_interview, $input);
		        break;
		    default:
				$setTitle = '生活習慣診断 フォローアップ';
				$content = View::make('followup.index', compact('select_interview', 'status'));
				return View::make('followup.content', compact('setTitle', 'content'));
		}	

	}
	

	/*	
	public function postIndex()
	{
        $input = Input::all();
        $base64_decode = $this->getBase64_decode($input);
        //メールからの場合は、emailとvalidation_keyが存在する前提
        //ログイン状態からだと、email,validation_keyがない可能性がある

        if (!$this->loggedin()) {
            //非ログインならメールからなので、emailとvalidation_keyが存在する
            $interview_results = \DB::table('t_interview')->where('validation_key',$base64_decode[3])->first();

            if(!$interview_results){
                return View::make("404");
            }
            $user = \DB::table('t_user')->where('user_id',$interview_results['user_id'])->first();

            $tenant_id = $user['tenant_id'];
            $tenant = \Tenant::where('tenant_id',"=",$tenant_id)->first();
            $token = $tenant['token'];

            // $input_string = "";
            // $cnt = count($input);
            // $i=0;
            // foreach($input as $key=>$val){
            //     $i++;
            //     $input_string .= $key . '=' . $val;
            //     if($i!=$cnt){
            //         $input_string .= "&";
            //     }
            // }
            // \Session::put('followup',$input_string);

            //return \Redirect::to("/top/{$token}?".$input_string);
            return \Redirect::to("/top/{$token}");
        }else{
            $user = Auth::tuser()->get();
        }

        $status = true;
        $interview_type = 1;
        $user_id = $user->user_id;
        $select_interview = Followup::select_interview($user_id, $interview_type);

        if(\Session::get("followup")){
            $str = \Session::get("followup");
            $str_arr = explode("&",$str);
            foreach($str_arr as $val){
                list($k,$v) = explode("=",$val);
                $input[$k] = $v;
            }
        }


        if(input::get('section_set_id')) {

            return $this->getAge($select_interview, $input);

        } else {

            if(isset($input['input_type'])) {
                $input_type = $input['input_type'];
            } else {
                $input_type = '';
            }

            switch ($input_type) {
                case 'email':
                    return $this->getSendmail($select_interview, $input);
                case 'confirm':
                    return $this->postConfirm($select_interview, $input);
                    break;
                case 'question':
                    return $this->postQuestion($select_interview, $input);
                    break;
                case 'completion':
                    return $this->postCompletion($select_interview, $input);
                    break;
                case 'judge':
                    return $this->postJudge($select_interview, $input);
                    break;
                default:
                    /*
                    $setTitle = '生活習慣診断 フォローアップ メールアドレス登録';
                    $content = View::make('followup.index', compact('select_interview', 'status'));
                    return View::make('followup.content', compact('setTitle', 'content'));
                    
            }

        }

        //$base64_decode = $this->getBase64_decode($input);
        if(count($base64_decode) != 4){
            //return View::make("404");
        }

        if(!$base64_decode[2] || !$base64_decode[3]){
            //no email or no validation_key
            $setTitle = '生活習慣診断 フォローアップ メールアドレス登録';
            $content = View::make('followup.index', compact('select_interview', 'status'));
            return View::make('followup.content', compact('setTitle', 'content'));
        }


        $tenant_id = $user['tenant_id'];
        $tenant = \Tenant::where('tenant_id',"=",$tenant_id)->first();
        $token = $tenant['token'];

        if (!$tenant->valid){ return \View::make('404');}
        if ($tenant==null){ return \View::make('404');}
        $tenant_id = $tenant->tenant_id;

        \Session::put('tenant_token',$token);


		if (!$this->loggedin()) {

            return View::make("404");
		}


		if(input::get('section_set_id')) {

			return $this->getAge($select_interview, $input);

		} else {

			if(isset($input['input_type'])) {
				$input_type = $input['input_type'];
			} else {
				$input_type = '';
			}

			switch ($input_type) {
			    case 'email':
			        return $this->getSendmail($select_interview, $input);
			    case 'confirm':
			        return $this->postConfirm($select_interview, $input);
			        break;
			    case 'question':
			        return $this->postQuestion($select_interview, $input);
			        break;
			    case 'completion':
			        return $this->postCompletion($select_interview, $input);
			        break;
			    case 'judge':
			        return $this->postJudge($select_interview, $input);
			        break;
			    default:
					$setTitle = '生活習慣診断 フォローアップ メールアドレス登録';
					$content = View::make('followup.index', compact('select_interview', 'status'));
					return View::make('followup.content', compact('setTitle', 'content'));
			}

		}
		
	}
	*/

	public function getEntry($select_interview, $input, $token)
	{	
		$base64_decode = $this->getBase64_decode($input);
		if($select_interview['interview_id'] == $base64_decode[1] AND $select_interview['email'] == $base64_decode[2]/* AND $select_interview['user_register_mail'] == $base64_decode[3]*/) {

			//$update_interview = Followup::update_interview(array('interview_id' => $base64_decode[1]/*, 'user_register_mail' => $base64_decode[3]*/), array(/*'user_register_mail' => 'Null',*/ 'validation_key'=>$this->randString()));
            $update_interview = Followup::update_interview(array('interview_id' => $base64_decode[1]), array('flg_followup' => 1));

			$setTitle = '生活習慣診断 フォローアップ メールアドレス登録';
			$content = View::make('followup.entry', compact('select_interview', 'entry'));
			return View::make('content', compact('setTitle', 'content'));

		} else {

			return \Redirect::to('/top/'.$token);
		}

	}

	public function getSendmail($select_interview, $input)
	{	
		$validator = Followup::validate($input);

		if (!$validator->fails()) {
			$param['week'] = 0;
			$param['first_interview_id'] = $select_interview['interview_id'];
			$param['name'] = $select_interview['first_name'].$select_interview['last_name'];
			$param['email'] = $input['email'];
			$param['tenant_id'] = $select_interview['tenant_id'];
			$param['type'] = 'entrymail';

			Followup::update_interview(array('interview_id' => $select_interview['interview_id']), array('email' => $input['email']));
			Followup::update_user(array('user_id' => $select_interview['user_id']), array('email' => $input['email']));

			$type = 'user_register_mail';
			$this->sendMail($param, $type);

			$setTitle = '生活習慣診断 フォローアップ メールアドレス登録';
			$content = View::make('followup.sendmail');
			return View::make('content', compact('setTitle', 'content'));

		} else { 

			$status = 'email';
			$setTitle = '生活習慣診断 フォローアップ';
			$content = View::make('followup.index', compact('select_interview', 'status'));
			return View::make('followup.content', compact('setTitle', 'content'));
		} 

	}

	public function getChangemail($select_interview, $input)
	{
		if(isset($input['followup_changerequest_do']) AND isset($input['mailaddress'])) {

			$checkmail = (bool) count(Followup::check_mailaddress($input['mailaddress']));
			if(!$checkmail){
                Followup::change_email($select_interview['user_id'], $input['mailaddress']);
                $setTitle = '生活習慣診断 フォローアップ 登録メールアドレス変更完了';
                $content = View::make('followup.changemail', compact('select_interview', 'checkmail'));
                return View::make('content', compact('setTitle', 'content'));
            }

		}
		//Followup::update_interview(array('interview_id' => $select_interview['interview_id']), array('user_change_mail' => Null));
		$setTitle = '生活習慣診断 フォローアップ 登録メールアドレス変更';
		$content = View::make('followup.changemail', compact('select_interview'),'checkmail');
		return View::make('content', compact('setTitle', 'content'));

	}

	
	public function getAge($select_interview, $input)
	{
		if (!$this->loggedin()) {
			
			return View::make("404");
		}
		$base64_decode = $this->getBase64_decode($input);
		Session::put('week', $base64_decode[0]);
		
		$interview_type	= Input::get('section_set_id'); // パラメータ取得およびチェック
		$week = $base64_decode[0]; // 経過週（x週目）
		$first_interview_id	= $base64_decode[1]; // 初回問診ID（interview_id）
		$email = $base64_decode[2]; // メールアドレス取得

		// 当日が初回問診から2週間経過しているかチェック
		if (date('Y-m-d') < date('Y-m-d',strtotime('+2 weeks' ,strtotime($select_interview['finished_datetime'])))) {

			$setTitle = 'エラー';
			$content = View::make('followup.error');

		} else {

			// 初回問診の指導内容取得
			$guidance_data = Followup::getGuidanceByInterviewId($first_interview_id);
			$exercise_value = $guidance_data['exercise_value'];
			Session::put('exercise_value', $exercise_value);
			$birth_date = $select_interview['birth_date'];
			$birth = $birth_date;
			$setTitle = '生活習慣診断 フォローアップ';
			$content = View::make('followup.age', compact('exercise_value', 'birth_date', 'birth'));
		}

		if($select_interview['interview_id'] == $base64_decode[1] AND $select_interview['email'] == $base64_decode[2] AND $select_interview['validation_key'] == $base64_decode[3]) {

			$update_interview = Followup::update_interview(array('interview_id' => $base64_decode[1], 'validation_key' => $base64_decode[3]), array('flg_followup' => 9, /*'validation_key' => ''*/));
		}
		
		return View::make('followup.content', compact('setTitle', 'content'));
		
	}
    public function getBase64_decode($input){
        $array_keys = array_keys($input);
        if($array_keys) {

	        if(@$array_keys[0] == 'followup_changerequest_do') $base64_decode = $array_keys[3];
	        else  $base64_decode = $array_keys[1];

	        if(isset($base64_decode)){
	            $base64_decode = base64_decode($base64_decode);
	            $base64_decode = str_replace('week=', '', $base64_decode);
	            $base64_decode = str_replace('interview_id=', '', $base64_decode);
	            $base64_decode = str_replace('email=', '', $base64_decode);
	            $base64_decode = str_replace('validation_key=', '', $base64_decode);
	            $base64_decode = explode('&', $base64_decode);

	            return $base64_decode;
	        }
    	}
    }

	public function postConfirm($select_interview, $input)
	{	
		$week = Session::get('week');
		$exercise_value  = Input::get('exercise_value');
		$setTitle = '生活習慣診断 フォローアップ';

		$birth_date = $select_interview['birth_date'];
		$birth = $input['birth_date_y'].'-'.$input['birth_date_m'].'-'.$input['birth_date_d'];
		if($birth == $select_interview['birth_date']) {

			$content = View::make('followup.confirm', compact('select_interview', 'week', 'exercise_value'));
			return View::make('followup.content', compact('setTitle', 'content'));

		} else {
			$error = 1;
			$content = View::make('followup.age', compact('exercise_value', 'birth', 'error'));
			return View::make('followup.content', compact('setTitle', 'content'));

		}

	}

	public function postQuestion($select_interview, $input)
	{
		$week = Session::get('week');
		$first_interview_id  = $select_interview['interview_id'];
		$user_data = $select_interview;
		$section_set_id = $this->followupType($week);

		$followup_data = Followup::getFollowupNotCompliedCheckByFirstInterviewId($first_interview_id);
		if (empty($followup_data)) {
			$type = 2;
		}else{
			$type = $followup_data['type'];
		}

		$first_interview_data = Followup::getFollowupByFirstInterviewId($first_interview_id);
		$section_list =  Followup::getSectionSetContentsByFollowupId($section_set_id, $user_data, $first_interview_data, $type);

		$section_set_by_page = array();
		foreach ($section_list as $section) {
			$section_set_by_page[$section['page_num']][$section['order_section']] = $section['section_id'];
		}
		
		// 運動強度判定値を取得
		if ($week == 2) {
			// 2週目の場合、セッションから初回問診時に判定された運動強度判定値を取得
			$exercise_value = Input::get('exercise_value');
			//print_r($exercise_value);
		}else{
			// マネージャ定義
			//$followup_manager = $this->backend->getManager('Followup');

			// 2週目以降の場合、前回のフォローアップ問診からデータを取得する
			//$last_followup_data = $followup_manager->getFollowupByFirstInterviewId($first_interview_id);
			// if(Ethna::isError($last_followup_data)){
			// 	$this->ae->add( null, $last_followup_data->getMessage());
			// }
			// 問診情報取得（初回問診の問診パターン（3x）を取得）
			//$interview_data = $followup_manager->getInterviewByFollowupId($first_interview_id);
			// if(Ethna::isError($interview_data)){
			// 	$this->ae->add( null, $interview_data->getMessage());
			// }

			//$exercise_value = $last_followup_data['exercise_value'];
			$exercise_value = $first_interview_data['exercise_value'];

		}

		// 運動種目を取得
		\FollowupMasterData::globals();
		foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $key => $val){
			if ($val['route'] = 1 && $val['exercise_value'] == $exercise_value){
				$followup_sports[$key] = $val['mets'];
			}
		}

		$section_set_by_page_array = $this->sectionSetByPage($section_set_by_page, 1);

		$section_list = $section_set_by_page_array['section_list'];
		$layer_list = $section_set_by_page_array['layer_list'];
		$choice_list = $section_set_by_page_array['choice_list'];

		$jqueryArray['jquery_display'] = $jquery_display = $section_set_by_page_array['jquery_display'];
		$jqueryArray['jquery_ui'] = $jquery_ui = $section_set_by_page_array['jquery_ui'];
		$jqueryArray['jquery_requiredCheck'] = $jquery_requiredCheck = $section_set_by_page_array['jquery_requiredCheck'];
		$jqueryArray['jquery_regexpCheck'] = $jquery_regexpCheck = $section_set_by_page_array['jquery_regexpCheck'];

		Session::put('section_set_id', $section_set_id);
		Session::put('start_datetime', date('Y-m-d H:i:s'));
		Session::put('section_set_by_page', $section_set_by_page);
		Session::put('followup_input_session', $base64_decode[0]);
		Session::put('first_interview_id', $first_interview_id);
		Session::put('type', $type);
		Session::put('week', $week);

		$setTitle = '生活習慣診断　フォローアップ診断';
		$headerScript = View::make('interview.header_script', array('jqueryArray' => $jqueryArray));
		$content = View::make('interview.question', compact('section_list', 'section_set_id'));
		return View::make('followup.content', compact('setTitle', 'content', 'headerScript', 'followup_sports', 'section_list'));

	}

	public function postCompletion($select_interview, $input)
	{
		
		// 問診完了画面
		//マネージャ定義
		//$followup_manager = $this->backend->getManager('Followup');
		/*
		// フォローアップ問診の回答が保存されているかチェック
		$current_followup_interview_id = $this->session->get('current_followup_interview_id');		// 現在のフォローアップ問診ID
		if (!empty($current_followup_interview_id)) {
			$current_followup_interview_id = $followup_manager->getInterviewByFollowupId($current_followup_interview_id);
			if(Ethna::isError($current_followup_interview_id)){
				$this->ae->add( null, $current_followup_interview_id->getMessage());
				return 'followup_trouble2';
			}
		}
		*/
		// フォローアップ問診の回答が保存されていない場合、新規の問診の回答と判断し、回答された内容を保存する
		if (empty($current_followup_interview_id)) {
			//渡された値を上書き保存
			//$followup_input_session = $this->session->get('followup_input_session');
			$followup_input_session = Session::get('followup_input_session');
			foreach ($_REQUEST as $key=>$value) {
				$followup_input_session[$key] = $value;
			}
			//$this->session->set('followup_input_session', $followup_input_session);
			Session::put('followup_input_session', $followup_input_session);

			/* 登録 ---------- */
			$user_data				= $select_interview; // ユーザ情報（birth_date／age）
			$section_set_id			= Session::get('section_set_id'); // フォローアップ問診パターンID
			$start_datetime			= Session::get('start_datetime'); // フォローアップ問診開始時間（問診ページ表示時間）
			$section_set_by_page	= Session::get('section_set_by_page');// フォローアップ問診内容
			$followup_input_session	= Session::get('followup_input_session'); // フォローアップ問診回答内容
			$first_interview_id		= Session::get('first_interview_id'); // 初回問診ID
			$type					= Session::get('type'); // フォローアップ問診タイプ（xW）
			$week					= Session::get('week'); // 経過週（x週目）
			
			// フォローアップ問診内容がsessionに残っているかチェック
			$section_id_sql_condition = '';
			foreach ($section_set_by_page as $page => $page_section_set_list) {
				foreach ($page_section_set_list as $section_id) {
					if (!empty($section_id_sql_condition)) {
						$section_id_sql_condition .= ', ';
					}
					$section_id_sql_condition .= $section_id;
				}
			}
			$section_id_sql_condition = explode(', ', $section_id_sql_condition);
			// if (empty($section_id_sql_condition)) {
			// 	$this->af->setApp('message', "この問診は判定が終了しています。");
			// 	return 'followup_trouble2';
			// }

			//対象設問の全選択肢取得
			$choice_list = Followup::getChoiceBySectionId($section_id_sql_condition);
			// if(Ethna::isError($choice_list)){
			// 	$this->ae->add( null, $choice_list->getMessage());
			// 	return 'followup_trouble2';
			// }

			//判断元選択肢のID一覧取得
			$layer_control_list = Followup::getLayerControlByChoiceBySectionIdList($section_id_sql_condition);
			// if(Ethna::isError($layer_control_list)){
			// 	$this->ae->add( null, $layer_control_list->getMessage());
			// 	return 'followup_trouble2';
			// }
			//layer_id, group_noをキーに並び替え
			$upper_id_list = array();
			if (is_array($layer_control_list)) {
				foreach ($layer_control_list as $value) {
					$upper_id_list[$value['layer_id']]['all_or_every'] = $value['all_or_every'];
					$upper_id_list[$value['layer_id']]['group'][$value['group_no']][] = $value;
					$upper_id_list[$value['layer_id']]['show_type'] =  $value['show_type'];
				}
			}

			// 初回問診情報を取得
			$guidance_data = Followup::getGuidanceByInterviewId($first_interview_id);
			// if(Ethna::isError($guidance_data)){
			// 	$this->ae->add( null, $guidance_data->getMessage());
			// }

			// 初回問診IDから、前回の問診結果データ取得
			$last_followup_data = Followup::getFollowupByFirstInterviewId($first_interview_id);
			// if(Ethna::isError($last_followup_data)){
			// 	$this->ae->add( null, $last_followup_data->getMessage());
			// }
			/*
			// 次の始まりの週を取得するために、現在登録されている最大経過週を取得
			$max_week = $followup_manager->getFollowupMaxWeekByFirstInterviewId($user_data['interview_id']);
			if (Ethna::isError($week)) {
				return $week;
			}
			*/
			// 現在のフェーズの2Wのデータを取得（特定の問診タイプのデータの最新を1件取得）
			// 判定結果に変更が無いか確認するために必要
			$followup_2w_data = Followup::getFollowupByFirstInterviewIdType($user_data['interview_id'], 2);
			// if (Ethna::isError($followup_2w_data)) {
			// 	return $followup_2w_data;
			// }

			// 今回のフォローアップ問診の入力予定日より次回以降の入力予定日を登録するため、問診データを取得
			if ($week != 48 && (($week == 2 && $type == 2) || $type == 12)) {
				$followup_data = Followup::getFollowupByFollowupId($user_data['interview_id'], $week);
				// if (Ethna::isError($followup_data)) {
				// 	return $followup_data;
				// }
			}

			//問診テーブル（t_interview）登録
			$followup_interview_id = $this->saveInput($user_data,						    // ユーザ情報（birth_date／age）
																  $section_set_id,			// フォローアップ問診パターンID（5x）
																  2,						// 問診タイプ（1:問診／2:フォローアップ／3:食事入力／4:運動入力）
																  $start_datetime,			// フォローアップ問診開始時間（問診ページ表示時間）
																  $followup_input_session,	// フォローアップ問診回答内容
																  $choice_list,				// フォローアップ設問の全選択肢取得
																  $upper_id_list,			// 選択された時に表示する内容（判断元選択肢のID）
																  $type,					// フォローアップ問診タイプ（xxW）
																  $week,					// 経過週（x週目）
																  $first_interview_id,		// 初回問診ID
																  $guidance_data,			// 初回問診情報
																  $last_followup_data,		// 実施済みの最新フォローアップデータ
																  $followup_2w_data			// 現在のフェーズの2Wのデータ
																  );

			// if(Ethna::isError($followup_interview_id)){
			// 	$db->rollback();//ロールバック
			// 	$this->ae->add( null, $followup_interview_id->getMessage());
			// 	$this->af->set('page', 1);
			// 	return 'followup_trouble2';
			// }

			// //●コミット
			// $db->commit();

			// $this->session->set('current_followup_interview_id', $followup_interview_id);
			Session::put('current_followup_interview_id', $followup_interview_id);

			// $this->af->setApp('week', $week);
			// $this->af->setApp('section_set_id', $section_set_id);

			$flg_show_judge = 1;
			$setTitle = '生活習慣診断 フォローアップ';
			$content = View::make('followup.completion', compact('week', 'flg_show_judge', 'interview_data'));
			return View::make('followup.content', compact('setTitle', 'content'));
        }

	}

	public function postJudge($select_interview, $input)
	{
		\FollowupJudgeData::globals();
		// 判定結果
		//マネージャ定義
		//$followup_manager		= $this->backend->getManager('Followup');

		// $followup_interview_id	= $this->session->get('current_followup_interview_id');			// フォローアップ問診ID
		// $first_interview_id		= $this->session->get('first_interview_id');					// 初回問診ID
		// $week					= $this->session->get('week');									// 経過週
		// $section_set_id			= $this->session->get('section_set_id');						// 問診タイプ取得

		$followup_interview_id	= Session::get('current_followup_interview_id'); // フォローアップ問診ID
		$first_interview_id		= Session::get('first_interview_id'); // 初回問診ID
		$week					= Session::get('week'); // 経過週
		$type                   = Session::get('type');
		$section_set_id			= Session::get('section_set_id'); // 問診タイプ取得a

		// 誕生日取得用（メニュー画面へ遷移させる）
		// $user_data = $this->session->get('user_data');/ ユーザ情報
		// $this->af->setApp('user_data', $user_data);
		$user_data = $select_interview;

		// t_choice_selection　から回答選択したデータを取得
		$choice_selection_data = Followup::getChoiceSelectionByInterviewId($followup_interview_id);
		// if(Ethna::isError($choice_selection_data)){
		// 	$this->ae->add( null, $first_interview_data->getMessage());
		// }

		// フォローアップ診断結果を取得
//		$followup_data = $followup_manager->getFollowupByFollowupId($first_interview_id, $followup_interview_id, $week);
		$followup_data = Followup::getFollowupByFollowupId($first_interview_id, $week);
		// if(Ethna::isError($followup_data)){
		// 	$this->ae->add( null, $followup_data->getMessage());
		// }
		// $this->af->setApp('followup_data', $followup_data);


		// フォローアップ問診の回答を取得
		$choice_interview_list = $this->choiceByFollowupInterviewId($choice_selection_data, $section_set_id);
		// if(Ethna::isError($choice_interview_list)){
		// 	$this->ae->add( null, $choice_interview_list->getMessage());
		// }
		// $this->af->setApp('choice_interview_list', $choice_interview_list);


		// フォローアップ問診の回答を取得
		$followup_judgment_list = $this->judgeDisplayByFollowupId($followup_data);
		// if(Ethna::isError($followup_judgment_list)){
		// 	$this->ae->add( null, $followup_judgment_list->getMessage());
		// }
		// $this->af->setApp('followup_judgment_list', $followup_judgment_list);

		// $this->af->setApp('absolute_url', AX_ABSOLUTE_URL);
		// $this->af->setApp('view_type', 'followup');user

		$followup_complete_flag = Followup::update_user(array('user_id' => $select_interview['user_id']), array('followup_complete_flag' => 1));

		$absolute_url = '';
		$view_type = 'followup';
		return View::make('followup.judge', array('week' => $week, 'type' => $type, 'choice_selection_data' => $choice_selection_data, 'choice_interview_list' => $choice_interview_list, 'followup_judgment_list' => $followup_judgment_list, 'first_interview_id' => $first_interview_id));
	}

	/**
	 * フォローアップ問診・回答データ登録
	 * Enter description here ...
	 * @param $user_data			：ユーザ情報（birth_date／age）
	 * @param $section_set_id		：フォローアップ問診タイプ
	 * @param $interview_type		：問診タイプ（1:問診／2:フォローアップ／3:食事入力／4:運動入力）
	 * @param $start_datetime		：フォローアップ問診開始時間（問診ページ表示時間）
	 * @param $input_raw_data		：問診回答内容
	 * @param $choice_list			：フォローアップ設問の全選択肢取得
	 * @param $upper_id_list		：選択された時に表示する内容（判断元選択肢のID）
	 * @param $type					：フォローアップ問診タイプ（xxW）
	 * @param $week					：経過週（x週目）
	 * @param $first_interview_id	：初回問診ID
	 * @param $guidance_data		：初回問診情報
	 * @param $last_followup_data	：実施済みの最新フォローアップデータ
	 * @param $followup_2w_data		：現在のフェーズの2Wのデータ
	 */
//	function saveInput($user_data, $section_set_id, $interview_type, $start_datetime, $input_raw_data, $choice_list, $upper_id_list, $type, $week, $first_interview_id, $guidance_data, $last_followup_data, $followup_2w_data, $followup_data = null)
	public function saveInput($user_data, $section_set_id, $interview_type, $start_datetime, $input_raw_data, $choice_list, $upper_id_list, $type, $week, $first_interview_id, $guidance_data, $last_followup_data, $followup_2w_data)
	{
		
		/* 問診データ登録 */
		$insert_values = array();
		$insert_values['name']				= $user_data['user_name'];				// 診察券番号
		$insert_values['gender']			= $user_data['gender'];					// 性別
		$insert_values['birth_date']		= $user_data['birth_date'];				// 誕生日
		$insert_values['section_set_id']	= $section_set_id;						// 問診セットID（フォローアップは50番台）
		$insert_values['interview_type']	= $interview_type;						// 問診タイプ（1:問診／2:フォローアップ／3:食事入力／4:運動入力）
		$insert_values['start_datetime']	= $start_datetime;						// フォローアップ問診開始時間（問診ページ表示時間）
		$insert_values['finished_datetime']	= date("Y-m-d H:i:s");					// フォローアップ問診終了時間
		$insert_values['email']				= $user_data['email'];					// メールアドレス

		//print_r($user_data['user_id']);die;
		$array = array('user_id' => $user_data['user_id'], 'section_set_id' => $section_set_id, 'interview_type' => $interview_type);
		$followup_interview_id = Followup::insert_interview($array);
		//print_r($followup_interview_id);

		// $followup_interview_id =  Ax_Util::createRow('t_interview', $insert_values);
		// if (Ethna::isError($followup_interview_id)) {
		// 	return $followup_interview_id;
		// }

		/* 回答登録 */
		//判断元選択肢の選択有無確認用ID一覧作成（※判断元となる選択肢は、現状ではラジオボタン・チェックボックスのみ）
		$selected_choice_id_list = array();
		foreach ($input_raw_data as $input_name => $value) {
			if (strpos($input_name, 'il_') === 0) {
				if (is_array($value)) {
					foreach ($value as $choice_id) {
						$selected_choice_id_list[] = $choice_id;
					}
				} else {
					$selected_choice_id_list[] = $value;
				}
			}
		}

		//一括insertデータ作成
		$rows = array();
		foreach ($input_raw_data as $input_name => $value) {
			$insert_values = null;
			//チェックボックス・ラジオボタン
			if (strpos($input_name, 'il_') === 0) {
				if (!is_array($value)) {
					$choice_id_list = array($value);
				} else {
					$choice_id_list = $value;
				}
				foreach ($choice_id_list as $choice_id) {
					// 回答データ登録用データ作成
					$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list);
					if (!empty($insert_values)) {
						$rows[] = $insert_values;
					}
				}
				//入力値
			} elseif (strpos($input_name, 'i_') === 0) {
				$choice_id = substr($input_name, 2);
				$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list, $value);
				if (!empty($insert_values)) {
					$rows[] = $insert_values;
				}
			}
		}

		//一括insert
		if (!empty($rows)) {
			$result = Followup::insertChoiceSelectionFollowup($followup_interview_id, $rows);
			// if (Ethna::isError($result)) {
			// 	return $result;
			// }
		}


		//-----------------------------------------------------
		// 問診結果判定処理
		//-----------------------------------------------------
		//----------------------------------
		// フォローアップ判定
		//----------------------------------
		$followup_judgement_data = $this->judgeByFollowupRecord($rows, $guidance_data, $last_followup_data, $week, $type, 2);


		//----------------------------------
		// フォローアップ基本登録用データ
		//----------------------------------
		if ($week != 48 && (($week == 2 && $type == 2) || $type == 12)) {

			// $insert_followup_values = $this->createFollowupInsertValue($user_data, $followup_judgement_data, $followup_2w_data, $followup_data, $week, $type);
			$insert_followup_values = $this->createFollowupInsertValue($user_data, $followup_judgement_data, $followup_2w_data, $week, $type);
			$insert_followup = Followup::insert_followup($insert_followup_values);
			//print_r($insert_followup_values);die;
			// for ($i = 0; $i < count($insert_followup_values); $i++) {
			// 	$result = Ax_Util::createRow('t_followup', $insert_followup_values[$i]);
			// 	if (Ethna::isError($result)) {
			// 		return $result;
			// 	}
			// }
		}

		//----------------------------------
		// 判定データ更新
		//----------------------------------
		// 更新データ編集
		// データが無い場合は、デフォルトと同様null値を登録する
 		$followuplist = array ('input_time'				=> null,
		 					   'input_count'			=> null,
		 					   'propose_time'			=> null,
		 					   'propose_count'			=> null,
		 					   'target_type'			=> null,
		 					   'current_weight'			=> null,
		 					   'input_date'				=> date('Y-m-d H:i:s'),
		 					   'followup_interview_id'	=> $followup_interview_id,
		 					   'mets'					=> null,
		 					   'exercise_value'			=> null,
		 					   'propose_calorie'		=> null,
		 					   'judgment'				=> null,
		 					   'judgment_type'			=> null,
		 					   'result'					=> null,
		 					   'finish_flg'				=> null,
 		);

		foreach ($followup_judgement_data as $key => $val){
			// 運動判定値のみ項目が違うのでkeyの判定を行う
			if ($val !== '') {
				// 運動判定値のみ項目が違うのでkeyの判定を行う
				if ($key == 'propose_exercise_value') {
					$followuplist['exercise_value'] = $val;
				}elseif ($key == 'current_weight'){
					// 現在の体重入力が無かった場合、前回の体重を登録
					if ($val === '') {
						$followuplist[$key] = $last_followup_data['current_weight'];
					}else{
						$followuplist[$key] = $val;
					}
				}else{
					$followuplist[$key] = $val;
				}
			}
		}

		// 更新条件設定
		//$where_data = array ('first_interview_id'=> $first_interview_id, 'week'=> $week, 'type' => $type);
		$where_data = array ('first_interview_id' => $first_interview_id, 'week'=> $week, 'type' => $type);
		$result = Followup::update_followup($followuplist, $where_data);

		// 対象データ更新
		//$result = $this->updateFields('t_followup' , $followuplist , $where_data);
		// if (Ethna::isError($result)) {
		// 	return $result;
		// }
        //$result = Followup::updateFollowupByFollowupId($followup_judgement_data, $week, $type, $first_interview_id, $followup_interview_id, $last_followup_data);

        /*
        $result = $this->updateFollowupByFollowupId($followup_judgement_data, $week, $type, $first_interview_id, $followup_interview_id, $last_followup_data);
        if (Ethna::isError($result)) {
            return $result;
        }
        */
		//----------------------------------
		// 終了フラグ更新
		//----------------------------------
		// finish_flg = 1の場合、全てのフォローアップ問診が完了なので、
		// 対象の問診IDの終了フラグ（finish_flg）に1を立てる
		if ($followup_judgement_data['finish_flg'] == 1) {
			$result = $this->updateFollowupFinishFlgByFirstInterviewId($first_interview_id);
			if (Ethna::isError($result)) {
				return $result;
			}
			// フォローアップフラグを'8'へ更新
			$result = $this->updateInterviewFlgFollowupByInterviewId($first_interview_id, 8);
			if (Ethna::isError($result)) {
				return $result;
			}
		}

		return $followup_interview_id;

	}

	/**
	 * フォローアップ判定
	 * Enter description here ...
	 * @param $rows						：フォローアップ問診データ
	 * @param $guidance_data			：初回問診データ
	 * @param $last_followup_data		：前回のフォローアップ問診結果
	 * @param $week						：今回のフォローアップ経過週
	 * @param $type						：今回のフォローアップ問診タイプ
	 * @param $judgement_type			：判定タイプ（0：新規判定／1：修正判定）
	 */
	public function judgeByFollowupRecord($rows, $guidance_data, $last_followup_data, $week, $type, $judgement_type) {

		//$this->logger->log(LOG_NOTICE, "record_by_choice [" . print_r($record_by_choice, true) . "]");

		//------------------------------------------------------------
		// 判定データの編集
		//------------------------------------------------------------
		// 回答内容をsection_idをkeyにして詰め替える
		foreach ($rows as $key => $val) {

			if (!empty($val['selection'])) {

				// 指導された運動をはじめましたか？／現在も運動を続けていますか？
				if ($val['selection'] == '5001' || $val['selection'] == '5004' || $val['selection'] == '5005') {
					if ($val['choice_id'] == '50001' || $val['choice_id'] == '50008' || $val['choice_id'] == '50010') {
						// はい
						$followup_result = 1;
					}elseif($val['choice_id'] == '50002' || $val['choice_id'] == '50009' || $val['choice_id'] == '50011') {
						// いいえ
						$followup_result = 0;
					}
				}
				if ($val['section_id'] == '5006' || $val['section_id'] == '5012') {
					// 運動種目の選択の場合、設定ファイルより必要な情報を取得する。（Yルート：5006／Nルート：5012）
					$sports_item = $GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'][$val['choice_id']];
				}else{

					if ($val['section_id'] == $section_check) {
						$cnt++;
					}else{
						$section_check = $val['section_id'];
						$cnt = 0;
					}
					$judgment_material[$val['section_id']][$cnt] = $val['choice_id'];
				}

			}elseif(!empty($val['input_float'])){
				if($val['choice_id']=='50070' || $val['choice_id']=='50144'){
					// 運動回数取得（Yルート：50070／Nルート：50144）
					$input_exercise_count = $val['input_float'];

				}elseif($val['choice_id']=='50071' || $val['choice_id']=='50145'){
					// 一回の時間を取得（Yルート：50071／Nルート：50145）
					$input_exercise_time = $val['input_float'];

				}elseif($val['choice_id']=='50085' || $val['choice_id']=='50146'){
					// 現在の体重
					$input_current_weight = $val['input_float'];

				}
			}
		}


		//------------------------------------------------------------
		// 判定が免除される月または問診タイプかどうかをチェック
		//------------------------------------------------------------
//		if($type == 2 || $last_followup_data['exercise_value'] == 'E' ||
		if($type == 2 ||
		(date(n, strtotime($last_followup_data['interview_date'])) == '3'  ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '4'  ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '9'  ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '10' ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '12') && $type == 4){

			// 判定が免除される月、運動強度(E)の場合、判定結果は「このまま運動を継続してください。」を固定で表示する
//			if ($type != 2 && $last_followup_data['exercise_value'] != 'E') {
			if ($type != 2) {
				$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][0]['judgment_type'];
			}
			// 判定が免除
			$special_exemption_flg = 1;

		}else{

			// 判定を行う
			$special_exemption_flg = 0;
		}


		//------------------------------------------------------------
		// 今回のフォローアップ問診を判定
		//------------------------------------------------------------

		// 指導された運動をはじめましたか？ または 現在も運動を続けていますか？

		// 判定に使用する値を取得
		if ($week >= 8) {

			if ($type >= 2 && $type <= 6) {
				// 経過週が2～6週目の場合、前回の問診で入力された体重を使用
				$weight	= $last_followup_data['current_weight'];
			}else{
				// 経過週が8週目以降は、フォローアップ問診で回答された体重を使用
				if (empty($input_current_weight)) {
					$weight	= $last_followup_data['current_weight'];
				}else{
					$weight	= $input_current_weight;
				}
			}
		}else{
			// 経過週が2～6週目の場合、初回問診で入力された体重を使用
			$weight	= $guidance_data['current_weight'];
		}

/*
		// 近日食事摂取カロリーを取得
		if ($week == 2) {
			// 経過週が2週目の場合、初回問診で算出されたカロリーを使用
			$calorie	= $guidance_data['current_calorie'];

		}else{
			// 経過週が4週目以降の場合、フォローアップ問診で提示されたカロリーを使用
			$calorie	= $last_followup_data['propose_calorie'];
		}
*/


		//--運動判定ここから（運動判定は判定が免除される月または問診タイプ2W以外はすべて実施する）--------------------------------------------------------------------------------------------------------------------------------------
		//------------------------------------------
		// 【運動】
		//------------------------------------------
//		if ($special_exemption_flg == 0) {
			// 判定材料となる質問をチェック
			foreach ($judgment_material as $key => $val) {

				if ($special_exemption_flg == 0) {

					//----------------------------------------------------
					// 続けることが難しい一番の理由は以下のどれですか？
					//----------------------------------------------------
					if($val[0] == '50005' || $val[0] == '50072'){				// 1：時間が取れない
						if($input_exercise_time >= 60){
							// 一回の時間が1時間（60分）以上の場合
							// 回答した時間（分換算）- 30分 = 新たに提示する時間
							$subtraction_value	= $GLOBALS['MAX_TIME_TO_SUBTRACT'];
						}else{
							// 一回の時間が1時間未満を入力していた場合
							// 回答した時間（分換算）- 10分 = 新たに提示する時間
							$subtraction_value	= $GLOBALS['MIN_TIME_TO_SUBTRACT'];
						}
						// 新たに提示する時間
						$propose_time = $input_exercise_time - $subtraction_value;
						if($propose_time < $subtraction_value){
							// 新たに提示する時間が減算した時間より下回った場合
							$propose_time = $subtraction_value;
						}
						$followup_judgement['propose_time']		= $propose_time;
						$followup_judgement['judgment_type']	= $GLOBALS['FOLLOWUP_JUDGE'][1]['judgment_type'];


					}elseif($val[0] == '50006' || $val[0] == '50073'){		// 2：回数が少なくなってしまう

						// 回答した運動回数 - 1回 = 新たに提示する運動回数
						$followup_judgement['propose_count'] = $input_exercise_count - $GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT'];
						if ($followup_judgement['propose_count'] < $GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT']) {
							// 新たに提示する時間が減算した時間より下回った場合
							$followup_judgement['propose_count'] = $GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT'];
						}
						$followup_judgement['judgment_type']	= $GLOBALS['FOLLOWUP_JUDGE'][2]['judgment_type'];


					}elseif ($val[0] == '50007' || $val[0] == '50074'){		// 3：運動内容がつらい

						// METS値 減算する値をセット
						$mets_subtract++;


					}elseif ($val[0] == '50080'){		// 1：かなりきついと感じる

						// METS値 減算する値をセット
						$mets_subtract++;
						$mets_subtract_flg = 1;

					}


					// 3：運動内容がつらい または 1：かなりきついと感じる 場合
					if ($mets_subtract > 0) {
						// 回答した運動種目のMETS値 - xMETS = 新たに提示する運動種目
						$followup_judgement['mets'] = $sports_item['mets'] - $mets_subtract;
						if ($followup_judgement['mets'] < $GLOBALS['MIN_METS_TO_SUBTRACT']) {
							// 新たに提示するMTES値が減算したMETS値より下回った場合、提示できる最小値を設定
							$followup_judgement['mets'] = $GLOBALS['MIN_VALUE_OF_METS'];
						}

						// 算出したMETS値から新しく提示する運動種目を取得
						foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $global_sports_key => $global_sports_val){
							if ($global_sports_val['mets'] == $followup_judgement['mets']) {
								$followup_judgement['propose_exercise_value']	= $global_sports_val['exercise_value'];		// 運動強度判定値
								break;
							}
						}
						// 新しく提示するスポーツが存在しなかった場合、
						// 対象のMETS値の運動内容が存在しなかった場合、運動強度判定の範囲内のスポーツを取得
						if (empty($followup_judgement['propose_exercise_value'])) {
							$followup_sports_by_choice_id = array_reverse($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID']);
							foreach ($followup_sports_by_choice_id as $exercise_value_key => $exercise_value_sports_val){
								if ($exercise_value_sports_val['route'] == 1 && $exercise_value_sports_val['mets'] <= $followup_judgement['mets']) {
									$followup_judgement['propose_exercise_value']	= $exercise_value_sports_val['exercise_value'];		// 運動強度判定値
									break;
								}
							}
						}

						// 診断結果内容
						$followup_judgement['judgment_type']	= $GLOBALS['FOLLOWUP_JUDGE'][3]['judgment_type'];

						// 「1：かなりきついと感じる」が選択された場合、回数、時間の判定は削除し、運動内容のみ表示を行う。
						if ($mets_subtract_flg >= 1) {
							unset($followup_judgement['propose_count']);
							unset($followup_judgement['propose_time']);
						}

					}

				}

				if ($val[0] == '50077' || $val[0] == '50078'){		// 1：腰痛・関節痛・下肢の痺れが出現した／2：胸痛・動悸・強い息切れがある
					//----------------------------------------------------
					// 体調に変化はありますか？
					//----------------------------------------------------
					// 1、2を選択した場合、体調に変化があると判断
					$bad_condition_flg = 1;
				}

//			}

			//------------------------------------------
			// 【食事】
			//------------------------------------------
			// 選択された運動に何らかの支障がある場合、カロリーを提示する
//			if ($followup_result == 0) {
			if ($followup_judgement['judgment_type'] == 1 ||  $followup_judgement['judgment_type'] == 2 || $followup_judgement['judgment_type'] == 3) {

				// 運動をしていない場合

				// 削減上限カロリーを算出
				// 初回問診で提示したカロリー（近日食事摂取カロリー）の15%が削減上限（四捨五入、切り上げを行うと15%を超えてしまう可能性があるので、小数点以下切り捨てとする）
				$upper_limit_calorie_reduction = floor($guidance_data['current_calorie'] * ($GLOBALS['UPPER_LIMIT_CALORIE_REDUCTION'] / 100));

				// 運動で再判定されている場合、食事制限として、摂取カロリーから減算するカロリー量の提示を行う。
				// ((時間(分) * 回答された運動種目のMTES値 * 1.05 * 体重) / 60) = 食事で減らすカロリー

				// 選択されたMets値から食事で減らすカロリーを算出
				$input_reduce_calorie	= floor(($input_exercise_time * $sports_item['mets'] * 1.05 * $weight) / 60);
				// 判定で出力されたMets値から食事で減らすカロリーを算出

				// 新たに運動内容（Mets値）が提示されているかチェック
				if (!empty($followup_judgement['mets'])) {
					$exercise_mets = $followup_judgement['mets'];
				}else{
					$exercise_mets = $sports_item['mets'];
				}

				// 新たに運動時間が提示されているかチェック
				if (!empty($followup_judgement['propose_time'])) {
					$exercise_time = $followup_judgement['propose_time'];
				}else{
					$exercise_time = $input_exercise_time;
				}
				$propose_reduce_calorie	= floor(($exercise_time * $exercise_mets * 1.05 * $weight) / 60);

				// 選択されたMets値から食事で減らすカロリー から 判定で出力されたMets値から食事で減らすカロリーをマイナス
				$reduce_calorie = $input_reduce_calorie - $propose_reduce_calorie;

				// 削減上限を超えているかチェック
				if ($reduce_calorie > $upper_limit_calorie_reduction) {
					// 算出された食事で減らすカロリーが削減上限を超えてしまっている場合、
					// 削減上限カロリーを減算し提示する
					$followup_judgement['propose_calorie'] = $guidance_data['current_calorie'] - $upper_limit_calorie_reduction;
				}else{
					// 算出された削減カロリーを減算し提示する
					$followup_judgement['propose_calorie'] = $guidance_data['current_calorie'] - $reduce_calorie;
				}

				// 運動判定で運動内容を再提示する必要が無かった場合（時間がとれない、回数が少なくなってしまう、運動内容がきつい以外が選択された場合）カロリーの提示のみ行う。
				if (empty($followup_judgement['judgment_type']) || $followup_judgement['judgment_type'] == '') {
					$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][5]['judgment_type'];
				}

			}else{
				// 運動をしている場合
				if(empty($followup_judgement['judgment_type']) || $followup_judgement['judgment_type'] === ''){
					// 「このまま運動を継続してください。」を設定
					$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][0]['judgment_type'];
				}
			}

			//---------------------------------------------
			// 今後の取り組みを編集
			//---------------------------------------------
			// 8W以降の場合、初回問診からの体重の増減を判定し今後の取り組みを表示
			if ($bad_condition_flg != 1 && $type >= 8 && $followup_judgement['judgment_type'] != 0) {
				if ( $input_current_weight >= ($guidance_data['current_weight'] - 1) && ($input_current_weight <= $guidance_data['current_weight'] + 1)) {
					// 現在の体重が初回問診時と比較して±1以内であった場合
					$followup_judgement['target_type']		= $GLOBALS['FOLLOWUP_TARGET'][1]['target_type'];
				}elseif (($guidance_data['current_weight'] - 1) > $input_current_weight){
					// 現在の体重が初回問診と比較して-1kg以上、減っている場合
					$followup_judgement['target_type']		= $GLOBALS['FOLLOWUP_TARGET'][0]['target_type'];
				}elseif (($guidance_data['current_weight'] + 1) < $input_current_weight){
					// 現体重が開始時より増えている場合
					$followup_judgement['target_type']		= $GLOBALS['FOLLOWUP_TARGET'][2]['target_type'];
				}
			}

		}
		//--運動判定ここまで（運動判定は判定が免除される月または問診タイプ2W以外はすべて実施する）--------------------------------------------------------------------------------------------------------------------------------------

		// 体調に変化があった場合、判定タイプを差替える
		if ($bad_condition_flg == 1) {
			// 体調に変化があった場合、これまでに算出した判定結果を差替える。
			$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][4]['judgment_type'];		// 医療機関を受診して、このまま運動を続けていいか確認しましょう。
			// 今後の取り組みを削除
			unset($followup_judgement['target_type']);
		}



		//------------------------------------------------------------
		// 前回の判定結果と比較
		//------------------------------------------------------------
		// 前回の判定結果を取得
		if (!empty($last_followup_data)) {
			$last_judgment_number = (int)substr($last_followup_data['judgment'], 1);
		}else{
			// 前回の判定結果を持っていない場合、今回が初めてのフォローアップ問診のため、初期値をセット
			$last_judgment_number = 1;
		}

		// 運動をしていない場合、前回判定値よりカウントアップ
		// ただし、2Wまたは4Wかつ初回問診日が3月、4月、9月、10月、12月の場合、判定結果の変更は無し
		if ($followup_result == 0 && $special_exemption_flg == 0) {
			$last_judgment_number++;
		}

		// 今回の判定結果をセット
		if (strlen($last_judgment_number) == 1) {
			$followup_judgement['judgment']	= 'P0' . $last_judgment_number;
		}else{
			$followup_judgement['judgment']	= 'P' . $last_judgment_number;
		}

		// 判定結果をセット
		$followup_judgement['result'] = $followup_result;


		//------------------------------------------------------------
		// 次回へ引き継ぐデータを設定
		//------------------------------------------------------------
		// 今回の判定で新たに提示されなかった項目については、前回のデータを引き継ぐ
		// ただし、以下の条件の場合、それぞれ引き継ぐデータを変更する
		// 2Wかつ2週目の場合、初回問診で提示されたデータを引き継ぐ
//		// ※運動強度が'E'判定の場合も同様
		// 2Wかつ2週目以外の場合、前回のフォローアップ問診で提示されたデータを引き継ぐ
		// ※以下で引き継いでいる項目は必須項目となり、この値を引き継がなかった場合、
		//   正常に次回問診内容が表示されないので注意

//		if ($last_followup_data['exercise_value'] == 'E' || ($type == 2 && $week != 2)) {
		if (($type == 2 && $week != 2)) {

			// 前回のデータを全て受け継ぐ

			// 入力された運動時間
			$followup_judgement['input_time']				= $last_followup_data['input_time'];
			// 入力された回数
			$followup_judgement['input_count']				= $last_followup_data['input_count'];
			// 提示METS値（Mets値 範囲のMAX値を設定）
			$followup_judgement['mets']						= $last_followup_data['mets'];
			// 運動強度判定値（A～I）
			$followup_judgement['propose_exercise_value']	= $last_followup_data['exercise_value'];

//			// 提示された運動時間
//			$followup_judgement['propose_time']		= $last_followup_data['propose_time'];
//			// 提示された回数
//			$followup_judgement['propose_count']	= $last_followup_data['propose_count'];

			// 提示カロリー
			$followup_judgement['propose_calorie']	= $last_followup_data['propose_calorie'];
			// 現在の体重
			$followup_judgement['current_weight']	= $weight;

		}else{

			if ($type == 2 && $week == 2) {

				// 入力された運動時間
				$followup_judgement['input_time']	= $guidance_data['exercise_time'] * 60;
				// 入力された回数
				$followup_judgement['input_count']	= $guidance_data['exercise_cnt'];
				// 提示METS値（Mets値 範囲のMAX値を設定）
				$followup_judgement['mets']			= $guidance_data['mets_max'];
				// 運動強度判定値（A～I）
				$followup_judgement['propose_exercise_value']	= $guidance_data['exercise_value'];
				// 提示カロリー
				$followup_judgement['propose_calorie']	= $guidance_data['current_calorie'];
				// 現在の体重
				$followup_judgement['current_weight']	= $weight;

			}else{

				// 入力値または前回の値を取得
				$followup_judgement['input_time']	= $input_exercise_time;			// 入力時間
				$followup_judgement['input_count']	= $input_exercise_count;		// 入力した運動回数

				// 入力された運動強度判定値（A～I）
				if (empty($followup_judgement['mets'])) {
					$followup_judgement['mets']	= $sports_item['mets'];
				}
				// 運動強度判定値（A～I）
				if (empty($followup_judgement['propose_exercise_value'])) {
					$followup_judgement['propose_exercise_value']	= $sports_item['exercise_value'];
				}

				// 提示カロリー
				if (empty($followup_judgement['propose_calorie']) || $followup_judgement['propose_calorie'] === '') {
					$followup_judgement['propose_calorie']	= $guidance_data['current_calorie'];
				}
				// 現在の体重
				if (empty($followup_judgement['current_weight']) || $followup_judgement['current_weight'] === '') {
					$followup_judgement['current_weight']	= $weight;
				}
			}
		}


		// フォローアップ問診終了判定
		if ($week == 48) {
			// 今回の問診が48週目の場合、フォローアップ問診を終了
			$followup_judgement['finish_flg'] = 1;
		}else{
			// 48週目以外の場合、フォローアップ問診を継続
			$followup_judgement['finish_flg'] = 0;
		}


		return $followup_judgement;

	}

	/**
	 * フォローアップ基本登録用データ
	 * Enter description here ...
	 * @param $user_data				：ユーザ情報
	 * @param $followup_judgement_data	：フォローアップ判定結果
	 * @param $followup_2W_data			：現在のフェーズの2Wのデータ
	 * @param $week						：経過週
	 * @param $type						：フォローアップ問診タイプ
	 */
//	function createFollowupInsertValue($user_data, $followup_judgement_data, $followup_2w_data, $followup_data, $week, $type)
	public function createFollowupInsertValue($user_data, $followup_judgement_data, $followup_2w_data, $week, $type)
	{
		//----------------------------------
		// 前回の判定結果と比較
		//----------------------------------
		// 問診タイプが12Wの場合
		// 最新の2Wと12Wの判定結果(judgment)を比較する
		// 変更がなかった場合	⇒次のフェーズのデータを登録する（24週から48週）の登録を行う。
		// 変更があった場合		⇒2Wから12Wを登録する。（次のフェーズへ移行する）
		if ($type == 12){
			// 今回のフォローアップ判定と2Wの判定結果を比較
			if ($followup_judgement_data['judgment'] == $followup_2w_data['judgment']) {
				// 変更が無かった場合
				$change_flg = 0;
			}else{
				// 変更があった場合
				$change_flg = 1;
			}
		}

		if ($type == 12 && $change_flg == 0){
			// 登録データ判定

			// 問診タイプが12Wかつ2Wから判定結果に変更がなかった場合
			// 24Wから48Wまでのデータを登録

			// 経過週計算用
			$week_sum = $week;
			$type_sum = $type;
			$loop_cnt = 2;			// データ登録件数

		}else{
			// 2Wから12Wまでのデータを登録
			if ($week == 2) {
				$week_sum = '';
			}else{
				$week_sum = $week;
			}
			$loop_cnt = 4;			// データ登録件数
		}

/*
		// 入力予定日の基準となる日付を取得
		if ($week == 2) {
			// 初回問診日を取得
			$plan_date = $user_data['finished_datetime'];
		}else{
			// 前回のフォローアップ問診の入力予定日を取得
			$plan_date = $followup_data['plan_date'];
		}

		for ($i = 0; $i <= $loop_cnt; $i++) {

			$insert_followup_values[$i]['first_interview_id']	= $user_data['interview_id'];			// 初回問診の問診ID
			$insert_followup_values[$i]['interview_date']		= $user_data['finished_datetime'];		// 初回問診日（t_interviewのfinished_datetimeを保存）

			// 経過週および問診タイプを算出
			if ($loop_cnt == 2) {
				// 問診タイプが24Wから48Wまでは12週間間隔
				$plan_date	= date("Y-m-d",strtotime("+12 week" ,strtotime($plan_date)));		// 入力予定日を算出
				$week_sum += 12;																// 経過週
				$type_sum += 12;																// 問診のタイプ（24,36,48）

			}else{

				if ($i <= 3) {
					// 問診タイプが2Wから8Wまでは2週間間隔
					$plan_date	= date("Y-m-d",strtotime("+2 week" ,strtotime($plan_date)));	// 入力予定日を算出
					$week_sum += 2;																// 経過週
					$type_sum += 2;																// 問診のタイプ（2,4,6,8,12,24,36,48）

				}elseif ($i == 4){
					// 問診タイプが8Wから12Wまでは4週間間隔
					$plan_date = date("Y-m-d",strtotime("+4 week" ,strtotime($plan_date)));		// 入力予定日を算出
					$week_sum += 4;																// 経過週
					$type_sum += 4;																// 問診のタイプ（2,4,6,8,12,24,36,48）
				}
			}

			$insert_followup_values[$i]['week']			= $week_sum;			// 経過週
			$insert_followup_values[$i]['type']			= $type_sum;			// 問診のタイプ
			$insert_followup_values[$i]['plan_date']	= $plan_date;			// 入力予定日

			if ($week_sum == 48) {
				// 48週目まで登録が完了したら登録データ生成完了
				break;
			}
		}
*/

		// 入力予定日の基準となる初回問診日付を取得
//		$plan_date = $user_data['finished_datetime'];


		for ($i = 0; $i <= $loop_cnt; $i++) {

			$insert_followup_values[$i]['first_interview_id']	= $user_data['interview_id'];			// 初回問診の問診ID
			$insert_followup_values[$i]['interview_date']		= $user_data['finished_datetime'];		// 初回問診日（t_interviewのfinished_datetimeを保存）


			// 経過週および問診タイプを算出
			if ($loop_cnt == 2) {
				// 問診タイプが24Wから48Wまでは12週間間隔
				$week_sum += 12;												// 経過週
				$type_sum += 12;												// 問診のタイプ（24,36,48）

			}else{

				if ($i <= 3) {
					// 問診タイプが2Wから8Wまでは2週間間隔
					$week_sum += 2;												// 経過週
					$type_sum += 2;												// 問診のタイプ（2,4,6,8,12,24,36,48）

				}elseif ($i == 4){
					// 問診タイプが8Wから12Wまでは4週間間隔
					$week_sum += 4;												// 経過週
					$type_sum += 4;												// 問診のタイプ（2,4,6,8,12,24,36,48）
				}
			}

			$insert_followup_values[$i]['week']			= $week_sum;			// 経過週
			$insert_followup_values[$i]['type']			= $type_sum;			// 問診のタイプ
			$insert_followup_values[$i]['plan_date']	= date("Y-m-d",strtotime("+$week_sum week" ,strtotime($user_data['finished_datetime'])));			// 入力予定日

			if ($week_sum == 48) {
				// 48週目まで登録が完了したら登録データ生成完了
				break;
			}
		}

		return $insert_followup_values;
	}

	/**
	 * フォローアップ回答データ登録用データ作成
	 * Enter description here ...
	 * @param $choice_id				：選択肢ID
	 * @param $choice_list				：フォローアップ設問の全選択肢取得
	 * @param $upper_id_list			：選択された時に表示する内容（判断元選択肢のID）
	 * @param $selected_choice_id_list	：フォローアップ問診・回答データ
	 * @param $value
	 */
	public function createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list, $value = "")
	{
		$choice_data	= $choice_list[$choice_id];						// 選択された内容
		$choice_type	= $choice_data['choice_type'];					// 選択肢のタイプ（radio等）
		$upper			= $upper_id_list[$choice_data['layer_id']];		// どの選択肢を選択したら表示される設問なのか

		if (!empty($upper)) { //子選択肢

			// 判断元の選択肢が選択されているかチェック（show_type［0:親選択が未選択の場合true。1:親選択肢が選択されている場合true］）
			$isParentSelected = $this->isSelectedUpper($selected_choice_id_list, $upper);
			if ($upper['show_type'] ==  0) {
				if ($isParentSelected) {
					return null;
				}
			} else {
				if (!$isParentSelected) {
					return null;
				}
			}
			if ($choice_type !== "radio" && $choice_type !== "checkbox") {
				if ($value === "" || is_null($value)) {
					return null;
				}
			}
		}


		$insert_values = array('choice_id' => $choice_id);

		// section_idを保存
		$insert_values['section_id'] = $choice_data['section_id'];
		switch ($choice_type) {
			case 'radio':
			case 'checkbox':
				$insert_values['selection'] = true;
				break;
			case 'string':
				$insert_values['input_string'] = $value;
				break;
			case 'numeric':
				$insert_values['input_float'] = $value;
				break;
			case 'date':
				$insert_values['input_datetime'] = $value . ' 00:00:00';
				break;
			case 'time':
				$insert_values['input_datetime'] = '1999-01-01 ' . $value;
				break;
			case 'datetime':
				$insert_values['input_datetime'] = $value;
				break;
			case 'year':
				$insert_values['input_datetime'] = $value . '-01-01 00:00:00';
				break;
			case 'month':
				$insert_values['input_datetime'] = '1999-' . $value . '-01 00:00:00';
				break;
			case 'day':
				$insert_values['input_datetime'] = '1999-01-' . $value . ' 00:00:00';
				break;
			case 'slider':
			case 'slider_hour':
			case 'slider_show_by_step':
			case 'slider_minute':
				$insert_values['input_float'] = $value;
				break;
			case 'tab':
			case 'accordion': //見出し
				return null;
		}
		return $insert_values;

	}

	/**
	 * 判断元の選択肢が選択されているかチェック
	 * Enter description here ...
	 * @param $selected_choice_id_list	：フォローアップ問診・回答データ
	 * @param $upper					：どの選択肢を選択したら表示される設問なのか
	 */
	public function isSelectedUpper($selected_choice_id_list, $upper) {
		$all_or_every = $upper['all_or_every'];
		$group = $upper['group'];

		$isSatisfy = false;
		switch ($all_or_every) {
			case "all":
				$isSatisfy = true;
				foreach ($group as $group_no => $upper_list) {
					foreach ($upper_list as $upper_data) {
						if (!in_array($upper_data['choice_id_upper'], $selected_choice_id_list)) {
							$isSatisfy = false;
							break;
						}
					}
				}
				break;
			case "every":
				$isSatisfy = false;
				foreach ($group as $group_no => $upper_list) {
					foreach ($upper_list as $upper_data) {
						if (in_array($upper_data['choice_id_upper'], $selected_choice_id_list)) {
							$isSatisfy = true;
							break;
						}
					}
				}
				break;
			default:
				break;
		}

		return $isSatisfy;
	}

	// フォローアップ問診の回答を取得
	public function choiceByFollowupInterviewId($choice_selection_data, $section_set_id) {

		// 選択したデータから質問内容と回答を取得
		// SQL文の編集
		foreach ($choice_selection_data as $key => $val){
			if (empty($choice_list)) {
				$choice_list  = $val['choice_id'];
			}else{
				$choice_list .= ',' . $val['choice_id'];
			}
		}
		$choice_section_data = Followup::getSectionChoiceByChoiceIdSectionSetId($choice_list, $section_set_id);
		// if (Ethna::isError($choice_section_data)) {
		// 	return $choice_section_data;
		// }

		// 質問と回答結果の編集
		foreach ($choice_section_data as $key => $val){

			if ($val['section_id'] == '5012' || $val['section_id'] == '5006') {
				// どんな運動をどの程度していますか？
				if ($val['choice_id'] != '50144' && $val['choice_id'] != '50145' && $val['choice_id'] != '50070' && $val['choice_id'] != '50071') {
					$choice_detail = $val['choice_string'] . 'を週に' ;
				}elseif ($val['choice_id'] == '50144' || $val['choice_id'] == '50070'){
					$choice_detail .= $choice_selection_data[$val['choice_id']]['input_float'] . '回、' ;
				}elseif ($val['choice_id'] == '50145' || $val['choice_id'] == '50071'){
					$choice_detail .= floor($choice_selection_data[$val['choice_id']]['input_float'] / 60) . '時間' . $choice_selection_data[$val['choice_id']]['input_float'] % 60 . '分';
					$choice_interview_list[$key]['section_string']	= $val['section_string'];
					$choice_interview_list[$key]['choice_string']	= $choice_detail;
				}

			}else{
				$choice_interview_list[$key]['section_string']	= $val['section_string'];
				if (!empty($val['choice_string'])) {
					$choice_interview_list[$key]['choice_string']	= $val['choice_string'];
				}else{
					$choice_interview_list[$key]['choice_string']	= $choice_selection_data[$val['choice_id']]['input_float'] . 'Kg';
				}
			}
		}
		return $choice_interview_list;
	}

	// フォローアップ問診の回答を取得
	public function judgeDisplayByFollowupId($followup_data) {

		// 判定結果を編集

		$judgment['judgment_type']	= $followup_data['judgment_type'];	$judgment_detail			= $GLOBALS['FOLLOWUP_JUDGE'][$followup_data['judgment_type']]['judgment_detail'];

		switch ($followup_data['judgment_type']) {
			case 0:		// このまま運動を継続してください。
			case 4:		// 医療機関を受診して、このまま運動を続けていいか確認しましょう。
				$judgment['judgment_detail'] = $judgment_detail;
				break;

			case 1:		// 運動の時間をCHOISE_TIMEからPROPOSE_TIMEに減らして取り組んで見てください。

				//----------------------------------
				// 時間の編集
				//----------------------------------

				// 診断結果編集
				$input_hour		= floor($followup_data['input_time'] / 60);
				$input_minutes	= $followup_data['input_time'] % 60;
				$input_time = $input_hour . '時間' . $input_minutes . '分';

				// 診断結果編集
				$propose_hour		= floor($followup_data['propose_time'] / 60);
				$propose_minutes	= $followup_data['propose_time'] % 60;
				$propose_time = $propose_hour . '時間' . $propose_minutes . '分';

				// 診断結果の時間を置換
				$search		= array('CHOISE_TIME', 'PROPOSE_TIME');
				$replace	= array($input_time, $propose_time);
				$judgment['judgment_detail'] = str_replace($search, $replace, $judgment_detail);

				break;

			case 2:		// 週にPROPOSE_COUNT回、運動に取り組むようにしましょう。
				//---------------------------------------------
				// 診断結果を編集
				//---------------------------------------------
				// 診断結果内容
				$judgment['judgment_detail'] = str_replace('PROPOSE_COUNT', $followup_data['propose_count'], $judgment_detail);
				break;

			case 3:		// 現在の生活改善を目指す場合、PROPOSE_SPORTSのような運動に取り組んでみてください。

				// METS値から新しく提示する運動種目を取得
				foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $key => $val){
					if ($val['route'] == 1 && $val['mets'] == $followup_data['mets']) {
						if (empty($sports)) {
							$sports .= $val['sports_name'];
						}else{
							$sports .= '、' . $val['sports_name'];
						}
					}
				}
				// 新しく提示するスポーツが存在しなかった場合、
				// 対象のMETS値の運動内容が存在しなかった場合、運動強度判定の範囲内のスポーツを取得
				if (empty($sports)) {
					foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $key => $val){
						if ($val['route'] == 1 && $val['exercise_value'] == $followup_data['exercise_value']) {
							$followup_sports_exercise_value_list[$key] = $val;
						}
					}
				}
				foreach ($followup_sports_exercise_value_list as $key => $val){
					if ($val['mets'] <= $followup_data['mets']) {
						if (empty($sports)) {
							$sports .= $val['sports_name'];
						}else{
							$sports .= '、' . $val['sports_name'];
						}
					}
				}

				// 診断結果内容
				$judgment['judgment_detail'] = str_replace('PROPOSE_SPORTS', $sports, $judgment_detail);

				break;

		}

		if ($followup_data['judgment_type'] != 0 && $followup_data['judgment_type'] != 4) {
			// あわせて、食事のカロリーをPROPOSE_CALORIEkcalにするようにチャレンジしてみてください。
			$judgment_detail = $GLOBALS['FOLLOWUP_JUDGE_CALOROE'][0]['judgment_detail'];
			$judgment['judgment_calorie'] = str_replace('PROPOSE_CALORIE', $followup_data['propose_calorie'], $judgment_detail);
		}

		if ($followup_data['target_type'] != '') {
			// 今後の取り組み目標
			$judgment['target_detail'] = $GLOBALS['FOLLOWUP_TARGET'][$followup_data['target_type']]['target_detail'];
		}

		return $judgment;

	}

	public function followupType($week)
	{
		$arrayName = array('51' => '2', '52' => '4', '53' => '6', '54' => '8', '55' => '12', '56' => '24', '57' => '36', '58' => '48');
		foreach ($arrayName as $key => $value) {
			if($week == $value) return $key;
		}
		// section_set_id => week
	}


}
