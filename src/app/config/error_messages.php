<?php
return array(
    /*
	  //users - TUser model
	  
   		"login_id.alphanum_under" => "The login id must contain only alpha numeric characters and underscore character.",
   	 	"login_id.required" => 'The login id is required.',
   	 	"login_id.min" => 'The login id must be at least 4 characters.',
		"login_id.max" => 'The login id must have less than 45 characters.',
		// note : currently in Laravel there is no placeholder available that can be put here to show the value of the login_id in the message, therefore
		// this unique message was created (in Japanase) in the code TUserController to show the value of the login_id in the message as requested.
   	 	"login_id.unique" => 'The login id has already been taken.' ,
   
	   	"nick_name.required" => 'The nick name is required.' ,

	   	"password.required" => 'The password is required.',
	   	"password.confirmed" => "The password confirmation does not match.",
	   	"password.halfwidth" => " The password must be half width characters and have length between 4 and 12 characters." ,
   		"password.min" => " The password must be half width characters and have length between 4 and 12 characters." ,
    	"password.max" => " The password must be half width characters and have length between 4 and 12 characters. ",
			  
		"password.csvpasscheck" => "The password should not be the same as the login id.",
			   
		"tenant_id.required" => "The tenant id is required.",
		
		"tenant_id.csvtenantcheck" => "The tenant id does not match the logged in tenant.",
		
		"email.email" => "The email is not a valid email address.",
		"email.unique" => "The email has already been taken.",
		
	
	   	"newpassword.confirmed" => 'パスワードが一致しません',
	   	"newpassword.required" => "半角英数字4〜12文字で入力してください",
	   	"newpassword.halfwidth" => "半角英数字4〜12文字で入力してください",
	   	"newpassword.min" => '半角英数字4〜12文字で入力してください',
	   	"newpassword.max" => '半角英数字4〜12文字で入力してください',
		
		"last_name_kana.hiragana_katakana" => "Last name kana must contain only Hiragana/Katakana characters.",
		"first_name_kana.hiragana_katakana" => "First name kana must contain only Hiragana/Katakana characters.",
		
	 //accounts   
	 	"login_name.required" => 'Login name is required.',
	 	"login_name.alphanum_under" => 'The login name must contain only alpha numeric characters and underscore character.' ,
	 	"login_name.min" => 'The login name must be at least 4 characters.',
	 	"login_name.unique" => "The login name has already been taken.",
		   
		   		//password field is described similar to TUser
		   
	// Tenant
		   
   		"token.alpha_num" => "Token must have only alpha numeric characters.",
   		"token.required" => "Token is required.",
   		"token.min" => "Token must be half width characters and have minimum 4 characters and maximum 12 characters.",
   		"token.max" => "Token must be half width characters and have minimum 4 characters and maximum 12 characters.",
   		"token.unique" => "The token has already been taken.",
		"token.halfwidth" => "Token must be half width characters and have minimum 4 characters and maximum 12 characters.",
		
   		"tenant_name.required" => "The tenant name is required.",
		"tenant_name.max" => " The tenant name must be less than 50 characters." ,
		
	    'followup_sender_address.email' => '通知メール　差出人メールアドレスを正しく入力してください',
	  	'followup_sender_address.required' => '通知メール　差出人メールアドレスを正しく入力してください',
		'followup_subject.required' => 'Followup subject is required.',
		'followup_sender_name.required' => 'Followup sender name is required.',
		'followup_body.required' => 'Followup body is required.',
		
	  	'remind_sender_address.email' => 'リマインドメール　差出人メールアドレスを正しく入力してください',
	  	'remind_sender_address.required' => 'リマインドメール　差出人メールアドレスを正しく入力してください',
		'remind_subject.required' => 'Remind subject is required.',
		'remind_sender_name.required' => 'Remind sender name is required.',
		'remind_body.required' => 'Remind body is required.'
		*/


	  //users - TUser model

   		"login_id.alphanum_under" => "ログインIDは半角英数字で入力してください。",
   	 	"login_id.required" => 'ログインIDを入力してください。',
   	 	"login_id.min" => 'ログインIDは半角英数字4〜12文字で入力してください。',
		"login_id.max" => 'ログインIDは半角英数字4〜12文字で入力してください。',
		// note : currently in Laravel there is no placeholder available that can be put here to show the value of the login_id in the message, therefore
		// this unique message was created (in Japanase) in the code TUserController to show the value of the login_id in the message as requested.
   	 	"login_id.unique" => 'このログインIDは既に登録されています。' ,

	   	"nick_name.required" => 'ニックネームを入力してください。' ,

	   	"password.required" => 'パスワードを入力してください。',
	   	"password.confirmed" => "パスワードが一致しません。",
	   	"password.halfwidth" => "パスワードは半角英数字4〜12文字で入力してください。" ,
   		"password.min" => "パスワードは半角英数字4〜12文字で入力してください。" ,
    	"password.max" => "パスワードは半角英数字4〜12文字で入力してください。 ",

		"password.csvpasscheck" => "ログインIDまたはパスワードが間違っています。",

		"tenant_id.required" => "テナントIDを入力してください。",

		"tenant_id.csvtenantcheck" => "登録しようとしているテナントIDが現在のテナントと一致しません。",

		"email.email" => "メールアドレスの形式ではありません。",
		"email.unique" => "このメールアドレスは既に登録されています。",


	   	"newpassword.confirmed" => 'パスワードが一致しません。',
	   	"newpassword.required" => "パスワードは半角英数字4〜12文字で入力してください。",
	   	"newpassword.halfwidth" => "パスワードは半角英数字4〜12文字で入力してください。",
	   	"newpassword.min" => 'パスワードは半角英数字4〜12文字で入力してください。',
	   	"newpassword.max" => 'パスワードは半角英数字4〜12文字で入力してください。',

		"last_name_kana.hiragana_katakana" => "姓カナはカタカナで入力してください。",
		"first_name_kana.hiragana_katakana" => "名カナはカタカナで入力してください。",

	 //accounts
        "login_name.required" => 'ログインIDを入力してください。',
        "name.required" => '氏名を入力してください。',
        "name_kana.required" => '氏名かなを入力してください。',
        "login_name.alphanum_under" => "ログインIDは半角英数字4〜12文字で入力してください。" ,
        "login_name.min" => "ログインIDは半角英数字4〜12文字で入力してください。" ,
	 	"login_name.max" => 'ログインIDは半角英数字4〜12文字で入力してください。',
	 	"login_name.unique" => "このログインIDは既に登録されています。",
        "name.max" => '氏名は45文字以内で入力してください。',
        "name_kana.max" => '氏名かなは45文字以内で入力してください。',

		   		//password field is described similar to TUser

	// Tenant

   		"token.alpha_num" => "テナントトークンは半角英数字4〜12文字で入力してください。。",
   		"token.required" => "テナントトークンを入力してください。",
   		"token.min" => "テナントトークンは半角英数字4〜12文字で入力してください。",
   		"token.max" => "テナントトークンは半角英数字4〜12文字で入力してください。",
   		"token.unique" => "このテナントトークンは既に登録されています。",
		"token.halfwidth" => "テナントトークンは半角英数字4〜12文字で入力してください。",

   		"tenant_name.required" => "テナント名を入力してください。",
		"tenant_name.max" => "テナント名は50文字以内で入力してください。" ,

	    'followup_sender_address.email' => '通知メール　差出人メールアドレスを正しく入力してください。',
	  	'followup_sender_address.required' => '通知メール　差出人メールアドレスを正しく入力してください。',
		'followup_subject.required' => 'メールタイトルを入力して下さい。',
		'followup_sender_name.required' => '差出人名を入力してください。',
		'followup_body.required' => 'メール内容を入力してください。',

	  	'remind_sender_address.email' => 'リマインドメール　差出人メールアドレスを正しく入力してください。',
	  	'remind_sender_address.required' => 'リマインドメール　差出人メールアドレスを正しく入力してください。',
		'remind_subject.required' => 'メールタイトルを入力して下さい。',
		'remind_sender_name.required' => '差出人名を入力してください。',
		'remind_body.required' => 'メール内容を入力してください。'
  );
