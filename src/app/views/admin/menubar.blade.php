<style type="text/css">
    #interview {
    }
</style>
<div id="header">
	<div class="header-inner" id="top">
	<h1 id="logo"><a id="topLink"><img src="{{ URL::to('/') }}/img/admin/common/logo.gif" height="30" alt="サービスロゴ" /></a></h1>
		<div id="btn_logout">
			管理者&nbsp;：&nbsp;{{ Auth::user()->get()->name }}さん<br />
			<a href="{{  url('admin/logout')  }}">ログアウト</a>
		</div>
	</div>

        <span><?php if (\Session::has('temp_tenant_id')){
                echo "id ".\Session::get('temp_tenant_id')."&nbsp;|&nbsp;";
                $tenant = DB::table("m_tenant")->where('tenant_id',\Session::get('temp_tenant_id'))->first();
                echo "".@$tenant["tenant_name"];
            }?></span>
	<div id="menuArea">
		<ul class="header-inner" id="menu">
			<li id="btn_top"><a href="{{ URL::to('/') }}/admin"><span>管理画面トップ</span></a></li>
			
		<?php if (Auth::user()->get()->role_id == 0) { ?>
			<li class="menu" onMouseOver="this.className='menu_on'" onMouseOut="this.className='menu'">
                <a href="/admin#tenant_">テナント管理</a>
				<ul>
					<li><a href="{{ URL::to('/') }}/admin/tenantlist">テナント一覧</a></li>
					<li><a href="{{ URL::to('/') }}/admin/registertenant">新規作成</a></li>
				</ul>
			</li>
		<?php } 
		  if ( ! ( (Auth::user()->get()->role_id == 0)  && (!Session::has('temp_tenant_id') ) ) ) {
			
		 ?>	
			  

			<li class="menu" onMouseOver="this.className='menu_on'" onMouseOut="this.className='menu'">
                <a href="/admin#interview_">問診結果管理</a>
				<ul>
					<li><a href="{{ URL::to('/') }}/admin/interview">問診結果</a></li>
				</ul>
			</li>

			<li class="menu" onMouseOver="this.className='menu_on'" onMouseOut="this.className='menu'">
                <a href="/admin#print_">印刷履歴管理</a>
				<ul>
					<li><a href="{{ URL::to('/') }}/admin/history">印刷履歴</a></li>
				</ul>
			</li>
			<?php if ((Auth::user()->get()->role_id == 1) || ((\Auth::user()->get()->role_id ==0)&&(\Session::has('temp_tenant_id')))  ){ ?>
			<li class="menu" onMouseOver="this.className='menu_on'" onMouseOut="this.className='menu'">
                <a href="/admin#account_">アカウント管理</a>
				<ul>
					<li><a href="{{ URL::to('/') }}/admin/accountlist">アカウント一覧</a></li>
					<li><a href="{{ URL::to('/') }}/admin/register">新規登録</a></li>
				</ul>
			</li>

			<li class="menu" onMouseOver="this.className='menu_on'" onMouseOut="this.className='menu'">
                <a href="/admin#mail_">メール内容管理</a>
				<ul>
					<li><a href="{{ URL::to('/') }}/admin/mailedit">メール内容</a></li>
				</ul>
			</li>
			<li class="menu" onMouseOver="this.className='menu_on'" onMouseOut="this.className='menu'">
                <a href="/admin#user_">ユーザー管理</a>
				<ul>
					<li><a href="{{ URL::to('/') }}/user/userlist">ユーザー一覧</a></li>
					<li><a href="{{ URL::to('/') }}/user/registeruser">新規登録</a></li>
					<li><a href="{{ URL::to('/') }}/user/csvuserreference">CSVユーザー一括登録</a></li>
				</ul>
			</li>
			
			<?php } ?>
		
		<?php } ?>
		</ul>
	</div><!-- /#menuArea -->
</div><!-- /#header -->
