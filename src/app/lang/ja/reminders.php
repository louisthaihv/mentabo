<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "パスワードは6文字以上の半角英数字で正しく入力してください。",

	"user" => "指定のメールアドレスは登録されていません。",

	"token" => "トークンが無効です。",

	"sent" => "パスワードリマインダーを送信しました。",

);
