
	<div class="push"></div>
</div><!-- /#wrap -->

<!-- ↓↓↓ footer  ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="footer">
<div id="footerInner">
	<ul>
		<li><a href="#">マニュアル</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
		<li><a href="#">FAQ</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
		<li><a href="#">お問い合わせ</a></li>
	</ul>
	<ul>
		<!--{assign var="url" value="`$config.static_absolute_url`?action_static_company=true"}-->
		<li><a href="#">運営会社</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
		<li><a href="#">個人情報の取扱いについて</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
		<li><a href="#">利用環境</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
		<li><a href="#">利用規約</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
		<li><a href="#">サイトマップ</a></li>
	</ul>
	<p id="copyright">Copyright(C) メディカルインテリジェンス株式会社. All Rights Reserved.</p>
</div><!-- /#footerInner -->
</div><!-- /#footer -->
<!-- ↑↑↑ footer  ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->

</body>
</html>
