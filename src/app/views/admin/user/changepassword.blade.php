@include('../../header')

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2>パスワード変更</h2>
	<div id="contentInner">
		
	
	<div id="main">
		
		
		<!-- <img src="{{ URL::to('/') }}/img/icon_required.png"/> <font color=red>*</font> は必須項目です。-->
		
		<form  action="edit_password" method="post">
			<?php
			
				if ($error!="") {
					echo "<li>".$error."</li>";
				}
				
			?>

            @if($errors->all('message'))
            <ul class="valid_error">
                @foreach($errors->all('<li>:message</li>') as $message)
                {{ $message }}
                @endforeach
            </ul>
            @endif
				<br/> <br/>
		
					<div>
						<h3 class="top">旧パスワード</h3>
						<img src="{{ URL::to('/') }}/img/icon_required.png"/> 変更前のパスワードを入力してください。<br/><br/>
						<dd><input type="password" name="password" value="" size= 60 /></dd><br/>
					</div>
			
					<div>
						<h3 class="top">新パスワード</h3>
						<img src="{{ URL::to('/') }}/img/icon_required.png"/> 新たに設定したいパスワードを入力してください。※半角英数字４−１２文字で設定してください。
						<dd><input type="password" name="newpassword" value="" size=60 /></dd><br/>
					</div>
			
					<div>
						<h3 class="top">新パスワード（確認）</h3>
						<img src="{{ URL::to('/') }}/img/icon_required.png"/> 新たに設定したいパスワードをもう一度入力してください。※半角英数字４−１２文字で設定してください。<br/><br/>
						<dd><input type="password" name="newpassword_confirmation" value="" size=60 /></dd><br/>
					</div>
			

				<br/>
				
				
				
					<div>
						<center><input type="submit" name="action_user_changepass" value="保存"></center>
					</div>
			
		</form>
		
		
	</div><!-- /#main -->
	

  </div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->



	<hr />
@include('../../footer')
