<?php

// Destinations
define('ADMIN_EMAIL', '');
define('LOG_FILE', app_path().'/logs/'.date('Y_m_d').'.log');
 
// Destination types
define('DEST_EMAIL', '1');
define('DEST_LOGFILE', '3');
 
function my_error_handler($errno, $errstr, $errfile, $errline)
{  
	switch ($errno) {
	case E_USER_ERROR:
	  // Send an e-mail to the administrator
	  error_log("Error: $errstr Fatal error on line $errline in file $errfile \n", DEST_EMAIL, ADMIN_EMAIL);

	  // Write the error to our log file
	  error_log("Error: $errstr Fatal error on line $errline in file $errfile \n", DEST_LOGFILE, LOG_FILE);
	  break;

	case E_USER_WARNING:
	  // Write the error to our log file
	  error_log("Warning: $errstr in $errfile on line $errline \n", DEST_LOGFILE, LOG_FILE);
	  break;

	case E_USER_NOTICE:
	  // Write the error to our log file
	  error_log("Notice: $errstr in $errfile on line $errline \n", DEST_LOGFILE, LOG_FILE);
	  break;

	default:
	  // Write the error to our log file
	  error_log("Unknown error [#$errno]: $errstr in $errfile on line $errline \n", DEST_LOGFILE, LOG_FILE);
	  break;
	}

	// Don't execute PHP's internal error handler
	return TRUE;
}
 
// Use set_error_handler() to tell PHP to use our method
$old_error_handler = set_error_handler("my_error_handler");