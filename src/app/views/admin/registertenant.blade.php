@include('admin/header')

<hr />
<div id="wrapper">
<div id="accountEntry" class="contents">
	<h2 id="pageTitle">新規作成</h2>
	<hr />

	<div class="buttonArea">
		<ul>
			<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
		</ul>
	</div>

	
	<div id="main">
		
			<P><span class="required">＊</span>は必須項目です。</P>

			<form  action="registertenant" method="post" enctype="multipart/form-data" >
                @if($errors->all('message'))
                    <ul class="valid_error">
                    @foreach($errors->all('<li>:message</li>') as $message)
                    {{ $message }}
                    @endforeach
                    </ul>
                @endif
				<div class="table-wrap">
					<table summary="アカウント登録">
												<tr>
							<th><span class="required">＊</span>テナント名</th>
							<td><input type="text" name="tenant_name" value="{{ $tenant->tenant_name }}" maxlength="50" /></td>
						</tr>
						<tr>
							<th>ロゴ画像</th>
							<td>
 								<script type="text/javascript">
								function HandleBrowseClick()
								{
								    var fileinput = document.getElementById("file1");
								    fileinput.click();
								}
 								function showFileName() {
 								   var t = document.getElementById("text_box1");
								   var f = document.getElementById("file1");
 								   t.value = f.value;
 								}
 								</script>
								 <input id ="file1" type="file" name="uploaded_file" style="display:none" accept="image/*" onchange="showFileName()" >
								 <input type="button" value="アップロード" id="fakeBrowse" onclick="HandleBrowseClick();"/>
								 <br/><input type="text" id="text_box1" name"text_box" value=""  readonly >

							</td>
						</tr>
			
						<tr>
							<th><span class="required">＊</span>テナントトークン</th>
							<td><input type="text" name="token" value="{{ $tenant->token }}" maxlength="12" /></td>
						</tr>

						<tr>
							<th><span class="required">＊</span>有効／無効</th>
							<td><select name="valid">
<option value="1" <?php if ($tenant->valid=="1") echo 'selected="selected"';?> >有効</option>
<option value="0" <?php if ($tenant->valid=="0") echo 'selected="selected"';?> >無効</option>
</select></td>
						</tr>
			
						<tr>
							<th colspan="2"  class="btn"><input type="submit" name="action_admin_tenant_save" value="保存"></th>
						</tr>
					</table>
					<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
				</div>
			</form>

		
	</div><!-- /#main -->

</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
