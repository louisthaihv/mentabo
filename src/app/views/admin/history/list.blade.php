<!--{include file="admin/header.tpl"}-->

<div id="wrapper">
<div id="printHistory" class="contents">
	<h2 id="pageTitle">印刷履歴一覧</h2>
	<hr />

	<!--{include file="admin/error.tpl"}-->

	<div id="main">

		<div class="embossLine search-condition">
			<form  action="" method="post">
				<dl>
					<dt>印刷日時</dt>
					<dd>
						{{ Form::selectRange('date_y_f', 2011, date('Y'), $input['date_y_f']) }}年
						{{ Form::selectRange('date_m_f', 1, 12, $input['date_m_f']) }}月
						{{ Form::selectRange('date_d_f', 1, 31, $input['date_d_f']) }}日
						{{ Form::selectRange('date_h_f', 0, 24, $input['date_h_f']) }}時
						{{ Form::selectRange('date_i_f', 0, 59, $input['date_i_f']) }}分
						～
						{{ Form::selectRange('date_y_t', 2011, date('Y'), $input['date_y_t']?:date('Y')) }}年
						{{ Form::selectRange('date_m_t', 1, 12, $input['date_m_t']?:date('m')) }}月
						{{ Form::selectRange('date_d_t', 1, 31, $input['date_d_t']?:date('d')) }}日
						{{ Form::selectRange('date_h_t', 0, 24, $input['date_h_t']?:23) }}時
						{{ Form::selectRange('date_i_t', 0, 59, $input['date_i_t']?:59) }}分
					</dd>
					<dt>印刷者名</dt>
					<dd>{{ Form::text('name', $input['name']) }}（前方一致）</dd>
					<dt>ニックネーム</dt>
					<dd>{{ Form::text('nick_name', $input['nick_name']) }}（前方一致)</dd>
					<dt>種類</dt>
					<dd>
						<label for="view_type_1">{{ Form::checkbox('view_type[]', 'interview', $data['input_view_type_1'],array('id'=>'view_type_1')) }}問診</label>
						<label for="view_type_2">{{ Form::checkbox('view_type[]', 'followup', $data['input_view_type_2'],array('id'=>'view_type_2')) }}フォローアップ</label>
						<label for="view_type_3">{{ Form::checkbox('view_type[]', 'admin', $data['input_view_type_3'],array('id'=>'view_type_3')) }}管理（問診）</label>
						<label for="view_type_4">{{ Form::checkbox('view_type[]', 'admin_followup', $data['input_view_type_4'],array('id'=>'view_type_4')) }}管理（フォローアップ）</label>
					</dd>
				</dl>
				<input type="hidden" name="type" value="search">
				<p><input type="submit" name="action_admin_history_print_list" value="検索" class="input-btn"></p>
			</form>
		</div><!-- /.embossLine -->
		
		<div>データ数：{{ count($history_list) }}</div>
		<div class="page">{{ $history_list->links() }}</div>
		<br>

		<div class="table-wrap">
			<table summary="問診結果一覧">
				<thead>
					<tr>
						<th>印刷日時</th>
						<th>印刷者アカウントID</th>
						<th>印刷者名</th>
						<th>ユーザーID</th>
						<th>経過週</th>
						<th>ニックネーム</th>
						<th>画面</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>印刷日時</th>
						<th>印刷者アカウントID</th>
						<th>印刷者名</th>
						<th>ユーザーID</th>
						<th>経過週</th>
						<th>ニックネーム</th>
						<th>画面</th>
					</tr>
				</tfoot>
				<tbody>

				@if($history_list)
					@foreach($history_list as $key=>$data)
						<tr>
							<td class="column1">{{ $data['create_datetime'] }}</td>
							<td class="column2">{{ $data['account_id'] }}</td>
							<td class="column3">{{ $data['name'] }}</td>
							<td class="column4">{{ $data['interview_id'] }}</td>
							<td class="column5">{{ $week_array[$key]['week'] }}</td>
							<td class="column6">{{ $data['nick_name'] }}</td>
							<td class="column7">{{ $data['view_type'] }}</td>
						</tr>
					@endforeach	
				@else
					<tr>
						<td colspan="5">データがありません</td>
					</tr>
				@endif	

				</tbody>
			</table>
			<div class="btm"><img src="{{ URL::to('/') }}img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>

		<br>
		<div>データ数：{{ count($history_list) }}</div>
		<div class="page">{{ $history_list->links() }}</div>
		

	</div><!-- /#main -->

</div><!-- /.contents -->
</div><!-- /#wrapper-->

<!--{include file="admin/footer.tpl"}-->
