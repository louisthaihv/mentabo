<div id="wrapper">
<div id="interviewDetail" class="contents">
	<h2 id="pageTitle">問診結果詳細</h2>
	<hr />

	<div class="buttonArea">
		<ul>
			<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
		</ul>
	</div>

	<!--{include file="admin/error.tpl"}-->
	<!--{if 1 == 1}-->
	<div id="main">

		@if($isExist)
		<div id="popup-dialog">
			<h3 class="title">問診結果編集<a href="#" class="close">&times;</a></h3>
			<div class="isi-dialog">
				<div id="loading" ><img src="{{ URL::to('/') }}/img/admin/common/loading.gif" width="50" height="50"/></div>
				<div id="iframeContainer"></div>
				<div class="button-wrapper"><button class="close">Close</button></div>
			</div>
		</div>
		<div id="dialog-overlay"></div>
		<div class="embossLine">
			<dl class="interviewData">
				<dt>問診ID</dt>
				<dd>{{ $interview_data['interview_id'] }}</dd>
				<dt>問診タイプ</dt>
				<dd>{{ $interview_data['section_set_name'] }}</dd>
				<dt>問診日時</dt>
				<dd>{{ $interview_data['start_datetime'] }} ～ {{ $interview_data['finished_datetime'] }}</dd>
			</dl>
		</div>

		<div class="embossLine">
			<dl class="interviewData">
				<dt>氏名</dt>
				<dd>{{ $interview_data['first_name'] }}{{ $interview_data['last_name'] }}</dd>
				<dt>生年月日</dt>
				<dd>{{ $interview_data['birth_date'] }}（問診時年齢 {{ $interview_data['years_old'] }}歳）</dd>
				<dt>性別</dt>
				<dd>{{{ isset($interview_data['gender']) ? '男性' : '女性' }}}</dd>
				@if($interview_data['flg_followup'] == 1)
				<dt>Eメール</dt>
				<dd> {{ $interview_data['email'] }}</dd>
				@endif
			</dl>
		</div>

		<div class="doctorView">
			@if($data_doctorView)
				<div class="table-wrap">
					<table summary="問診結果詳細">
					@foreach ($data_doctorView as $key => $item)
						<tr>
							<th>{{ $item['section_string'] }}</th>
							<?php 
								if(isset($resulteditFlg)) $resulteditFlg = true;
								else $resulteditFlg = false;
							 ?>
							<?php if ($followup_list) {?>
							<th class="edit notprint-wrap"><img src="{{ URL::to('/') }}/img/admin/common/btn_edit.png" class="choice" id="para_{{ $interview_data['interview_id'] }}_{{ $item['section_id'] }}_{{ $interview_data['user_id'] }}"></th>
							<?php } ?>
							<td>{{ $data_doctorView[$key]['answer'] }}</td>
						</tr>
					@endforeach
					</table>
					<div class="btm"><img src="{{ URL::to('/') }}/img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
				</div>
			@endif	
		</div>

		@if(@resultFlg  == true)
		<div class="patientView">
			<div id="interviewResult">
			<br />

			@if($interview_data['section_set_id'] == 31)
				@include('interview.result_full')
			@elseif($interview_data['section_set_id'] == 32)
				@include('interview.result_basic')
			@elseif($interview_data['section_set_id'] == 33)
				@include('interview.result_simple')
			@elseif($interview_data['section_set_id'] == 34)
				@include('interview.result_light')
			@elseif($interview_data['section_set_id'] == 35)
				@include('interview.result_light')
			@endif	

			@include('interview.instruction')
			</div>
		</div><!-- /.patientView -->
		@endif	
		<!--{* フォローアップ一覧 *}-->
		@if($followup_list)
		<div class="notprint-wrap">
		<div class="table-wrap">
			<table summary="問診結果一覧">
				<thead>
					<tr>
						<th class="column1"></th>
						<th class="column2">No</th>
						<th class="column3">経過週</th>
						<th class="column4">フォローアップ診断日時</th>
						<th class="column5"></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="column1"></th>
						<th class="column2">No</th>
						<th class="column3">経過週</th>
						<th class="column4">フォローアップ診断日時</th>
						<th class="column5"></th>
					</tr>
				</tfoot>
				<tbody>
						@foreach ($followup_list as $key => $value)
						<tr>
							<td class="center">
							<input type="button" name="action_admin_followup_detail" id="followup_detail{{ $key + 1 }}" value="詳細" onClick="location.href='{{ URL::to('/') }}/admin/followup/detail/{{ $interview_data['interview_id'] }}/{{ $value['type'] }}/{{ $value['followup_interview_id'] }}'" />
							</td >
							<td class="center">{{ $key + 1 }}</td>
							<td class="center">{{ $value['week'] }}週</td>
							<td class="center">{{ $value['input_date'] }}</td>
							<td class="center"> 
							@if ( $key === 0)
							<input type="button" name="followup_detail" id="followup_detail{{ $key + 1 }}" value="編集" onClick="location.href='{{ URL::to('/') }}/admin/followup/edit/{{ $interview_data['interview_id'] }}/{{ $value['type'] }}/{{ $value['followup_interview_id'] }}/1'" />
							@else
							--
							@endif
							</td>
						</tr>
						@endforeach
				</tbody>
			</table>
			<div class="btm"><img src="{{ URL::to('/') }}/img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
			
		</div>
		</div>
		@endif

		<!--{* フォローアップ一覧 ここまで *}-->
		@if($is_followup == 1)
		<div class="notprint-wrap">
			{{-- <!-- ここからユーザーログインさせたくないので一旦削除 --><input type="button" value="フォローアップ問診を行う" class="bigbtn"  /> --}}
			<div class="sendfollowupArea">
			<form  action="{{ URL::to('/') }}/admin/interview/sendmail/{{ $interview_data['interview_id'] }}" method="post">
			<input type="hidden" name="send_interview_id" value="{{ $interview_data['interview_id'] }}" />
			<input type="hidden" name="send_base_email" value="{{ $interview_data['email'] }}" />
			<input type="hidden" name="send_name" value="{{ $interview_data['first_name'] }}{{ $interview_data['last_name'] }}" />
			送信先メールアドレス <input type="text" name="sendfollowup_email" value="{{ $interview_data['email'] }}" /> <br><br>
			<input type="submit" name="action_admin_interview_mailsend"  value="次回フォローアップのメールを送信先メールアドレスに送信する" />
			</form>
			</div>
		</div>
		@endif

		<div class="buttonArea">
			<ul>
				<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
			</ul>
		</div>
		@endif
	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->
