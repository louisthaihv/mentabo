<?php

namespace Admin;
use View;
use Input;
use AdminHistory;

class HistoryController extends \BaseController {

	public function getIndex()
	{
		$skip = 0;
		$take = 30;
		$input = Input::all();

        if(isset($input['view_type'])){
            $data['input_view_type_1'] = (in_array('interview',$input['view_type']))?true:false;
            $data['input_view_type_2'] = (in_array('followup',$input['view_type']))?true:false;
            $data['input_view_type_3'] = (in_array('admin',$input['view_type']))?true:false;
            $data['input_view_type_4'] = (in_array('admin_followup',$input['view_type']))?true:false;
        }else{
            $data['input_view_type_1'] = true;
            $data['input_view_type_2'] = true;
            $data['input_view_type_3'] = true;
            $data['input_view_type_4'] = true;

			$input = array(
				'date_y_f' => '',
				'date_m_f' => '',
				'date_d_f' => '',
				'date_h_f' => '',
				'date_i_f' => '',
				'date_y_t' => '',
				'date_m_t' => '',
				'date_d_t' => '',
				'date_h_t' => '',
				'date_i_t' => '',
				'name' => '',
				'nick_name' => '',
				'interview_id' => '',
			); 
        }

		//印刷履歴一覧取得
		$history_list = AdminHistory::selectPrintHistoryList($skip, $take, $input);
		$history_list->setBaseUrl($_SERVER['REQUEST_URI']);

		//Straight 20130729 印刷履歴にフォローアップ情報追加
        $week_array = [];
		if(count($history_list)){
			foreach($history_list as $key => $val){
				if(strpos($val['view_type'], 'followup') !== false){
					$list = AdminHistory::selectFollowup($val['interview_id']);
                    $week_array[$key]['week'] = $list[0]['week'].'週';
					if(is_array($list) && !empty($list)){
						$list2 = AdminHistory::selectInterview($list[0]['first_interview_id']);
						if(is_array($list2) && !empty($list2)){
							//$history_list[$key]['interview_name'] = $list2[0]['name'];
						}
					}
				} else {
                    $week_array[$key]['week'] = '--';
				}
			}
		}

		$content = View::make('admin.history.list', compact('history_list', 'input'));
		$view = View::make('admin.content', compact('content'));
        View::share('data',$data);
        View::share('week_array',$week_array);
        return $view;
	}

	
}
