
<p class="user-confirm"><b><?php echo $select_interview['first_name'] ?><?php echo $select_interview['last_name'] ?>　<?php echo floor((time() - strtotime($select_interview['birth_date'])) / (60*60*24*365)) ?>歳　<?php echo ($select_interview['gender'] == '1')? '女性' : '男性' ?></b> でよろしいですか？</p> <input type="hidden" name="input_type" value="question" />
<input type="hidden" name="birth_date_y" value="" />
<input type="hidden" name="birth_date_m" value="" />
<input type="hidden" name="birth_date_d" value="" />
<input type="hidden" name="section_set_id" value="" />
<input type="hidden" name="first_interview_id" value="" />
<input type="hidden" name="exercise_value" value="{{ $exercise_value  }}">
<div class="btn-area">
	<!--{if $app.finish_flg != 1}-->
	@if($week != 1) 
			<input type="submit" name="action_followup_question" value="第{{ $week }}週のフォローアップ診断を行う" />
	@endif		
	<!--{/if}-->
	<!--{if $session.week != 2}-->
	@if($week != 2)
			<input type="submit" name="action_followup_history" value="過去のフォローアップ 診断を見る" />
	@endif		
	<!--{/if}-->
</div>