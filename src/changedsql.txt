ALTER TABLE `t_account` ADD `remember_token` VARCHAR(100) NOT NULL AFTER `valid`;


// skip this INSERT command , an account can be added through creating a new tenant
INSERT INTO `t_account` (`account_id`, `tenant_id`, `login_name`, `name`, `name_kana`, `password`, `role_id`, `valid`, `remember_token`, `create_datetime`, `update_datetime`, `update_account_id`) VALUES
(2, 1, 'admin2', 'admin2', 'admin2', 0x243279243130242f4d53373251486234734a4242437439516461465875367a5876504a37783470766e45385334525049734b4858564f5345792f3761, 1, 1, '', NULL, NULL, 1);

//pass is admin2


CREATE TABLE `files_tenant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `mime` varchar(50) NOT NULL,
  `size` int(11) NOT NULL,
  `data` mediumblob NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenant_id` (`tenant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;



--
-- Table structure for table `m_tenant`
--

CREATE TABLE `m_tenant` (
  `tenant_id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_name` varchar(50) NOT NULL,
  `logo_url` varchar(100) NOT NULL,
  `valid` tinyint(4) DEFAULT NULL,
  `interview_type` tinyint(4) NOT NULL DEFAULT '1',
  `token` varchar(12) NOT NULL,
  PRIMARY KEY (`tenant_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_tenant`
--

INSERT INTO `m_tenant` (`tenant_id`, `tenant_name`, `logo_url`, `valid`, `interview_type`, `token`) VALUES
(1, 'Name of 1', '', 1, 1, 'sHdk8wiSkiO9'),
(2, 'name of tenant 2', '', 1, 1, 'fdkdsfkl2as');


ALTER TABLE `t_user` ADD `remember_token` VARCHAR(100) NOT NULL AFTER `promotion_code`;


ALTER TABLE `t_user` ADD `age` INT NOT NULL AFTER `birth_date`;
ALTER TABLE `t_user` ADD `interview_step` INT NOT NULL AFTER `remember_token`;
ALTER TABLE `t_interview` ADD `section_set_by_page` INT NOT NULL AFTER `input_raw_data`;
ALTER TABLE `t_interview` ADD `input_raw_data` TEXT NOT NULL AFTER `section_set_by_page `;


ALTER TABLE `t_user` ADD `login_id` VARCHAR(45) NOT NULL AFTER `user_id`;


INSERT INTO `metabolic`.`m_section_set_contents` (`section_set_id`, `section_id`, `page_num`, `order_section`, `flg_display`, `section_type`, `display_position`, `block_id`, `party_id`) VALUES 
('35', '2101', '1', '1', '1', 'mental', 'NULL', '', ''),
('35', '2102', '1', '2', '1', 'mental', 'NULL', '', ''),
('35', '2103', '1', '3', '1', 'mental', 'NULL', '', ''),
('35', '2104', '1', '4', '1', 'mental', 'NULL', '', ''),
('35', '2105', '1', '5', '1', 'mental', 'NULL', '', ''),
('35', '2106', '1', '6', '1', 'mental', 'NULL', '', ''),
('35', '2107', '2', '1', '1', 'mental', 'NULL', '', ''),
('35', '2108', '2', '2', '1', 'mental', 'NULL', '', ''),
('35', '2109', '2', '3', '1', 'mental', 'NULL', '', ''),
('35', '2110', '2', '4', '1', 'mental', 'NULL', '', ''),
('35', '2111', '2', '5', '1', 'mental', 'NULL', '', ''),
('35', '2112', '2', '6', '1', 'mental', 'NULL', '', ''),
('35', '2113', '2', '7', '1', 'mental', 'NULL', '', ''),
('35', '2114', '1', '7', '1', 'mental', 'NULL', '', ''),
('35', '2115', '1', '8', '1', 'mental', 'NULL', '', ''),
('35', '2116', '1', '9', '1', 'mental', 'NULL', '', ''),
('35', '2117', '1', '10', '1', 'mental', 'NULL', '', ''),
('35', '2118', '1', '11', '1', 'mental', 'NULL', '', ''),
('35', '2119', '1', '12', '1', 'mental', 'NULL', '', ''),
('35', '2120', '1', '13', '1', 'mental', 'NULL', '', '');



INSERT INTO `m_role` (`role_id` , `role_name` , `auth_pattern` )VALUES ( '0', 'システム管理者', 'SYSTEM_ADMIN' );   
 # // ****** Note have to change role_id to 0,the above still makes the role_id 9, not 0


ALTER TABLE `t_account` ADD `tenant_id` INT NOT NULL AFTER `account_id`;
ALTER TABLE `t_user` ADD `tenant_id` INT NOT NULL AFTER `user_id`;
ALTER TABLE `t_interview` ADD `tenant_id` INT NOT NULL AFTER `interview_id`;


INSERT INTO `t_account` (`account_id`, `tenant_id`,`login_name`, `name`, `name_kana`, `password`, `role_id`, `valid`, `remember_token`, `create_datetime`, `update_datetime`, `update_account_id`) VALUES (NULL, 0,'system_admin', 'system_admin', 'system_admin', '$2y$10$rkhtJxUI7IcRbPNNU3ScXe.1u2IQ3u5p0ektee0W1fteO9Tf2DahO', '0', '1', '', NULL, NULL, NULL);

// pass is system_admin


ALTER TABLE `t_user` ADD `nick_name` VARCHAR(45) NOT NULL AFTER `login_id`;

ALTER TABLE `t_user` ADD `years_old` SMALLINT NOT NULL AFTER `birth_date`, ADD `months_old` TINYINT NOT NULL AFTER `years_old`;



ALTER TABLE `t_user` ADD `followup_complete_flag` INT NOT NULL AFTER `interview_step`;

ALTER TABLE `t_user` ADD `interview_complete_flag` INT NOT NULL AFTER `followup_complete_flag`


ALTER TABLE `t_user` CHANGE `email` `email` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Eメール';


ALTER TABLE `t_user` ADD `saved_basic_info` TINYINT NOT NULL DEFAULT '0' AFTER `promotion_code`;





ManhDT

ALTER TABLE `t_interview` ADD `interview_step` INT NOT NULL AFTER `validation_key`;
ALTER TABLE `t_interview` ADD `section_set_by_page` TEXT NOT NULL AFTER `interview_step`;
ALTER TABLE `t_interview` ADD `input_raw_data` TEXT NOT NULL AFTER `section_set_by_page`;
ALTER TABLE `t_user` ADD `interview_complete_flag` INT NOT NULL AFTER `remember_token`;
ALTER TABLE `t_user` ADD `followup_complete_flag` INT NOT NULL AFTER `interview_complete_flag`;


UPDATE `mmetaboweb_db`.`m_layer` SET `help_text` = '<img src="../../img/layer/157_help.jpg" alt="食事チェックシート" />' WHERE `m_layer`.`layer_id` = 157 AND `m_layer`.`section_id` = 2023;



ALTER TABLE `t_interview` CHANGE `gender` `gender` ENUM('1','2') NULL DEFAULT NULL COMMENT '''1:男性、2：女性';
ALTER TABLE `t_user` CHANGE `gender` `gender` ENUM('1','2') NOT NULL COMMENT '''1:男性、2：女性';

UPDATE `m_choice` SET `choice_string` = 'ランニング(きつい)' WHERE `m_choice`.`choice_id` = 10217 AND `m_choice`.`layer_id` = 183 AND `m_choice`.`section_id` = 2099;
