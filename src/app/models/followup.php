<?php

class Followup extends Eloquent {

	public static function check_mailaddress($email)
	{	
		return DB::table('t_user')->where('email', $email)->first();
	}

	public static function change_email($user_id, $email)
	{
        $result = DB::table('t_interview')->where('user_id', $user_id)->update(array('email' => $email));
		if($result)return DB::table('t_user')->where('user_id', $user_id)->update(array('email' => $email));
        return false;
	}

	public static function select_interview($user_id, $interview_type)
	{	
		return DB::table('t_user')->join('t_interview', 't_user.user_id', '=', 't_interview.user_id')->where('t_interview.interview_type', $interview_type)->where('t_user.user_id', $user_id)->orderBy('t_interview.interview_id', 'desc')->first(array('*', 't_user.tenant_id', 't_user.email', 't_user.birth_date'));
	}

	public static function check_email($user_id, $user_email)
	{	
		return DB::table('t_user')->where('user_id', '!=', $user_id)->where('email', $user_email)->first();
	}

	public static function update_interview($where, $update) {

		return DB::table('t_interview')->where($where)->update($update);
	}

	public static function insert_interview($array) {

		return DB::table('t_interview')->insertGetId($array);
	}

	public static function insert_followup($insert_followup_values) {

		return DB::table('t_followup')->insert($insert_followup_values);
	}	

	public static function update_followup($followuplist, $where_data) {

		return DB::table('t_followup')->where($where_data)->update($followuplist);
	}	

	public static function update_user($where, $update) {

		return DB::table('t_user')->where($where)->update($update);
	}

    public static function updateFollowupByFollowupId($followup_judgement_data, $week, $type, $first_interview_id, $followup_interview_id, $last_followup_data){
        //
    }


	// 未実施の最も古い週経過のデータを取得
	public static function getFollowupNotCompliedCheckByFirstInterviewId($first_interview_id){

		return DB::table('t_followup')->where('first_interview_id', $first_interview_id)->whereNull('input_date')->orderBy('week', 'asc')->first();
	}

	// // 実施済みの最新フォローアップデータ1件を取得
	// public static function getFollowupByFirstInterviewId($first_interview_id){

	// 	return DB::table('t_followup')->where('first_interview_id', $first_interview_id)->whereNotNull('input_date')->orderBy('week', 'asc')->first();
	// }

	// フォローアップ設問ID一覧取得
	public static function getSectionSetContentsByFollowupId($section_set_id, $user_data, $first_interview_data, $type) {
		
		return DB::table('m_section_set_contents')
            ->join('m_section', 'm_section_set_contents.section_id', '=', 'm_section.section_id')
            ->where('m_section_set_contents.section_set_id', $section_set_id)
            ->where('m_section_set_contents.flg_display', 1)
			->where( function ($query) use ($user_data) { $query->whereNull('target_gender')->orWhere( 'target_gender', '=', $user_data['gender']); }) 
			->where( function ($query) use ($user_data) { $query->whereNull('target_age_from')->orWhere( 'target_age_from', '<=', $user_data['years_old']); })
			->where( function ($query) use ($user_data) { $query->whereNull('target_age_to')->orWhere( 'target_age_to', '>=', $user_data['years_old']); })
			->where( function ($query) use ($type, $first_interview_data) {
			    if ($type > 2) $query->where('m_section.target_question', $first_interview_data['result']);
			})
            ->orderBy('page_num', 'asc')
            ->orderBy('order_section', 'asc')
            ->select('m_section_set_contents.section_set_id', 'm_section_set_contents.section_id', 'm_section_set_contents.page_num', 'm_section_set_contents.order_section', 'm_section.section_string')
            ->get();
            //->toSql();
	}

	public static function getChoiceBySectionId($condition_string)
	{
		$list = array();
		$getChoiceBySectionId = DB::table('m_choice')->whereIn('section_id', $condition_string)->get();
		foreach ($getChoiceBySectionId as $key => $value) {
			$list[$value['choice_id']] = $value;
		}
		return $list;
	}

	public static function getLayerControlByChoiceBySectionIdList($section_id_sql_condition)
	{
		$return =  \DB::table('m_layer_control_by_choice')
            ->whereExists(function($query) use ($section_id_sql_condition)
            {
                $query->select(\DB::raw('layer_id'))
                      ->from('m_choice')
                      ->whereIn('m_choice.section_id', $section_id_sql_condition)
                      ->whereRaw('m_choice.layer_id = m_layer_control_by_choice.layer_id');
            })
            ->get();     

		foreach ($return as $key => $value) {
			$list[] = $value;
		}
		return $list;
	}

	// 初回問診の指導内容を取得
	public static function getGuidanceByInterviewId($interview_id){
		return DB::table('t_guidance')->where('interview_id', $interview_id)->first();
	}

	// 実施済みの最新フォローアップデータ1件を取得
	public static function getFollowupByFirstInterviewId($first_interview_id){

		return DB::table('t_followup')->where('first_interview_id', $first_interview_id)->whereNotNull('input_date')->orderBy('week', 'asc')->first();
	}

	// 特定の問診タイプのデータの最新を1件取得

	public static function getFollowupByFirstInterviewIdType($first_interview_id, $type){

		return DB::table('t_followup')->where('first_interview_id', $first_interview_id)->where('type', $type)->orderBy('input_date', 'desc')->first();
	}	

	// フォローアップ問診IDから対象のフォローアップデータを取得
	public static function getFollowupByFollowupId($first_interview_id, $week){
		return DB::table('t_followup')->where('first_interview_id', $first_interview_id)->where('week', $week)->first();

	}

	public static function insertChoiceSelectionFollowup($interview_id, $rows)
	{
		foreach ($rows as $data) {
		
			$string['interview_id'] = $interview_id;

			if(isset($data['choice_id'])) {
				$string['choice_id'] = $data['choice_id'];
			} else {
				$string['choice_id'] = '0';	
			}
				
			if (isset($data['selection'])) {
				$string['selection'] = 1;
			} else {
				$string['selection'] =  0;
			}	

			if (isset($data['input_float'])) {
				$string['input_float'] = $data['input_float'];
			} else {
				$string['input_float'] = 'null';
			}

			if (isset($data['input_datetime'])) {
				$string['input_datetime'] = $data['input_datetime'];
			} else {
				$string['input_datetime'] = 'null';
			}

			if (isset($data['input_string'])) {
				$string['input_string'] = $data['input_string'];
			} else {
				$string['input_string'] = 'null';
			}

			DB::table('t_choice_selection')->insert($string);
		}	
	}

	// 問診IDから選択されたデータを取得
	public static function getChoiceSelectionByInterviewId($interview_id){
		$return = DB::table('t_choice_selection')->where('interview_id', $interview_id)->get();
		
		$list = array();
		foreach ($return as $key => $row) {
			$list[$row['choice_id']] = $row;
		}
		return $list;
	}

	// 問診IDから対象のデータを取得
	public static function getSectionChoiceByChoiceIdSectionSetId($choice_id, $section_set_id) {	
		return DB::select( DB::raw("SELECT mc.section_id, ms.section_string, mc.choice_id, mc.choice_string FROM m_choice AS mc LEFT JOIN m_section AS ms ON mc.section_id = ms.section_id INNER JOIN m_section_set_contents AS msc ON ms.section_id = msc.section_id WHERE mc.choice_id IN ($choice_id) AND msc.section_set_id = '$section_set_id' ORDER BY msc.order_section, mc.choice_id"));

	}

	/**
	 * 特定のフォローアップ問診タイプのデータを取得
	 * Enter description here ...
	 * @param $first_interview_id	：初回問診ID
	 * @param $type					：フォローアップ問診ID
	 */
	public static function getFollowup($followup_interview_id){

		return DB::table('t_followup')->where('followup_interview_id', $followup_interview_id)->first();

	}


	//-------------------------------------
	// データ取得
	//-------------------------------------

	// t_interview

	/**
	 * 問診テーブル取得
	 * Enter description here ...
	 * @param $interview_id		：問診ID
	 */
	public static function getInterviewByFollowupId($interview_id) {

		return DB::table('t_user')->join('t_interview', 't_user.user_id', '=', 't_interview.user_id')->where('t_interview.interview_id', $interview_id)->first(array('*', 't_user.tenant_id', 't_user.email'));	

	}

	/**
	 * 特定の問診のひとつ前のデータを取得
	 * Enter description here ...
	 * @param $first_interview_id	：初回問診ID
	 * @param $type					：フォローアップ問診ID
	 */
	public static function getFollowupByFollowupInterviewId($first_interview_id, $followup_interview_id){

		return DB::table('t_followup')->where('first_interview_id', $first_interview_id)->where('followup_interview_id', $followup_interview_id)->where('input_date', '<>', 'NULL')->orderBy('week', 'desc')->first();

	}

	/**
	 * 問診が完了しているフォローアップ問診を全件取得
	 * Enter description here ...
	 * @param $first_interview_id		：初回問診ID
	 */
	public static function getFollowupListByFirstInterviewId($first_interview_id){

		$return = DB::table('t_followup')->where('first_interview_id', $first_interview_id)->where('input_date', '<>', 'NULL')->get();
		$list = array();
		foreach ($return as $key => $row) {
			$list[] = $row;
		}
		return $list;

	}

	public static function getSection($section_id){

		return DB::table('m_section')->where('section_id', $section_id)->first();

	}

	public static function getLayer($section_id){

		$return = DB::table('m_layer')->where('section_id', $section_id)->orderBy('order_in_section', 'asc')->get();
		$list = array();
		foreach ($return as $key => $row) {
			$list[] = $row;
		}
		return $list;

	}

	public static function getLayerInclusion($layer_id){

		$return = DB::table('m_layer_inclusion')->where('layer_id', $layer_id)->get();
		$list = array();
		foreach ($return as $key => $row) {
			$list[] = $row;
		}
		return $list;

	}

	public static function getLayerControlByChoice($layer_id){

		$return = DB::table('m_layer_control_by_choice')->where('layer_id', $layer_id)->where('control_type', 'show')->orderBy('group_no', 'asc')->get();
		$list = array();
		foreach ($return as $key => $row) {
			$list[] = $row;
		}
		return $list;

	}

	public static function getChoice($section_id, $exercise_value){

		$return = DB::table('m_choice')->where('section_id', $section_id)->where('lead', $exercise_value)->orWhere('lead', '')->orderBy('order_in_layer', 'asc')->get();
		$list = array();
		foreach ($return as $key => $row) {
			$list[] = $row;
		}
		return $list;

	}

	//-------------------------------------
	// データ削除
	//-------------------------------------
	/**
	 * 問診への回答テーブル　対象データ削除
	 * Enter description here ...
	 * @param $interview_id	： 問診ID
	 */
	public static function deleteChoiceSelectionInterviewId($interview_id) {

		return DB::table('t_choice_selection')->where('interview_id', $interview_id)->delete();

	}

	/**
	 * フォローアップ情報　対象データ削除
	 * Enter description here ...
	 * @param $first_interview_id	： 初回問診ID
	 */
	public static function deleteFollowupFirstInterviewId($first_interview_id) {

		return DB::table('t_choice_selection')->where('interview_id', $first_interview_id)->whereNull('input_date')->delete();

	}

	//-------------------------------------
	// データ更新
	//-------------------------------------
	/**
	 * データ更新　汎用
	 * Enter description here ...
	 * @param $account_id
	 * @param $login_name
	 * @param $password
	 * @param $authority
	 */
	public static function updateFields($table_name , $fields_data , $where_data){

		return DB::table($table_name)->where($where_data)->update($fields_data);

	}

	public static function rules () {

        $arrRule = array( 
            'email' => 'required|email'  
        );
        return $arrRule;
    }

    public static function validate($data)
    {
        $messages = array();
        return Validator::make($data, Followup::rules(), $messages);
    }


}

?>