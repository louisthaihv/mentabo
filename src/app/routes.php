<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::controller('/api', 'api\ApiController');
//Route::controller('/interview/judge', 'interview\InterviewJudgeController');
Route::resource('/interview', 'interview\InterviewController@getIndex');
//Route::controller('/followup/checkmail', 'followup\IndexController');
Route::controller('/followup', 'followup\IndexController');
Route::controller('/notice', 'notice\IndexController');

Route::resource('/history', 'history\HistoryController@getIndex');

Route::get('admin/followup/detail/{interview_id}/{type}/{followup_interview_id}','admin\FollowupController@getDetail');
Route::get('admin/followup/edit/{interview_id}/{type}/{followup_interview_id}/{page}','admin\FollowupController@getEdit');
Route::get('admin/followup/update/{interview_id}/{type}/{followup_interview_id}/{page}','admin\FollowupController@getUpdate');
Route::post('admin/followup/update/{interview_id}/{type}/{followup_interview_id}/{page}','admin\FollowupController@getUpdate');

Route::group(array('prefix' => 'admin'), function() {

	Route::resource('/history', 'admin\HistoryController@getIndex');
	//Route::resource('/interview/history', 'admin\InterviewController@printHistory');
	Route::resource('/interview', 'admin\InterviewController@getIndex');
	Route::resource('/interview/download', 'admin\DownloadController@getIndex');
	Route::resource('/interview/download/all', 'admin\DownloadController@getAll');
	
	//Route::get('/sendmail/{id}', 'admin\SendMailController@getIndex');
	Route::post('/interview/sendmail/{id}', 'admin\InterviewController@postSendMail');

});

Route::controller('admin', 'admin\IndexController');
Route::controller('user', 'admin\TUserController');


Route::get('logo/{id}','admin\IndexController@getShowfiletenant2');

Route::get('top/{token}','admin\TUserController@getLogin');

Route::get('portal','admin\TUserController@getPortal');
Route::get('mypage','admin\TUserController@getMypage');

Route::get('basic_info','admin\TUserController@getChangebasicinfo');
Route::post('basic_info','admin\TUserController@postChangebasicinfo');

Route::get('edit_password','admin\TUserController@getChangepassword');
Route::post('edit_password','admin\TUserController@postChangepassword');

Route::get('logout','admin\TUserController@getLogout');


App::missing(function($exception)
{
    return Response::view('404', array(), 404);
});


Validator::extend('alphanum_under',function($field,$value,$params) {
	if(preg_match('/[^a-z_0-9]/i', $value))
	{
	  return false;
	}
	else return true;
});

Validator::extend('halfwidth',function($field,$value,$params) {
	$len = strlen($value);
	
	$mblen = mb_strlen($value, "UTF-8") * 3;
	 
	if($len == $mblen) {
	    return false;
	} else
		return true;
});


Validator::extend('hiragana_katakana',function($field,$value,$params) {
	if(preg_match("/[^\x{3041}-\x{3096}\x{30A0}-\x{30FF}]/u", $value))
	{
	  return false;
	}
	else return true;
});

Validator::extend('csvpasscheck',function($field,$value,$params) {
	if ($value==$params[0]){
		return false;
	}
	else return true;
});

Validator::extend('csvtenantcheck',function($field,$value,$params) {
	if ($value==$params[0]){
		return true;
	}
	else return false;
});



