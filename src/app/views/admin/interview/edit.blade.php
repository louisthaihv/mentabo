<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
<title>生活習慣診断</title>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link type="text/css" href="{{ URL::to('/') }}/css/sunny/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/common/format.css" media="all" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/common/base.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href=".{{ URL::to('/') }}/css/common_style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/print_style.css" media="print" />
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-1.6.3.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.alerts.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/style.js"></script>
<script type="text/javascript">
$(function(){
	$(document).ready(function(){ control_inputs(); });
	$("input").change(function(){ control_inputs(); });
	function cal_hour( minute ){ return Math.floor(minute/60); }
	function cal_min( minute ){ return ("0"+String(minute-60*Math.floor(minute/60))).slice(-2); }
	function control_inputs(){

		<?php echo $jquery['jquery_display'] ?>

	}
	<?php echo $jquery['jquery_ui'] ?>

	$("#form_main").submit(function() {
		var tmp_error_text = '';
		ok_section_ids = new Array();
		error_section_ids = new Array();

		<?php echo $jquery['jquery_requiredCheck'] ?>
		<?php echo $jquery['jquery_regexpCheck'] ?>

		if (tmp_error_text.length >= 1) {
			var error_text = "<ul>" + tmp_error_text + "</ul>";
			$("div.compulsoryInputError").html(error_text).show();
			$("div.compulsoryInputError").css("display", "block");
			while (section_id = ok_section_ids.pop()) {
				$("#s_" + section_id).css("background-color", "#FFFFFF");
			}
			while (section_id = error_section_ids.pop()) {
				$("#s_" + section_id).css("background-color", "#FFA0A2");
			}
			scrollTo(0,0);
			return false;
		}
		return true;
	});
});

</script>

<script type="text/javascript" src="{{ URL::to('/') }}/js/admin_addition.js"></script>
<link type="text/css" href="{{ URL::to('/') }}/css/addition.css" rel="stylesheet" />
</head>
<body id="interview" >
<div id="wrap">


	<div id="noscript">
		<p>このページは、Javascriptを使用しています。<br>Javaascriptを有効にしてご利用ください。</p>
	</div>
	<script>
		document.getElementById("noscript").style.display = "none";
	</script>

	<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
	<div id="content" style="padding-bottom:  10px;">
		<div id="contentInner">
			<form action="" method="GET">
			<div class="compulsoryInputError"></div>
			<input type="hidden" name="action_admin_interview_detailcompletion" value="true">
			<input type="hidden" name="interview_id" value="<!--{$app.interview_id}-->" />
			<input type="hidden" name="section_set_id" value="<!--{$app.section_set_id}-->" />
			<input type="hidden" name="section_id" value="<!--{$app.section_id}-->" />

			<!--{include file="interview/include_display_questions.tpl"}-->
			<?php echo $content ?>

			<div class="btn-area" id="detailedit">
				<input type="hidden" name="ms" id="close" value="close" />
				<input type="submit" name="btn_edit" id="btn_edit" value="登録する" />
			</form>
				<br><br>
				<form id="form_main" action="" method="POST">
					<input type="submit" name="btn_cancel" id="btn_cancel" value="キャンセル" />
				</form>
			</div>

			
		</div><!-- /#contentInner -->

	</div><!-- /#content -->
	<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->
	<script>
	document.getElementById("content").style.display = "block";
	</script>

</div><!-- /#wrap -->
</body>
</html>

