@include('header')
<div id="interviewResult">
<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2>フォローアップ問診　第<?php echo $week ?>週　結果</h2>
	<div id="contentInner">

			<dl>
			<dt><span>回答内容</span></dt>
			<dd class="result-followup">
				<br />
				<table>
					<!--{foreach from = $app.choice_interview_list key = key item = item }-->
					<?php foreach ($choice_interview_list as $key => $item) { ?>
							<tr>
								<td class="result-followup_bg"><?php echo $item['section_string']?><!--{$item.section_string}--></td>
								<td><?php echo $item['choice_string']?><!--{$item.choice_string}--></td>
							</tr>
					<?php } ?>		
					<!--{/foreach}-->
				</table>
			</dd>
			<!--{if $session.type != 2 && $app.followup_data.exercise_value != 'E'}-->
			<?php if($type != 2 AND $followup_data['exercise_value'] != 'E' ) { ?>
					<dt><span>診断結果</span></dt>

					<!--{if $app.followup_judgment_list.judgment_type != 0 && $app.followup_judgment_list.judgment_type != 4 && $app.followup_judgment_list.judgment_type != 5}-->
					<?php if($followup_judgment_list['judgment_type'] != 0 AND $followup_judgment_list['judgment_type'] != 4 AND $followup_judgment_list['judgment_type'] != 5) {?>
							<dt><span>運動</span></dt>
							<dd>
								<!--{$app.followup_judgment_list.judgment_detail}-->
							</dd>
					<?php } ?>		
					<!--{/if}-->
					<!--{if $app.followup_judgment_list.judgment_type != 0 && $app.followup_judgment_list.judgment_type != 4}-->
					<?php if($followup_judgment_list['judgment_type'] != 0 AND $followup_judgment_list['judgment_type'] != 4 AND $followup_judgment_list['judgment_type'] != 5) { ?>		
							<dt><span>食事</span></dt>
							<dd>
								<!--{$app.followup_judgment_list.judgment_calorie}-->
							</dd>
					<?php } else { ?>	
					<!--{else}-->
							<dd>
							<!--{$app.followup_judgment_list.judgment_detail}-->
							<?php echo $followup_judgment_list['judgment_detail'] ?>
							</dd>
					<?php } ?>	
					<!--{/if}-->

					<!--{if isset($app.followup_judgment_list.target_detail) }-->
					<?php if($followup_judgment_list['target_detail']) { ?>
							<dt><span>今後の取り組み</span></dt>
							<dd>
								<!--{$app.followup_judgment_list.target_detail}-->
								<?php echo $followup_judgment_list['target_detail'] ?>
							</dd>
					<?php } ?>
					<!--{/if}-->
			<?php } ?>		
			<!--{/if}-->
			</dl>

			<div id="instruction-area">
				<form  action="" method="post">
					<input type="hidden" id="absolute_url" value="">
					<input type="hidden" id="view_type" value="{{ $view_type }}">
					<input type="hidden" id="interview_id" value="{{ $first_interview_id }}">

					<p><!--{$smarty.now|date_format:'%Y'}--><?php echo date('Y') ?>年<!--{$smarty.now|date_format:'%m'}--><?php echo date('m') ?>月<!--{$smarty.now|date_format:'%d'}--><?php echo date('d') ?>日</p>

					<p>入力者：<input type="text" id="name" /></p>

					<p><input id="followup-print-btn" type="button" value="印刷" /></p>
				</form>
			</div>

			<!--{include file="instruction_followup.tpl"}-->

			<div class="btn-area">
			<form action="{{ URL::to('/') }}/mypage" method="GET">
				<div class="btn-area">
					<input type="submit" name="action_followup_user_confirm" value="問診終了" />
				</div>
			</form>

			</div>


	</div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->
</div>
@include('footer')