<?php

namespace Notice;
use Notice;

class IndexController extends \BaseController {

	public function getIndex()
	{
		//マネージャ起動
		//$adminFollowup_Manager = $this->backend->getManager("adminfollowup");

		//今日の日付（ここで宣言することで各検索の）
		$now_date = date("Y-m-d H:i:s");
		//今日の2週間前の日付を作成
		$last_2week_date = date("Y-m-d H:i:s",strtotime("-2 week",strtotime($now_date)));
		//今日の49週間前の日付を作成
		$last_48week_date = date("Y-m-d H:i:s",strtotime("-49 week",strtotime($now_date)));
		
		//テンポラリファイル名をここで明示
		//$adminFollowup_Manager->setTemporaryTableName("interview","temp_interview");
		//$adminFollowup_Manager->setTemporaryTableName("send","temp_sendmail");
		
		//該当の可能性があるt_interview を分離してテンポラリテーブルに投入
		//条件：flg_followupが1 かつ 入力終了日時が48週より若いデータ
		$res = Notice::createTemporaryInterviewTable($last_48week_date);
		// if(Ethna::isError($res)){
		// 	$this->ae->add( null, $res->getMessage());
		// 	return false;
		// }
		
		//メール送信用テンポラリテーブルを強制的に作成
		$data = 99;
		$ress = Notice::createTemporarySendTable($data);
		// if(Ethna::isError($res)){
		// 	$this->ae->add( null, $res->getMessage());
		// 	return false;
		// }
		
		//*********************************************************************************************
		//まだフォローアップを開始していないデータを収集する
		//*********************************************************************************************
		
		//起動日当日に送信するメールアドレスを取得する(初回版) ****************************************
		$notice_data = Notice::insertInterviewNoticeDataByDate($last_2week_date, true);
		// if(Ethna::isError($notice_data)){
		// 	$this->ae->add( null, $notice_data->getMessage());
		// 	return false;
		// }

		//初回からフォローアップしていないデータを取得(初回版) ****************************************
		$notice_data = Notice::insertInterviewRemindDataByDate($last_2week_date,true);
		// if(Ethna::isError($notice_data)){
		// 	$this->ae->add( null, $notice_data->getMessage());
		// 	return false;
		// }
		
		//*********************************************************************************************
		//フォローアップを開始しているデータを収集する
		//*********************************************************************************************
		
		//過去にフォローアップをやっていない週があるアドレス ***********
		$notice_data = Notice::insertFollowupRemindDataByDate($now_date, true);
		
		//起動日当日に送信するメールアドレスを取得する(フォローアップ) *******************************
		$notice_data = Notice::insertFollowupNoticeDataByDate($now_date, true);

		// if(Ethna::isError($notice_data)){
		// 	$this->ae->add( null, $notice_data->getMessage());
		// 	return false;
		// }
		
		//*********************************************************************************************
		//メールを送信する
		//*********************************************************************************************
		if($notice_data){
		
			//XMLファイルを取得する
			//$xml_file_dir = dirname(dirname(__FILE__)) . '/../../data/followup/';
			
			//$xml_file_notice_name = "followup.xml";
			//$xml_file_remind_name = "remind.xml";
			//$xml_file_data = array();
			
			//$xml_file_data["notice"] = simplexml_load_file($xml_file_dir . $xml_file_notice_name);
			//$xml_file_data["remind"] = simplexml_load_file($xml_file_dir . $xml_file_remind_name);
			
			//$send_mail =& new Ethna_MailSender($this->backend);
			
			$send_res = array();
			foreach($notice_data as $key => $val){

				//メール送信
				$selectUser = Notice::selectUser($val['user_id']);
				//$adminFollowup_Manager->sendMail($val, $xml_file_data, $send_mail, $param);
				$param['week'] = $val['week'];
				$param['first_interview_id'] = $val['interview_id'];
				$param['name'] = $selectUser['first_name'].$selectUser['last_name'];
				$param['email'] = $selectUser['email'];
				$param['tenant_id'] = $selectUser['tenant_id'];

			    $this->sendMail($param);
				$res = Notice::setSendLog($val['interview_id'], $val['mail_type'], $val['week'], $val['validation_key'], $val['email']);
				
				
			}
		}

		//送信履歴を更新
		//return true;

	}

}
