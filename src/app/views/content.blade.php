@include('header')

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2><?php if(isset($setTitle)) echo $setTitle ?></h2>
	<div id="contentInner">

		<?php if(isset($content)) echo $content ?>

	</div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->

@include('footer')