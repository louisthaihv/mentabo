@include('admin/header')

<hr />
<div id="wrapper">
<div id="userList" class="contents">
	<h2 id="pageTitle">ユーザー一覧</h2>
	<hr />

	
	<div id="main">

		<div class="table-wrap">
			<table summary="アカウント一覧">
				<thead>
					<tr>
						<th></th>
						<th>ユーザーID</th>
						<th>ログインID</th>
						<th>ニックネーム</th>
						<th>性別</th>
						<th>生年月日</th>
						<th>有効／無効</th>
						
					</tr>
				</thead>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th>ユーザーID</th>
						<th>ログインID</th>
						<th>ニックネーム</th>
						<th>性別</th>
						<th>生年月日</th>
						<th>有効／無効</th>
					
					</tr>
				</tfoot>
									<tbody>
					@foreach ($users as $user)
											<tr>
							<td class="column1">
									<input type="button" value="編集" onClick="location.href='edituser/{{$user->user_id}}'" class="input-btn">
														</td>
							<td class="column2">{{ $user->user_id }}</td>
							<td class="column3">{{ $user->login_id }}</td>
							<td class="column4">{{ $user->nick_name }}</td>
							<td class="column5 center"><?php if ($user->gender=='1')  echo '男'; else echo '女';?></td>
							<td class="column6 center">{{ $user->birth_date }}</td>
							<td class="column7 center">@if ($user->invalid)
                                                    有効
												@else 
                                                    無効
											    @endif
											</td>
											
						</tr>
					@endforeach
												</table>

			<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>

	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
