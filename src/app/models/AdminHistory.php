<?php

class AdminHistory extends Eloquent {



	public static  function selectPrintHistoryList($skip, $take, $input) {

		if($input) {

			$name = $input['name'];
			$view_type = (isset($input['view_type'])) ? $input['view_type'] : array(0,0,0,0,0);
			$create_datetime1 = $input['date_y_f'].'-'.$input['date_m_f'].'-'.@$input['date_d_f'].' '.$input['date_h_f'].':'.$input['date_i_f'].':'.'00';
            $create_datetime2 = $input['date_y_t'].'-'.$input['date_m_t'].'-'.@$input['date_d_t'].' '.$input['date_h_t'].':'.$input['date_i_t'].':'.'00';
            $printer = $input['name'];
            $nick_name = $input['nick_name'];

		} else {

			$name = '';
			$view_type = array(0,0,0,0);
			$create_datetime1 = ''; 
			$create_datetime2 = '';
            $printer = '';
            $nick_name = '';
		}
 
		return DB::table('t_print_history')
        ->leftJoin('t_interview', function($join)
        {
            $join->on('t_print_history.interview_id', '=', 't_interview.interview_id');
        })
        ->leftJoin('t_user', function($join)
        {
            $join->on('t_user.user_id', '=', 't_interview.user_id');
        })
		->where( function ($query) use ($create_datetime1) {
		    if ($create_datetime1) { $query->orwhere('t_print_history.create_datetime', '>=', $create_datetime1);}	
		})
		->where( function ($query) use ($create_datetime2) {
		    if ($create_datetime2) { $query->orwhere('t_print_history.create_datetime', '<=', $create_datetime2);}
		})
        ->where( function ($query) use ($printer) {
            if ($printer) { $query->orwhere('t_print_history.name', 'like', $printer.'%');}
        })
        ->where( function ($query) use ($nick_name) {
            if ($nick_name) { $query->orwhere('t_user.nick_name', 'like', $nick_name.'%');}
        })
		->where( function ($query) use ($view_type) {
		   if (isset($view_type[0])) { $query->orwhere('t_print_history.view_type', '=', $view_type[0]);}
		   if (isset($view_type[1])) { $query->orwhere('t_print_history.view_type', '=', $view_type[1]);}	
		   if (isset($view_type[2])) { $query->orwhere('t_print_history.view_type', '=', $view_type[2]);}	
		   if (isset($view_type[3])) { $query->orwhere('t_print_history.view_type', '=', $view_type[3]);}	
		})
        ->orderBy('t_print_history.create_datetime', 'DESC')
        ->orderBy('t_print_history.print_history_id', 'DESC')
		->paginate($take, array (
			't_print_history.print_history_id', 
			't_print_history.interview_id',
			't_print_history.name', 
			't_print_history.view_type',
			't_print_history.create_datetime', 
			't_interview.finished_datetime as finished_datetime',
			't_user.nick_name',
			DB::raw('(CASE WHEN t_print_history.account_id > 0 THEN t_print_history.account_id ELSE "" END) AS account_id')));

	}

	public static function selectFollowup($followup_interview_id) {

		return DB::table('t_followup')->where('followup_interview_id', $followup_interview_id)->orderBy('week', 'asc')->get();
	}

	public static function selectInterview($interview_id) {

		return DB::table('t_interview')->where('interview_id', $interview_id)->get();
	}
	
}

?>