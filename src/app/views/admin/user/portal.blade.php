@include('../../header')

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2>ポータルトップ</h2>
	<div id="contentInner">
		
	
	<div id="main">
        <?php if($saved_basic_info == 0): ?>
        <div style="margin: 20px auto;width:480px;">
            <p>生活習慣診断は、マイページより基本情報を登録後にご利用になれます。</p>
        </div>
        <?php endif; ?>

		<div class="choices-half">
		
			<center>
				<input type="radio" id="t_101" name = "link" value="" onclick="location.href=process( {{ $interview_complete_flag }} )"  >
				<label id="b1" for="t_101"><?php if ($interview_complete_flag==0) echo '生活習慣診断'; else echo 'フォローアップ問診';?></label>
				<br/> <br/> <br/>
			<input type="radio" id="t_102" name = "link" value="" onclick="location.href='mypage'" >
			<label for="t_102">マイページ</label>
			</center>
		</div>
	
		<script type="text/javascript">
	
		
			function process(t){
				//var s = "interview?section_set_id="+"{{ $interview_type }}";
				var s = "interview";
				var s2 = "<?php echo $followup_url; ?>";
			
				if (t==0) {
					return s; 
				}
				else {
					return s2;
				}
			}
			function check_saved_basic_info(v){
				if (v==0){
					var t = document.getElementById("t_101");
					t.disabled = true;
				}
			}
			
			check_saved_basic_info( {{ $saved_basic_info }} );
		</script>
		
		<!-- <img src="{{ URL::to('/') }}/img/icon_required.png"/> <font color=red>*</font> は必須項目です。-->
		
	</div><!-- /#main -->
	

  </div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->



	<hr />
@include('../../footer')
