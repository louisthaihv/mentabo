<script type="text/javascript">
$(function(){
	$(document).ready(function(){ control_inputs(); });
	$("input").change(function(){ control_inputs(); });
	function cal_hour( minute ){ return Math.floor(minute/60); }
	function cal_min( minute ){ return ("0"+String(minute-60*Math.floor(minute/60))).slice(-2); }
	function control_inputs(){
		<?php echo $jqueryArray['jquery_display'] ?>
	}
	<?php echo $jqueryArray['jquery_ui'] ?>
	
	$("#form_main").submit(function() {
		var tmp_error_text = '';
		ok_section_ids = new Array();
		error_section_ids = new Array();

		<?php echo $jqueryArray['jquery_requiredCheck'] ?>
		<?php echo $jqueryArray['jquery_regexpCheck'] ?>

		if (tmp_error_text.length >= 1) {
			var error_text = "<ul>" + tmp_error_text + "</ul>";
			$("div.compulsoryInputError").html(error_text).show();
			$("div.compulsoryInputError").css("display", "block");
			while (section_id = ok_section_ids.pop()) {
				$("#s_" + section_id).css("background-color", "#FFFFFF");
				$("#s_" + section_id).css("background", 'url("<?php echo URL::to('/') ?>/img/h3_bg.gif") no-repeat scroll 0 100% transparent');
			}
			while (section_id = error_section_ids.pop()) {
				$("#s_" + section_id).css("background-color", "#FFA0A2");
				$("#s_" + section_id).css("background", 'url("<?php echo URL::to('/') ?>/img/h3_bg_error.gif") no-repeat scroll 0 100% transparent');
			}
			scrollTo(0,0);
			return false;
		}
		return true;
	});
});

</script>
