
<div id="interviewDetail" class="contents">
	<h2 id="pageTitle">フォローアップ詳細 </h2>
	<hr />

	<!--{include file="admin/error.tpl"}-->

	<div id="main">

		<div id="content">
			<div class="embossLine">
				<dl class="interviewData">
					<dt>問診ID</dt>
					<dd>{{ $followup_data['first_interview_id'] }}</dd>
					<dt>経過週</dt>
					<dd>{{ $followup_data['week'] }}週</dd>
					<dt>問診タイプ</dt>
					<dd>{{ $followup_data['type'] }}W</dd>
					<dt>問診開始日時</dt>
					<dd>{{ $interview_data['start_datetime'] }}～{{ $followup_data['input_date'] }}</dd>
				</dl>
			</div>
			<div class="embossLine">
				<dl class="interviewData">
					<dt>氏名</dt>
					<dd>{{ $interview_data['first_name'] }}{{ $interview_data['last_name'] }}</dd>
					<dt>生年月日</dt>
					<dd>{{ $interview_data['birth_date'] }}（問診時年齢 {{ $interview_data['age'] }}歳）</dd>
					<dt>性別</dt>
					<dd>{{ ($interview_data['gender'] == '1') ? '男性' : '女性' }}</dd>
				</dl>
			</div>

			<div class="doctorView">
				@if($choice_interview_list)
					<div class="table-wrap">
						<table summary="問診結果詳細">
						@foreach ($choice_interview_list as $key => $item)
 							<tr>
								<th>{{ $item['section_string'] }}</th>
								<td>{{ $item['choice_string'] }}</td>
							</tr>
						@endforeach	
						</table>
						<div class="btm"><img src="{{ URL::to('/') }}/img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
					</div>
				@endif	
			</div>

			<!--{if $app.edit_flg == 1}-->
			@if($edit_flg == 1)
					<div style="text-align: center;">
						<input type="submit" class="notprint-wrap"  onClick="location.href='{{ URL::to('/') }}/admin/followup/edit/{{ $followup_data['first_interview_id'] }}/{{ $followup_data['type'] }}/{{ $followup_data['followup_interview_id'] }}/{{ $pagecount }}'" value="編集">
					</div>
					<br />
			@endif		
			<!--{/if}-->

			<!--{if $app.editFlg}-->
			@if($editFlg)
					<div id="followupDetail">
						<div class="table-wrap">

 							<!--{if $app.followup_data.type != 2}-->
 							@if($followup_data != 2)
									<dt><span>診断結果</span></dt>

									<dl>
										<!--{if $app.followup_judgment_list.judgment_type != 0 && $app.followup_judgment_list.judgment_type != 4}-->
										@if($followup_judgment_list['judgment_type'] != 0)
												<dt><span>運動</span></dt>
												<dd>
													<!--{$app.followup_judgment_list.judgment_detail}-->
													{{ $followup_judgment_list['judgment_detail'] }}
												</dd>

												<dt><span>食事</span></dt>
												<dd>
													<!--{$app.followup_judgment_list.judgment_calorie}-->
													{{ $followup_judgment_list['judgment_calorie'] }}
												</dd>
										@else		
										<!--{else}-->
												<dd>
												<!--{$app.followup_judgment_list.judgment_detail}-->
												{{ $followup_judgment_list['judgment_detail'] }}
												</dd>
										@endif		
										<!--{/if}-->


										<!--{if isset($app.followup_judgment_list.target_detail) }-->
										@if(isset($followup_judgment_list['target_detail']))
												<dt><span>今後の取り組み</span></dt>
												<dd>
													<!--{$app.followup_judgment_list.target_detail}-->
													{{ $$followup_judgment_list['target_detail'] }}
												</dd>
										@endif		
										<!--{/if}-->
									</dl>
							@endif		
							<!--{/if}-->

							<!--{include file="instruction_followup2.tpl"}-->
							@include('interview.instruction_followup2', array('view_type' => $view_type, 'followup_data' => $followup_data, 'interview_data' => $interview_data))

							<div class="btm"><img src="{{ URL::to('/') }}/img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>

						</div><!-- /#table-wrap -->
					</div><!-- /#followupDetail -->
			@endif		
			<!--{/if}-->
		</div><!-- /#content -->

		<div class="buttonArea">
			<ul>
				<li class="btn_back"><a href="Javascript:void(0)" onclick="history.go(-1);return false;">一覧へ戻る</a></li>
			</ul>
		</div>

	</div><!-- /#main -->
</div><!-- /.contents -->
