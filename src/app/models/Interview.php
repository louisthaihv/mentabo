<?php

class Interview extends Eloquent {

	public static function selectInterview($where) {
		return DB::table('t_user')->join('t_interview', 't_user.user_id', '=', 't_interview.user_id')->where($where)->orderBy('interview_id', 'desc')->first(array('*', 't_user.tenant_id', 't_user.email', 't_user.email', 't_user.birth_date'));
	}

	public static function selectTenant($where) {

		return DB::table('m_tenant')->where($where)->first();
	}
	public static function updateStep($user_id, $step_id)
	{	
		return DB::table('t_user')->where('user_id', $user_id)->update(array('interview_step' => $step_id));
	}

	public static function update_interview($interview_id, $user_id)
	{	
		return DB::table('t_interview')->where('interview_id', $interview_id)->where('user_id', $user_id)->update(array('start_datetime' => date('Y-m-d H:i:s'), 'finished_datetime' => date('Y-m-d H:i:s'), 'input_raw_data' => '', 'section_set_by_page' => ''));
	}

	public static function update_interviewCompleteFlag($user_id, $interview_step, $interview_complete_flag)
	{	
		return DB::table('t_user')->where('user_id', $user_id)->update(array('interview_step' => $interview_step, 'interview_complete_flag' => $interview_complete_flag));
	}

	public static function update_inputRawData($interview_id, $user_id, $input_raw_data)
	{	
		return DB::table('t_interview')->where('interview_id', $interview_id)->where('user_id', $user_id)->update(array('input_raw_data' => $input_raw_data));
	}

	public static function update_sectionSetByPage($interview_id, $user_id, $section_set_by_page)
	{	
		return DB::table('t_interview')->where('interview_id', $interview_id)->where('user_id', $user_id)->update(array('section_set_by_page' => $section_set_by_page));
	}

	public static function getSectionSetContentsById($section_set_id, $selectUser) {

		return DB::table('m_section_set_contents')
            ->join('m_section', 'm_section_set_contents.section_id', '=', 'm_section.section_id')
            ->where('m_section_set_contents.section_set_id', $section_set_id)
            ->where('m_section_set_contents.flg_display', 1)
			->where( function ($query) use ($selectUser) { $query->whereNull('target_gender')->orWhere( 'target_gender', '=', $selectUser['gender']); }) 
			->where( function ($query) use ($selectUser) { $query->whereNull('target_age_from')->orWhere( 'target_age_from', '<=', $selectUser['years_old']); })
			->where( function ($query) use ($selectUser) { $query->whereNull('target_age_to')->orWhere( 'target_age_to', '>=', $selectUser['years_old']); })
            ->get();
            //->toSql();
	}

	public static function getChoiceBySectionId($condition_string)
	{
		$list = array();
		$getChoiceBySectionId = DB::table('m_choice')->whereIn('section_id', $condition_string)->get();
		foreach ($getChoiceBySectionId as $key => $value) {
			$list[$value['choice_id']] = $value;
		}
		return $list;
	}	

	public static function getLayerControlByChoiceBySectionIdList($section_id_sql_condition)
	{
		$return =  \DB::table('m_layer_control_by_choice')
            ->whereExists(function($query) use ($section_id_sql_condition)
            {
                $query->select(\DB::raw('layer_id'))
                      ->from('m_choice')
                      ->whereIn('m_choice.section_id', $section_id_sql_condition)
                      ->whereRaw('m_choice.layer_id = m_layer_control_by_choice.layer_id');
            })
            ->get();     

        $list = array();    
		foreach ($return as $key => $value) {
			$list[] = $value;
		}
		return $list;
	}

	public static function insertChoiceSelection($interview_id, $rows)
	{
		foreach ($rows as $data) {
		
			$string['interview_id'] = $interview_id;

			if(isset($data['choice_id'])) {
				$string['choice_id'] = $data['choice_id'];
			} else {
				$string['choice_id'] = '0';	
			}
				
			if (isset($data['selection'])) {
				$string['selection'] = 1;
			} else {
				$string['selection'] =  0;
			}	

			if (isset($data['input_float'])) {
				$string['input_float'] = $data['input_float'];
			} else {
				$string['input_float'] = 'null';
			}

			if (isset($data['input_datetime'])) {
				$string['input_datetime'] = $data['input_datetime'];
			} else {
				$string['input_datetime'] = 'null';
			}

			if (isset($data['input_string'])) {
				$string['input_string'] = $data['input_string'];
			} else {
				$string['input_string'] = 'null';
			}

			DB::table('t_choice_selection')->insert($string);
		}	
	}

	public static function judgeByInterviewId($interview_id)
	{
		return DB::table('t_choice_selection')
        ->join('m_choice', function($join) use ($interview_id)
        {
            $join->on('t_choice_selection.choice_id', '=', 'm_choice.choice_id')->where('t_choice_selection.interview_id', '=', $interview_id);
        })
        ->get();
	}

	public static function selectFoodDataList()
	{
		$m_food_info = DB::table('m_food_info')->select('FoodID', 'UnitWeight', 'Col_001', 'Col_003', 'Col_004', 'Col_005', 'Col_020', 'Col_023', 'Col_043', 'Col_044', 'Col_051', 'Col_058', 'Col_059', 'Col_070')->get();
        foreach ($m_food_info as $key => $value) {
        	$list[$value['FoodID']] = $value;
        }
		return $list;
	}

}

?>