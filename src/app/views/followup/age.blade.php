<?php if(isset($error)){ ?>
<fieldset class="error" style="display: block">
<legend>エラー</legend>
	<ul>
		<li>生年月日に誤りがあります。</li>
	</ul>
</fieldset>
<?php } ?>

<fieldset class="error">
<legend>エラー</legend>
	<ul>
		<li class="birth_date_y">生年月日（年）を正しく入力してください</li>
		<li class="birth_date_m">生年月日（月）を正しく入力してください</li>
		<li class="birth_date_d">生年月日（日）を正しく入力してください</li>
	</ul>
</fieldset>

<input type="hidden" name="input_type" value="confirm" />
<h3>生年月日</h3>
<p class="lead"><span><img src="{{ URL::to('/') }}/img/icon_required.png" alt="必須" /></span>(例)1962年8月10日</p>
<div class="choices">

		<?php
			$current_year = date('Y');
			$c1 = $current_year - 100;
			$c2 = $current_year - 18;
			$birth_date = explode('-', $birth);
		?>

		<select id="c_10002" name="birth_date_y">
			<option value="" >---</option>
			<?php for ($y = $c1; $y <= $c2; $y++) {?>
			<option value="<?php echo $y ?>" <?php if($y==$birth_date[0]) echo 'selected' ?>><?php echo $y ?></option>
			<?php } ?>
		</select>年

		<select id="c_10003" name="birth_date_m">
			<option value="" >---</option>
			<?php for ($m = 1; $m <= 12; $m++) {?>
			<option value="<?php if($m < 10) echo '0' ?><?php echo $m ?>"  <?php if($m==$birth_date[1]) echo 'selected' ?>><?php echo $m ?></option>
			<?php } ?>
		</select>月

		<select id="c_10004" name="birth_date_d">
			<option value="" >---</option>
			<?php for ($d = 1; $d <= 31; $d++) {?>
			<option value="<?php if($d < 10) echo '0' ?><?php echo $d ?>"  <?php if($d==$birth_date[2]) echo 'selected' ?>><?php echo $d ?></option>
			<?php } ?>
		</select>日

		<div class="btn-area">
			<input type="hidden" name="exercise_value" value="{{ $exercise_value  }}">
			<input type="submit" id="action_followup_user_confirm" name="action_followup_user_confirm" value="次へ">
		</div>
</div>

<script>
$(document).ready(function(){ 

	$( "#form_main" ).submit(function() {

		var birth_date_y = $('select[name=birth_date_y]').val();
		var birth_date_m = $('select[name=birth_date_m]').val();
		var birth_date_d = $('select[name=birth_date_d]').val();

		$('.birth_date_y, .birth_date_m, .birth_date_d').hide();
		if(birth_date_y == '' || birth_date_m == '' || birth_date_d ==''){
			$('.error').show();
			return false;	
		} 
		if(birth_date_y == '') $('.birth_date_y').show();
		if(birth_date_m == '') $('.birth_date_m').show();
		if(birth_date_d == '') $('.birth_date_d').show();


	  	
	});

});
</script>

<style>
.error, .birth_date_y, .birth_date_m, .birth_date_d {
	display: none;
}

</style>