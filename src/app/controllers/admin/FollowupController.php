<?php

namespace Admin;

use View;
use Input;
use Auth;
use Session;
use Followup;

class FollowupController extends \BaseController {

	public function getDetail($interview_id, $type, $followup_interview_id)
	{
		\FollowupJudgeData::globals();
		$first_interview_id = $interview_id;
		// 初回問診IDから、実施済みの最新フォローアップ問診データを取得
		// $newest_followup_data = $followup_manager->getFollowupByFirstInterviewId($first_interview_id);
		$newest_followup_data = Followup::getFollowupByFirstInterviewId($first_interview_id);
		// if(Ethna::isError($newest_followup_data)){
		// 	$this->ae->add( null, $newest_followup_data->getMessage());
		// }


		// 指定されたフォローアップ問診IDが最新かチェック
		if ($newest_followup_data['followup_interview_id'] == $followup_interview_id) {
			// 最新データの場合、編集可
			$edit_flg = 1;
		}else{
			// 最新データで無い場合、編集不可
			$edit_flg = 0;
		}
		//$this->af->setApp('edit_flg', $edit_flg);

		//フォローアップ問診IDを取得
		if(empty($followup_interview_id)){
			//$followup_interview_id = $this->session->get('followup_interview_id');
		}
		if(empty($followup_interview_id)){
			// $this->ae->add( null, "フォローアップ問診IDが存在しません");
			// return 'admin_trouble';
		}

		// 対象のフォローアップ診断結果を取得
		//$followup_data = $followup_manager->getFollowup($followup_interview_id);
		$followup_data = Followup::getFollowup($followup_interview_id);
		//print_r($followup_data['first_interview_id']);die;
		//print_r($followup_data);
		// if(Ethna::isError($followup_data)){
		// 	$this->ae->add( null, $followup_data->getMessage());
		// }
		//$this->af->setApp('followup_data', $followup_data);

		// 問診パターンを取得
		//$c_followup_type	= $this->config->get('followup_type'); // フォローアップ問診ID設定
		//$section_set_id		= array_search($followup_data['type'], $c_followup_type);
		$section_set_id	= $this->followupType($followup_data['type']);
		// if (empty($section_set_id)) {
		// 	$this->ae->add( null, "問診パターンエラー");
		// 	return 'admin_trouble';
		// }

		//------------------------------------------------
		// 問診情報からフォローアップ問診データを取得
		//------------------------------------------------
		$interview_data =  Followup::getInterviewByFollowupId($followup_data['first_interview_id']);
		// if(Ethna::isError($interview_data)){
		// 	$this->ae->add(null, "問診IDが存在しません");
		// 	return 'admin_trouble';
		// }

		// 年齢を算出
		// $age = Ax_Util::calcAge($interview_data['start_datetime'], $interview_data['birth_date']);
		// $interview_data['age'] = $age;
		// $this->af->setApp('interview_data', $interview_data);
		$interview_data['age'] = $interview_data['years_old'];

		//------------------------------------------------
		// フォローアップ回答内容取得
		//------------------------------------------------
		// 問診IDから選択されたデータを取得
		//$choice_selection_data = $followup_manager->getChoiceSelectionByInterviewId($followup_interview_id);
		$choice_selection_data = Followup::getChoiceSelectionByInterviewId($followup_interview_id);
		// if(Ethna::isError($choice_selection_data)){
		// 	$this->ae->add( null, $first_interview_data->getMessage());
		// }

		// フォローアップ問診の回答内容を取得
		$choice_interview_list = $this->choiceByFollowupInterviewId($choice_selection_data, $section_set_id);
		// if(Ethna::isError($choice_interview_list)){
		// 	$this->ae->add( null, $choice_interview_list->getMessage());
		// }
		// $this->af->setApp('choice_interview_list', $choice_interview_list);

		//------------------------------------------------
		// フォローアップ問診の診断結果を取得
		//------------------------------------------------
		$followup_judgment_list = $this->judgeDisplayByFollowupId($followup_data);
		// if(Ethna::isError($followup_judgment_list)){
		// 	$this->ae->add( null, $followup_judgment_list->getMessage());
		// }
		//$this->af->setApp('followup_judgment_list', $followup_judgment_list);


		// //権限による操作の制限を設定
		// parent::preforward();
		// $editFlg = parent::roleAuthCheck('followup', 'edit');
		// print_r($editFlg);die;
		// $this->af->setApp('editFlg', $editFlg);

		// // 履歴印刷用
		// $this->af->setApp('absolute_url',		AX_ABSOLUTE_URL);
		// //Straight 20130909 followup から admin_followup に変更
		// $this->af->setApp('view_type',			'admin_followup');

		$editFlg = 1;
		$pagecount = 1;
		$view_type = 'admin_followup';

		$content = View::make('admin.followup.detail', compact('edit_flg', 'editFlg', 'view_type', 'pagecount', 'interview_data', 'followup_data', 'choice_interview_list', 'followup_judgment_list'));
		return View::make('admin.content', compact('setTitle', 'content'));

	}

	public function getEdit($first_interview_id, $type, $followup_interview_id, $page)
	{
		/*******************************************************
		 * 初期表示の際に表示する何週目(xxW)の設問を取得し、
		 * セッションに保持しておく
		 *******************************************************/
		//$type					= $this->af->get('type');							// 問診タイプ取得（xxW）
		//$followup_interview_id	= $this->af->get('followup_interview_id');			// フォローアップ問診ID
		//$first_interview_id		= $this->af->get('first_interview_id');				// 初回問診ID
		//$pagecount				= $this->af->get('pagecount');						// ページ数カウント

		// マネージャ定義
		//$followup_manager = $this->backend->getManager('followup');

		// 初回問診IDより最新のフォローアップ問診IDを取得
		//$followup_data = $followup_manager->getFollowupByFirstInterviewId($first_interview_id);
		$followup_data = Followup::getFollowupByFirstInterviewId($first_interview_id);
		// if (Ethna::isError($followup_data)) {
		// 	$this->ae->add('null', $followup_data->getMessage());
		// 	return 'admin_trouble';
		// }
		if ($followup_data['followup_interview_id'] != $followup_interview_id) {
			//$this->ae->add( null, "最新のフォローアップ問診ではありません。");
			//return 'admin_trouble';
		}

		//--------------------------------------------
		// 問診パターンID管理マスタよりデータを取得
		//--------------------------------------------
		// $c_followup_type	= $this->config->get('followup_type');			// フォローアップ問診ID設定
		// $section_set_id		= array_search($type, $c_followup_type);
		$section_set_id	= $this->followupType($followup_data['type']);
		// if (empty($section_set_id)) {
		// 	$this->ae->add( null, "問診パターンエラー");
		// 	return 'admin_trouble';
		// }

		// 指定されたフォローアップ問診データを取得

		// マネージャ定義
		//$followup_manager = $this->backend->getManager('followup');

		// ユーザ情報を取得
		//$user_data = $followup_manager->getInterviewByFollowupId($first_interview_id);
		$user_data = Followup::getInterviewByFollowupId($first_interview_id);
		// 年齢計算
		// $birth_date_y = date('Y', $user_data['birth_date']);
		// $birth_date_m = sprintf("%02d", date('m', $user_data['birth_date']));
		// $birth_date_d = sprintf("%02d", date('d', $user_data['birth_date']));
		// $birth_date = implode("-", array($birth_date_y, $birth_date_m, $birth_date_d));
		// $user_data['age'] = Ax_Util::calcAge(date("Y-m-d"), $birth_date);
		$user_data['age'] = $user_data['years_old'];

		// フォローアップ問診IDから、前回のフォローアップデータ取得
		// 2W以降、前回の判定結果によって出力問題を変更するため判定結果を取得
		//$last_followup_data = $followup_manager->getFollowupByFollowupInterviewId($first_interview_id, $followup_interview_id);
		$last_followup_data = Followup::getFollowupByFollowupInterviewId($first_interview_id, $followup_interview_id);
		
		// if(Ethna::isError($last_followup_data)){
		// 	$this->ae->add( null, $last_followup_data->getMessage());
		// }
		if (empty($last_followup_data)) {
			// データが存在しなかった場合、編集しようとしているデータが初回（2週目）のデータのため、対象問診IDからデータを取得する
			//$last_followup_data = $followup_manager->getFollowup($followup_interview_id);
			//$last_followup_data = Followup::getFollowup($followup_interview_id);
			// if(Ethna::isError($last_followup_data)){
			// 	$this->ae->add( null, $last_followup_data->getMessage());
			// }
		}

		//選択した問診タイプの設問ID一覧取得
		//$section_list = $followup_manager->getSectionSetContentsByFollowupId($section_set_id, $user_data, $last_followup_data, $type);
		$section_list = Followup::getSectionSetContentsByFollowupId($section_set_id, $user_data, $last_followup_data, $type);
		//print_r($section_list);
		// if (Ethna::isError($section_list)) {
		// 	$this->ae->add('null', $section_list->getMessage());
		// 	return 'followup_trouble2';
		// }

		//ページごとに並び替え
		$section_set_update_by_page = array();
		foreach ($section_list as $section) {
			$section_set_update_by_page[$section['page_num']][$section['order_section']] = $section['section_id'];
		}
		Session::put('section_set_update_by_page', $section_set_update_by_page);
		// マネージャ定義
		//$followup_manager = $this->backend->getManager('Followup');

		// 初回問診データを取得
		// $interview_data = $followup_manager->getInterviewByFollowupId($first_interview_id);
		$interview_data = Followup::getInterviewByFollowupId($first_interview_id);
		// if(Ethna::isError($interview_data)){
		// 	$this->ae->add( null, $interview_data->getMessage());
		// }
		// 年齢計算
		// $birth_date_y = date('Y', $interview_data['birth_date']);
		// $birth_date_m = sprintf("%02d", date('m', $interview_data['birth_date']));
		// $birth_date_d = sprintf("%02d", date('d', $interview_data['birth_date']));
		// $birth_date = implode("-", array($birth_date_y, $birth_date_m, $birth_date_d));
		// $interview_data['age'] = Ax_Util::calcAge(date("Y-m-d"), $birth_date);
		$interview_data['age'] = $interview_data['years_old'];

		//------------------------------------------------
		// フォローアップ回答内容取得
		//------------------------------------------------
		// 問診IDから選択されたデータを取得
		//$choice_selection_data = $followup_manager->getChoiceSelectionByInterviewId($followup_interview_id);
		$choice_selection_data = Followup::getChoiceSelectionByInterviewId($followup_interview_id);

		// if(Ethna::isError($choice_selection_data)){
		// 	$this->ae->add( null, $first_interview_data->getMessage());
		// }
		// $this->af->setApp('input_data', $choice_selection_data);

		// 選択された問診IDから運動内容、運動強度を取得
		if ($last_followup_data['week'] == 2) {
			// 2週目の場合、初回問診時に判定された運動強度判定値を取得
			// 初回問診の指導内容を取得
			$guidance_data = Followup::getGuidanceByInterviewId($first_interview_id);
			// if(Ethna::isError($guidance_data)){
			// 	$this->ae->add( null, $guidance_data->getMessage());
			// }
			$exercise_value = $guidance_data['exercise_value'];

		}else{

			// 運動種目を取得
			\FollowupMasterData::globals();
			foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $key => $val){
				if (!empty($choice_selection_data[$key])){
					$exercise_value = $val['exercise_value'];
				}
			}
			// 選択内容の表示非表示を制御するために前回のデータを取得
			if ($followup_data['type'] >= 4) {
				$last_followup_data_list = Followup::getFollowupListByFirstInterviewId($first_interview_id);
				// if(Ethna::isError($last_followup_data_list)){
				// 	$this->ae->add( null, $last_followup_data->getMessage());
				// }
				$last_followup_data_tmp	= array_reverse($last_followup_data_list);
				$last_followup_data		= $last_followup_data_tmp[1];
			}
		}
		//if (!empty($section_set_by_page[$page])) {

		//表示用の設問情報を取得
		//$followup_manager = $this->backend->getManager('followup');
		$page = empty($page)? 1: $page;
		$section_list = array();
		$layer_list = array();
		foreach ($section_set_update_by_page[$page] as $section_id) {
			list($section_data, $layer_list_show) = $this->_getSectionDataToShowById($section_id, $exercise_value);
			// if (Ethna::isError($section_data)){
			// 	$this->ae->add('null', $section_data->getMessage());
			// }
			$section_list[] = $section_data;
			$layer_list += $layer_list_show;
		}

		list($jquery_requiredCheck, $jquery_regexpCheck) = $this->_createJqueryInputCheck($section_list);
		$jqueryArray['jquery_display'] = $this->_createJqueryDisplay($section_list, $type);
		$jqueryArray['jquery_ui'] = $this->_createJqueryUI($section_list, $followup_data, $interview_data, $type, $choice_selection_data, $last_followup_data);
		$jqueryArray['jquery_requiredCheck'] = $jquery_requiredCheck;
		$jqueryArray['jquery_regexpCheck'] = $jquery_regexpCheck;

		//}	
		foreach ($choice_selection_data as $key => $value) {
			$input_data[$value['choice_id']] = $value['choice_id'];
		}
		$question = View::make('admin.followup.include_display_questions', compact('section_list', 'section_set_id', 'input_data'));
		$content = View::make('admin.followup.edit', compact('question', 'jqueryArray', 'followup_data', 'interview_data', 'followup_sports', 'section_list', 'type', 'page'));
		return View::make('admin.content', compact('setTitle', 'content'));

	}

	public function getUpdate($first_interview_id, $type, $followup_interview_id, $page)
	{
		// 問診完了画面
		//マネージャ定義
		//$followup_manager = $this->backend->getManager('Followup');

		// フォローアップ問診の回答が保存されていた場合、新しく入力されたデータへ更新する
		foreach ($_REQUEST as $key=>$value) {
			$followup_update_session[$key] = $value;
		}
		//$section_set_update_by_page	= $this->session->get('section_set_update_by_page');	// フォローアップ問診内容
		$section_set_update_by_page = Session::get('section_set_update_by_page');	// フォローアップ問診内容


		// フォローアップ問診内容がsessionに残っているかチェック
		$section_id_sql_condition = '';
		foreach ($section_set_update_by_page as $page => $page_section_set_list) {
			foreach ($page_section_set_list as $section_id) {
				if (!empty($section_id_sql_condition)) {
					$section_id_sql_condition .= ', ';
				}
				$section_id_sql_condition .= $section_id;
			}
		}
		$section_id_sql_condition = explode(', ', $section_id_sql_condition);

		//対象設問の全選択肢取得
		$choice_list = Followup::getChoiceBySectionId($section_id_sql_condition);
		// $choice_list = $followup_manager->getChoiceBySectionId($section_id_sql_condition);
		// if(Ethna::isError($choice_list)){
		// 	$this->ae->add( null, $choice_list->getMessage());
		// 	return 'followup_trouble2';
		// }

		//判断元選択肢のID一覧取得
		$layer_control_list = Followup::getLayerControlByChoiceBySectionIdList($section_id_sql_condition);
		//$layer_control_list = $followup_manager->getLayerControlByChoiceBySectionIdList($section_id_sql_condition);
		// if(Ethna::isError($layer_control_list)){
		// 	$this->ae->add( null, $layer_control_list->getMessage());
		// 	return 'followup_trouble2';
		// }

		//layer_id, group_noをキーに並び替え
		$upper_id_list = array();
		foreach ($layer_control_list as $value) {
			$upper_id_list[$value['layer_id']]['all_or_every'] = $value['all_or_every'];
			$upper_id_list[$value['layer_id']]['group'][$value['group_no']][] = $value;
			$upper_id_list[$value['layer_id']]['show_type'] =  $value['show_type'];
		}

		// 初回問診情報を取得
		//$first_interview_id = $this->af->get('first_interview_id');
		$guidance_data = Followup::getGuidanceByInterviewId($first_interview_id);
		//$guidance_data = $followup_manager->getGuidanceByInterviewId($first_interview_id);
		// if(Ethna::isError($guidance_data)){
		// 	$this->ae->add( null, $guidance_data->getMessage());
		// }

		// 前回のフォローアップ問診データを取得
		// 問診が完了しているフォローアップ問診を全件取得
		$last_followup_data_list = Followup::getFollowupListByFirstInterviewId($first_interview_id);
		// $last_followup_data_list = $followup_manager->getFollowupListByFirstInterviewId($first_interview_id);

		// if(Ethna::isError($last_followup_data)){
		// 	$this->ae->add( null, $last_followup_data->getMessage());
		// }
		$last_followup_data_list_temp	= array_reverse($last_followup_data_list);
		$followup_data					= $last_followup_data_list_temp[0];		// 修正対象のデータ
		$last_followup_data				= $last_followup_data_list_temp[1];		// 前回のデータ

		// 12Wの修正の場合、以降のフォローアップ問診の問診タイプの判断が必要となる
		if ($followup_update_session['type'] == 12) {
			// ユーザ情報を取得
			$user_data = Followup::getInterviewByFollowupId($first_interview_id);
			//$user_data = $followup_manager->getInterviewByFollowupId($first_interview_id);
			// 年齢計算
			// $birth_date_y = date('Y', $user_data['birth_date']);
			// $birth_date_m = sprintf("%02d", date('m', $user_data['birth_date']));
			// $birth_date_d = sprintf("%02d", date('d', $user_data['birth_date']));
			// $birth_date = implode("-", array($birth_date_y, $birth_date_m, $birth_date_d));
			// $user_data['age'] = Ax_Util::calcAge(date("Y-m-d"), $birth_date);
			//$user_data['age'] = 

			// 現在のフェーズの2Wのデータを取得（特定の問診タイプのデータの最新を1件取得）
			// 判定結果に変更が無いか確認するために必要
			$followup_2w_data = Followup::getFollowupByFirstInterviewIdType($first_interview_id, 2);
			//$followup_2w_data = $followup_manager->getFollowupByFirstInterviewIdType($first_interview_id, 2);
			// if (Ethna::isError($followup_2w_data)) {
			// 	return $followup_2w_data;
			// }
		}
			
		//フォローアップ問診・回答データ修正
		// フォローアップ問診回答内容
		$this->dataRectification($followup_update_session,	$choice_list, $upper_id_list, $followup_data['week'], $guidance_data, $followup_data, $last_followup_data, $user_data, $followup_2w_data);
		return \Redirect::to('/admin/followup/detail/'.$first_interview_id.'/'.$type.'/'.$followup_interview_id);
		
	}

	public function followupType($week)
	{
		$arrayName = array('51' => '2', '52' => '4', '53' => '6', '54' => '8', '55' => '12', '56' => '24', '57' => '36', '58' => '48');
		foreach ($arrayName as $key => $value) {
			if($week == $value) return $key;
		}
		// section_set_id => week
	}



	// フォローアップ問診の回答を取得
	public function choiceByFollowupInterviewId($choice_selection_data, $section_set_id) {

		// 選択したデータから質問内容と回答を取得
		// SQL文の編集
		foreach ($choice_selection_data as $key => $val){
			if (empty($choice_list)) {
				$choice_list  = $val['choice_id'];
			}else{
				$choice_list .= ',' . $val['choice_id'];
			}
		}
		$choice_section_data = Followup::getSectionChoiceByChoiceIdSectionSetId($choice_list, $section_set_id);
		// if (Ethna::isError($choice_section_data)) {
		// 	return $choice_section_data;
		// }

		// 質問と回答結果の編集
		foreach ($choice_section_data as $key => $val){

			if ($val['section_id'] == '5012' || $val['section_id'] == '5006') {
				// どんな運動をどの程度していますか？
				if ($val['choice_id'] != '50144' && $val['choice_id'] != '50145' && $val['choice_id'] != '50070' && $val['choice_id'] != '50071') {
					$choice_detail = $val['choice_string'] . 'を週に' ;
				}elseif ($val['choice_id'] == '50144' || $val['choice_id'] == '50070'){
					$choice_detail .= $choice_selection_data[$val['choice_id']]['input_float'] . '回、' ;
				}elseif ($val['choice_id'] == '50145' || $val['choice_id'] == '50071'){
					$choice_detail .= floor($choice_selection_data[$val['choice_id']]['input_float'] / 60) . '時間' . $choice_selection_data[$val['choice_id']]['input_float'] % 60 . '分';
					$choice_interview_list[$key]['section_string']	= $val['section_string'];
					$choice_interview_list[$key]['choice_string']	= $choice_detail;
				}

			}else{
				$choice_interview_list[$key]['section_string']	= $val['section_string'];
				if (!empty($val['choice_string'])) {
					$choice_interview_list[$key]['choice_string']	= $val['choice_string'];
				}else{
					$choice_interview_list[$key]['choice_string']	= $choice_selection_data[$val['choice_id']]['input_float'] . 'Kg';
				}
			}
		}
		return $choice_interview_list;
	}

	// フォローアップ問診の回答を取得
	public function judgeDisplayByFollowupId($followup_data) {

		
		$followup_data['judgment_type'] = $followup_data['type'];
		// 判定結果を編集
		$judgment['judgment_type']	= $followup_data['judgment_type'];
		$judgment_detail			= $GLOBALS['FOLLOWUP_JUDGE'][$followup_data['judgment_type']]['judgment_detail'];

		switch ($followup_data['judgment_type']) {
			case 0:		// このまま運動を継続してください。
			case 4:		// 医療機関を受診して、このまま運動を続けていいか確認しましょう。
				$judgment['judgment_detail'] = $judgment_detail;
				break;

			case 1:		// 運動の時間をCHOISE_TIMEからPROPOSE_TIMEに減らして取り組んで見てください。

				//----------------------------------
				// 時間の編集
				//----------------------------------

				// 診断結果編集
				$input_hour		= floor($followup_data['input_time'] / 60);
				$input_minutes	= $followup_data['input_time'] % 60;
				$input_time = $input_hour . '時間' . $input_minutes . '分';

				// 診断結果編集
				$propose_hour		= floor($followup_data['propose_time'] / 60);
				$propose_minutes	= $followup_data['propose_time'] % 60;
				$propose_time = $propose_hour . '時間' . $propose_minutes . '分';

				// 診断結果の時間を置換
				$search		= array('CHOISE_TIME', 'PROPOSE_TIME');
				$replace	= array($input_time, $propose_time);
				$judgment['judgment_detail'] = str_replace($search, $replace, $judgment_detail);

				break;

			case 2:		// 週にPROPOSE_COUNT回、運動に取り組むようにしましょう。
				//---------------------------------------------
				// 診断結果を編集
				//---------------------------------------------
				// 診断結果内容
				$judgment['judgment_detail'] = str_replace('PROPOSE_COUNT', $followup_data['propose_count'], $judgment_detail);
				break;

			case 3:		// 現在の生活改善を目指す場合、PROPOSE_SPORTSのような運動に取り組んでみてください。

				// METS値から新しく提示する運動種目を取得
				foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $key => $val){
					if ($val['route'] == 1 && $val['mets'] == $followup_data['mets']) {
						if (empty($sports)) {
							$sports .= $val['sports_name'];
						}else{
							$sports .= '、' . $val['sports_name'];
						}
					}
				}
				// 新しく提示するスポーツが存在しなかった場合、
				// 対象のMETS値の運動内容が存在しなかった場合、運動強度判定の範囲内のスポーツを取得
				if (empty($sports)) {
					foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $key => $val){
						if ($val['route'] == 1 && $val['exercise_value'] == $followup_data['exercise_value']) {
							$followup_sports_exercise_value_list[$key] = $val;
						}
					}
				}
				foreach ($followup_sports_exercise_value_list as $key => $val){
					if ($val['mets'] <= $followup_data['mets']) {
						if (empty($sports)) {
							$sports .= $val['sports_name'];
						}else{
							$sports .= '、' . $val['sports_name'];
						}
					}
				}

				// 診断結果内容
				$judgment['judgment_detail'] = str_replace('PROPOSE_SPORTS', $sports, $judgment_detail);

				break;

		}

		if ($followup_data['judgment_type'] != 0 && $followup_data['judgment_type'] != 4) {
			// あわせて、食事のカロリーをPROPOSE_CALORIEkcalにするようにチャレンジしてみてください。
			$judgment_detail = $GLOBALS['FOLLOWUP_JUDGE_CALOROE'][0]['judgment_detail'];
			$judgment['judgment_calorie'] = str_replace('PROPOSE_CALORIE', $followup_data['propose_calorie'], $judgment_detail);
		}

		if ($followup_data['target_type'] != '') {
			// 今後の取り組み目標
			$judgment['target_detail'] = $GLOBALS['FOLLOWUP_TARGET'][$followup_data['target_type']]['target_detail'];
		}

		return $judgment;

	}


	/**
	 * 表示用の設問情報を取得
	 * Enter description here ...
	 * @param $section_id		：問診ID
	 * @param $exercise_value	：運動強度指導例
	 */
	public function _getSectionDataToShowById($section_id, $exercise_value)
	{
		//設問マスタ データ取得（m_section）
		//$condition_string = " WHERE section_id = " . mysql_real_escape_string($section_id) ." ";
		//$section_data = Ax_Util::getRow('m_section', $condition_string);
		$section_data = Followup::getSection($section_id);
		// if (Ethna::isError($section_data)) {
		// 	return $section_data;
		// }

		//設問に対するレイヤー情報を一気に取得（m_layer）
		//$condition_string = " WHERE section_id = '".$section_id."' ";
		//$condition_string .= " ORDER BY order_in_section ";
		//$layer_list = Ax_Util::getRowList('m_layer', $condition_string);
		$layer_list = Followup::getLayer($section_id);
		// if (Ethna::isError($layer_list)) {
		// 	return $layer_list;
		// }

		$layer_inclusion_list = array();
		$layer_ctrl_list = array();
		$layer_list_show = array(); //setAppNE用
		foreach ($layer_list as $layer_data) {

			//レイヤー包括情報を取得（m_layer_inclusion）
			//$condition_string = " WHERE layer_id = '".$layer_data['layer_id']."' ";
			//$layer_inclusion_list_temp = Ax_Util::getRowList('m_layer_inclusion', $condition_string);
			$layer_inclusion_list_temp = Followup::getLayerInclusion($layer_data['layer_id']);
			// if (Ethna::isError($layer_inclusion_list_temp)) {
			// 	return $layer_inclusion_list_temp;
			// }

			if (!empty($layer_inclusion_list_temp)) {
				//$layer_inclusion_list[$layer_data['layer_id']] = $layer_inclusion_list_temp;	//レイヤーIDをキーに保存
				foreach ($layer_inclusion_list_temp as $layer_inclusion_data) {
					$layer_inclusion_list[] = $layer_inclusion_data;	//統合
				}
			}

			//レイヤー表示条件情報を取得（m_layer_control_by_choice）
			// $condition_string  = " WHERE layer_id = '".$layer_data['layer_id']."' ";
			// $condition_string .= " AND control_type = 'show' ";
			// $condition_string .= " ORDER BY group_no ";
			// $layer_ctrl_list_temp = Ax_Util::getRowList('m_layer_control_by_choice', $condition_string);
			$layer_ctrl_list_temp = Followup::getLayerControlByChoice($layer_data['layer_id']);
			// if (Ethna::isError($layer_ctrl_list_temp)) {
			// 	return $layer_ctrl_list_temp;
			// }
			if (!empty($layer_ctrl_list_temp)) {
				//レイヤー表示条件情報をグループ番号に分類
				$layer_ctrl_list_key_group_no = array();
				foreach ($layer_ctrl_list_temp as $layer_ctrl_data) {
					$layer_ctrl_list_key_group_no[$layer_ctrl_data['group_no']][] = $layer_ctrl_data;	//グループ番号ごとのレイヤー表示条件一覧
				}
				$layer_ctrl_list[$layer_data['layer_id']] = $layer_ctrl_list_key_group_no;	//レイヤーIDをキーに保存
			}

			$layer_list_show[$layer_data['layer_id']]['annotation'] = $layer_data['annotation'];
			$layer_list_show[$layer_data['layer_id']]['help_text'] = $layer_data['help_text'];
		}

		//レイヤー包括情報があれば、木構造に変換
		if (!empty($layer_inclusion_list)) {
			$layer_inclusion_tree = $this->_createInclusionTree($layer_inclusion_list);
		}

		//設問に対する選択肢情報を一気に取得（m_choice）
		// $condition_string = " WHERE section_id = '".$section_id."' ";
		// $condition_string .= "   AND (lead = '". $exercise_value."' ";
		// $condition_string .= "   OR lead = '') ";
		// $condition_string .= " ORDER BY order_in_layer ";
		// $choice_list = Ax_Util::getRowList('m_choice', $condition_string);
		$choice_list = Followup::getChoice($section_id, $exercise_value);
		// if (Ethna::isError($choice_list)) {
		// 	return $choice_list;
		// }
		//画像が存在するならばshow_imageフラグを立てる
		foreach ($choice_list as $idx => $choice_data) {
			//if (file_exists(AX_IMAGE_CHOICE_PATH . $choice_data['choice_id'] . '.png')) {
			if (file_exists($_SERVER['DOCUMENT_ROOT'].'/img/choice/'.$choice_data['choice_id'] . '.png')) {
				$choice_list[$idx]['show_image'] = 1;
			}
		}
		//$this->logger->log(LOG_NOTICE, "choice_list [" . print_r($choice_list, true) . "]");
		//選択肢をレイヤーIDに分類
		$choice_list_key_layer_id = array();
		foreach ($choice_list as $choice_data) {
			$choice_list_key_layer_id[$choice_data['layer_id']][] = $choice_data;	//レイヤーIDごとの選択肢一覧
		}

		//まとめ
		$layer_choice_list = array();
		foreach ($layer_list as $idx => $layer_data) {
			$layer_choice_list[$idx]['layer_data'] = $layer_data;
			$layer_choice_list[$idx]['choice_list'] = $choice_list_key_layer_id[$layer_data['layer_id']];
			if (!empty($layer_ctrl_list[$layer_data['layer_id']])) {
				$layer_choice_list[$idx]['layer_control_by_choice_list'] = $layer_ctrl_list[$layer_data['layer_id']];
			}
		}
		//$section_data['layer_list_key_layer_id'] = $layer_list_key_layer_id;
		if (!empty($layer_inclusion_tree)) {
			$section_data['layer_inclusion_tree'] = $layer_inclusion_tree;
			//layer_choice_listをレイヤーIDベースの配列に詰め代え
			foreach ($layer_choice_list as $data){
				$layer_choice_list_swapped[$data['layer_data']['layer_id']] = $data;
			}
			$layer_choice_list = $layer_choice_list_swapped;
		}

		$section_data['layer_choice_list'] = $layer_choice_list;

		return array($section_data, $layer_list_show);
	}

	//レイヤーの包括関係の配列を作成する
	public function _createInclusionTree($layer_inclusion_list)
	{
		$node = array();
		//詰め替え
		foreach ($layer_inclusion_list as $layer_inclusion_data) {
			$node[$layer_inclusion_data['layer_id_upper']][$layer_inclusion_data['layer_id']] =& $node[$layer_inclusion_data['layer_id']];
			$joined_node[] = $layer_inclusion_data['layer_id'];
		}
		//$this->logger->log(LOG_NOTICE, "node [" . print_r($node, true) . "]");
		//不要データをトリミングして値渡しコピー
		foreach ($node as $layer_id => $data_arr) {
			if (!in_array($layer_id, $joined_node)) {
				$inclusion_tree[$layer_id] = $data_arr;
			}
		}
		return $inclusion_tree;
	}

	/**
	 * 表示非表示のJquery生成
	 */
	public function _createJqueryDisplay($section_list, $type) {
		
		foreach ($section_list as $key1 => $section_data) {
			foreach ($section_data['layer_choice_list'] as $key2 => $layer_choice_data) {

				$layer_control_by_choice_list = $layer_choice_data['layer_control_by_choice_list'];
				if ($layer_control_by_choice_list) {
					$i = 0;
					foreach ($layer_control_by_choice_list as $group_no => $lcbc_list) {
						$count = count($layer_control_by_choice_list[$group_no]);
						$i++;
						if ($i == 1) {
							$jquery .= "if(";
						} else {
							$jquery .= " & ";
						}

						$j = 0;
						foreach ($lcbc_list as $idx_lcbc => $layer_control_by_choice_data) {
							$j++;
							if ($j == 1) {
							} elseif ($layer_control_by_choice_data['all_or_every'] === "all") {
								$jquery .= " & ";
							} elseif ($layer_control_by_choice_data['all_or_every'] === "every") {
								$jquery .= " | ";
							}
							$jquery .= "$(\"#c_".$layer_control_by_choice_data['choice_id_upper'].":checked\").val() == '".$layer_control_by_choice_data['choice_id_upper']."'";
						}
					}

					$reset = "";

					foreach ($layer_choice_data['choice_list'] as $choice_data) {
						$choice_id = $choice_data['choice_id'];
						$choice_type = $choice_data['choice_type'];
						$value_default = $choice_data['value_default'];
						$unit = $choice_data['choice_string_unit'];
						switch ($choice_type) {
							case "radio":
							case "checkbox":
							case "date":
							case "time":
							case "datetime":
							case "tab":
							case "accordion":
								break;
							case "string":
							case "numeric":
								if (empty($value_default)) {
									$value = "";
								} else {
									$value = $value_default;
								}
								$reset .= "$(\"#c_".$choice_id."\").val(\"".$value."\");";
								break;
							case "slider":
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"".$value_default.$unit."\");";
								$reset .= "$(\"#c_".$choice_id."\").val(\"".$value_default."\");";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\",String(".$value_default."));";
								break;
							case "slider_hour":
								if ($choice_data['ui_type'] == "2") {
									$unit = "時間";
								} else {
									$unit = "時";
								}
								$floor_value = floor($value_default);
								if ($floor_value != $value_default) {
									$unit .= "半";
								}
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"".$floor_value.$unit."\"); ";
								$reset .= "$(\"#c_".$choice_id."\" ).val(".$value_default."); ";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(".$value_default.")); ";
								break;
							case "slider_minute":
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(".$value_default.") + \"時間\" + cal_min(".$value_default.") + \"分\");";
								$reset .= "$(\"#c_".$choice_id."\" ).val(".$value_default."); ";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(".$value_default.")); ";
								break;
							case "slider_show_by_step";
								$value = round($value_default / $choice_data['value_step']) / 10;
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"".$value.$unit."(".$value_default."g)\"); ";
								$reset .= "$(\"#c_".$choice_id."\").val(".$value_default."); ";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(".$value_default.")); ";
								break;
							case "year":
							case "month":
							case "day":
								if (empty($value_default)) {
									$value = "";
								} else {
									$value = $value_default;
								}
								$reset .= "$(\"#c_".$choice_id."\").val(\"".$value."\");";
								break;
							default:
								break;
						}
					}
					$jquery .= ") { ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']."\").show(\"slow\"); ";
					$jquery .= "} else { ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." input\").attr(\"checked\", false); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." input\").button(\"refresh\"); ";
					$jquery .= $reset;
//					$jquery .= "$(\"#s_".$section_data['section_id']."\").css(\"background-color\", \"#FFFFFF\"); ";
//					$jquery .= "$(\"#s_".$section_data['section_id']."\").css(\"background\", 'url(\"../img/h3_bg.gif\") no-repeat scroll 0 100% transparent'); ";
					$jquery .= "$(\"#s_".$section_data['section_id']."\"); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." h3\").css(\"background-color\", \"#FFFFFF\"); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." h3\").css(\"background\", 'url(\"../img/h3_bg.gif\") no-repeat scroll 0 100% transparent'); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']."\").hide(\"slow\"); ";
					$jquery .= "}"."\n";
				}
			}
		}
		return $jquery;
	}

	/**
	 * UIのJquery生成
	 */
//	function createJqueryUI($section_list) {
	public function _createJqueryUI($section_list, $followup_data, $interview_data, $type, $choice_selection_data, $last_followup_data = null) {

		//通勤時間の合計処理
		$s2034_choice_id_list = array(10164, 10165, 10166, 10423);

		// 質問の表示制御を行うための項目を取得

		if (!empty($last_followup_data)) {
			// mets値を取得
			$mets = $last_followup_data['mets'];

			// 提示した運動時間を取得
			if ($last_followup_data['propose_time'] != '') {
				$time = $last_followup_data['propose_time'];
			}else{
				// 運動時間が提示されていなかった場合、入力した運動時間を取得
				$time = $last_followup_data['input_time'];
			}

			// 提示した運動回数を取得
			if ($last_followup_data['propose_count'] != '') {
				$count = $last_followup_data['propose_count'];
			}else{
				// 運動時間が提示されていなかった場合、入力した運動時間を取得
				$count = $last_followup_data['input_count'];
			}

			// 提示した運動回数を取得
			if ($last_followup_data['propose_count'] != '') {
				$count = $last_followup_data['propose_count'];
			}else{
				// 運動時間が提示されていなかった場合、入力した運動時間を取得
				$count = $last_followup_data['input_count'];
			}
		}

		// 4W～48Wの場合で、[5006：どんな運動をどの程度していますか？］の質問を行っているか
		$question_flg = 0;
		if (!empty($choice_selection_data['50072']) || !empty($choice_selection_data['50073']) || !empty($choice_selection_data['50074']) || !empty($choice_selection_data['50075']) || !empty($choice_selection_data['50076'])){
			// 質問をしている
			$question_flg = 1;
		}

		foreach ($section_list as $idx => $section_data) {
			foreach ($section_data['layer_choice_list'] as $idx2 => $layer_choice_data) {
				$layer_type = "";
				foreach ($layer_choice_data['choice_list'] as $idx3 => $choice_data) {
					$input_name			= "i_".$choice_data['choice_id'];
					$input_layer_name	= "il_".$layer_choice_data['layer_data']['layer_id'];
					$choice_id			= $choice_data['choice_id'];
					$choice_type		= $choice_data['choice_type'];
					$ui_type			= $choice_data['ui_type'];
					switch ($choice_type) {
						case "radio":
							// 4W以降のフォローアップ質問の場合のみ出力
							if ($type >= 4 && $layer_choice_data['layer_data']['section_id'] == 5006){
//								if ($section_data === reset($section_list) && $layer_choice_data === reset($section_data['layer_choice_list']) && $choice_data === reset($layer_choice_data['choice_list'])) {
								if ($question_flg == 1) {
									$jquery .= " $(\"#l_5009\").show(\"slow\");";
								}
								if (empty($output_flg)) {
									/**********************************************************************************************************************
										4W～48Wの場合
										［5006：どんな運動をどの程度していますか？］の質問に対し以下の条件の場合にのみ
										次の質問［5007：運動内容で支障になることがありますか？］の表示を行う

										【表示条件】
											(簡易版)「種目何でも、週3回以上、1回に10分以上」
											(通常版)【指導運動】項目の出力内容
											(FULL版)【指導運動】項目の出力内容
											上記内容と［5006：どんな運動をどの程度していますか？］で回答された内容を比較し、未満の場合に質問する。
									**********************************************************************************************************************/
									if ($interview_data['section_set_id'] == '33'){

										$jquery .= "$('input[name=\"il_5006\"]:radio' ).change( function() {  ";
										$jquery .= "	var count = 3;";
										$jquery .= "	if($(\"#c_50070\").val() >= count) {";
//										$jquery .= "		alert($(this).val() + '運動している radio');";
										$jquery .= "		$(\"#l_5009\").hide(\"slow\");";
										$jquery .= "	} else if($(\"#c_50070\").val() < count) {";
//										$jquery .= "		alert($(this).val() + '運動していない');";
										$jquery .= "		$(\"#l_5009\").show(\"slow\");";
										$jquery .= "	}";
										$jquery .= "});";

										$output_flg = 1;

									}elseif ($interview_data['section_set_id'] == '31' || $interview_data['section_set_id'] == '32'){
										$jquery .= "$('input[name=\"il_5006\"]:radio' ).change( function() {  ";
//										$jquery .= "	alert($(this).val()); ";
										$jquery .= "	var mets = eval('$(\"#' + $(this).val() + '\").val()');";
										$jquery .= "	if(mets < " . $mets . " || $(\"#c_50070\").val() < " . $count . " || $(\"#c_50071\").val() < " . $time . ") {";
//										$jquery .= "		jAlert(mets + '提示したMETS値より低い');";
										$jquery .= "		$(\"#l_5009\").show(\"slow\");";
										$jquery .= "	}else{";
//										$jquery .= "		jAlert(mets + '提示したMETS値より高い	');";
										$jquery .= "		$(\"#l_5009\").hide(\"slow\");";
										$jquery .= "	}";
										$jquery .= "});";

										$output_flg = 1;
									}
								}
							}
						case "checkbox":
						case "date":
						case "time":
						case "datetime":
						case "string":
						case "year":
						case "month":
						case "day":
						case "tab":
						case "accordion":
							break;
						case "slider":
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							// 選択された内容を表示
							if (!empty($choice_selection_data[$choice_id])) {
								$jquery .= "	value: ".$choice_selection_data[$choice_id]['input_float'].",";
							}else{
								$jquery .= "	value: ".$choice_data['value_default'].",";
							}
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	change: function( event, ui ) {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + ui.value + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(ui.value);";

							/**********************************************************************************************************************
								4W～48Wの場合
								［5006：どんな運動をどの程度していますか？］の質問に対し以下の条件の場合にのみ
								次の質問［5007：運動内容で支障になることがありますか？］の表示を行う

								【表示条件】
									(簡易版)「種目何でも、週3回以上、1回に10分以上」
									(通常版)【指導運動】項目の出力内容
									(FULL版)【指導運動】項目の出力内容
									上記内容と［5006：どんな運動をどの程度していますか？］で回答された内容を比較し、未満の場合に質問する。
							**********************************************************************************************************************/

							// ［50070：回数］の時のみ出力
							if ($type >= 4 && $choice_id == '50070'){

								if ($interview_data['section_set_id'] == '33'){
									// 簡易版
									$jquery .= "	var count = 3;";
									$jquery .= "	if(ui.value >= count && $(\"#c_50071\").val() >= 10) {";
//									$jquery .= "		jAlert(ui.value + '運動している slider');";
									$jquery .= "		$(\"#l_5009 input\").removeAttr(\"checked\");";
									$jquery .= "		$(\"#l_5009\").hide(\"slow\");";
									$jquery .= "	} else {";
//									$jquery .= "		jAlert(ui.value + '運動してない');";
									$jquery .= "		$(\"#l_5009\").show(\"slow\");";
									$jquery .= "	}";

								}elseif ($interview_data['section_set_id'] == '31' || $interview_data['section_set_id'] == '32'){
									// 通常版およびFULL版
									$jquery .= "	if($(\"input:radio[name='il_5006']:checked\").val()) {";
									$jquery .= "		var mets = eval('$(\"#' + $(\"input:radio[name='il_5006']:checked\").val() + '\").val()');";
									$jquery .= "		if(mets < " . $mets . " || $(\"#c_50070\").val() < " . $count . " || $(\"#c_50071\").val() < " . $time . ") {";
//									$jquery .= "			jAlert(mets + '提示したMETS値より低い');";
									$jquery .= "			$(\"#l_5009\").show(\"slow\");";
									$jquery .= "		}else{";
									$jquery .= "			$(\"#l_5009\").hide(\"slow\");";
									$jquery .= "		}";
									$jquery .= "	}else{";
//									$jquery .= "		jAlert(ui.value + '運動してない');";
									$jquery .= "		$(\"#l_5009\").hide(\"slow\");";
									$jquery .= "	}";
								}
							}

							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"".$choice_data['choice_string_unit']."\" );";
							$jquery .= "$(\"#c_".$choice_id."\").val($( \"#c_".$choice_id."_slider\").slider(\"value\"));";

							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\",String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\",String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "\n";
							break;
						case "slider_hour":
							if ($ui_type == "2") {
								$unit = "時間";
							} else {
								$unit = "時";
							}
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							$jquery .= "	value: ".$choice_data['value_default'].",";
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	slide: function( event, ui ) {";
							$jquery .= "		if(Math.floor(ui.value)==ui.value){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + Math.floor(ui.value) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + Math.floor(ui.value) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(ui.value);";
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\" ).val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"".$unit."\");";
							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";
							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "\n";
							break;
						case "slider_minute":
							//通勤時間の１日合計算出
							if ($choice_id == 10423) {
								$jquery .= "function sum_s_2034() {";
								$jquery .= "	amount = Math.max(0, Math.round((parseFloat($(\"#c_10164\").val()) + parseFloat($(\"#c_10165\").val()) + parseFloat($(\"#c_10166\").val()) + parseFloat($(\"#c_10423\").val())) * 10) / 10); ";
								$jquery .= "	$(\"#c_10448\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
								$jquery .= "};";
								$jquery .= "\n";
								$jquery .= "$(\"#c_10448\").val(\"2時間00分\");";
								$jquery .= "\n";
							}
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							// 選択された内容を表示
							if (!empty($choice_selection_data[$choice_id])) {
								$jquery .= "	value: ".$choice_selection_data[$choice_id]['input_float'].",";
							}else{
								$jquery .= "	value: ".$choice_data['value_default'].",";
							}
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	change: function( event, ui ) {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(ui.value) + \"時間\" + cal_min(ui.value) + \"分\");";
							$jquery .= "		$(\"#c_".$choice_id."\").val(ui.value);";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}

							// ［50071：時間］の時のみ出力
							if ($type >= 4 && $choice_id == '50071'){

								if ($interview_data['section_set_id'] == '33'){

									// 簡易版
									$jquery .= "		var count = 3;";
									$jquery .= "		if($(\"#c_50070\").val() >= count && $(\"#c_50071\").val() >= 10) {";
//									$jquery .= "			jAlert(ui.value + '運動している slider_minute');";
									$jquery .= "			$(\"#l_5009 input\").removeAttr(\"checked\");";
									$jquery .= "			$(\"#l_5009\").hide(\"slow\");";
									$jquery .= "		} else {";
//									$jquery .= "			jAlert(ui.value + '運動してない');";
									$jquery .= "			$(\"#l_5009\").show(\"slow\");";
									$jquery .= "		}";

								}elseif ($interview_data['section_set_id'] == '31' || $interview_data['section_set_id'] == '32'){

									// 通常版およびFULL版
									$jquery .= "		var count = 3;";
									$jquery .= "		if($(\"input:radio[name='il_5006']:checked\").val()) {";
									$jquery .= "			var mets = eval('$(\"#' + $(\"input:radio[name='il_5006']:checked\").val() + '\").val()');";
									$jquery .= "			if(mets < " . $mets . " || $(\"#c_50070\").val() < " . $count . " || $(\"#c_50071\").val() < " . $time . ") {";
//									$jquery .= "				jAlert(mets + '提示したMETS値より低い');";
									$jquery .= "				$(\"#l_5009\").show(\"slow\");";
									$jquery .= "			}else{";
									$jquery .= "					$(\"#l_5009\").hide(\"slow\");";
									$jquery .= "			}";
									$jquery .= "		} else if(ui.value < count) {";
//									$jquery .= "			jAlert(ui.value + '運動してない');";
									$jquery .= "			$(\"#l_5009\").hide(\"slow\");";
									$jquery .= "		}";
								}
							}

							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour($(\"#c_".$choice_id."_slider\").slider(\"value\")) + \"時間\" + cal_min($(\"#c_".$choice_id."_slider\").slider(\"value\")) + \"分\");";
							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";
							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
								$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
								$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;
								
							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
								$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "\n";
							break;
						case "slider_show_by_step":
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							$jquery .= "	value: ".$choice_data['value_default'].",";
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	slide: function(event, ui) {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(ui.value / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + ui.value + \"g)\");";
							$jquery .= "		$(\"#c_".$choice_id."\").val(ui.value);";
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") / ".$choice_data['value_step']." / 10 + \"".$choice_data['choice_string_unit']."(\"  + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"g)\");";
							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";
							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
							$jquery .= "	amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
							$jquery .= "	amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "});";
							$jquery .= "\n";
//							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
//							$jquery .= "	range: \"min\",";
//							$jquery .= "	value: ".$choice_data['value_default'].",";
//							$jquery .= "	min: ".$choice_data['value_min'].",";
//							$jquery .= "	max: ".$choice_data['value_max'].",";
//							$jquery .= "	step: ".$choice_data['value_step'].",";
//							$jquery .= "	slide: function(event, ui) {";
//							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(ui.value / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + ui.value + \"g)\");";
//							$jquery .= "		$(\"#c_".$choice_id."\").val(ui.value);";
//							$jquery .= "	}";
//							$jquery .= "});";
//							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") / ".$choice_data['value_step']." + \"".$choice_data['choice_string_unit']."(\"  + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"g)\");";
//							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";
//							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
//							$jquery .= "	amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
//							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
//							$jquery .= "});";
//							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
//							$jquery .= "	amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
//							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
//							$jquery .= "});";
//							$jquery .= "\n";
							break;
						default:
							break;
					}
				}
			}
		}
		return $jquery;
	}

	/**
	 * 入力チェックのJquery生成
	 */
	public function _createJqueryInputCheck($section_list) {
		$jquery_required = "";
		$jquery_regexp = "";
		foreach ($section_list as $key1 => $section_data) {
			if ($section_data['flg_must_input'] == 1) {
				foreach ($section_data['layer_choice_list'] as $key2 => $layer_choice_list) {
					if (!empty($layer_choice_list['choice_list'])) {
						$i = 0;
						foreach ($layer_choice_list['choice_list'] as $key3 => $choice_list) {
							$i++;
							$choice_type = $choice_list['choice_type'];
							$choice_id = $choice_list['choice_id'];

							$prefix = "";
							if ($i == 1) {
								$prefix = "if (";
							} else {
								$prefix = " && ";
							}

							$blPostfix = false;
							switch ($choice_type) {
								case "radio":
								case "checkbox":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id.":checked\").val() == undefined ";
									$blPostfix = true;
									break;
								case "string":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"\" ";
									$blPostfix = true;
									break;
								case "numeric":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && ($(\"#c_".$choice_id."\").val() == \"0\" || $(\"#c_".$choice_id."\").val() == \"\") ";
									$blPostfix = true;
									$jquery_regexp .= "if($(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id."\").val().length > 0) {";
									$jquery_regexp .= "	if($(\"#c_".$choice_id."\").val().match(/^(?:[1-9]\d*|0)(?:\.\d*[0-9])?$/g)) {";
									$jquery_regexp .= "		ok_section_ids.push(".$section_data['section_id'].");";
									$jquery_regexp .= "	} else {";
									$jquery_regexp .= "		tmp_error_text += \"<li>設問「".$section_data['section_string']."」には数値を入力してください</li>\";";
									$jquery_regexp .= "		error_section_ids.push(".$section_data['section_id'].");";
									$jquery_regexp .= "	}";
									$jquery_regexp .= "}"."\n";
									break;
								case "year":
								case "month":
								case "day":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"\" ";
									$blPostfix = true;
									break;
								case "slider":
								case "slider_minute":
								case "slider_show_by_step":
									$jquery_required .= $prefix."$(\"#c_".$choice_id."_show:visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"0\" ";
									$blPostfix = true;
									break;
								case "slider_hour":
									if ($choice_list['ui_type'] == "2") {
										$jquery_required .= $prefix."$(\"#c_".$choice_id."_show:visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"0\" ";
										$blPostfix = true;
									}
									break;
								case "date":
								case "time":
								case "datetime":
									//未使用
									break;
								case "tab":
								case "accordion":
									//見出し
									break;
								default:
									break;
							}
						}
						if ($blPostfix) {
							$jquery_required .= ") {";
							$jquery_required .= "	tmp_error_text += \"<li>設問「".$section_data['section_string']."」を入力してください</li>\";";
							$jquery_required .= "	error_section_ids.push(".$section_data['section_id'].");";
							$jquery_required .= "} else { ";
							$jquery_required .= "	ok_section_ids.push(".$section_data['section_id'].");";
							$jquery_required .= "}"."\n";
						}
					}
				}
			}
		}

		return array($jquery_required, $jquery_regexp);
	}

	/**
	 * フォローアップ問診・回答データ修正
	 * Enter description here ...
	 * @param $followup_update_session	：問診回答内容
	 * @param $choice_list				：フォローアップ設問の全選択肢取得
	 * @param $upper_id_list			：選択された時に表示する内容（判断元選択肢のID）
	 * @param $week						：経過週（x週目）
	 * @param $guidance_data			：初回問診情報
	 * @param $followup_data			：修正対象のフォローアップデータ
	 * @param $last_followup_data		：前回のフォローアップデータ
	 * @param $user_data				：ユーザ情報
	 * @param $followup_2w_data			：現在のフェーズの2Wのデータ
	 */
	public function dataRectification($followup_update_session, $choice_list, $upper_id_list, $week, $guidance_data, $followup_data, $last_followup_data, $user_data = null, $followup_2w_data = null)
	{

		//-----------------------------------------------------
		// 回答データ編集
		//-----------------------------------------------------
		/* 回答登録 */
		//判断元選択肢の選択有無確認用ID一覧作成（※判断元となる選択肢は、現状ではラジオボタン・チェックボックスのみ）
		$selected_choice_id_list = array();
		foreach ($followup_update_session as $input_name => $value) {
			if (strpos($input_name, 'il_') === 0) {
				if (is_array($value)) {
					foreach ($value as $choice_id) {
						$selected_choice_id_list[] = $choice_id;
					}
				} else {
					$selected_choice_id_list[] = $value;
				}
			}
		}

		$rows = array();
		foreach ($followup_update_session as $input_name => $value) {
			$insert_values = null;
			//チェックボックス・ラジオボタン
			if (strpos($input_name, 'il_') === 0) {
				if (!is_array($value)) {
					$choice_id_list = array($value);
				} else {
					$choice_id_list = $value;
				}
				foreach ($choice_id_list as $choice_id) {
					// 回答データ登録用データ作成
					$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list);
					if (!empty($insert_values)) {
						$rows[] = $insert_values;
					}
				}
				//入力値
			} elseif (strpos($input_name, 'i_') === 0) {
				$choice_id = substr($input_name, 2);
				$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list, $value);
				if (!empty($insert_values)) {
					$rows[] = $insert_values;
				}
			}
		}

		//-----------------------------------------------------
		// 問診結果再判定処理
		//-----------------------------------------------------
		//----------------------------------
		// フォローアップ判定
		//----------------------------------
		$followup_judgement_data = $this->judgeByFollowupRecord($rows, $guidance_data, $last_followup_data, $followup_data['week'], $followup_update_session['type']);


		//-----------------------------------------------------
		// データ更新処理
		//-----------------------------------------------------

		//----------------------------------
		// フォローアップ問診への回答更新
		//----------------------------------
		// 既存データ削除
		$result = Followup::deleteChoiceSelectionInterviewId($followup_update_session['followup_interview_id']);
		// if (Ethna::isError($result)) {
		// 	return $result;
		// }

		// 新規回答登録（一括insert）
		if (!empty($rows)) {
			//$result = $this->insertChoiceSelectionFollowup($followup_update_session['followup_interview_id'], $rows);
			$result = Followup::insertChoiceSelectionFollowup($followup_update_session['followup_interview_id'], $rows);
			// if (Ethna::isError($result)) {
			// 	return $result;
			// }
		}

		//----------------------------------
		// フォローアップ判定データ更新
		//----------------------------------
		// 更新データ編集
		// データが無い場合は、デフォルトと同様null値を登録する
 		$followuplist = array ('mets'				=> null,
		 					   'exercise_value'		=> null,
		 					   'propose_calorie'	=> null,
		 					   'current_weight'		=> null,
		 					   'target_type'		=> null,
		 					   'judgment'			=> null,
							   'judgment_type'		=> null,
							   'input_time'			=> null,
							   'propose_time'		=> null,
							   'input_count'		=> null,
							   'propose_count'		=> null,
							   'result'				=> null,
		);
		// 再判定で取得したデータを設定
		foreach ($followup_judgement_data as $key => $val){
			// 運動判定値のみ項目が違うのでkeyの判定を行う
			if ($key == 'propose_exercise_value') {
				$followuplist['exercise_value'] = $val;
			}else{
				$followuplist[$key] = $val;
			}
		}

		// 更新条件設定
		$where_data = array ('followup_interview_id' => $followup_update_session['followup_interview_id']);

		// 対象データ更新
		$result = Followup::updateFields('t_followup' , $followuplist , $where_data);
		//$result = $this->updateFields('t_followup' , $followuplist , $where_data);
		// if (Ethna::isError($result)) {
		// 	return $result;
		// }

		// 修正対象の問診のタイプが12Wの場合、12W以降のフォローアップ問診回数に変更がある場合があるので、
		// 修正前と修正後の結果に変更がないかチェックを行う
		if ($followup_update_session['type'] == 12 && $followup_judgement_data['result'] != $followup_data['result']) {

			// 既存データ削除
			$result = $this->deleteFollowupFirstInterviewId($followup_update_session['first_interview_id']);
			// if (Ethna::isError($result)) {
			// 	return $result;
			// }
			// フォローアップ基本登録用データ
			$insert_followup_values = $this->createFollowupInsertValue($user_data, $followup_judgement_data, $followup_2w_data, $week, $followup_update_session['type']);
			$insert_followup = Followup::insert_followup($insert_followup_values);
			// for ($i = 0; $i < count($insert_followup_values); $i++) {
			// 	$result = Ax_Util::createRow('t_followup', $insert_followup_values[$i]);
			// 	if (Ethna::isError($result)) {
			// 		return $result;
			// 	}
			// }
		}

		return true;
	}

	/**
	 * フォローアップ判定
	 * Enter description here ...
	 * @param $rows						：フォローアップ問診データ
	 * @param $guidance_data			：初回問診データ
	 * @param $last_followup_data		：前回のフォローアップ問診結果
	 * @param $week						：今回のフォローアップ経過週
	 * @param $type						：今回のフォローアップ問診タイプ
	 * @param $judgement_type			：判定タイプ（0：新規判定／1：修正判定）
	 */
	public function judgeByFollowupRecord($rows, $guidance_data, $last_followup_data, $week, $type, $judgement_type) {

		//------------------------------------------------------------
		// 判定データの編集
		//------------------------------------------------------------
		// 回答内容をsection_idをkeyにして詰め替える
		foreach ($rows as $key => $val) {

			if (!empty($val['selection'])) {

				// 指導された運動をはじめましたか？／現在も運動を続けていますか？
				if ($val['selection'] == '5001' || $val['selection'] == '5004' || $val['selection'] == '5005') {
					if ($val['choice_id'] == '50001' || $val['choice_id'] == '50008' || $val['choice_id'] == '50010') {
						// はい
						$followup_result = 1;
					}elseif($val['choice_id'] == '50002' || $val['choice_id'] == '50009' || $val['choice_id'] == '50011') {
						// いいえ
						$followup_result = 0;
					}
				}
				if ($val['section_id'] == '5006' || $val['section_id'] == '5012') {
					// 運動種目の選択の場合、設定ファイルより必要な情報を取得する。（Yルート：5006／Nルート：5012）
					$sports_item = $GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'][$val['choice_id']];
				}else{

					if ($val['section_id'] == $section_check) {
						$cnt++;
					}else{
						$section_check = $val['section_id'];
						$cnt = 0;
					}
					$judgment_material[$val['section_id']][$cnt] = $val['choice_id'];
				}

			}elseif(!empty($val['input_float'])){
				if($val['choice_id']=='50070' || $val['choice_id']=='50144'){
					// 運動回数取得（Yルート：50070／Nルート：50144）
					$input_exercise_count = $val['input_float'];

				}elseif($val['choice_id']=='50071' || $val['choice_id']=='50145'){
					// 一回の時間を取得（Yルート：50071／Nルート：50145）
					$input_exercise_time = $val['input_float'];

				}elseif($val['choice_id']=='50085' || $val['choice_id']=='50146'){
					// 現在の体重
					$input_current_weight = $val['input_float'];

				}
			}
		}


		//------------------------------------------------------------
		// 判定が免除される月または問診タイプかどうかをチェック
		//------------------------------------------------------------
//		if($type == 2 || $last_followup_data['exercise_value'] == 'E' ||
		if($type == 2 ||
		(date(n, strtotime($last_followup_data['interview_date'])) == '3'  ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '4'  ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '9'  ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '10' ||
		 date(n, strtotime($last_followup_data['interview_date'])) == '12') && $type == 4){

			// 判定が免除される月、運動強度(E)の場合、判定結果は「このまま運動を継続してください。」を固定で表示する
//			if ($type != 2 && $last_followup_data['exercise_value'] != 'E') {
			if ($type != 2) {
				$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][0]['judgment_type'];
			}
			// 判定が免除
			$special_exemption_flg = 1;

		}else{

			// 判定を行う
			$special_exemption_flg = 0;
		}


		//------------------------------------------------------------
		// 今回のフォローアップ問診を判定
		//------------------------------------------------------------

		// 指導された運動をはじめましたか？ または 現在も運動を続けていますか？

		// 判定に使用する値を取得
		if ($week >= 8) {

			if ($type >= 2 && $type <= 6) {
				// 経過週が2～6週目の場合、前回の問診で入力された体重を使用
				$weight	= $last_followup_data['current_weight'];
			}else{
				// 経過週が8週目以降は、フォローアップ問診で回答された体重を使用
				if (empty($input_current_weight)) {
					$weight	= $last_followup_data['current_weight'];
				}else{
					$weight	= $input_current_weight;
				}
			}
		}else{
			// 経過週が2～6週目の場合、初回問診で入力された体重を使用
			$weight	= $guidance_data['current_weight'];
		}

/*
		// 近日食事摂取カロリーを取得
		if ($week == 2) {
			// 経過週が2週目の場合、初回問診で算出されたカロリーを使用
			$calorie	= $guidance_data['current_calorie'];

		}else{
			// 経過週が4週目以降の場合、フォローアップ問診で提示されたカロリーを使用
			$calorie	= $last_followup_data['propose_calorie'];
		}
*/


		//--運動判定ここから（運動判定は判定が免除される月または問診タイプ2W以外はすべて実施する）--------------------------------------------------------------------------------------------------------------------------------------
		//------------------------------------------
		// 【運動】
		//------------------------------------------
//		if ($special_exemption_flg == 0) {
			// 判定材料となる質問をチェック
			foreach ($judgment_material as $key => $val) {

				if ($special_exemption_flg == 0) {

					//----------------------------------------------------
					// 続けることが難しい一番の理由は以下のどれですか？
					//----------------------------------------------------
					if($val[0] == '50005' || $val[0] == '50072'){				// 1：時間が取れない
						if($input_exercise_time >= 60){
							// 一回の時間が1時間（60分）以上の場合
							// 回答した時間（分換算）- 30分 = 新たに提示する時間
							$subtraction_value	= $GLOBALS['MAX_TIME_TO_SUBTRACT'];
						}else{
							// 一回の時間が1時間未満を入力していた場合
							// 回答した時間（分換算）- 10分 = 新たに提示する時間
							$subtraction_value	= $GLOBALS['MIN_TIME_TO_SUBTRACT'];
						}
						// 新たに提示する時間
						$propose_time = $input_exercise_time - $subtraction_value;
						if($propose_time < $subtraction_value){
							// 新たに提示する時間が減算した時間より下回った場合
							$propose_time = $subtraction_value;
						}
						$followup_judgement['propose_time']		= $propose_time;
						$followup_judgement['judgment_type']	= $GLOBALS['FOLLOWUP_JUDGE'][1]['judgment_type'];


					}elseif($val[0] == '50006' || $val[0] == '50073'){		// 2：回数が少なくなってしまう

						// 回答した運動回数 - 1回 = 新たに提示する運動回数
						$followup_judgement['propose_count'] = $input_exercise_count - $GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT'];
						if ($followup_judgement['propose_count'] < $GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT']) {
							// 新たに提示する時間が減算した時間より下回った場合
							$followup_judgement['propose_count'] = $GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT'];
						}
						$followup_judgement['judgment_type']	= $GLOBALS['FOLLOWUP_JUDGE'][2]['judgment_type'];


					}elseif ($val[0] == '50007' || $val[0] == '50074'){		// 3：運動内容がつらい

						// METS値 減算する値をセット
						$mets_subtract++;


					}elseif ($val[0] == '50080'){		// 1：かなりきついと感じる

						// METS値 減算する値をセット
						$mets_subtract++;
						$mets_subtract_flg = 1;

					}


					// 3：運動内容がつらい または 1：かなりきついと感じる 場合
					if ($mets_subtract > 0) {
						// 回答した運動種目のMETS値 - xMETS = 新たに提示する運動種目
						$followup_judgement['mets'] = $sports_item['mets'] - $mets_subtract;
						if ($followup_judgement['mets'] < $GLOBALS['MIN_METS_TO_SUBTRACT']) {
							// 新たに提示するMTES値が減算したMETS値より下回った場合、提示できる最小値を設定
							$followup_judgement['mets'] = $GLOBALS['MIN_VALUE_OF_METS'];
						}

						// 算出したMETS値から新しく提示する運動種目を取得
						foreach ($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'] as $global_sports_key => $global_sports_val){
							if ($global_sports_val['mets'] == $followup_judgement['mets']) {
								$followup_judgement['propose_exercise_value']	= $global_sports_val['exercise_value'];		// 運動強度判定値
								break;
							}
						}
						// 新しく提示するスポーツが存在しなかった場合、
						// 対象のMETS値の運動内容が存在しなかった場合、運動強度判定の範囲内のスポーツを取得
						if (empty($followup_judgement['propose_exercise_value'])) {
							$followup_sports_by_choice_id = array_reverse($GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID']);
							foreach ($followup_sports_by_choice_id as $exercise_value_key => $exercise_value_sports_val){
								if ($exercise_value_sports_val['route'] == 1 && $exercise_value_sports_val['mets'] <= $followup_judgement['mets']) {
									$followup_judgement['propose_exercise_value']	= $exercise_value_sports_val['exercise_value'];		// 運動強度判定値
									break;
								}
							}
						}

						// 診断結果内容
						$followup_judgement['judgment_type']	= $GLOBALS['FOLLOWUP_JUDGE'][3]['judgment_type'];

						// 「1：かなりきついと感じる」が選択された場合、回数、時間の判定は削除し、運動内容のみ表示を行う。
						if ($mets_subtract_flg >= 1) {
							unset($followup_judgement['propose_count']);
							unset($followup_judgement['propose_time']);
						}

					}

				}

				if ($val[0] == '50077' || $val[0] == '50078'){		// 1：腰痛・関節痛・下肢の痺れが出現した／2：胸痛・動悸・強い息切れがある
					//----------------------------------------------------
					// 体調に変化はありますか？
					//----------------------------------------------------
					// 1、2を選択した場合、体調に変化があると判断
					$bad_condition_flg = 1;
				}

//			}

			//------------------------------------------
			// 【食事】
			//------------------------------------------
			// 選択された運動に何らかの支障がある場合、カロリーを提示する
//			if ($followup_result == 0) {
			if ($followup_judgement['judgment_type'] == 1 ||  $followup_judgement['judgment_type'] == 2 || $followup_judgement['judgment_type'] == 3) {

				// 運動をしていない場合

				// 削減上限カロリーを算出
				// 初回問診で提示したカロリー（近日食事摂取カロリー）の15%が削減上限（四捨五入、切り上げを行うと15%を超えてしまう可能性があるので、小数点以下切り捨てとする）
				$upper_limit_calorie_reduction = floor($guidance_data['current_calorie'] * ($GLOBALS['UPPER_LIMIT_CALORIE_REDUCTION'] / 100));

				// 運動で再判定されている場合、食事制限として、摂取カロリーから減算するカロリー量の提示を行う。
				// ((時間(分) * 回答された運動種目のMTES値 * 1.05 * 体重) / 60) = 食事で減らすカロリー

				// 選択されたMets値から食事で減らすカロリーを算出
				$input_reduce_calorie	= floor(($input_exercise_time * $sports_item['mets'] * 1.05 * $weight) / 60);
				// 判定で出力されたMets値から食事で減らすカロリーを算出

				// 新たに運動内容（Mets値）が提示されているかチェック
				if (!empty($followup_judgement['mets'])) {
					$exercise_mets = $followup_judgement['mets'];
				}else{
					$exercise_mets = $sports_item['mets'];
				}

				// 新たに運動時間が提示されているかチェック
				if (!empty($followup_judgement['propose_time'])) {
					$exercise_time = $followup_judgement['propose_time'];
				}else{
					$exercise_time = $input_exercise_time;
				}
				$propose_reduce_calorie	= floor(($exercise_time * $exercise_mets * 1.05 * $weight) / 60);

				// 選択されたMets値から食事で減らすカロリー から 判定で出力されたMets値から食事で減らすカロリーをマイナス
				$reduce_calorie = $input_reduce_calorie - $propose_reduce_calorie;

				// 削減上限を超えているかチェック
				if ($reduce_calorie > $upper_limit_calorie_reduction) {
					// 算出された食事で減らすカロリーが削減上限を超えてしまっている場合、
					// 削減上限カロリーを減算し提示する
					$followup_judgement['propose_calorie'] = $guidance_data['current_calorie'] - $upper_limit_calorie_reduction;
				}else{
					// 算出された削減カロリーを減算し提示する
					$followup_judgement['propose_calorie'] = $guidance_data['current_calorie'] - $reduce_calorie;
				}

				// 運動判定で運動内容を再提示する必要が無かった場合（時間がとれない、回数が少なくなってしまう、運動内容がきつい以外が選択された場合）カロリーの提示のみ行う。
				if (empty($followup_judgement['judgment_type']) || $followup_judgement['judgment_type'] == '') {
					$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][5]['judgment_type'];
				}

			}else{
				// 運動をしている場合
				if(empty($followup_judgement['judgment_type']) || $followup_judgement['judgment_type'] === ''){
					// 「このまま運動を継続してください。」を設定
					$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][0]['judgment_type'];
				}
			}

			//---------------------------------------------
			// 今後の取り組みを編集
			//---------------------------------------------
			// 8W以降の場合、初回問診からの体重の増減を判定し今後の取り組みを表示
			if ($bad_condition_flg != 1 && $type >= 8 && $followup_judgement['judgment_type'] != 0) {
				if ( $input_current_weight >= ($guidance_data['current_weight'] - 1) && ($input_current_weight <= $guidance_data['current_weight'] + 1)) {
					// 現在の体重が初回問診時と比較して±1以内であった場合
					$followup_judgement['target_type']		= $GLOBALS['FOLLOWUP_TARGET'][1]['target_type'];
				}elseif (($guidance_data['current_weight'] - 1) > $input_current_weight){
					// 現在の体重が初回問診と比較して-1kg以上、減っている場合
					$followup_judgement['target_type']		= $GLOBALS['FOLLOWUP_TARGET'][0]['target_type'];
				}elseif (($guidance_data['current_weight'] + 1) < $input_current_weight){
					// 現体重が開始時より増えている場合
					$followup_judgement['target_type']		= $GLOBALS['FOLLOWUP_TARGET'][2]['target_type'];
				}
			}

		}
		//--運動判定ここまで（運動判定は判定が免除される月または問診タイプ2W以外はすべて実施する）--------------------------------------------------------------------------------------------------------------------------------------

		// 体調に変化があった場合、判定タイプを差替える
		if ($bad_condition_flg == 1) {
			// 体調に変化があった場合、これまでに算出した判定結果を差替える。
			$followup_judgement['judgment_type'] = $GLOBALS['FOLLOWUP_JUDGE'][4]['judgment_type'];		// 医療機関を受診して、このまま運動を続けていいか確認しましょう。
			// 今後の取り組みを削除
			unset($followup_judgement['target_type']);
		}



		//------------------------------------------------------------
		// 前回の判定結果と比較
		//------------------------------------------------------------
		// 前回の判定結果を取得
		if (!empty($last_followup_data)) {
			$last_judgment_number = (int)substr($last_followup_data['judgment'], 1);
		}else{
			// 前回の判定結果を持っていない場合、今回が初めてのフォローアップ問診のため、初期値をセット
			$last_judgment_number = 1;
		}

		// 運動をしていない場合、前回判定値よりカウントアップ
		// ただし、2Wまたは4Wかつ初回問診日が3月、4月、9月、10月、12月の場合、判定結果の変更は無し
		if ($followup_result == 0 && $special_exemption_flg == 0) {
			$last_judgment_number++;
		}

		// 今回の判定結果をセット
		if (strlen($last_judgment_number) == 1) {
			$followup_judgement['judgment']	= 'P0' . $last_judgment_number;
		}else{
			$followup_judgement['judgment']	= 'P' . $last_judgment_number;
		}

		// 判定結果をセット
		$followup_judgement['result'] = $followup_result;


		//------------------------------------------------------------
		// 次回へ引き継ぐデータを設定
		//------------------------------------------------------------
		// 今回の判定で新たに提示されなかった項目については、前回のデータを引き継ぐ
		// ただし、以下の条件の場合、それぞれ引き継ぐデータを変更する
		// 2Wかつ2週目の場合、初回問診で提示されたデータを引き継ぐ
//		// ※運動強度が'E'判定の場合も同様
		// 2Wかつ2週目以外の場合、前回のフォローアップ問診で提示されたデータを引き継ぐ
		// ※以下で引き継いでいる項目は必須項目となり、この値を引き継がなかった場合、
		//   正常に次回問診内容が表示されないので注意

//		if ($last_followup_data['exercise_value'] == 'E' || ($type == 2 && $week != 2)) {
		if (($type == 2 && $week != 2)) {

			// 前回のデータを全て受け継ぐ

			// 入力された運動時間
			$followup_judgement['input_time']				= $last_followup_data['input_time'];
			// 入力された回数
			$followup_judgement['input_count']				= $last_followup_data['input_count'];
			// 提示METS値（Mets値 範囲のMAX値を設定）
			$followup_judgement['mets']						= $last_followup_data['mets'];
			// 運動強度判定値（A～I）
			$followup_judgement['propose_exercise_value']	= $last_followup_data['exercise_value'];

//			// 提示された運動時間
//			$followup_judgement['propose_time']		= $last_followup_data['propose_time'];
//			// 提示された回数
//			$followup_judgement['propose_count']	= $last_followup_data['propose_count'];

			// 提示カロリー
			$followup_judgement['propose_calorie']	= $last_followup_data['propose_calorie'];
			// 現在の体重
			$followup_judgement['current_weight']	= $weight;

		}else{

			if ($type == 2 && $week == 2) {

				// 入力された運動時間
				$followup_judgement['input_time']	= $guidance_data['exercise_time'] * 60;
				// 入力された回数
				$followup_judgement['input_count']	= $guidance_data['exercise_cnt'];
				// 提示METS値（Mets値 範囲のMAX値を設定）
				$followup_judgement['mets']			= $guidance_data['mets_max'];
				// 運動強度判定値（A～I）
				$followup_judgement['propose_exercise_value']	= $guidance_data['exercise_value'];
				// 提示カロリー
				$followup_judgement['propose_calorie']	= $guidance_data['current_calorie'];
				// 現在の体重
				$followup_judgement['current_weight']	= $weight;

			}else{

				// 入力値または前回の値を取得
				$followup_judgement['input_time']	= $input_exercise_time;			// 入力時間
				$followup_judgement['input_count']	= $input_exercise_count;		// 入力した運動回数

				// 入力された運動強度判定値（A～I）
				if (empty($followup_judgement['mets'])) {
					$followup_judgement['mets']	= $sports_item['mets'];
				}
				// 運動強度判定値（A～I）
				if (empty($followup_judgement['propose_exercise_value'])) {
					$followup_judgement['propose_exercise_value']	= $sports_item['exercise_value'];
				}

				// 提示カロリー
				if (empty($followup_judgement['propose_calorie']) || $followup_judgement['propose_calorie'] === '') {
					$followup_judgement['propose_calorie']	= $guidance_data['current_calorie'];
				}
				// 現在の体重
				if (empty($followup_judgement['current_weight']) || $followup_judgement['current_weight'] === '') {
					$followup_judgement['current_weight']	= $weight;
				}
			}
		}


		// フォローアップ問診終了判定
		if ($week == 48) {
			// 今回の問診が48週目の場合、フォローアップ問診を終了
			$followup_judgement['finish_flg'] = 1;
		}else{
			// 48週目以外の場合、フォローアップ問診を継続
			$followup_judgement['finish_flg'] = 0;
		}


		return $followup_judgement;

	}

	public function createChoiceSelectionInsertValue($choice_id, $choice_list , $choice_data, $upper_id_list)
	{
		//$choice_data = $choice_list[$choice_id]; //現在の選択肢の情報
		$choice_type = $choice_data['type']; //現在の選択肢のタイプ
		$upper = $upper_id_list[$choice_data['layer_id']];

		if (!empty($upper)) {

			$isParentSelected = $this->isSelectedUpper($choice_list, $upper);
			if ($upper['show_type'] ==  0) {
				if ($isParentSelected) {
					return null;
				}
			} else {
				if (!$isParentSelected) {
					return null;
				}
			}
			if ($choice_type !== "radio" && $choice_type !== "checkbox") {
				if ($choice_data["data"] === "" || is_null($choice_data["data"])) {
					return null;
				}
			}
		}

		$value = $choice_data["data"];

		$insert_values = array('choice_id' => $choice_id);
		switch ($choice_type) {
			case 'radio':
			case 'checkbox':
				$insert_values['selection'] = true;
				break;
			case 'string':
				$insert_values['input_string'] = $value;
				break;
			case 'numeric':
				$insert_values['input_float'] = $value;
				break;
			case 'date':
				$insert_values['input_datetime'] = $value . ' 00:00:00';
				break;
			case 'time':
				$insert_values['input_datetime'] = '1999-01-01 ' . $value;
				break;
			case 'datetime':
				$insert_values['input_datetime'] = $value;
				break;
			case 'year':
				$insert_values['input_datetime'] = $value . '-01-01 00:00:00';
				break;
			case 'month':
				$insert_values['input_datetime'] = '1999-' . $value . '-01 00:00:00';
				break;
			case 'day':
				$insert_values['input_datetime'] = '1999-01-' . $value . ' 00:00:00';
				break;
			case 'slider':
			case 'slider_hour':
			case 'slider_show_by_step':
			case 'slider_minute':
				$insert_values['input_float'] = $value;
				break;
			case 'tab':
			case 'accordion': //見出し
				return null;
		}

		return $insert_values;
	}
	

}
