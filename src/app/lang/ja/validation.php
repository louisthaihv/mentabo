<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| following language lines contain default error messages used by
	| validator class. Some of these rules have multiple versions such
	| as size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attributeに同意してください。",
	"active_url"           => ":attribute は正しいRLの形式ではありません。",
	"after"                => ":attributeは:dateより後の日付を指定してください。",
	"alpha"                => ":attributeには空白・改行・記号以外の文字を入力してください。",
	"alpha_dash"           => ":attributeは半角数字又はハイフンで入力してください。",
	"alpha_num"            => ":attributeは数字または文字列を入力してください。",
	"array"                => ":attributeが配列の形式ではありません。",
	"before"               => ":attributeは:dateより前の日付を指定してください。",
	"between"              => array(
		"numeric" => ":attributeは:minから:maxの間で入力してください。",
		"file"    => ":attributeは:min kbから:max kbの間で指定してください。",
		"string"  => ":attributeは:min文字から:max文字の間で入力してください。",
		"array"   => ":attributeは:minから:maxまでの大きさの配列を指定してください。",
	),
	"confirmed"            => ":attributeが確認用フィールドと一致しません。",
	"date"                 => ":attributeは存在する日付ではありません。",
	"date_format"          => ":attributeは:formatの形式で入力してください。",
	"different"            => ":attributeと:otherは別の内容を入力してください。",
	"digits"               => ":attributeは:digits桁を指定してください。",
	"digits_between"       => ":attributeは:min桁から:max桁で指定してください。",
	"email"                => ":attributeは正しいメールアドレスの形式で入力してください。",
	"exists"               => ":attributeは存在しません。",
	"image"                => ":attributeは画像の形式である必要があります。",
	"in"                   => ":attributeの値が無効です。",
	"integer"              => ":attributeには半角整数値を指定してください。",
	"ip"                   => ":attributeはIPアドレスを正しく入力してください。",
	"max"                  => array(
		"numeric" => ":attributeは:max文字以下で入力してください。",
		"file"    => ":attributeは:maxキロバイト以下で指定してください。",
		"string"  => ":attributeは:max文字以下で入力してください。",
		"array"   => ":attributeは:max個以下の配列を指定してください。",
	),
	"mimes"                => ":attributeのmimeタイプが:valuesと一致しません。",
	"min"                  => array(
		"numeric" => ":attributeは:min以上を指定してください。",
		"file"    => ":attributeは:minキロバイト以上を指定してください。",
		"string"  => ":attributeは:min文字以上で指定してください。",
		"array"   => ":attributeは:min個以上の配列を指定してください。",
	),
	"not_in"               => ":attributeの値が無効です。",
	"numeric"              => ":attributeは半角数値を入力してください。",
	"regex"                => ":attributeは無効な形式です。",
	"required"             => ":attributeを入力してください。",
	"required_if"          => ":otherが:valueのときは、:attributeを入力してください。",
	"required_with"        => ":valuesに値が入力されている場合、:attributeを入力してください。",
	"required_with_all"    => ":valuesが全て入力されている場合、:attributeを入力してください。",
	"required_without"     => ":valuesのいずれかが入力されていない場合、:attributeを入力してください。",
	"required_without_all" => ":valuesが全て入力されていない場合、:attributeを入力してください。",
	"same"                 => ":attributeが:otherと一致しません。",
	"size"                 => array(
		"numeric" => ":attributeは:sizeを指定してください。",
		"file"    => ":attributeは:sizeキロバイトで指定してください。.",
		"string"  => ":attributeは:size文字を指定してください。.",
		"array"   => ":attributeは:size個の配列を指定してください。.",
	),
	"unique"               => ":attributeは既に使用されています。",
	"url"                  => ":attributeは正しいURLの形式ではありません。",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
        /*
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
        */

	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
        'email' => 'メールアドレス',
        'password' => 'パスワード',
    ),

);
