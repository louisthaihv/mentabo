﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head>
<title>[管理]生活習慣診断</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Expires" content="Fri, 18 Jul 1980 23:00:00 GMT">
<link rel="stylesheet" type="text/css" href="../css/admin_common.css" />
<link rel="stylesheet" type="text/css" href="../css/admin_login.css" />
</head>
<body>

	<div id="header">
		<div class="header-inner" id="top">
			<h1 id="logo"><a id="topLink"><img src="../img/admin/common/logo.gif" height="30" alt="サービスロゴ" /></a></h1>
		</div>
	</div>

	<hr />

	<div id="wrapper">
		<!-- #contents -->
		<div id="adminLogin" class="contents">

			
			<div id="loginBox">
				<div id="loginBox-inner">
					<h2>管理画面ログイン</h2>

					<form action="{{ url('admin/login') }}" method="POST">
						<dl class="clearfix">
							<dt>ID:</dt>
							<dd><input type="text" name="login_name" /></dd>
						</dl>
						<dl class="clearfix">
							<dt>Password:</dt>
							<dd><input type="password" name="passward" /></dd>
						</dl>
						<p id="btn_login"><input id="login" name="action_admin_login_do" value="ログイン" title="ログイン" type="submit" /></p>
					</form>

				</div><!-- /#loginBox-inner -->
				<div class="btm"><img src="../img/admin/login/login_bg_btm.png" width="302" height="10" alt="" /></div>
			</div><!-- /#loginBox -->

		</div><!-- /.contents -->
	</div><!-- /#wrapper-->

	<hr />

	<div id="footer">
	</div>

</body>
</html>