@include('header')

<?php if(isset($headerScript)) echo $headerScript ?>

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2><?php if(isset($setTitle)) echo $setTitle ?></h2>

	<?php if(isset($interviewStep)) echo $interviewStep ?>
	
	<div id="contentInner">


		<div class="compulsoryInputError" style="display:none"></div>
		
		<?php if(isset($content)) { ?>
			<?php if(isset($content)) echo $content ?>
		<?php } ?>	

	</div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->
</div>
@include('footer')
