$(function(){
	//画面がロードされたら実行
	$(document).ready(function(){
		control_inputs();
	});

	//inputが変更されたら実行
	$("input").change(function(){
		control_inputs();
	});

	//スライダー使用関数
	function cal_hour( minute ){
		return Math.floor(minute/60);
	}
	function cal_min( minute ){
		return ("0"+String(minute-60*Math.floor(minute/60))).slice(-2);	//ゼロ埋め 2桁で分を返す
	}

	$(".choices input:radio").button();
	$(".choices input:checkbox").button();
	$(".choices-full input:radio").button();
	$(".choices-full input:checkbox").button();
	$(".choices-half input:radio").button();
	$(".choices-half input:checkbox").button();
	$(".choices-third input:radio").button();
	$(".choices-third input:checkbox").button();
	$(".choices-quarter input:radio").button();
	$(".choices-quarter input:checkbox").button();

	$(".tabs").tabs();
	$(".accordion").accordion({
		autoHeight: false,
		navigation: true
	});

	//表示/非表示の制御
	function control_inputs(){
		if($("#c_10010:checked").val() == '10010'){ $("#l_106").show("slow"); } else { $("#l_106").hide("slow"); }
		if($("#c_10011:checked").val() == '10011'){ $("#l_107").show("slow"); } else { $("#l_107").hide('slow'); }
		if($("#c_10012:checked").val() == '10012'){ $("#l_108").show("slow"); } else { $("#l_108").hide("slow"); }
		if($("#c_10013:checked").val() == '10013'){ $("#l_109").show("slow"); } else { $("#l_109").hide("slow"); }
		if($("#c_10014:checked").val() == '10014'){ $("#l_110").show("slow"); } else { $("#l_110").hide("slow"); }
		if($("#c_10015:checked").val() == '10015'){ $("#l_111").show("slow"); } else { $("#l_111").hide("slow"); }
		if($("#c_10016:checked").val() == '10016'){ $("#l_112").show("slow"); } else { $("#l_112").hide("slow"); }
		if($("#c_10017:checked").val() == '10017'){ $("#l_113").show("slow"); } else { $("#l_113").hide("slow"); }
		if($("#c_10018:checked").val() == '10018'){ $("#l_114").show("slow"); } else { $("#l_114").hide("slow"); }
		if($("#c_10019:checked").val() == '10019'){ $("#l_115").show("slow"); } else { $("#l_115").hide("slow"); }
		if($("#c_10020:checked").val() == '10020'){ $("#l_116").show("slow"); } else { $("#l_116").hide("slow"); }
		if($("#c_10021:checked").val() == '10021'){ $("#l_117").show("slow"); } else { $("#l_117").hide("slow"); }
		if($("#c_10087:checked").val() == '10087'){ $("#l_127").show("slow"); } else { $("#l_127").hide("slow"); }
		if($("#c_10087:checked").val() == '10087'){ $("#l_127").show("slow"); } else { $("#l_127").hide("slow"); }
		if($("#c_10067:checked").val() == '10067'){ $("#l_128").show("slow"); } else { $("#l_128").hide("slow"); }
		if($("#c_10068:checked").val() == '10068'){ $("#l_129").show("slow"); } else { $("#l_129").hide("slow"); }
		if($("#c_10069:checked").val() == '10069'){ $("#l_130").show("slow"); } else { $("#l_130").hide("slow"); }
		if($("#c_10070:checked").val() == '10070'){ $("#l_131").show("slow"); } else { $("#l_131").hide("slow"); }
		if($("#c_10071:checked").val() == '10071'){ $("#l_132").show("slow"); } else { $("#l_132").hide("slow"); }
		if($("#c_10072:checked").val() == '10072'){ $("#l_133").show("slow"); } else { $("#l_133").hide("slow"); }
		if($("#c_10073:checked").val() == '10073'){ $("#l_134").show("slow"); } else { $("#l_134").hide("slow"); }
		if($("#c_10074:checked").val() == '10074'){ $("#l_135").show("slow"); } else { $("#l_135").hide("slow"); }
		if($("#c_10075:checked").val() == '10075'){ $("#l_136").show("slow"); } else { $("#l_136").hide("slow"); }
		if($("#c_10076:checked").val() == '10076'){ $("#l_137").show("slow"); } else { $("#l_137").hide("slow"); }
		if($("#c_10077:checked").val() == '10077'){ $("#l_138").show("slow"); } else { $("#l_138").hide("slow"); }
		if($("#c_10078:checked").val() == '10078'){ $("#l_139").show("slow"); } else { $("#l_139").hide("slow"); }
		if($("#c_10079:checked").val() == '10079'){ $("#l_140").show("slow"); } else { $("#l_140").hide("slow"); }
		if($("#c_10080:checked").val() == '10080'){ $("#l_141").show("slow"); } else { $("#l_141").hide("slow"); }
		if($("#c_10081:checked").val() == '10081'){ $("#l_142").show("slow"); } else { $("#l_142").hide("slow"); }
		if($("#c_10082:checked").val() == '10082'){ $("#l_143").show("slow"); } else { $("#l_143").hide("slow"); }
		if($("#c_10083:checked").val() == '10083'){ $("#l_144").show("slow"); } else { $("#l_144").hide("slow"); }
		if($("#c_10084:checked").val() == '10084'){ $("#l_145").show("slow"); } else { $("#l_145").hide("slow"); }
		if($("#c_10085:checked").val() == '10085'){ $("#l_146").show("slow"); } else { $("#l_146").hide("slow"); }
		if($("#c_10086:checked").val() == '10086'){ $("#l_147").show("slow"); } else { $("#l_147").hide("slow"); }
		if($("#c_10087:checked").val() == '10087'){ $("#l_148").show("slow"); } else { $("#l_148").hide("slow"); }
		if($("#c_10124:checked").val() == '10124'){ $("#l_237").show("slow"); } else { $("#l_237").hide("slow"); }
		if($("#c_10175:checked").val() == '10175'){ $("#l_204").show("slow"); } else { $("#l_204").hide("slow"); }
		if($("#c_10125:checked").val() == '10125'){ $("#l_154").show("slow"); } else { $("#l_154").hide("slow"); }
		if($("#c_10128:checked").val() == '10128'){ $("#l_155").show("slow"); } else { $("#l_155").hide("slow"); }
		if($("#c_10144:checked").val() == '10144' | $("#c_10145:checked").val() == '10145' | $("#c_10146:checked").val() == '10146' | $("#c_10147:checked").val() == '10147' | $("#c_10148:checked").val() == '10148'){ $("#l_161").show("slow"); } else { $("#l_161").hide("slow"); }
		if($("#c_10087:checked").val() == '10087'){ $("#l_127").show("slow"); } else { $("#l_127").hide("slow"); }
		if($("#c_10067:checked").val() == '10067'){ $("#l_128").show("slow"); } else { $("#l_128").hide("slow"); }
		if($("#c_10068:checked").val() == '10068'){ $("#l_129").show("slow"); } else { $("#l_129").hide("slow"); }
		if($("#c_10069:checked").val() == '10069'){ $("#l_130").show("slow"); } else { $("#l_130").hide("slow"); }
		if($("#c_10070:checked").val() == '10070'){ $("#l_131").show("slow"); } else { $("#l_131").hide("slow"); }
		if($("#c_10071:checked").val() == '10071'){ $("#l_132").show("slow"); } else { $("#l_132").hide("slow"); }
		if($("#c_10072:checked").val() == '10072'){ $("#l_133").show("slow"); } else { $("#l_133").hide("slow"); }
		if($("#c_10073:checked").val() == '10073'){ $("#l_134").show("slow"); } else { $("#l_134").hide("slow"); }
		if($("#c_10074:checked").val() == '10074'){ $("#l_135").show("slow"); } else { $("#l_135").hide("slow"); }
		if($("#c_10075:checked").val() == '10075'){ $("#l_136").show("slow"); } else { $("#l_136").hide("slow"); }
		if($("#c_10076:checked").val() == '10076'){ $("#l_137").show("slow"); } else { $("#l_137").hide("slow"); }
		if($("#c_10077:checked").val() == '10077'){ $("#l_138").show("slow"); } else { $("#l_138").hide("slow"); }
		if($("#c_10078:checked").val() == '10078'){ $("#l_139").show("slow"); } else { $("#l_139").hide("slow"); }
		if($("#c_10079:checked").val() == '10079'){ $("#l_140").show("slow"); } else { $("#l_140").hide("slow"); }
		if($("#c_10080:checked").val() == '10080'){ $("#l_141").show("slow"); } else { $("#l_141").hide("slow"); }
		if($("#c_10081:checked").val() == '10081'){ $("#l_142").show("slow"); } else { $("#l_142").hide("slow"); }
		if($("#c_10082:checked").val() == '10082'){ $("#l_143").show("slow"); } else { $("#l_143").hide("slow"); }
		if($("#c_10083:checked").val() == '10083'){ $("#l_144").show("slow"); } else { $("#l_144").hide("slow"); }
		if($("#c_10084:checked").val() == '10084'){ $("#l_145").show("slow"); } else { $("#l_145").hide("slow"); }
		if($("#c_10085:checked").val() == '10085'){ $("#l_146").show("slow"); } else { $("#l_146").hide("slow"); }
		if($("#c_10086:checked").val() == '10086'){ $("#l_147").show("slow"); } else { $("#l_147").hide("slow"); }
		if($("#c_10087:checked").val() == '10087'){ $("#l_148").show("slow"); } else { $("#l_148").hide("slow"); }
		if($("#c_10124:checked").val() == '10124'){ $("#l_237").show("slow"); } else { $("#l_237").hide("slow"); }
		if($("#c_10246:checked").val() == '10246'){ $("#l_189").show("slow"); } else { $("#l_189").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' ){ $("#l_197").show("slow"); } else { $("#l_197").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' & $("#c_10266:checked").val() == '10266' ){ $("#l_198").show("slow"); } else { $("#l_198").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' ){ $("#l_199").show("slow"); } else { $("#l_199").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' & $("#c_10269:checked").val() == '10269' ){ $("#l_200").show("slow"); } else { $("#l_200").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' ){ $("#l_201").show("slow"); } else { $("#l_201").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' ){ $("#l_202").show("slow"); } else { $("#l_202").hide("slow"); }
		if($("#c_10238:checked").val() == '10238' & $("#c_10241:checked").val() == '10241' & $("#c_10243:checked").val() == '10243' & $("#c_10245:checked").val() == '10245' ){ $("#l_203").show("slow"); } else { $("#l_203").hide("slow"); }
		if($("#c_10263:checked").val() == '10263'){ $("#l_196").show("slow"); } else { $("#l_196").hide("slow"); }
		if($("#c_10178:checked").val() == '10178' | $("#c_10179:checked").val() == '10179'){ $("#l_171").show("slow"); } else { $("#l_171").hide("slow"); }
		if($("#c_10178:checked").val() == '10178' | $("#c_10179:checked").val() == '10179'){ $("#l_183").show("slow"); } else { $("#l_183").hide("slow"); }
		if($("#c_10178:checked").val() == '10178' | $("#c_10179:checked").val() == '10179'){ $("#l_170").show("slow"); } else { $("#l_170").hide("slow"); }
		if($("#c_10252:checked").val() == '10252'){ $("#l_191").show("slow"); } else { $("#l_191").hide("slow"); }
	}

	////////// slider //////////
	$( "#c_10164_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 180, step: 10,
		slide: function( event, ui ) {
			$( "#c_10164_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10164" ).val(ui.value);
		}
	});
	$( "#c_10164_show" ).val( "" + cal_hour($("#c_10164_slider").slider("value")) + "時間" + cal_min($("#c_10164_slider").slider("value")) + "分");
	$( "#c_10164" ).val( $( "#c_10164_slider" ).slider( "value" ) );
    $( "#c_10164_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10164" ).val())-10) * 10) / 10);
		$( "#c_10164" ).val(String(amount));
		$( "#c_10164_slider" ).slider( "value", String(amount) );
		$( "#c_10164_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10164_plus" ).click(function(){
		amount = Math.min(180, Math.round((parseFloat($( "#c_10164" ).val())+10) * 10) / 10);
		$( "#c_10164" ).val(String(amount));
		$( "#c_10164_slider" ).slider( "value", String(amount) );
		$( "#c_10164_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10165_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 180, step: 10,
		slide: function( event, ui ) {
			$( "#c_10165_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10165" ).val(ui.value);
		}
	});
	$( "#c_10165_show" ).val( "" + cal_hour($("#c_10165_slider").slider("value")) + "時間" + cal_min($("#c_10165_slider").slider("value")) + "分");
	$( "#c_10165" ).val( $( "#c_10165_slider" ).slider( "value" ) );
	$( "#c_10165_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10165" ).val())-10) * 10) / 10);
		$( "#c_10165" ).val(String(amount));
		$( "#c_10165_slider" ).slider( "value", String(amount) );
		$( "#c_10165_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10165_plus" ).click(function(){
		amount = Math.min(180, Math.round((parseFloat($( "#c_10165" ).val())+10) * 10) / 10);
		$( "#c_10165" ).val(String(amount));
		$( "#c_10165_slider" ).slider( "value", String(amount) );
		$( "#c_10165_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10166_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 180, step: 10,
		slide: function( event, ui ) {
			$( "#c_10166_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10166" ).val(ui.value);
		}
	});
	$( "#c_10166_show" ).val( "" + cal_hour($("#c_10166_slider").slider("value")) + "時間" + cal_min($("#c_10166_slider").slider("value")) + "分");
	$( "#c_10166" ).val( $( "#c_10166_slider" ).slider( "value" ) );
	$( "#c_10166_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10166" ).val())-10) * 10) / 10);
		$( "#c_10166" ).val(String(amount));
		$( "#c_10166_slider" ).slider( "value", String(amount) );
		$( "#c_10166_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10166_plus" ).click(function(){
		amount = Math.min(180, Math.round((parseFloat($( "#c_10166" ).val())+10) * 10) / 10);
		$( "#c_10166" ).val(String(amount));
		$( "#c_10166_slider" ).slider( "value", String(amount) );
		$( "#c_10166_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10167_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10167_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10167" ).val(ui.value);
		}
	});
	$( "#c_10167_show" ).val( "" + cal_hour($("#c_10167_slider").slider("value")) + "時間" + cal_min($("#c_10167_slider").slider("value")) + "分");
	$( "#c_10167" ).val( $( "#c_10167_slider" ).slider( "value" ) );
	$( "#c_10167_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10167" ).val())-10) * 10) / 10);
		$( "#c_10167" ).val(String(amount));
		$( "#c_10167_slider" ).slider( "value", String(amount) );
		$( "#c_10167_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10167_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10167" ).val())+10) * 10) / 10);
		$( "#c_10167" ).val(String(amount));
		$( "#c_10167_slider" ).slider( "value", String(amount) );
		$( "#c_10167_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10168_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10168_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10168" ).val(ui.value);
		}
	});
	$( "#c_10168_show" ).val( "" + cal_hour($("#c_10168_slider").slider("value")) + "時間" + cal_min($("#c_10168_slider").slider("value")) + "分");
	$( "#c_10168" ).val( $( "#c_10168_slider" ).slider( "value" ) );
	$( "#c_10168_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10168" ).val())-10) * 10) / 10);
		$( "#c_10168" ).val(String(amount));
		$( "#c_10168_slider" ).slider( "value", String(amount) );
		$( "#c_10168_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10168_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10168" ).val())+10) * 10) / 10);
		$( "#c_10168" ).val(String(amount));
		$( "#c_10168_slider" ).slider( "value", String(amount) );
		$( "#c_10168_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10169_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10169_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10169" ).val(ui.value);
		}
	});
	$( "#c_10169_show" ).val( "" + cal_hour($("#c_10169_slider").slider("value")) + "時間" + cal_min($("#c_10169_slider").slider("value")) + "分");
	$( "#c_10169" ).val( $( "#c_10169_slider" ).slider( "value" ) );
	$( "#c_10169_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10169" ).val())-10) * 10) / 10);
		$( "#c_10169" ).val(String(amount));
		$( "#c_10169_slider" ).slider( "value", String(amount) );
		$( "#c_10169_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10169_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10169" ).val())+10) * 10) / 10);
		$( "#c_10169" ).val(String(amount));
		$( "#c_10169_slider" ).slider( "value", String(amount) );
		$( "#c_10169_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10170_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10170_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10170" ).val(ui.value);
		}
	});
	$( "#c_10170_show" ).val( "" + cal_hour($("#c_10170_slider").slider("value")) + "時間" + cal_min($("#c_10170_slider").slider("value")) + "分");
	$( "#c_10170" ).val( $( "#c_10170_slider" ).slider( "value" ) );
	$( "#c_10170_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10170" ).val())-10) * 10) / 10);
		$( "#c_10170" ).val(String(amount));
		$( "#c_10170_slider" ).slider( "value", String(amount) );
		$( "#c_10170_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10170_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10170" ).val())+10) * 10) / 10);
		$( "#c_10170" ).val(String(amount));
		$( "#c_10170_slider" ).slider( "value", String(amount) );
		$( "#c_10170_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10171_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10171_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10171" ).val(ui.value);
		}
	});
	$( "#c_10171_show" ).val( "" + cal_hour($("#c_10171_slider").slider("value")) + "時間" + cal_min($("#c_10171_slider").slider("value")) + "分");
	$( "#c_10171" ).val( $( "#c_10171_slider" ).slider( "value" ) );
	$( "#c_10171_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10171" ).val())-10) * 10) / 10);
		$( "#c_10171" ).val(String(amount));
		$( "#c_10171_slider" ).slider( "value", String(amount) );
		$( "#c_10171_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10171_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10171" ).val())+10) * 10) / 10);
		$( "#c_10171" ).val(String(amount));
		$( "#c_10171_slider" ).slider( "value", String(amount) );
		$( "#c_10171_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10172_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10172_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10172" ).val(ui.value);
		}
	});
	$( "#c_10172_show" ).val( "" + cal_hour($("#c_10172_slider").slider("value")) + "時間" + cal_min($("#c_10172_slider").slider("value")) + "分");
	$( "#c_10172" ).val( $( "#c_10172_slider" ).slider( "value" ) );
	$( "#c_10172_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10172" ).val())-10) * 10) / 10);
		$( "#c_10172" ).val(String(amount));
		$( "#c_10172_slider" ).slider( "value", String(amount) );
		$( "#c_10172_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10172_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10172" ).val())+10) * 10) / 10);
		$( "#c_10172" ).val(String(amount));
		$( "#c_10172_slider" ).slider( "value", String(amount) );
		$( "#c_10172_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10173_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10173_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10173" ).val(ui.value);
		}
	});
	$( "#c_10173_show" ).val( "" + cal_hour($("#c_10173_slider").slider("value")) + "時間" + cal_min($("#c_10173_slider").slider("value")) + "分");
	$( "#c_10173" ).val( $( "#c_10173_slider" ).slider( "value" ) );
	$( "#c_10173_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10173" ).val())-10) * 10) / 10);
		$( "#c_10173" ).val(String(amount));
		$( "#c_10173_slider" ).slider( "value", String(amount) );
		$( "#c_10173_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10173_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10173" ).val())+10) * 10) / 10);
		$( "#c_10173" ).val(String(amount));
		$( "#c_10173_slider" ).slider( "value", String(amount) );
		$( "#c_10173_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10174_slider" ).slider({
		range: "min",
		value: 30,
		min: 0, max: 1200, step: 10,
		slide: function( event, ui ) {
			$( "#c_10174_show" ).val( "" + cal_hour(ui.value) + "時間" + cal_min(ui.value) + "分" );
			$( "#c_10174" ).val(ui.value);
		}
	});
	$( "#c_10174_show" ).val( "" + cal_hour($("#c_10174_slider").slider("value")) + "時間" + cal_min($("#c_10174_slider").slider("value")) + "分");
	$( "#c_10174" ).val( $( "#c_10174_slider" ).slider( "value" ) );
	$( "#c_10174_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10174" ).val())-10) * 10) / 10);
		$( "#c_10174" ).val(String(amount));
		$( "#c_10174_slider" ).slider( "value", String(amount) );
		$( "#c_10174_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});
	$( "#c_10174_plus" ).click(function(){
		amount = Math.min(1200, Math.round((parseFloat($( "#c_10174" ).val())+10) * 10) / 10);
		$( "#c_10174" ).val(String(amount));
		$( "#c_10174_slider" ).slider( "value", String(amount) );
		$( "#c_10174_show" ).val( "" + cal_hour(amount) + "時間" + cal_min(amount) + "分" );
	});

	$( "#c_10278_slider" ).slider({
		range: "min",
		value: 1,
		min: 0, max: 18, step: 0.5,
		slide: function( event, ui ) {
			if(Math.floor(ui.value)==ui.value){
				$( "#c_10278_show" ).val( "" + Math.floor(ui.value) + "時間" );
			} else{
				$( "#c_10278_show" ).val( "" + Math.floor(ui.value) + "時間半" );
			}
			$( "#c_10278" ).val(ui.value);
		}
	});
	$( "#c_10278_show" ).val( "" + $( "#c_10278_slider" ).slider( "value" ) + "時間" );
	$( "#c_10278" ).val( $( "#c_10278_slider" ).slider( "value" ) );
	$( "#c_10278_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10278" ).val())-0.5) * 10) / 10);
		$( "#c_10278" ).val(String(amount));
		$( "#c_10278_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10278_show" ).val( "" + String(Math.floor(amount)) + "時間" );
		} else{
			$( "#c_10278_show" ).val( "" + String(Math.floor(amount)) + "時間半" );
		}
	});
	$( "#c_10278_plus" ).click(function(){
		amount = Math.min(18, Math.round((parseFloat($( "#c_10278" ).val())+0.5) * 10) / 10);
		$( "#c_10278" ).val(String(amount));
		$( "#c_10278_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10278_show" ).val( "" + String(Math.floor(amount)) + "時間" );
		} else{
			$( "#c_10278_show" ).val( "" + String(Math.floor(amount)) + "時間半" );
		}
	});

	$( "#c_10279_slider" ).slider({
		range: "min",
		value: 1,
		min: 0, max: 100, step: 1,
		slide: function( event, ui ) {
			$( "#c_10279_show" ).val( "" + ui.value + " kgくらいのもの");
			$( "#c_10279" ).val(ui.value);
		}
	});
	$( "#c_10279_show" ).val( "" + $( "#c_10279_slider" ).slider( "value" ) + " kgくらいのもの" );
	$( "#c_10279" ).val( $( "#c_10279_slider" ).slider( "value" ) );
	$( "#c_10279_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10279" ).val())-1) * 10) / 10);
		$( "#c_10279" ).val(String(amount));
		$( "#c_10279_slider" ).slider( "value", String(amount) );
		$( "#c_10279_show" ).val( "" + String(amount) + " kgくらいのもの" );
	});
	$( "#c_10279_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10279" ).val())+1) * 10) / 10;
		$( "#c_10279" ).val(String(amount));
		$( "#c_10279_slider" ).slider( "value", String(amount) );
		$( "#c_10279_show" ).val( "" + String(amount) + " kgくらいのもの" );
	});

	$( "#c_10129_slider" ).slider({
		range: "min",
		value: 5,
		min: 1, max: 60, step: 1,
			slide: function( event, ui ) {
			$( "#c_10129_show" ).val( "" + ui.value + " 本");
			$( "#c_10129" ).val(ui.value);
		}
	});
	$( "#c_10129_show" ).val( "" + $( "#c_10129_slider" ).slider( "value" ) + " 本" );
	$( "#c_10129" ).val( $( "#c_10129_slider" ).slider( "value" ) );
	$( "#c_10129_minus" ).click(function(){
		amount = Math.max(1, Math.round((parseFloat($( "#c_10129" ).val())-1) * 10) / 10);
		$( "#c_10129" ).val(String(amount));
		$( "#c_10129_slider" ).slider( "value", String(amount) );
		$( "#c_10129_show" ).val( "" + String(amount) + " 本" );
	});
	$( "#c_10129_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10129" ).val())+1) * 10) / 10;
		$( "#c_10129" ).val(String(amount));
		$( "#c_10129_slider" ).slider( "value", String(amount) );
		$( "#c_10129_show" ).val( "" + String(amount) + " 本" );
	});

	$( "#c_10130_slider" ).slider({
		range: "min",
		value: 3,
		min: 1, max: 60, step: 1,
		slide: function( event, ui ) {
			$( "#c_10130_show" ).val( "" + ui.value + " 年間");
			$( "#c_10130" ).val(ui.value);
		}
	});
	$( "#c_10130_show" ).val( "" + $( "#c_10130_slider" ).slider( "value" ) + " 年間" );
	$( "#c_10130" ).val( $( "#c_10130_slider" ).slider( "value" ) );
	$( "#c_10130_minus" ).click(function(){
		amount = Math.max(1, Math.round((parseFloat($( "#c_10130" ).val())-1) * 10) / 10);
		$( "#c_10130" ).val(String(amount));
		$( "#c_10130_slider" ).slider( "value", String(amount) );
		$( "#c_10130_show" ).val( "" + String(amount) + " 年間" );
	});
	$( "#c_10130_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10130" ).val())+1) * 10) / 10;
		$( "#c_10130" ).val(String(amount));
		$( "#c_10130_slider" ).slider( "value", String(amount) );
		$( "#c_10130_show" ).val( "" + String(amount) + " 年間" );
	});

	$( "#c_10131_slider" ).slider({
		range: "min",
		value: 5,
		min: 1, max: 60, step: 1,
		slide: function( event, ui ) {
			$( "#c_10131_show" ).val( "" + ui.value + " 本");
			$( "#c_10131" ).val(ui.value);
		}
	});
	$( "#c_10131_show" ).val( "" + $( "#c_10131_slider" ).slider( "value" ) + " 本" );
	$( "#c_10131" ).val( $( "#c_10131_slider" ).slider( "value" ) );
	$( "#c_10131_minus" ).click(function(){
		amount = Math.max(1, Math.round((parseFloat($( "#c_10131" ).val())-1) * 10) / 10);
		$( "#c_10131" ).val(String(amount));
		$( "#c_10131_slider" ).slider( "value", String(amount) );
		$( "#c_10131_show" ).val( "" + String(amount) + " 本" );
	});
	$( "#c_10131_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10131" ).val())+1) * 10) / 10;
		$( "#c_10131" ).val(String(amount));
		$( "#c_10131_slider" ).slider( "value", String(amount) );
		$( "#c_10131_show" ).val( "" + String(amount) + " 本" );
	});

	$( "#c_10132_slider" ).slider({
		range: "min",
		value: 10,
		min: 1, max: 60, step: 1,
		slide: function( event, ui ) {
			$( "#c_10132_show" ).val( "" + ui.value + " 年間");
			$( "#c_10132" ).val(ui.value);
		}
	});
	$( "#c_10132_show" ).val( "" + $( "#c_10132_slider" ).slider( "value" ) + " 年間" );
	$( "#c_10132" ).val( $( "#c_10132_slider" ).slider( "value" ) );
	$( "#c_10132_minus" ).click(function(){
		amount = Math.max(1, Math.round((parseFloat($( "#c_10132" ).val())-1) * 10) / 10);
		$( "#c_10132" ).val(String(amount));
		$( "#c_10132_slider" ).slider( "value", String(amount) );
		$( "#c_10132_show" ).val( "" + String(amount) + " 年間" );
	});
	$( "#c_10132_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10132" ).val())+1) * 10) / 10;
		$( "#c_10132" ).val(String(amount));
		$( "#c_10132_slider" ).slider( "value", String(amount) );
		$( "#c_10132_show" ).val( "" + String(amount) + " 年間" );
	});

	$( "#c_10133_slider" ).slider({
		range: "min",
		value: 10,
		min: 1, max: 60, step: 1,
		slide: function( event, ui ) {
			$( "#c_10133_show" ).val( "" + ui.value + " 年間");
			$( "#c_10133" ).val(ui.value);
		}
	});
	$( "#c_10133_show" ).val( "" + $( "#c_10133_slider" ).slider( "value" ) + " 年間" );
	$( "#c_10133" ).val( $( "#c_10133_slider" ).slider( "value" ) );
	$( "#c_10133_minus" ).click(function(){
		amount = Math.max(1, Math.round((parseFloat($( "#c_10133" ).val())-1) * 10) / 10);
		$( "#c_10133" ).val(String(amount));
		$( "#c_10133_slider" ).slider( "value", String(amount) );
		$( "#c_10133_show" ).val( "" + String(amount) + " 年間" );
	});
	$( "#c_10133_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10133" ).val())+1) * 10) / 10;
		$( "#c_10133" ).val(String(amount));
		$( "#c_10133_slider" ).slider( "value", String(amount) );
		$( "#c_10133_show" ).val( "" + String(amount) + " 年間" );
	});

	$( "#c_10150_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 5000, step: 100,
		slide: function( event, ui ) {
			$( "#c_10150_show" ).val( "" + ui.value + " ml");
			$( "#c_10150" ).val(ui.value);
		}
	});
	$( "#c_10150_show" ).val( "" + $( "#c_10150_slider" ).slider( "value" ) + " ml" );
	$( "#c_10150" ).val( $( "#c_10150_slider" ).slider( "value" ) );
	$( "#c_10150_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10150" ).val())-100) * 10) / 10);
		$( "#c_10150" ).val(String(amount));
		$( "#c_10150_slider" ).slider( "value", String(amount) );
		$( "#c_10150_show" ).val( "" + String(amount) + " ml" );
	});
	$( "#c_10150_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10150" ).val())+100) * 10) / 10;
		$( "#c_10150" ).val(String(amount));
		$( "#c_10150_slider" ).slider( "value", String(amount) );
		$( "#c_10150_show" ).val( "" + String(amount) + " ml" );
	});

	$( "#c_10151_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 10, step: 1,
		slide: function( event, ui ) {
			$( "#c_10151_show" ).val( "" + ui.value + " 合");
			$( "#c_10151" ).val(ui.value);
		}
	});
	$( "#c_10151_show" ).val( "" + $( "#c_10151_slider" ).slider( "value" ) + " 合" );
	$( "#c_10151" ).val( $( "#c_10151_slider" ).slider( "value" ) );
	$( "#c_10151_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10151" ).val())-1) * 10) / 10);
		$( "#c_10151" ).val(String(amount));
		$( "#c_10151_slider" ).slider( "value", String(amount) );
		$( "#c_10151_show" ).val( "" + String(amount) + " 合" );
	});
	$( "#c_10151_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10151" ).val())+1) * 10) / 10;
		$( "#c_10151" ).val(String(amount));
		$( "#c_10151_slider" ).slider( "value", String(amount) );
		$( "#c_10151_show" ).val( "" + String(amount) + " 合" );
	});

	$( "#c_10152_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1800, step: 15,
		slide: function( event, ui ) {
			$( "#c_10152_show" ).val( "" + ui.value + " ml");
			$( "#c_10152" ).val(ui.value);
		}
	});
	$( "#c_10152_show" ).val( "" + $( "#c_10152_slider" ).slider( "value" ) + " ml" );
	$( "#c_10152" ).val( $( "#c_10152_slider" ).slider( "value" ) );
	$( "#c_10152_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10152" ).val())-15) * 10) / 10);
		$( "#c_10152" ).val(String(amount));
		$( "#c_10152_slider" ).slider( "value", String(amount) );
		$( "#c_10152_show" ).val( "" + String(amount) + " ml" );
	});
	$( "#c_10152_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10152" ).val())+15) * 10) / 10;
		$( "#c_10152" ).val(String(amount));
		$( "#c_10152_slider" ).slider( "value", String(amount) );
		$( "#c_10152_show" ).val( "" + String(amount) + " ml" );
	});

	$( "#c_10153_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 3600, step: 50,
		slide: function( event, ui ) {
			$( "#c_10153_show" ).val( "" + ui.value + " ml");
			$( "#c_10153" ).val(ui.value);
		}
	});
	$( "#c_10153_show" ).val( "" + $( "#c_10153_slider" ).slider( "value" ) + " ml" );
	$( "#c_10153" ).val( $( "#c_10153_slider" ).slider( "value" ) );
	$( "#c_10153_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10153" ).val())-50) * 10) / 10);
		$( "#c_10153" ).val(String(amount));
		$( "#c_10153_slider" ).slider( "value", String(amount) );
		$( "#c_10153_show" ).val( "" + String(amount) + " ml" );
	});
	$( "#c_10153_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10153" ).val())+50) * 10) / 10;
		$( "#c_10153" ).val(String(amount));
		$( "#c_10153_slider" ).slider( "value", String(amount) );
		$( "#c_10153_show" ).val( "" + String(amount) + " ml" );
	});

	$( "#c_10154_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1800, step: 15,
		slide: function( event, ui ) {
			$( "#c_10154_show" ).val( "" + ui.value + " ml");
			$( "#c_10154" ).val(ui.value);
		}
	});
	$( "#c_10154_show" ).val( "" + $( "#c_10154_slider" ).slider( "value" ) + " ml" );
	$( "#c_10154" ).val( $( "#c_10154_slider" ).slider( "value" ) );
	$( "#c_10154_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10154" ).val())-15) * 10) / 10);
		$( "#c_10154" ).val(String(amount));
		$( "#c_10154_slider" ).slider( "value", String(amount) );
		$( "#c_10154_show" ).val( "" + String(amount) + " ml" );
	});
	$( "#c_10154_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10154" ).val())+15) * 10) / 10;
		$( "#c_10154" ).val(String(amount));
		$( "#c_10154_slider" ).slider( "value", String(amount) );
		$( "#c_10154_show" ).val( "" + String(amount) + " ml" );
	});

	$( "#c_10155_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1800, step: 15,
		slide: function( event, ui ) {
			$( "#c_10155_show" ).val( "" + ui.value + " ml");
			$( "#c_10155" ).val(ui.value);
		}
	});
	$( "#c_10155_show" ).val( "" + $( "#c_10155_slider" ).slider( "value" ) + " ml" );
	$( "#c_10155" ).val( $( "#c_10155_slider" ).slider( "value" ) );
	$( "#c_10155_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10155" ).val())-15) * 10) / 10);
		$( "#c_10155" ).val(String(amount));
		$( "#c_10155_slider" ).slider( "value", String(amount) );
		$( "#c_10155_show" ).val( "" + String(amount) + " ml" );
	});
	$( "#c_10155_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10155" ).val())+15) * 10) / 10;
		$( "#c_10155" ).val(String(amount));
		$( "#c_10155_slider" ).slider( "value", String(amount) );
		$( "#c_10155_show" ).val( "" + String(amount) + " ml" );
	});

	$( "#c_10066_slider" ).slider({
		range: "min",
		value: 60,
		min: 40, max: 150, step: 1,
		slide: function( event, ui ) {
			$( "#c_10066_show" ).val( "" + ui.value + " cm");
			$( "#c_10066" ).val(ui.value);
		}
	});
	$( "#c_10066_show" ).val( "" + $( "#c_10066_slider" ).slider( "value" ) + " cm" );
	$( "#c_10066" ).val( $( "#c_10066_slider" ).slider( "value" ) );
	$( "#c_10066_minus" ).click(function(){
		amount = Math.max(40, Math.round((parseFloat($( "#c_10066" ).val())-1) * 10) / 10);
		$( "#c_10066" ).val(String(amount));
		$( "#c_10066_slider" ).slider( "value", String(amount) );
		$( "#c_10066_show" ).val( "" + String(amount) + " cm" );
	});
	$( "#c_10066_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10066" ).val())+1) * 10) / 10;
		$( "#c_10066" ).val(String(amount));
		$( "#c_10066_slider" ).slider( "value", String(amount) );
		$( "#c_10066_show" ).val( "" + String(amount) + " cm" );
	});

	$( "#c_10194_slider" ).slider({
		range: "min",
		value: 1,
		min: 0, max: 7, step: 1,
		slide: function( event, ui ) {
			$( "#c_10194_show" ).val( "" + ui.value + " 回");
			$( "#c_10194" ).val(ui.value);
		}
	});
	$( "#c_10194_show" ).val( "" + $( "#c_10194_slider" ).slider( "value" ) + " 回" );
	$( "#c_10194" ).val( $( "#c_10194_slider" ).slider( "value" ) );
	$( "#c_10194_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10194" ).val())-1) * 10) / 10);
		$( "#c_10194" ).val(String(amount));
		$( "#c_10194_slider" ).slider( "value", String(amount) );
		$( "#c_10194_show" ).val( "" + String(amount) + " 回" );
	});
	$( "#c_10194_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10194" ).val())+1) * 10) / 10;
		$( "#c_10194" ).val(String(amount));
		$( "#c_10194_slider" ).slider( "value", String(amount) );
		$( "#c_10194_show" ).val( "" + String(amount) + " 回" );
	});

	$( "#c_10195_slider" ).slider({
		range: "min",
		value: 2,
		min: 0, max: 18, step: 0.5,
		slide: function( event, ui ) {
			if(Math.floor(ui.value)==ui.value){
				$( "#c_10195_show" ).val( "" + Math.floor(ui.value) + "時間" );
			} else{
				$( "#c_10195_show" ).val( "" + Math.floor(ui.value) + "時間半" );
			}
			$( "#c_10195" ).val(ui.value);
		}
	});
	$( "#c_10195_show" ).val( "" + $( "#c_10195_slider" ).slider( "value" ) + "時間" );
	$( "#c_10195" ).val( $( "#c_10195_slider" ).slider( "value" ) );
	$( "#c_10195_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10195" ).val())-0.5) * 10) / 10);
		$( "#c_10195" ).val(String(amount));
		$( "#c_10195_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10195_show" ).val( "" + String(Math.floor(amount)) + "時間" );
		} else{
			$( "#c_10195_show" ).val( "" + String(Math.floor(amount)) + "時間半" );
		}
	});
	$( "#c_10195_plus" ).click(function(){
		amount = Math.min(18, Math.round((parseFloat($( "#c_10195" ).val())+0.5) * 10) / 10);
		$( "#c_10195" ).val(String(amount));
		$( "#c_10195_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10195_show" ).val( "" + String(Math.floor(amount)) + "時間" );
		} else{
			$( "#c_10195_show" ).val( "" + String(Math.floor(amount)) + "時間半" );
		}
	});

	$( "#c_10311_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1400, step: 14,
		slide: function( event, ui ) {
			$( "#c_10311_show" ).val( "" + Math.round(ui.value / 14) / 10 + "杯(" + ui.value + "g)" );
			$( "#c_10311" ).val(ui.value);
		}
	});
	$( "#c_10311_show" ).val( "" + $("#c_10311_slider").slider("value") / 14 + "杯("  + $("#c_10311_slider").slider("value") + "g)");
	$( "#c_10311" ).val( $( "#c_10311_slider" ).slider( "value" ) );
	$( "#c_10311_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10311" ).val())-14) * 10) / 10);
		$( "#c_10311" ).val(String(amount));
		$( "#c_10311_slider" ).slider( "value", String(amount) );
		$( "#c_10311_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10311_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10311" ).val())+14) * 10) / 10;
		$( "#c_10311" ).val(String(amount));
		$( "#c_10311_slider" ).slider( "value", String(amount) );
		$( "#c_10311_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10312_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10312_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10312" ).val(ui.value);
		}
	});
	$( "#c_10312_show" ).val( "" + $("#c_10312_slider").slider("value") / 21.5 + "人前("  + $("#c_10312_slider").slider("value") + "g)");
	$( "#c_10312" ).val( $( "#c_10312_slider" ).slider( "value" ) );
	$( "#c_10312_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10312" ).val())-21.5) * 10) / 10);
		$( "#c_10312" ).val(String(amount));
		$( "#c_10312_slider" ).slider( "value", String(amount) );
		$( "#c_10312_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10312_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10312" ).val())+21.5) * 10) / 10;
		$( "#c_10312" ).val(String(amount));
		$( "#c_10312_slider" ).slider( "value", String(amount) );
		$( "#c_10312_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10313_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10313_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10313" ).val(ui.value);
		}
	});
	$( "#c_10313_show" ).val( "" + $("#c_10313_slider").slider("value") / 21.5 + "人前("  + $("#c_10313_slider").slider("value") + "g)");
	$( "#c_10313" ).val( $( "#c_10313_slider" ).slider( "value" ) );
	$( "#c_10313_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10313" ).val())-21.5) * 10) / 10);
		$( "#c_10313" ).val(String(amount));
		$( "#c_10313_slider" ).slider( "value", String(amount) );
		$( "#c_10313_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10313_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10313" ).val())+21.5) * 10) / 10;
		$( "#c_10313" ).val(String(amount));
		$( "#c_10313_slider" ).slider( "value", String(amount) );
		$( "#c_10313_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10314_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 600, step: 6,
		slide: function( event, ui ) {
			$( "#c_10314_show" ).val( "" + Math.round(ui.value / 6) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10314" ).val(ui.value);
		}
	});
	$( "#c_10314_show" ).val( "" + $("#c_10314_slider").slider("value") / 6 + "単位("  + $("#c_10314_slider").slider("value") + "g)");
	$( "#c_10314" ).val( $( "#c_10314_slider" ).slider( "value" ) );
	$( "#c_10314_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10314" ).val())-6) * 10) / 10);
		$( "#c_10314" ).val(String(amount));
		$( "#c_10314_slider" ).slider( "value", String(amount) );
		$( "#c_10314_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10314_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10314" ).val())+6) * 10) / 10;
		$( "#c_10314" ).val(String(amount));
		$( "#c_10314_slider" ).slider( "value", String(amount) );
		$( "#c_10314_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10315_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10315_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10315" ).val(ui.value);
		}
	});
	$( "#c_10315_show" ).val( "" + $("#c_10315_slider").slider("value") / 15 + "単位("  + $("#c_10315_slider").slider("value") + "g)");
	$( "#c_10315" ).val( $( "#c_10315_slider" ).slider( "value" ) );
	$( "#c_10315_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10315" ).val())-15) * 10) / 10);
		$( "#c_10315" ).val(String(amount));
		$( "#c_10315_slider" ).slider( "value", String(amount) );
		$( "#c_10315_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10315_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10315" ).val())+15) * 10) / 10;
		$( "#c_10315" ).val(String(amount));
		$( "#c_10315_slider" ).slider( "value", String(amount) );
		$( "#c_10315_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10316_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10316_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10316" ).val(ui.value);
		}
	});
	$( "#c_10316_show" ).val( "" + $("#c_10316_slider").slider("value") / 7 + "単位("  + $("#c_10316_slider").slider("value") + "g)");
	$( "#c_10316" ).val( $( "#c_10316_slider" ).slider( "value" ) );
	$( "#c_10316_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10316" ).val())-7) * 10) / 10);
		$( "#c_10316" ).val(String(amount));
		$( "#c_10316_slider" ).slider( "value", String(amount) );
		$( "#c_10316_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10316_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10316" ).val())+7) * 10) / 10;
		$( "#c_10316" ).val(String(amount));
		$( "#c_10316_slider" ).slider( "value", String(amount) );
		$( "#c_10316_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10317_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10317_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10317" ).val(ui.value);
		}
	});
	$( "#c_10317_show" ).val( "" + $("#c_10317_slider").slider("value") / 7 + "単位("  + $("#c_10317_slider").slider("value") + "g)");
	$( "#c_10317" ).val( $( "#c_10317_slider" ).slider( "value" ) );
	$( "#c_10317_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10317" ).val())-7) * 10) / 10);
		$( "#c_10317" ).val(String(amount));
		$( "#c_10317_slider" ).slider( "value", String(amount) );
		$( "#c_10317_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10317_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10317" ).val())+7) * 10) / 10;
		$( "#c_10317" ).val(String(amount));
		$( "#c_10317_slider" ).slider( "value", String(amount) );
		$( "#c_10317_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10318_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10318_show" ).val( "" + Math.round(ui.value / 5) / 10 + "個(" + ui.value + "g)" );
			$( "#c_10318" ).val(ui.value);
		}
	});
	$( "#c_10318_show" ).val( "" + $("#c_10318_slider").slider("value") / 5 + "個("  + $("#c_10318_slider").slider("value") + "g)");
	$( "#c_10318" ).val( $( "#c_10318_slider" ).slider( "value" ) );
	$( "#c_10318_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10318" ).val())-5) * 10) / 10);
		$( "#c_10318" ).val(String(amount));
		$( "#c_10318_slider" ).slider( "value", String(amount) );
		$( "#c_10318_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10318_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10318" ).val())+5) * 10) / 10;
		$( "#c_10318" ).val(String(amount));
		$( "#c_10318_slider" ).slider( "value", String(amount) );
		$( "#c_10318_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10319_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10319_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10319" ).val(ui.value);
		}
	});
	$( "#c_10319_show" ).val( "" + $("#c_10319_slider").slider("value") / 9 + "単位("  + $("#c_10319_slider").slider("value") + "g)");
	$( "#c_10319" ).val( $( "#c_10319_slider" ).slider( "value" ) );
	$( "#c_10319_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10319" ).val())-9) * 10) / 10);
		$( "#c_10319" ).val(String(amount));
		$( "#c_10319_slider" ).slider( "value", String(amount) );
		$( "#c_10319_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10319_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10319" ).val())+9) * 10) / 10;
		$( "#c_10319" ).val(String(amount));
		$( "#c_10319_slider" ).slider( "value", String(amount) );
		$( "#c_10319_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10320_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10320_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10320" ).val(ui.value);
		}
	});
	$( "#c_10320_show" ).val( "" + $("#c_10320_slider").slider("value") / 9 + "単位("  + $("#c_10320_slider").slider("value") + "g)");
	$( "#c_10320" ).val( $( "#c_10320_slider" ).slider( "value" ) );
	$( "#c_10320_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10320" ).val())-9) * 10) / 10);
		$( "#c_10320" ).val(String(amount));
		$( "#c_10320_slider" ).slider( "value", String(amount) );
		$( "#c_10320_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10320_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10320" ).val())+9) * 10) / 10;
		$( "#c_10320" ).val(String(amount));
		$( "#c_10320_slider" ).slider( "value", String(amount) );
		$( "#c_10320_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10321_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10321_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10321" ).val(ui.value);
		}
	});
	$( "#c_10321_show" ).val( "" + $("#c_10321_slider").slider("value") / 15 + "単位("  + $("#c_10321_slider").slider("value") + "g)");
	$( "#c_10321" ).val( $( "#c_10321_slider" ).slider( "value" ) );
	$( "#c_10321_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10321" ).val())-15) * 10) / 10);
		$( "#c_10321" ).val(String(amount));
		$( "#c_10321_slider" ).slider( "value", String(amount) );
		$( "#c_10321_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10321_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10321" ).val())+15) * 10) / 10;
		$( "#c_10321" ).val(String(amount));
		$( "#c_10321_slider" ).slider( "value", String(amount) );
		$( "#c_10321_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10322_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10322_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10322" ).val(ui.value);
		}
	});
	$( "#c_10322_show" ).val( "" + $("#c_10322_slider").slider("value") / 15 + "単位("  + $("#c_10322_slider").slider("value") + "g)");
	$( "#c_10322" ).val( $( "#c_10322_slider" ).slider( "value" ) );
	$( "#c_10322_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10322" ).val())-15) * 10) / 10);
		$( "#c_10322" ).val(String(amount));
		$( "#c_10322_slider" ).slider( "value", String(amount) );
		$( "#c_10322_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10322_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10322" ).val())+15) * 10) / 10;
		$( "#c_10322" ).val(String(amount));
		$( "#c_10322_slider" ).slider( "value", String(amount) );
		$( "#c_10322_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10323_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10323_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10323" ).val(ui.value);
		}
	});
	$( "#c_10323_show" ).val( "" + $("#c_10323_slider").slider("value") / 15 + "単位("  + $("#c_10323_slider").slider("value") + "g)");
	$( "#c_10323" ).val( $( "#c_10323_slider" ).slider( "value" ) );
	$( "#c_10323_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10323" ).val())-15) * 10) / 10);
		$( "#c_10323" ).val(String(amount));
		$( "#c_10323_slider" ).slider( "value", String(amount) );
		$( "#c_10323_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10323_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10323" ).val())+15) * 10) / 10;
		$( "#c_10323" ).val(String(amount));
		$( "#c_10323_slider" ).slider( "value", String(amount) );
		$( "#c_10323_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10324_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10324_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10324" ).val(ui.value);
		}
	});
	$( "#c_10324_show" ).val( "" + $("#c_10324_slider").slider("value") / 15 + "単位("  + $("#c_10324_slider").slider("value") + "g)");
	$( "#c_10324" ).val( $( "#c_10324_slider" ).slider( "value" ) );
	$( "#c_10324_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10324" ).val())-15) * 10) / 10);
		$( "#c_10324" ).val(String(amount));
		$( "#c_10324_slider" ).slider( "value", String(amount) );
		$( "#c_10324_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10324_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10324" ).val())+15) * 10) / 10;
		$( "#c_10324" ).val(String(amount));
		$( "#c_10324_slider" ).slider( "value", String(amount) );
		$( "#c_10324_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10325_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10325_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10325" ).val(ui.value);
		}
	});
	$( "#c_10325_show" ).val( "" + $("#c_10325_slider").slider("value") / 20 + "単位("  + $("#c_10325_slider").slider("value") + "g)");
	$( "#c_10325" ).val( $( "#c_10325_slider" ).slider( "value" ) );
	$( "#c_10325_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10325" ).val())-20) * 10) / 10);
		$( "#c_10325" ).val(String(amount));
		$( "#c_10325_slider" ).slider( "value", String(amount) );
		$( "#c_10325_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10325_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10325" ).val())+20) * 10) / 10;
		$( "#c_10325" ).val(String(amount));
		$( "#c_10325_slider" ).slider( "value", String(amount) );
		$( "#c_10325_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10326_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1000, step: 10,
		slide: function( event, ui ) {
			$( "#c_10326_show" ).val( "" + Math.round(ui.value / 10) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10326" ).val(ui.value);
		}
	});
	$( "#c_10326_show" ).val( "" + $("#c_10326_slider").slider("value") / 10 + "単位("  + $("#c_10326_slider").slider("value") + "g)");
	$( "#c_10326" ).val( $( "#c_10326_slider" ).slider( "value" ) );
	$( "#c_10326_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10326" ).val())-10) * 10) / 10);
		$( "#c_10326" ).val(String(amount));
		$( "#c_10326_slider" ).slider( "value", String(amount) );
		$( "#c_10326_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10326_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10326" ).val())+10) * 10) / 10;
		$( "#c_10326" ).val(String(amount));
		$( "#c_10326_slider" ).slider( "value", String(amount) );
		$( "#c_10326_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10327_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10327_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10327" ).val(ui.value);
		}
	});
	$( "#c_10327_show" ).val( "" + $("#c_10327_slider").slider("value") / 20 + "単位("  + $("#c_10327_slider").slider("value") + "g)");
	$( "#c_10327" ).val( $( "#c_10327_slider" ).slider( "value" ) );
	$( "#c_10327_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10327" ).val())-20) * 10) / 10);
		$( "#c_10327" ).val(String(amount));
		$( "#c_10327_slider" ).slider( "value", String(amount) );
		$( "#c_10327_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10327_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10327" ).val())+20) * 10) / 10;
		$( "#c_10327" ).val(String(amount));
		$( "#c_10327_slider" ).slider( "value", String(amount) );
		$( "#c_10327_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10328_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10328_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10328" ).val(ui.value);
		}
	});
	$( "#c_10328_show" ).val( "" + $("#c_10328_slider").slider("value") / 20 + "単位("  + $("#c_10328_slider").slider("value") + "g)");
	$( "#c_10328" ).val( $( "#c_10328_slider" ).slider( "value" ) );
	$( "#c_10328_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10328" ).val())-20) * 10) / 10);
		$( "#c_10328" ).val(String(amount));
		$( "#c_10328_slider" ).slider( "value", String(amount) );
		$( "#c_10328_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10328_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10328" ).val())+20) * 10) / 10;
		$( "#c_10328" ).val(String(amount));
		$( "#c_10328_slider" ).slider( "value", String(amount) );
		$( "#c_10328_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10329_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10329_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10329" ).val(ui.value);
		}
	});
	$( "#c_10329_show" ).val( "" + $("#c_10329_slider").slider("value") / 20 + "単位("  + $("#c_10329_slider").slider("value") + "g)");
	$( "#c_10329" ).val( $( "#c_10329_slider" ).slider( "value" ) );
	$( "#c_10329_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10329" ).val())-20) * 10) / 10);
		$( "#c_10329" ).val(String(amount));
		$( "#c_10329_slider" ).slider( "value", String(amount) );
		$( "#c_10329_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10329_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10329" ).val())+20) * 10) / 10;
		$( "#c_10329" ).val(String(amount));
		$( "#c_10329_slider" ).slider( "value", String(amount) );
		$( "#c_10329_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10330_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10330_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10330" ).val(ui.value);
		}
	});
	$( "#c_10330_show" ).val( "" + $("#c_10330_slider").slider("value") / 5 + "単位("  + $("#c_10330_slider").slider("value") + "g)");
	$( "#c_10330" ).val( $( "#c_10330_slider" ).slider( "value" ) );
	$( "#c_10330_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10330" ).val())-5) * 10) / 10);
		$( "#c_10330" ).val(String(amount));
		$( "#c_10330_slider" ).slider( "value", String(amount) );
		$( "#c_10330_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10330_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10330" ).val())+5) * 10) / 10;
		$( "#c_10330" ).val(String(amount));
		$( "#c_10330_slider" ).slider( "value", String(amount) );
		$( "#c_10330_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10331_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10331_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10331" ).val(ui.value);
		}
	});
	$( "#c_10331_show" ).val( "" + $("#c_10331_slider").slider("value") / 5 + "単位("  + $("#c_10331_slider").slider("value") + "g)");
	$( "#c_10331" ).val( $( "#c_10331_slider" ).slider( "value" ) );
	$( "#c_10331_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10331" ).val())-5) * 10) / 10);
		$( "#c_10331" ).val(String(amount));
		$( "#c_10331_slider" ).slider( "value", String(amount) );
		$( "#c_10331_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10331_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10331" ).val())+5) * 10) / 10;
		$( "#c_10331" ).val(String(amount));
		$( "#c_10331_slider" ).slider( "value", String(amount) );
		$( "#c_10331_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10332_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10332_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10332" ).val(ui.value);
		}
	});
	$( "#c_10332_show" ).val( "" + $("#c_10332_slider").slider("value") / 5 + "単位("  + $("#c_10332_slider").slider("value") + "g)");
	$( "#c_10332" ).val( $( "#c_10332_slider" ).slider( "value" ) );
	$( "#c_10332_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10332" ).val())-5) * 10) / 10);
		$( "#c_10332" ).val(String(amount));
		$( "#c_10332_slider" ).slider( "value", String(amount) );
		$( "#c_10332_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10332_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10332" ).val())+5) * 10) / 10;
		$( "#c_10332" ).val(String(amount));
		$( "#c_10332_slider" ).slider( "value", String(amount) );
		$( "#c_10332_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10333_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10333_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10333" ).val(ui.value);
		}
	});
	$( "#c_10333_show" ).val( "" + $("#c_10333_slider").slider("value") / 5 + "単位("  + $("#c_10333_slider").slider("value") + "g)");
	$( "#c_10333" ).val( $( "#c_10333_slider" ).slider( "value" ) );
	$( "#c_10333_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10333" ).val())-5) * 10) / 10);
		$( "#c_10333" ).val(String(amount));
		$( "#c_10333_slider" ).slider( "value", String(amount) );
		$( "#c_10333_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10333_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10333" ).val())+5) * 10) / 10;
		$( "#c_10333" ).val(String(amount));
		$( "#c_10333_slider" ).slider( "value", String(amount) );
		$( "#c_10333_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10334_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1400, step: 14,
		slide: function( event, ui ) {
			$( "#c_10334_show" ).val( "" + Math.round(ui.value / 14) / 10 + "杯(" + ui.value + "g)" );
			$( "#c_10334" ).val(ui.value);
		}
	});
	$( "#c_10334_show" ).val( "" + $("#c_10334_slider").slider("value") / 14 + "杯("  + $("#c_10334_slider").slider("value") + "g)");
	$( "#c_10334" ).val( $( "#c_10334_slider" ).slider( "value" ) );
	$( "#c_10334_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10334" ).val())-14) * 10) / 10);
		$( "#c_10334" ).val(String(amount));
		$( "#c_10334_slider" ).slider( "value", String(amount) );
		$( "#c_10334_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10334_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10334" ).val())+14) * 10) / 10;
		$( "#c_10334" ).val(String(amount));
		$( "#c_10334_slider" ).slider( "value", String(amount) );
		$( "#c_10334_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10335_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10335_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10335" ).val(ui.value);
		}
	});
	$( "#c_10335_show" ).val( "" + $("#c_10335_slider").slider("value") / 21.5 + "人前("  + $("#c_10335_slider").slider("value") + "g)");
	$( "#c_10335" ).val( $( "#c_10335_slider" ).slider( "value" ) );
	$( "#c_10335_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10335" ).val())-21.5) * 10) / 10);
		$( "#c_10335" ).val(String(amount));
		$( "#c_10335_slider" ).slider( "value", String(amount) );
		$( "#c_10335_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10335_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10335" ).val())+21.5) * 10) / 10;
		$( "#c_10335" ).val(String(amount));
		$( "#c_10335_slider" ).slider( "value", String(amount) );
		$( "#c_10335_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10336_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10336_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10336" ).val(ui.value);
		}
	});
	$( "#c_10336_show" ).val( "" + $("#c_10336_slider").slider("value") / 21.5 + "人前("  + $("#c_10336_slider").slider("value") + "g)");
	$( "#c_10336" ).val( $( "#c_10336_slider" ).slider( "value" ) );
	$( "#c_10336_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10336" ).val())-21.5) * 10) / 10);
		$( "#c_10336" ).val(String(amount));
		$( "#c_10336_slider" ).slider( "value", String(amount) );
		$( "#c_10336_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10336_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10336" ).val())+21.5) * 10) / 10;
		$( "#c_10336" ).val(String(amount));
		$( "#c_10336_slider" ).slider( "value", String(amount) );
		$( "#c_10336_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10337_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 600, step: 6,
		slide: function( event, ui ) {
			$( "#c_10337_show" ).val( "" + Math.round(ui.value / 6) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10337" ).val(ui.value);
		}
	});
	$( "#c_10337_show" ).val( "" + $("#c_10337_slider").slider("value") / 6 + "単位("  + $("#c_10337_slider").slider("value") + "g)");
	$( "#c_10337" ).val( $( "#c_10337_slider" ).slider( "value" ) );
	$( "#c_10337_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10337" ).val())-6) * 10) / 10);
		$( "#c_10337" ).val(String(amount));
		$( "#c_10337_slider" ).slider( "value", String(amount) );
		$( "#c_10337_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10337_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10337" ).val())+6) * 10) / 10;
		$( "#c_10337" ).val(String(amount));
		$( "#c_10337_slider" ).slider( "value", String(amount) );
		$( "#c_10337_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10338_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10338_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10338" ).val(ui.value);
		}
	});
	$( "#c_10338_show" ).val( "" + $("#c_10338_slider").slider("value") / 15 + "単位("  + $("#c_10338_slider").slider("value") + "g)");
	$( "#c_10338" ).val( $( "#c_10338_slider" ).slider( "value" ) );
	$( "#c_10338_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10338" ).val())-15) * 10) / 10);
		$( "#c_10338" ).val(String(amount));
		$( "#c_10338_slider" ).slider( "value", String(amount) );
		$( "#c_10338_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10338_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10338" ).val())+15) * 10) / 10;
		$( "#c_10338" ).val(String(amount));
		$( "#c_10338_slider" ).slider( "value", String(amount) );
		$( "#c_10338_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10339_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10339_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10339" ).val(ui.value);
		}
	});
	$( "#c_10339_show" ).val( "" + $("#c_10339_slider").slider("value") / 7 + "単位("  + $("#c_10339_slider").slider("value") + "g)");
	$( "#c_10339" ).val( $( "#c_10339_slider" ).slider( "value" ) );
	$( "#c_10339_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10339" ).val())-7) * 10) / 10);
		$( "#c_10339" ).val(String(amount));
		$( "#c_10339_slider" ).slider( "value", String(amount) );
		$( "#c_10339_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10339_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10339" ).val())+7) * 10) / 10;
		$( "#c_10339" ).val(String(amount));
		$( "#c_10339_slider" ).slider( "value", String(amount) );
		$( "#c_10339_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10340_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10340_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10340" ).val(ui.value);
		}
	});
	$( "#c_10340_show" ).val( "" + $("#c_10340_slider").slider("value") / 7 + "単位("  + $("#c_10340_slider").slider("value") + "g)");
	$( "#c_10340" ).val( $( "#c_10340_slider" ).slider( "value" ) );
	$( "#c_10340_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10340" ).val())-7) * 10) / 10);
		$( "#c_10340" ).val(String(amount));
		$( "#c_10340_slider" ).slider( "value", String(amount) );
		$( "#c_10340_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10340_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10340" ).val())+7) * 10) / 10;
		$( "#c_10340" ).val(String(amount));
		$( "#c_10340_slider" ).slider( "value", String(amount) );
		$( "#c_10340_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10341_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10341_show" ).val( "" + Math.round(ui.value / 5) / 10 + "個(" + ui.value + "g)" );
			$( "#c_10341" ).val(ui.value);
		}
	});
	$( "#c_10341_show" ).val( "" + $("#c_10341_slider").slider("value") / 5 + "個("  + $("#c_10341_slider").slider("value") + "g)");
	$( "#c_10341" ).val( $( "#c_10341_slider" ).slider( "value" ) );
	$( "#c_10341_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10341" ).val())-5) * 10) / 10);
		$( "#c_10341" ).val(String(amount));
		$( "#c_10341_slider" ).slider( "value", String(amount) );
		$( "#c_10341_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10341_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10341" ).val())+5) * 10) / 10;
		$( "#c_10341" ).val(String(amount));
		$( "#c_10341_slider" ).slider( "value", String(amount) );
		$( "#c_10341_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10342_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10342_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10342" ).val(ui.value);
		}
	});
	$( "#c_10342_show" ).val( "" + $("#c_10342_slider").slider("value") / 9 + "単位("  + $("#c_10342_slider").slider("value") + "g)");
	$( "#c_10342" ).val( $( "#c_10342_slider" ).slider( "value" ) );
	$( "#c_10342_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10342" ).val())-9) * 10) / 10);
		$( "#c_10342" ).val(String(amount));
		$( "#c_10342_slider" ).slider( "value", String(amount) );
		$( "#c_10342_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10342_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10342" ).val())+9) * 10) / 10;
		$( "#c_10342" ).val(String(amount));
		$( "#c_10342_slider" ).slider( "value", String(amount) );
		$( "#c_10342_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10343_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10343_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10343" ).val(ui.value);
		}
	});
	$( "#c_10343_show" ).val( "" + $("#c_10343_slider").slider("value") / 9 + "単位("  + $("#c_10343_slider").slider("value") + "g)");
	$( "#c_10343" ).val( $( "#c_10343_slider" ).slider( "value" ) );
	$( "#c_10343_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10343" ).val())-9) * 10) / 10);
		$( "#c_10343" ).val(String(amount));
		$( "#c_10343_slider" ).slider( "value", String(amount) );
		$( "#c_10343_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10343_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10343" ).val())+9) * 10) / 10;
		$( "#c_10343" ).val(String(amount));
		$( "#c_10343_slider" ).slider( "value", String(amount) );
		$( "#c_10343_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10344_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10344_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10344" ).val(ui.value);
		}
	});
	$( "#c_10344_show" ).val( "" + $("#c_10344_slider").slider("value") / 15 + "単位("  + $("#c_10344_slider").slider("value") + "g)");
	$( "#c_10344" ).val( $( "#c_10344_slider" ).slider( "value" ) );
	$( "#c_10344_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10344" ).val())-15) * 10) / 10);
		$( "#c_10344" ).val(String(amount));
		$( "#c_10344_slider" ).slider( "value", String(amount) );
		$( "#c_10344_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10344_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10344" ).val())+15) * 10) / 10;
		$( "#c_10344" ).val(String(amount));
		$( "#c_10344_slider" ).slider( "value", String(amount) );
		$( "#c_10344_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10345_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10345_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10345" ).val(ui.value);
		}
	});
	$( "#c_10345_show" ).val( "" + $("#c_10345_slider").slider("value") / 15 + "単位("  + $("#c_10345_slider").slider("value") + "g)");
	$( "#c_10345" ).val( $( "#c_10345_slider" ).slider( "value" ) );
	$( "#c_10345_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10345" ).val())-15) * 10) / 10);
		$( "#c_10345" ).val(String(amount));
		$( "#c_10345_slider" ).slider( "value", String(amount) );
		$( "#c_10345_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10345_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10345" ).val())+15) * 10) / 10;
		$( "#c_10345" ).val(String(amount));
		$( "#c_10345_slider" ).slider( "value", String(amount) );
		$( "#c_10345_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10346_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10346_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10346" ).val(ui.value);
		}
	});
	$( "#c_10346_show" ).val( "" + $("#c_10346_slider").slider("value") / 15 + "単位("  + $("#c_10346_slider").slider("value") + "g)");
	$( "#c_10346" ).val( $( "#c_10346_slider" ).slider( "value" ) );
	$( "#c_10346_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10346" ).val())-15) * 10) / 10);
		$( "#c_10346" ).val(String(amount));
		$( "#c_10346_slider" ).slider( "value", String(amount) );
		$( "#c_10346_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10346_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10346" ).val())+15) * 10) / 10;
		$( "#c_10346" ).val(String(amount));
		$( "#c_10346_slider" ).slider( "value", String(amount) );
		$( "#c_10346_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10347_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10347_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10347" ).val(ui.value);
		}
	});
	$( "#c_10347_show" ).val( "" + $("#c_10347_slider").slider("value") / 15 + "単位("  + $("#c_10347_slider").slider("value") + "g)");
	$( "#c_10347" ).val( $( "#c_10347_slider" ).slider( "value" ) );
	$( "#c_10347_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10347" ).val())-15) * 10) / 10);
		$( "#c_10347" ).val(String(amount));
		$( "#c_10347_slider" ).slider( "value", String(amount) );
		$( "#c_10347_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10347_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10347" ).val())+15) * 10) / 10;
		$( "#c_10347" ).val(String(amount));
		$( "#c_10347_slider" ).slider( "value", String(amount) );
		$( "#c_10347_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10348_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10348_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10348" ).val(ui.value);
		}
	});
	$( "#c_10348_show" ).val( "" + $("#c_10348_slider").slider("value") / 20 + "単位("  + $("#c_10348_slider").slider("value") + "g)");
	$( "#c_10348" ).val( $( "#c_10348_slider" ).slider( "value" ) );
	$( "#c_10348_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10348" ).val())-20) * 10) / 10);
		$( "#c_10348" ).val(String(amount));
		$( "#c_10348_slider" ).slider( "value", String(amount) );
		$( "#c_10348_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10348_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10348" ).val())+20) * 10) / 10;
		$( "#c_10348" ).val(String(amount));
		$( "#c_10348_slider" ).slider( "value", String(amount) );
		$( "#c_10348_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10349_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1000, step: 10,
		slide: function( event, ui ) {
			$( "#c_10349_show" ).val( "" + Math.round(ui.value / 10) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10349" ).val(ui.value);
		}
	});
	$( "#c_10349_show" ).val( "" + $("#c_10349_slider").slider("value") / 10 + "単位("  + $("#c_10349_slider").slider("value") + "g)");
	$( "#c_10349" ).val( $( "#c_10349_slider" ).slider( "value" ) );
	$( "#c_10349_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10349" ).val())-10) * 10) / 10);
		$( "#c_10349" ).val(String(amount));
		$( "#c_10349_slider" ).slider( "value", String(amount) );
		$( "#c_10349_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10349_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10349" ).val())+10) * 10) / 10;
		$( "#c_10349" ).val(String(amount));
		$( "#c_10349_slider" ).slider( "value", String(amount) );
		$( "#c_10349_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10350_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10350_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10350" ).val(ui.value);
		}
	});
	$( "#c_10350_show" ).val( "" + $("#c_10350_slider").slider("value") / 20 + "単位("  + $("#c_10350_slider").slider("value") + "g)");
	$( "#c_10350" ).val( $( "#c_10350_slider" ).slider( "value" ) );
	$( "#c_10350_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10350" ).val())-20) * 10) / 10);
		$( "#c_10350" ).val(String(amount));
		$( "#c_10350_slider" ).slider( "value", String(amount) );
		$( "#c_10350_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10350_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10350" ).val())+20) * 10) / 10;
		$( "#c_10350" ).val(String(amount));
		$( "#c_10350_slider" ).slider( "value", String(amount) );
		$( "#c_10350_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10351_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10351_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10351" ).val(ui.value);
		}
	});
	$( "#c_10351_show" ).val( "" + $("#c_10351_slider").slider("value") / 20 + "単位("  + $("#c_10351_slider").slider("value") + "g)");
	$( "#c_10351" ).val( $( "#c_10351_slider" ).slider( "value" ) );
	$( "#c_10351_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10351" ).val())-20) * 10) / 10);
		$( "#c_10351" ).val(String(amount));
		$( "#c_10351_slider" ).slider( "value", String(amount) );
		$( "#c_10351_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10351_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10351" ).val())+20) * 10) / 10;
		$( "#c_10351" ).val(String(amount));
		$( "#c_10351_slider" ).slider( "value", String(amount) );
		$( "#c_10351_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10352_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10352_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10352" ).val(ui.value);
		}
	});
	$( "#c_10352_show" ).val( "" + $("#c_10352_slider").slider("value") / 20 + "単位("  + $("#c_10352_slider").slider("value") + "g)");
	$( "#c_10352" ).val( $( "#c_10352_slider" ).slider( "value" ) );
	$( "#c_10352_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10352" ).val())-20) * 10) / 10);
		$( "#c_10352" ).val(String(amount));
		$( "#c_10352_slider" ).slider( "value", String(amount) );
		$( "#c_10352_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10352_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10352" ).val())+20) * 10) / 10;
		$( "#c_10352" ).val(String(amount));
		$( "#c_10352_slider" ).slider( "value", String(amount) );
		$( "#c_10352_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10353_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10353_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10353" ).val(ui.value);
		}
	});
	$( "#c_10353_show" ).val( "" + $("#c_10353_slider").slider("value") / 5 + "単位("  + $("#c_10353_slider").slider("value") + "g)");
	$( "#c_10353" ).val( $( "#c_10353_slider" ).slider( "value" ) );
	$( "#c_10353_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10353" ).val())-5) * 10) / 10);
		$( "#c_10353" ).val(String(amount));
		$( "#c_10353_slider" ).slider( "value", String(amount) );
		$( "#c_10353_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10353_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10353" ).val())+5) * 10) / 10;
		$( "#c_10353" ).val(String(amount));
		$( "#c_10353_slider" ).slider( "value", String(amount) );
		$( "#c_10353_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10354_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10354_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10354" ).val(ui.value);
		}
	});
	$( "#c_10354_show" ).val( "" + $("#c_10354_slider").slider("value") / 5 + "単位("  + $("#c_10354_slider").slider("value") + "g)");
	$( "#c_10354" ).val( $( "#c_10354_slider" ).slider( "value" ) );
	$( "#c_10354_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10354" ).val())-5) * 10) / 10);
		$( "#c_10354" ).val(String(amount));
		$( "#c_10354_slider" ).slider( "value", String(amount) );
		$( "#c_10354_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10354_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10354" ).val())+5) * 10) / 10;
		$( "#c_10354" ).val(String(amount));
		$( "#c_10354_slider" ).slider( "value", String(amount) );
		$( "#c_10354_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10355_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10355_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10355" ).val(ui.value);
		}
	});
	$( "#c_10355_show" ).val( "" + $("#c_10355_slider").slider("value") / 5 + "単位("  + $("#c_10355_slider").slider("value") + "g)");
	$( "#c_10355" ).val( $( "#c_10355_slider" ).slider( "value" ) );
	$( "#c_10355_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10355" ).val())-5) * 10) / 10);
		$( "#c_10355" ).val(String(amount));
		$( "#c_10355_slider" ).slider( "value", String(amount) );
		$( "#c_10355_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10355_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10355" ).val())+5) * 10) / 10;
		$( "#c_10355" ).val(String(amount));
		$( "#c_10355_slider" ).slider( "value", String(amount) );
		$( "#c_10355_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10356_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10356_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10356" ).val(ui.value);
		}
	});
	$( "#c_10356_show" ).val( "" + $("#c_10356_slider").slider("value") / 5 + "単位("  + $("#c_10356_slider").slider("value") + "g)");
	$( "#c_10356" ).val( $( "#c_10356_slider" ).slider( "value" ) );
	$( "#c_10356_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10356" ).val())-5) * 10) / 10);
		$( "#c_10356" ).val(String(amount));
		$( "#c_10356_slider" ).slider( "value", String(amount) );
		$( "#c_10356_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10356_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10356" ).val())+5) * 10) / 10;
		$( "#c_10356" ).val(String(amount));
		$( "#c_10356_slider" ).slider( "value", String(amount) );
		$( "#c_10356_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10357_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1400, step: 14,
		slide: function( event, ui ) {
			$( "#c_10357_show" ).val( "" + Math.round(ui.value / 14) / 10 + "杯(" + ui.value + "g)" );
			$( "#c_10357" ).val(ui.value);
		}
	});
	$( "#c_10357_show" ).val( "" + $("#c_10357_slider").slider("value") / 14 + "杯("  + $("#c_10357_slider").slider("value") + "g)");
	$( "#c_10357" ).val( $( "#c_10357_slider" ).slider( "value" ) );
	$( "#c_10357_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10357" ).val())-14) * 10) / 10);
		$( "#c_10357" ).val(String(amount));
		$( "#c_10357_slider" ).slider( "value", String(amount) );
		$( "#c_10357_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10357_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10357" ).val())+14) * 10) / 10;
		$( "#c_10357" ).val(String(amount));
		$( "#c_10357_slider" ).slider( "value", String(amount) );
		$( "#c_10357_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10358_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10358_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10358" ).val(ui.value);
		}
	});
	$( "#c_10358_show" ).val( "" + $("#c_10358_slider").slider("value") / 21.5 + "人前("  + $("#c_10358_slider").slider("value") + "g)");
	$( "#c_10358" ).val( $( "#c_10358_slider" ).slider( "value" ) );
	$( "#c_10358_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10358" ).val())-21.5) * 10) / 10);
		$( "#c_10358" ).val(String(amount));
		$( "#c_10358_slider" ).slider( "value", String(amount) );
		$( "#c_10358_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10358_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10358" ).val())+21.5) * 10) / 10;
		$( "#c_10358" ).val(String(amount));
		$( "#c_10358_slider" ).slider( "value", String(amount) );
		$( "#c_10358_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10359_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10359_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10359" ).val(ui.value);
		}
	});
	$( "#c_10359_show" ).val( "" + $("#c_10359_slider").slider("value") / 21.5 + "人前("  + $("#c_10359_slider").slider("value") + "g)");
	$( "#c_10359" ).val( $( "#c_10359_slider" ).slider( "value" ) );
	$( "#c_10359_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10359" ).val())-21.5) * 10) / 10);
		$( "#c_10359" ).val(String(amount));
		$( "#c_10359_slider" ).slider( "value", String(amount) );
		$( "#c_10359_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10359_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10359" ).val())+21.5) * 10) / 10;
		$( "#c_10359" ).val(String(amount));
		$( "#c_10359_slider" ).slider( "value", String(amount) );
		$( "#c_10359_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10360_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 600, step: 6,
		slide: function( event, ui ) {
			$( "#c_10360_show" ).val( "" + Math.round(ui.value / 6) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10360" ).val(ui.value);
		}
	});
	$( "#c_10360_show" ).val( "" + $("#c_10360_slider").slider("value") / 6 + "単位("  + $("#c_10360_slider").slider("value") + "g)");
	$( "#c_10360" ).val( $( "#c_10360_slider" ).slider( "value" ) );
	$( "#c_10360_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10360" ).val())-6) * 10) / 10);
		$( "#c_10360" ).val(String(amount));
		$( "#c_10360_slider" ).slider( "value", String(amount) );
		$( "#c_10360_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10360_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10360" ).val())+6) * 10) / 10;
		$( "#c_10360" ).val(String(amount));
		$( "#c_10360_slider" ).slider( "value", String(amount) );
		$( "#c_10360_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10361_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10361_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10361" ).val(ui.value);
		}
	});
	$( "#c_10361_show" ).val( "" + $("#c_10361_slider").slider("value") / 15 + "単位("  + $("#c_10361_slider").slider("value") + "g)");
	$( "#c_10361" ).val( $( "#c_10361_slider" ).slider( "value" ) );
	$( "#c_10361_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10361" ).val())-15) * 10) / 10);
		$( "#c_10361" ).val(String(amount));
		$( "#c_10361_slider" ).slider( "value", String(amount) );
		$( "#c_10361_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10361_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10361" ).val())+15) * 10) / 10;
		$( "#c_10361" ).val(String(amount));
		$( "#c_10361_slider" ).slider( "value", String(amount) );
		$( "#c_10361_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10362_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10362_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10362" ).val(ui.value);
		}
	});
	$( "#c_10362_show" ).val( "" + $("#c_10362_slider").slider("value") / 7 + "単位("  + $("#c_10362_slider").slider("value") + "g)");
	$( "#c_10362" ).val( $( "#c_10362_slider" ).slider( "value" ) );
	$( "#c_10362_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10362" ).val())-7) * 10) / 10);
		$( "#c_10362" ).val(String(amount));
		$( "#c_10362_slider" ).slider( "value", String(amount) );
		$( "#c_10362_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10362_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10362" ).val())+7) * 10) / 10;
		$( "#c_10362" ).val(String(amount));
		$( "#c_10362_slider" ).slider( "value", String(amount) );
		$( "#c_10362_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10363_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10363_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10363" ).val(ui.value);
		}
	});
	$( "#c_10363_show" ).val( "" + $("#c_10363_slider").slider("value") / 7 + "単位("  + $("#c_10363_slider").slider("value") + "g)");
	$( "#c_10363" ).val( $( "#c_10363_slider" ).slider( "value" ) );
	$( "#c_10363_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10363" ).val())-7) * 10) / 10);
		$( "#c_10363" ).val(String(amount));
		$( "#c_10363_slider" ).slider( "value", String(amount) );
		$( "#c_10363_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10363_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10363" ).val())+7) * 10) / 10;
		$( "#c_10363" ).val(String(amount));
		$( "#c_10363_slider" ).slider( "value", String(amount) );
		$( "#c_10363_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10364_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10364_show" ).val( "" + Math.round(ui.value / 5) / 10 + "個(" + ui.value + "g)" );
			$( "#c_10364" ).val(ui.value);
		}
	});
	$( "#c_10364_show" ).val( "" + $("#c_10364_slider").slider("value") / 5 + "個("  + $("#c_10364_slider").slider("value") + "g)");
	$( "#c_10364" ).val( $( "#c_10364_slider" ).slider( "value" ) );
	$( "#c_10364_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10364" ).val())-5) * 10) / 10);
		$( "#c_10364" ).val(String(amount));
		$( "#c_10364_slider" ).slider( "value", String(amount) );
		$( "#c_10364_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10364_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10364" ).val())+5) * 10) / 10;
		$( "#c_10364" ).val(String(amount));
		$( "#c_10364_slider" ).slider( "value", String(amount) );
		$( "#c_10364_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10365_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10365_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10365" ).val(ui.value);
		}
	});
	$( "#c_10365_show" ).val( "" + $("#c_10365_slider").slider("value") / 9 + "単位("  + $("#c_10365_slider").slider("value") + "g)");
	$( "#c_10365" ).val( $( "#c_10365_slider" ).slider( "value" ) );
	$( "#c_10365_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10365" ).val())-9) * 10) / 10);
		$( "#c_10365" ).val(String(amount));
		$( "#c_10365_slider" ).slider( "value", String(amount) );
		$( "#c_10365_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10365_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10365" ).val())+9) * 10) / 10;
		$( "#c_10365" ).val(String(amount));
		$( "#c_10365_slider" ).slider( "value", String(amount) );
		$( "#c_10365_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10366_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10366_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10366" ).val(ui.value);
		}
	});
	$( "#c_10366_show" ).val( "" + $("#c_10366_slider").slider("value") / 9 + "単位("  + $("#c_10366_slider").slider("value") + "g)");
	$( "#c_10366" ).val( $( "#c_10366_slider" ).slider( "value" ) );
	$( "#c_10366_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10366" ).val())-9) * 10) / 10);
		$( "#c_10366" ).val(String(amount));
		$( "#c_10366_slider" ).slider( "value", String(amount) );
		$( "#c_10366_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10366_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10366" ).val())+9) * 10) / 10;
		$( "#c_10366" ).val(String(amount));
		$( "#c_10366_slider" ).slider( "value", String(amount) );
		$( "#c_10366_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10367_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10367_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10367" ).val(ui.value);
		}
	});
	$( "#c_10367_show" ).val( "" + $("#c_10367_slider").slider("value") / 15 + "単位("  + $("#c_10367_slider").slider("value") + "g)");
	$( "#c_10367" ).val( $( "#c_10367_slider" ).slider( "value" ) );
	$( "#c_10367_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10367" ).val())-15) * 10) / 10);
		$( "#c_10367" ).val(String(amount));
		$( "#c_10367_slider" ).slider( "value", String(amount) );
		$( "#c_10367_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10367_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10367" ).val())+15) * 10) / 10;
		$( "#c_10367" ).val(String(amount));
		$( "#c_10367_slider" ).slider( "value", String(amount) );
		$( "#c_10367_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10368_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10368_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10368" ).val(ui.value);
		}
	});
	$( "#c_10368_show" ).val( "" + $("#c_10368_slider").slider("value") / 15 + "単位("  + $("#c_10368_slider").slider("value") + "g)");
	$( "#c_10368" ).val( $( "#c_10368_slider" ).slider( "value" ) );
	$( "#c_10368_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10368" ).val())-15) * 10) / 10);
		$( "#c_10368" ).val(String(amount));
		$( "#c_10368_slider" ).slider( "value", String(amount) );
		$( "#c_10368_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10368_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10368" ).val())+15) * 10) / 10;
		$( "#c_10368" ).val(String(amount));
		$( "#c_10368_slider" ).slider( "value", String(amount) );
		$( "#c_10368_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10369_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10369_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10369" ).val(ui.value);
		}
	});
	$( "#c_10369_show" ).val( "" + $("#c_10369_slider").slider("value") / 15 + "単位("  + $("#c_10369_slider").slider("value") + "g)");
	$( "#c_10369" ).val( $( "#c_10369_slider" ).slider( "value" ) );
	$( "#c_10369_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10369" ).val())-15) * 10) / 10);
		$( "#c_10369" ).val(String(amount));
		$( "#c_10369_slider" ).slider( "value", String(amount) );
		$( "#c_10369_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10369_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10369" ).val())+15) * 10) / 10;
		$( "#c_10369" ).val(String(amount));
		$( "#c_10369_slider" ).slider( "value", String(amount) );
		$( "#c_10369_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10370_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10370_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10370" ).val(ui.value);
		}
	});
	$( "#c_10370_show" ).val( "" + $("#c_10370_slider").slider("value") / 15 + "単位("  + $("#c_10370_slider").slider("value") + "g)");
	$( "#c_10370" ).val( $( "#c_10370_slider" ).slider( "value" ) );
	$( "#c_10370_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10370" ).val())-15) * 10) / 10);
		$( "#c_10370" ).val(String(amount));
		$( "#c_10370_slider" ).slider( "value", String(amount) );
		$( "#c_10370_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10370_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10370" ).val())+15) * 10) / 10;
		$( "#c_10370" ).val(String(amount));
		$( "#c_10370_slider" ).slider( "value", String(amount) );
		$( "#c_10370_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10371_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10371_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10371" ).val(ui.value);
		}
	});
	$( "#c_10371_show" ).val( "" + $("#c_10371_slider").slider("value") / 20 + "単位("  + $("#c_10371_slider").slider("value") + "g)");
	$( "#c_10371" ).val( $( "#c_10371_slider" ).slider( "value" ) );
	$( "#c_10371_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10371" ).val())-20) * 10) / 10);
		$( "#c_10371" ).val(String(amount));
		$( "#c_10371_slider" ).slider( "value", String(amount) );
		$( "#c_10371_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10371_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10371" ).val())+20) * 10) / 10;
		$( "#c_10371" ).val(String(amount));
		$( "#c_10371_slider" ).slider( "value", String(amount) );
		$( "#c_10371_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10372_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1000, step: 10,
		slide: function( event, ui ) {
			$( "#c_10372_show" ).val( "" + Math.round(ui.value / 10) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10372" ).val(ui.value);
		}
	});
	$( "#c_10372_show" ).val( "" + $("#c_10372_slider").slider("value") / 10 + "単位("  + $("#c_10372_slider").slider("value") + "g)");
	$( "#c_10372" ).val( $( "#c_10372_slider" ).slider( "value" ) );
	$( "#c_10372_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10372" ).val())-10) * 10) / 10);
		$( "#c_10372" ).val(String(amount));
		$( "#c_10372_slider" ).slider( "value", String(amount) );
		$( "#c_10372_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10372_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10372" ).val())+10) * 10) / 10;
		$( "#c_10372" ).val(String(amount));
		$( "#c_10372_slider" ).slider( "value", String(amount) );
		$( "#c_10372_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10373_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10373_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10373" ).val(ui.value);
		}
	});
	$( "#c_10373_show" ).val( "" + $("#c_10373_slider").slider("value") / 20 + "単位("  + $("#c_10373_slider").slider("value") + "g)");
	$( "#c_10373" ).val( $( "#c_10373_slider" ).slider( "value" ) );
	$( "#c_10373_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10373" ).val())-20) * 10) / 10);
		$( "#c_10373" ).val(String(amount));
		$( "#c_10373_slider" ).slider( "value", String(amount) );
		$( "#c_10373_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10373_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10373" ).val())+20) * 10) / 10;
		$( "#c_10373" ).val(String(amount));
		$( "#c_10373_slider" ).slider( "value", String(amount) );
		$( "#c_10373_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10374_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10374_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10374" ).val(ui.value);
		}
	});
	$( "#c_10374_show" ).val( "" + $("#c_10374_slider").slider("value") / 20 + "単位("  + $("#c_10374_slider").slider("value") + "g)");
	$( "#c_10374" ).val( $( "#c_10374_slider" ).slider( "value" ) );
	$( "#c_10374_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10374" ).val())-20) * 10) / 10);
		$( "#c_10374" ).val(String(amount));
		$( "#c_10374_slider" ).slider( "value", String(amount) );
		$( "#c_10374_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10374_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10374" ).val())+20) * 10) / 10;
		$( "#c_10374" ).val(String(amount));
		$( "#c_10374_slider" ).slider( "value", String(amount) );
		$( "#c_10374_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10375_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10375_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10375" ).val(ui.value);
		}
	});
	$( "#c_10375_show" ).val( "" + $("#c_10375_slider").slider("value") / 20 + "単位("  + $("#c_10375_slider").slider("value") + "g)");
	$( "#c_10375" ).val( $( "#c_10375_slider" ).slider( "value" ) );
	$( "#c_10375_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10375" ).val())-20) * 10) / 10);
		$( "#c_10375" ).val(String(amount));
		$( "#c_10375_slider" ).slider( "value", String(amount) );
		$( "#c_10375_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10375_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10375" ).val())+20) * 10) / 10;
		$( "#c_10375" ).val(String(amount));
		$( "#c_10375_slider" ).slider( "value", String(amount) );
		$( "#c_10375_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10376_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10376_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10376" ).val(ui.value);
		}
	});
	$( "#c_10376_show" ).val( "" + $("#c_10376_slider").slider("value") / 5 + "単位("  + $("#c_10376_slider").slider("value") + "g)");
	$( "#c_10376" ).val( $( "#c_10376_slider" ).slider( "value" ) );
	$( "#c_10376_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10376" ).val())-5) * 10) / 10);
		$( "#c_10376" ).val(String(amount));
		$( "#c_10376_slider" ).slider( "value", String(amount) );
		$( "#c_10376_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10376_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10376" ).val())+5) * 10) / 10;
		$( "#c_10376" ).val(String(amount));
		$( "#c_10376_slider" ).slider( "value", String(amount) );
		$( "#c_10376_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10377_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10377_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10377" ).val(ui.value);
		}
	});
	$( "#c_10377_show" ).val( "" + $("#c_10377_slider").slider("value") / 5 + "単位("  + $("#c_10377_slider").slider("value") + "g)");
	$( "#c_10377" ).val( $( "#c_10377_slider" ).slider( "value" ) );
	$( "#c_10377_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10377" ).val())-5) * 10) / 10);
		$( "#c_10377" ).val(String(amount));
		$( "#c_10377_slider" ).slider( "value", String(amount) );
		$( "#c_10377_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10377_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10377" ).val())+5) * 10) / 10;
		$( "#c_10377" ).val(String(amount));
		$( "#c_10377_slider" ).slider( "value", String(amount) );
		$( "#c_10377_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10378_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10378_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10378" ).val(ui.value);
		}
	});
	$( "#c_10378_show" ).val( "" + $("#c_10378_slider").slider("value") / 5 + "単位("  + $("#c_10378_slider").slider("value") + "g)");
	$( "#c_10378" ).val( $( "#c_10378_slider" ).slider( "value" ) );
	$( "#c_10378_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10378" ).val())-5) * 10) / 10);
		$( "#c_10378" ).val(String(amount));
		$( "#c_10378_slider" ).slider( "value", String(amount) );
		$( "#c_10378_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10378_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10378" ).val())+5) * 10) / 10;
		$( "#c_10378" ).val(String(amount));
		$( "#c_10378_slider" ).slider( "value", String(amount) );
		$( "#c_10378_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10379_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10379_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10379" ).val(ui.value);
		}
	});
	$( "#c_10379_show" ).val( "" + $("#c_10379_slider").slider("value") / 5 + "単位("  + $("#c_10379_slider").slider("value") + "g)");
	$( "#c_10379" ).val( $( "#c_10379_slider" ).slider( "value" ) );
	$( "#c_10379_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10379" ).val())-5) * 10) / 10);
		$( "#c_10379" ).val(String(amount));
		$( "#c_10379_slider" ).slider( "value", String(amount) );
		$( "#c_10379_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10379_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10379" ).val())+5) * 10) / 10;
		$( "#c_10379" ).val(String(amount));
		$( "#c_10379_slider" ).slider( "value", String(amount) );
		$( "#c_10379_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10380_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1400, step: 14,
		slide: function( event, ui ) {
			$( "#c_10380_show" ).val( "" + Math.round(ui.value / 14) / 10 + "杯(" + ui.value + "g)" );
			$( "#c_10380" ).val(ui.value);
		}
	});
	$( "#c_10380_show" ).val( "" + $("#c_10380_slider").slider("value") / 14 + "杯("  + $("#c_10380_slider").slider("value") + "g)");
	$( "#c_10380" ).val( $( "#c_10380_slider" ).slider( "value" ) );
	$( "#c_10380_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10380" ).val())-14) * 10) / 10);
		$( "#c_10380" ).val(String(amount));
		$( "#c_10380_slider" ).slider( "value", String(amount) );
		$( "#c_10380_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10380_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10380" ).val())+14) * 10) / 10;
		$( "#c_10380" ).val(String(amount));
		$( "#c_10380_slider" ).slider( "value", String(amount) );
		$( "#c_10380_show" ).val( "" + Math.round(amount / 14) / 10 + "杯(" + amount + "g)" );
	});
	$( "#c_10381_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10381_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10381" ).val(ui.value);
		}
	});
	$( "#c_10381_show" ).val( "" + $("#c_10381_slider").slider("value") / 21.5 + "人前("  + $("#c_10381_slider").slider("value") + "g)");
	$( "#c_10381" ).val( $( "#c_10381_slider" ).slider( "value" ) );
	$( "#c_10381_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10381" ).val())-21.5) * 10) / 10);
		$( "#c_10381" ).val(String(amount));
		$( "#c_10381_slider" ).slider( "value", String(amount) );
		$( "#c_10381_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10381_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10381" ).val())+21.5) * 10) / 10;
		$( "#c_10381" ).val(String(amount));
		$( "#c_10381_slider" ).slider( "value", String(amount) );
		$( "#c_10381_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10382_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2150, step: 21.5,
		slide: function( event, ui ) {
			$( "#c_10382_show" ).val( "" + Math.round(ui.value / 21.5) / 10 + "人前(" + ui.value + "g)" );
			$( "#c_10382" ).val(ui.value);
		}
	});
	$( "#c_10382_show" ).val( "" + $("#c_10382_slider").slider("value") / 21.5 + "人前("  + $("#c_10382_slider").slider("value") + "g)");
	$( "#c_10382" ).val( $( "#c_10382_slider" ).slider( "value" ) );
	$( "#c_10382_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10382" ).val())-21.5) * 10) / 10);
		$( "#c_10382" ).val(String(amount));
		$( "#c_10382_slider" ).slider( "value", String(amount) );
		$( "#c_10382_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10382_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10382" ).val())+21.5) * 10) / 10;
		$( "#c_10382" ).val(String(amount));
		$( "#c_10382_slider" ).slider( "value", String(amount) );
		$( "#c_10382_show" ).val( "" + Math.round(amount / 21.5) / 10 + "人前(" + amount + "g)" );
	});
	$( "#c_10383_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 600, step: 6,
		slide: function( event, ui ) {
			$( "#c_10383_show" ).val( "" + Math.round(ui.value / 6) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10383" ).val(ui.value);
		}
	});
	$( "#c_10383_show" ).val( "" + $("#c_10383_slider").slider("value") / 6 + "単位("  + $("#c_10383_slider").slider("value") + "g)");
	$( "#c_10383" ).val( $( "#c_10383_slider" ).slider( "value" ) );
	$( "#c_10383_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10383" ).val())-6) * 10) / 10);
		$( "#c_10383" ).val(String(amount));
		$( "#c_10383_slider" ).slider( "value", String(amount) );
		$( "#c_10383_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10383_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10383" ).val())+6) * 10) / 10;
		$( "#c_10383" ).val(String(amount));
		$( "#c_10383_slider" ).slider( "value", String(amount) );
		$( "#c_10383_show" ).val( "" + Math.round(amount / 6) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10384_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10384_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10384" ).val(ui.value);
		}
	});
	$( "#c_10384_show" ).val( "" + $("#c_10384_slider").slider("value") / 15 + "単位("  + $("#c_10384_slider").slider("value") + "g)");
	$( "#c_10384" ).val( $( "#c_10384_slider" ).slider( "value" ) );
	$( "#c_10384_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10384" ).val())-15) * 10) / 10);
		$( "#c_10384" ).val(String(amount));
		$( "#c_10384_slider" ).slider( "value", String(amount) );
		$( "#c_10384_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10384_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10384" ).val())+15) * 10) / 10;
		$( "#c_10384" ).val(String(amount));
		$( "#c_10384_slider" ).slider( "value", String(amount) );
		$( "#c_10384_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});



	$( "#c_10385_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10385_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10385" ).val(ui.value);
		}
	});
	$( "#c_10385_show" ).val( "" + $("#c_10385_slider").slider("value") / 7 + "単位("  + $("#c_10385_slider").slider("value") + "g)");
	$( "#c_10385" ).val( $( "#c_10385_slider" ).slider( "value" ) );
	$( "#c_10385_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10385" ).val())-7) * 10) / 10);
		$( "#c_10385" ).val(String(amount));
		$( "#c_10385_slider" ).slider( "value", String(amount) );
		$( "#c_10385_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10385_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10385" ).val())+7) * 10) / 10;
		$( "#c_10385" ).val(String(amount));
		$( "#c_10385_slider" ).slider( "value", String(amount) );
		$( "#c_10385_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10386_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 700, step: 7,
		slide: function( event, ui ) {
			$( "#c_10386_show" ).val( "" + Math.round(ui.value / 7) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10386" ).val(ui.value);
		}
	});
	$( "#c_10386_show" ).val( "" + $("#c_10386_slider").slider("value") / 7 + "単位("  + $("#c_10386_slider").slider("value") + "g)");
	$( "#c_10386" ).val( $( "#c_10386_slider" ).slider( "value" ) );
	$( "#c_10386_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10386" ).val())-7) * 10) / 10);
		$( "#c_10386" ).val(String(amount));
		$( "#c_10386_slider" ).slider( "value", String(amount) );
		$( "#c_10386_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10386_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10386" ).val())+7) * 10) / 10;
		$( "#c_10386" ).val(String(amount));
		$( "#c_10386_slider" ).slider( "value", String(amount) );
		$( "#c_10386_show" ).val( "" + Math.round(amount / 7) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10387_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10387_show" ).val( "" + Math.round(ui.value / 5) / 10 + "個(" + ui.value + "g)" );
			$( "#c_10387" ).val(ui.value);
		}
	});
	$( "#c_10387_show" ).val( "" + $("#c_10387_slider").slider("value") / 5 + "個("  + $("#c_10387_slider").slider("value") + "g)");
	$( "#c_10387" ).val( $( "#c_10387_slider" ).slider( "value" ) );
	$( "#c_10387_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10387" ).val())-5) * 10) / 10);
		$( "#c_10387" ).val(String(amount));
		$( "#c_10387_slider" ).slider( "value", String(amount) );
		$( "#c_10387_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10387_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10387" ).val())+5) * 10) / 10;
		$( "#c_10387" ).val(String(amount));
		$( "#c_10387_slider" ).slider( "value", String(amount) );
		$( "#c_10387_show" ).val( "" + Math.round(amount / 5) / 10 + "個(" + amount + "g)" );
	});
	$( "#c_10388_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10388_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10388" ).val(ui.value);
		}
	});
	$( "#c_10388_show" ).val( "" + $("#c_10388_slider").slider("value") / 9 + "単位("  + $("#c_10388_slider").slider("value") + "g)");
	$( "#c_10388" ).val( $( "#c_10388_slider" ).slider( "value" ) );
	$( "#c_10388_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10388" ).val())-9) * 10) / 10);
		$( "#c_10388" ).val(String(amount));
		$( "#c_10388_slider" ).slider( "value", String(amount) );
		$( "#c_10388_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10388_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10388" ).val())+9) * 10) / 10;
		$( "#c_10388" ).val(String(amount));
		$( "#c_10388_slider" ).slider( "value", String(amount) );
		$( "#c_10388_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10389_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 900, step: 9,
		slide: function( event, ui ) {
			$( "#c_10389_show" ).val( "" + Math.round(ui.value / 9) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10389" ).val(ui.value);
		}
	});
	$( "#c_10389_show" ).val( "" + $("#c_10389_slider").slider("value") / 9 + "単位("  + $("#c_10389_slider").slider("value") + "g)");
	$( "#c_10389" ).val( $( "#c_10389_slider" ).slider( "value" ) );
	$( "#c_10389_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10389" ).val())-9) * 10) / 10);
		$( "#c_10389" ).val(String(amount));
		$( "#c_10389_slider" ).slider( "value", String(amount) );
		$( "#c_10389_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10389_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10389" ).val())+9) * 10) / 10;
		$( "#c_10389" ).val(String(amount));
		$( "#c_10389_slider" ).slider( "value", String(amount) );
		$( "#c_10389_show" ).val( "" + Math.round(amount / 9) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10390_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10390_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10390" ).val(ui.value);
		}
	});
	$( "#c_10390_show" ).val( "" + $("#c_10390_slider").slider("value") / 15 + "単位("  + $("#c_10390_slider").slider("value") + "g)");
	$( "#c_10390" ).val( $( "#c_10390_slider" ).slider( "value" ) );
	$( "#c_10390_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10390" ).val())-15) * 10) / 10);
		$( "#c_10390" ).val(String(amount));
		$( "#c_10390_slider" ).slider( "value", String(amount) );
		$( "#c_10390_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10390_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10390" ).val())+15) * 10) / 10;
		$( "#c_10390" ).val(String(amount));
		$( "#c_10390_slider" ).slider( "value", String(amount) );
		$( "#c_10390_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10391_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10391_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10391" ).val(ui.value);
		}
	});
	$( "#c_10391_show" ).val( "" + $("#c_10391_slider").slider("value") / 15 + "単位("  + $("#c_10391_slider").slider("value") + "g)");
	$( "#c_10391" ).val( $( "#c_10391_slider" ).slider( "value" ) );
	$( "#c_10391_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10391" ).val())-15) * 10) / 10);
		$( "#c_10391" ).val(String(amount));
		$( "#c_10391_slider" ).slider( "value", String(amount) );
		$( "#c_10391_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10391_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10391" ).val())+15) * 10) / 10;
		$( "#c_10391" ).val(String(amount));
		$( "#c_10391_slider" ).slider( "value", String(amount) );
		$( "#c_10391_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10392_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10392_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10392" ).val(ui.value);
		}
	});
	$( "#c_10392_show" ).val( "" + $("#c_10392_slider").slider("value") / 15 + "単位("  + $("#c_10392_slider").slider("value") + "g)");
	$( "#c_10392" ).val( $( "#c_10392_slider" ).slider( "value" ) );
	$( "#c_10392_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10392" ).val())-15) * 10) / 10);
		$( "#c_10392" ).val(String(amount));
		$( "#c_10392_slider" ).slider( "value", String(amount) );
		$( "#c_10392_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10392_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10392" ).val())+15) * 10) / 10;
		$( "#c_10392" ).val(String(amount));
		$( "#c_10392_slider" ).slider( "value", String(amount) );
		$( "#c_10392_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10393_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1500, step: 15,
		slide: function( event, ui ) {
			$( "#c_10393_show" ).val( "" + Math.round(ui.value / 15) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10393" ).val(ui.value);
		}
	});
	$( "#c_10393_show" ).val( "" + $("#c_10393_slider").slider("value") / 15 + "単位("  + $("#c_10393_slider").slider("value") + "g)");
	$( "#c_10393" ).val( $( "#c_10393_slider" ).slider( "value" ) );
	$( "#c_10393_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10393" ).val())-15) * 10) / 10);
		$( "#c_10393" ).val(String(amount));
		$( "#c_10393_slider" ).slider( "value", String(amount) );
		$( "#c_10393_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10393_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10393" ).val())+15) * 10) / 10;
		$( "#c_10393" ).val(String(amount));
		$( "#c_10393_slider" ).slider( "value", String(amount) );
		$( "#c_10393_show" ).val( "" + Math.round(amount / 15) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10394_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10394_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10394" ).val(ui.value);
		}
	});
	$( "#c_10394_show" ).val( "" + $("#c_10394_slider").slider("value") / 20 + "単位("  + $("#c_10394_slider").slider("value") + "g)");
	$( "#c_10394" ).val( $( "#c_10394_slider" ).slider( "value" ) );
	$( "#c_10394_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10394" ).val())-20) * 10) / 10);
		$( "#c_10394" ).val(String(amount));
		$( "#c_10394_slider" ).slider( "value", String(amount) );
		$( "#c_10394_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10394_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10394" ).val())+20) * 10) / 10;
		$( "#c_10394" ).val(String(amount));
		$( "#c_10394_slider" ).slider( "value", String(amount) );
		$( "#c_10394_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10395_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 1000, step: 10,
		slide: function( event, ui ) {
			$( "#c_10395_show" ).val( "" + Math.round(ui.value / 10) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10395" ).val(ui.value);
		}
	});
	$( "#c_10395_show" ).val( "" + $("#c_10395_slider").slider("value") / 10 + "単位("  + $("#c_10395_slider").slider("value") + "g)");
	$( "#c_10395" ).val( $( "#c_10395_slider" ).slider( "value" ) );
	$( "#c_10395_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10395" ).val())-10) * 10) / 10);
		$( "#c_10395" ).val(String(amount));
		$( "#c_10395_slider" ).slider( "value", String(amount) );
		$( "#c_10395_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10395_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10395" ).val())+10) * 10) / 10;
		$( "#c_10395" ).val(String(amount));
		$( "#c_10395_slider" ).slider( "value", String(amount) );
		$( "#c_10395_show" ).val( "" + Math.round(amount / 10) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10396_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10396_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10396" ).val(ui.value);
		}
	});
	$( "#c_10396_show" ).val( "" + $("#c_10396_slider").slider("value") / 20 + "単位("  + $("#c_10396_slider").slider("value") + "g)");
	$( "#c_10396" ).val( $( "#c_10396_slider" ).slider( "value" ) );
	$( "#c_10396_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10396" ).val())-20) * 10) / 10);
		$( "#c_10396" ).val(String(amount));
		$( "#c_10396_slider" ).slider( "value", String(amount) );
		$( "#c_10396_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10396_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10396" ).val())+20) * 10) / 10;
		$( "#c_10396" ).val(String(amount));
		$( "#c_10396_slider" ).slider( "value", String(amount) );
		$( "#c_10396_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10397_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10397_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10397" ).val(ui.value);
		}
	});
	$( "#c_10397_show" ).val( "" + $("#c_10397_slider").slider("value") / 20 + "単位("  + $("#c_10397_slider").slider("value") + "g)");
	$( "#c_10397" ).val( $( "#c_10397_slider" ).slider( "value" ) );
	$( "#c_10397_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10397" ).val())-20) * 10) / 10);
		$( "#c_10397" ).val(String(amount));
		$( "#c_10397_slider" ).slider( "value", String(amount) );
		$( "#c_10397_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10397_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10397" ).val())+20) * 10) / 10;
		$( "#c_10397" ).val(String(amount));
		$( "#c_10397_slider" ).slider( "value", String(amount) );
		$( "#c_10397_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10398_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 2000, step: 20,
		slide: function( event, ui ) {
			$( "#c_10398_show" ).val( "" + Math.round(ui.value / 20) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10398" ).val(ui.value);
		}
	});
	$( "#c_10398_show" ).val( "" + $("#c_10398_slider").slider("value") / 20 + "単位("  + $("#c_10398_slider").slider("value") + "g)");
	$( "#c_10398" ).val( $( "#c_10398_slider" ).slider( "value" ) );
	$( "#c_10398_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10398" ).val())-20) * 10) / 10);
		$( "#c_10398" ).val(String(amount));
		$( "#c_10398_slider" ).slider( "value", String(amount) );
		$( "#c_10398_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10398_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10398" ).val())+20) * 10) / 10;
		$( "#c_10398" ).val(String(amount));
		$( "#c_10398_slider" ).slider( "value", String(amount) );
		$( "#c_10398_show" ).val( "" + Math.round(amount / 20) / 10 + "単位(" + amount + "g)" );
	});

	$( "#c_10399_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10399_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10399" ).val(ui.value);
		}
	});
	$( "#c_10399_show" ).val( "" + $("#c_10399_slider").slider("value") / 5 + "単位("  + $("#c_10399_slider").slider("value") + "g)");
	$( "#c_10399" ).val( $( "#c_10399_slider" ).slider( "value" ) );
	$( "#c_10399_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10399" ).val())-5) * 10) / 10);
		$( "#c_10399" ).val(String(amount));
		$( "#c_10399_slider" ).slider( "value", String(amount) );
		$( "#c_10399_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10399_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10399" ).val())+5) * 10) / 10;
		$( "#c_10399" ).val(String(amount));
		$( "#c_10399_slider" ).slider( "value", String(amount) );
		$( "#c_10399_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10400_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10400_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10400" ).val(ui.value);
		}
	});
	$( "#c_10400_show" ).val( "" + $("#c_10400_slider").slider("value") / 5 + "単位("  + $("#c_10400_slider").slider("value") + "g)");
	$( "#c_10400" ).val( $( "#c_10400_slider" ).slider( "value" ) );
	$( "#c_10400_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10400" ).val())-5) * 10) / 10);
		$( "#c_10400" ).val(String(amount));
		$( "#c_10400_slider" ).slider( "value", String(amount) );
		$( "#c_10400_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10400_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10400" ).val())+5) * 10) / 10;
		$( "#c_10400" ).val(String(amount));
		$( "#c_10400_slider" ).slider( "value", String(amount) );
		$( "#c_10400_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10401_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10401_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10401" ).val(ui.value);
		}
	});
	$( "#c_10401_show" ).val( "" + $("#c_10401_slider").slider("value") / 5 + "単位("  + $("#c_10401_slider").slider("value") + "g)");
	$( "#c_10401" ).val( $( "#c_10401_slider" ).slider( "value" ) );
	$( "#c_10401_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10401" ).val())-5) * 10) / 10);
		$( "#c_10401" ).val(String(amount));
		$( "#c_10401_slider" ).slider( "value", String(amount) );
		$( "#c_10401_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10401_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10401" ).val())+5) * 10) / 10;
		$( "#c_10401" ).val(String(amount));
		$( "#c_10401_slider" ).slider( "value", String(amount) );
		$( "#c_10401_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10402_slider" ).slider({
		range: "min",
		value: 0,
		min: 0, max: 500, step: 5,
		slide: function( event, ui ) {
			$( "#c_10402_show" ).val( "" + Math.round(ui.value / 5) / 10 + "単位(" + ui.value + "g)" );
			$( "#c_10402" ).val(ui.value);
		}
	});
	$( "#c_10402_show" ).val( "" + $("#c_10402_slider").slider("value") / 5 + "単位("  + $("#c_10402_slider").slider("value") + "g)");
	$( "#c_10402" ).val( $( "#c_10402_slider" ).slider( "value" ) );
	$( "#c_10402_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10402" ).val())-5) * 10) / 10);
		$( "#c_10402" ).val(String(amount));
		$( "#c_10402_slider" ).slider( "value", String(amount) );
		$( "#c_10402_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});
	$( "#c_10402_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10402" ).val())+5) * 10) / 10;
		$( "#c_10402" ).val(String(amount));
		$( "#c_10402_slider" ).slider( "value", String(amount) );
		$( "#c_10402_show" ).val( "" + Math.round(amount / 5) / 10 + "単位(" + amount + "g)" );
	});

	//設問に対する記述
	$( "#c_10162_slider" ).slider({
		range: "min",
		value: 9,
		min: 0, max: 24, step: 0.5,
		slide: function( event, ui ) {
			if(Math.floor(ui.value)==ui.value){
				$( "#c_10162_show" ).val( "" + Math.floor(ui.value) + "時" );
			} else {
				$( "#c_10162_show" ).val( "" + Math.floor(ui.value) + "時半" );
			}
			$( "#c_10162" ).val(ui.value);
		}
	});
	$( "#c_10162_show" ).val( "" + $( "#c_10162_slider" ).slider( "value" ) + "時" );
	$( "#c_10162" ).val( $( "#c_10162_slider" ).slider( "value" ) );
	$( "#c_10162_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10162" ).val())-0.5) * 10) / 10);
		$( "#c_10162" ).val(String(amount));
		$( "#c_10162_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10162_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10162_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});
	$( "#c_10162_plus" ).click(function(){
		amount = Math.min(24, Math.round((parseFloat($( "#c_10162" ).val())+0.5) * 10) / 10);
		$( "#c_10162" ).val(String(amount));
		$( "#c_10162_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10162_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10162_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});
	$( "#c_10163_slider" ).slider({
		range: "min",
		value: 9,
		min: 0, max: 24, step: 0.5,
		slide: function( event, ui ) {
			if(Math.floor(ui.value)==ui.value){
				$( "#c_10163_show" ).val( "" + Math.floor(ui.value) + "時" );
			} else {
				$( "#c_10163_show" ).val( "" + Math.floor(ui.value) + "時半" );
			}
			$( "#c_10163" ).val(ui.value);
		}
	});
	$( "#c_10163_show" ).val( "" + $( "#c_10163_slider" ).slider( "value" ) + "時" );
	$( "#c_10163" ).val( $( "#c_10163_slider" ).slider( "value" ) );
	$( "#c_10163_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10163" ).val())-0.5) * 10) / 10);
		$( "#c_10163" ).val(String(amount));
		$( "#c_10163_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10163_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10163_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});
	$( "#c_10163_plus" ).click(function(){
		amount = Math.min(24, Math.round((parseFloat($( "#c_10163" ).val())+0.5) * 10) / 10);
		$( "#c_10163" ).val(String(amount));
		$( "#c_10163_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10163_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10163_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});

	$( "#c_10143_slider" ).slider({
		range: "min",
		value: 18,
		min: 0, max: 24, step: 0.5,
		slide: function( event, ui ) {
			if(Math.floor(ui.value)==ui.value){
				$( "#c_10143_show" ).val( "" + Math.floor(ui.value) + "時" );
			} else {
				$( "#c_10143_show" ).val( "" + Math.floor(ui.value) + "時半" );
			}
			$( "#c_10143" ).val(ui.value);
		}
	});
	$( "#c_10143_show" ).val( "" + $( "#c_10143_slider" ).slider( "value" ) + "時" );
	$( "#c_10143" ).val( $( "#c_10143_slider" ).slider( "value" ) );
	$( "#c_10143_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10143" ).val())-0.5) * 10) / 10);
		$( "#c_10143" ).val(String(amount));
		$( "#c_10143_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10143_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10143_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});
	$( "#c_10143_plus" ).click(function(){
		amount = Math.min(24, Math.round((parseFloat($( "#c_10143" ).val())+0.5) * 10) / 10);
		$( "#c_10143" ).val(String(amount));
		$( "#c_10143_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10143_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10143_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});

	$( "#c_10189_slider" ).slider({
		range: "min",
		value: 18,
		min: 0, max: 24, step: 0.5,
		slide: function( event, ui ) {
			if(Math.floor(ui.value)==ui.value){
				$( "#c_10189_show" ).val( "" + Math.floor(ui.value) + "時" );
			} else {
				$( "#c_10189_show" ).val( "" + Math.floor(ui.value) + "時半" );
			}
			$( "#c_10189" ).val(ui.value);
		}
	});
	$( "#c_10189_show" ).val( "" + $( "#c_10189_slider" ).slider( "value" ) + "時" );
	$( "#c_10189" ).val( $( "#c_10189_slider" ).slider( "value" ) );
	$( "#c_10189_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10189" ).val())-0.5) * 10) / 10);
		$( "#c_10189" ).val(String(amount));
		$( "#c_10189_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10189_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10189_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});
	$( "#c_10189_plus" ).click(function(){
		amount = Math.min(24, Math.round((parseFloat($( "#c_10189" ).val())+0.5) * 10) / 10);
		$( "#c_10189" ).val(String(amount));
		$( "#c_10189_slider" ).slider( "value", String(amount) );
		if(Math.floor(amount)==amount){
			$( "#c_10189_show" ).val( "" + String(Math.floor(amount)) + "時" );
		} else {
			$( "#c_10189_show" ).val( "" + String(Math.floor(amount)) + "時半" );
		}
	});

	$( "#c_10196_slider" ).slider({
		range: "min",
		value: 140,
		min: 0, max: 300, step: 1,
		slide: function( event, ui ) {
			$( "#c_10196_show" ).val( "" + ui.value + " mmHg");
			$( "#c_10196" ).val(ui.value);
		}
	});
	$( "#c_10196_show" ).val( "" + $( "#c_10196_slider" ).slider( "value" ) + " mmHg" );
	$( "#c_10196" ).val( $( "#c_10196_slider" ).slider( "value" ) );
	$( "#c_10196_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10196" ).val())-1) * 10) / 10);
		$( "#c_10196" ).val(String(amount));
		$( "#c_10196_slider" ).slider( "value", String(amount) );
		$( "#c_10196_show" ).val( "" + String(amount) + " mmHg" );
	});
	$( "#c_10196_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10196" ).val())+1) * 10) / 10;
		$( "#c_10196" ).val(String(amount));
		$( "#c_10196_slider" ).slider( "value", String(amount) );
		$( "#c_10196_show" ).val( "" + String(amount) + " mmHg" );
	});
	$( "#c_10197_slider" ).slider({
		range: "min",
		value: 90,
		min: 0, max: 300, step: 1,
		slide: function( event, ui ) {
			$( "#c_10197_show" ).val( "" + ui.value + " mmHg");
			$( "#c_10197" ).val(ui.value);
		}
	});
	$( "#c_10197_show" ).val( "" + $( "#c_10197_slider" ).slider( "value" ) + " mmHg" );
	$( "#c_10197" ).val( $( "#c_10197_slider" ).slider( "value" ) );
	$( "#c_10197_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10197" ).val())-1) * 10) / 10);
		$( "#c_10197" ).val(String(amount));
		$( "#c_10197_slider" ).slider( "value", String(amount) );
		$( "#c_10197_show" ).val( "" + String(amount) + " mmHg" );
	});
	$( "#c_10197_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10197" ).val())+1) * 10) / 10;
		$( "#c_10197" ).val(String(amount));
		$( "#c_10197_slider" ).slider( "value", String(amount) );
		$( "#c_10197_show" ).val( "" + String(amount) + " mmHg" );
	});

	$( "#c_10280_slider" ).slider({
		range: "min",
		value: 115,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10280_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10280" ).val(ui.value);
		}
	});
	$( "#c_10280_show" ).val( "" + $( "#c_10280_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10280" ).val( $( "#c_10280_slider" ).slider( "value" ) );
	$( "#c_10280_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10280" ).val())-1) * 10) / 10);
		$( "#c_10280" ).val(String(amount));
		$( "#c_10280_slider" ).slider( "value", String(amount) );
		$( "#c_10280_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10280_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10280" ).val())+1) * 10) / 10;
		$( "#c_10280" ).val(String(amount));
		$( "#c_10280_slider" ).slider( "value", String(amount) );
		$( "#c_10280_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10281_slider" ).slider({
		range: "min",
		value: 180,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10281_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10281" ).val(ui.value);
		}
	});
	$( "#c_10281_show" ).val( "" + $( "#c_10281_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10281" ).val( $( "#c_10281_slider" ).slider( "value" ) );
	$( "#c_10281_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10281" ).val())-1) * 10) / 10);
		$( "#c_10281" ).val(String(amount));
		$( "#c_10281_slider" ).slider( "value", String(amount) );
		$( "#c_10281_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10281_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10281" ).val())+1) * 10) / 10;
		$( "#c_10281" ).val(String(amount));
		$( "#c_10281_slider" ).slider( "value", String(amount) );
		$( "#c_10281_show" ).val( "" + String(amount) + " mg/dl" );
	});

	$( "#c_10282_slider" ).slider({
		range: "min",
		value: 80,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10282_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10282" ).val(ui.value);
		}
	});
	$( "#c_10282_show" ).val( "" + $( "#c_10282_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10282" ).val( $( "#c_10282_slider" ).slider( "value" ) );
	$( "#c_10282_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10282" ).val())-1) * 10) / 10);
		$( "#c_10282" ).val(String(amount));
		$( "#c_10282_slider" ).slider( "value", String(amount) );
	$( "#c_10282_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10282_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10282" ).val())+1) * 10) / 10;
		$( "#c_10282" ).val(String(amount));
		$( "#c_10282_slider" ).slider( "value", String(amount) );
		$( "#c_10282_show" ).val( "" + String(amount) + " mg/dl" );
	});

	$( "#c_10283_slider" ).slider({
		range: "min",
		value: 220,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10283_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10283" ).val(ui.value);
		}
	});
	$( "#c_10283_show" ).val( "" + $( "#c_10283_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10283" ).val( $( "#c_10283_slider" ).slider( "value" ) );
	$( "#c_10283_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10283" ).val())-1) * 10) / 10);
		$( "#c_10283" ).val(String(amount));
		$( "#c_10283_slider" ).slider( "value", String(amount) );
		$( "#c_10283_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10283_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10283" ).val())+1) * 10) / 10;
		$( "#c_10283" ).val(String(amount));
		$( "#c_10283_slider" ).slider( "value", String(amount) );
		$( "#c_10283_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10284_slider" ).slider({
		range: "min",
		value: 220,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10284_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10284" ).val(ui.value);
		}
	});
	$( "#c_10284_show" ).val( "" + $( "#c_10284_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10284" ).val( $( "#c_10284_slider" ).slider( "value" ) );
	$( "#c_10284_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10284" ).val())-1) * 10) / 10);
		$( "#c_10284" ).val(String(amount));
		$( "#c_10284_slider" ).slider( "value", String(amount) );
		$( "#c_10284_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10284_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10284" ).val())+1) * 10) / 10;
		$( "#c_10284" ).val(String(amount));
		$( "#c_10284_slider" ).slider( "value", String(amount) );
		$( "#c_10284_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10285_slider" ).slider({
		range: "min",
		value: 220,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10285_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10285" ).val(ui.value);
		}
	});
	$( "#c_10285_show" ).val( "" + $( "#c_10285_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10285" ).val( $( "#c_10285_slider" ).slider( "value" ) );
	$( "#c_10285_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10285" ).val())-1) * 10) / 10);
		$( "#c_10285" ).val(String(amount));
		$( "#c_10285_slider" ).slider( "value", String(amount) );
		$( "#c_10285_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10285_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10285" ).val())+1) * 10) / 10;
		$( "#c_10285" ).val(String(amount));
		$( "#c_10285_slider" ).slider( "value", String(amount) );
		$( "#c_10285_show" ).val( "" + String(amount) + " mg/dl" );
	});

	$( "#c_10286_slider" ).slider({
		range: "min",
		value: 6,
		min: 0, max: 20, step: 1,
		slide: function( event, ui ) {
			$( "#c_10286_show" ).val( "" + ui.value + " mg/dl");
			$( "#c_10286" ).val(ui.value);
		}
	});
	$( "#c_10286_show" ).val( "" + $( "#c_10286_slider" ).slider( "value" ) + " mg/dl" );
	$( "#c_10286" ).val( $( "#c_10286_slider" ).slider( "value" ) );
	$( "#c_10286_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10286" ).val())-1) * 10) / 10);
		$( "#c_10286" ).val(String(amount));
		$( "#c_10286_slider" ).slider( "value", String(amount) );
	$( "#c_10286_show" ).val( "" + String(amount) + " mg/dl" );
	});
	$( "#c_10286_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10286" ).val())+1) * 10) / 10;
		$( "#c_10286" ).val(String(amount));
		$( "#c_10286_slider" ).slider( "value", String(amount) );
		$( "#c_10286_show" ).val( "" + String(amount) + " mg/dl" );
	});

	$( "#c_10287_slider" ).slider({
		range: "min",
		value: 20,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10287_show" ).val( "" + ui.value + " IU/l");
			$( "#c_10287" ).val(ui.value);
		}
	});
	$( "#c_10287_show" ).val( "" + $( "#c_10287_slider" ).slider( "value" ) + " IU/l" );
	$( "#c_10287" ).val( $( "#c_10287_slider" ).slider( "value" ) );
	$( "#c_10287_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10287" ).val())-1) * 10) / 10);
		$( "#c_10287" ).val(String(amount));
		$( "#c_10287_slider" ).slider( "value", String(amount) );
		$( "#c_10287_show" ).val( "" + String(amount) + " IU/l" );
	});
	$( "#c_10287_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10287" ).val())+1) * 10) / 10;
		$( "#c_10287" ).val(String(amount));
		$( "#c_10287_slider" ).slider( "value", String(amount) );
		$( "#c_10287_show" ).val( "" + String(amount) + " IU/l" );
	});

	$( "#c_10288_slider" ).slider({
		range: "min",
		value: 20,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10288_show" ).val( "" + ui.value + " IU/l");
			$( "#c_10288" ).val(ui.value);
		}
	});
	$( "#c_10288_show" ).val( "" + $( "#c_10288_slider" ).slider( "value" ) + " IU/l" );
	$( "#c_10288" ).val( $( "#c_10288_slider" ).slider( "value" ) );
	$( "#c_10288_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10288" ).val())-1) * 10) / 10);
		$( "#c_10288" ).val(String(amount));
		$( "#c_10288_slider" ).slider( "value", String(amount) );
		$( "#c_10288_show" ).val( "" + String(amount) + " IU/l" );
	});
	$( "#c_10288_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10288" ).val())+1) * 10) / 10;
		$( "#c_10288" ).val(String(amount));
		$( "#c_10288_slider" ).slider( "value", String(amount) );
		$( "#c_10288_show" ).val( "" + String(amount) + " IU/l" );
	});

	$( "#c_10289_slider" ).slider({
		range: "min",
		value: 20,
		min: 0, max: 500, step: 1,
		slide: function( event, ui ) {
			$( "#c_10289_show" ).val( "" + -ui.value + " IU/l");
			$( "#c_10289" ).val(ui.value);
		}
	});
	$( "#c_10289_show" ).val( "" + $( "#c_10289_slider" ).slider( "value" ) + " IU/l" );
	$( "#c_10289" ).val( $( "#c_10289_slider" ).slider( "value" ) );
	$( "#c_10289_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10289" ).val())-1) * 10) / 10);
		$( "#c_10289" ).val(String(amount));
		$( "#c_10289_slider" ).slider( "value", String(amount) );
		$( "#c_10289_show" ).val( "" + String(amount) + " IU/l" );
	});
	$( "#c_10289_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10289" ).val())+1) * 10) / 10;
		$( "#c_10289" ).val(String(amount));
		$( "#c_10289_slider" ).slider( "value", String(amount) );
		$( "#c_10289_show" ).val( "" + String(amount) + " IU/l" );
	});

	$( "#c_10290_slider" ).slider({
		range: "min",
		value: 5,
		min: 0, max: 20, step: 0.1,
		slide: function( event, ui ) {
			$( "#c_10290_show" ).val( "" + ui.value + " %");
			$( "#c_10290" ).val(ui.value);
		}
	});
	$( "#c_10290_show" ).val( "" + $( "#c_10290_slider" ).slider( "value" ) + " %" );
	$( "#c_10290" ).val( $( "#c_10290_slider" ).slider( "value" ) );
	$( "#c_10290_minus" ).click(function(){
		amount = Math.max(0, Math.round((parseFloat($( "#c_10290" ).val())-0.1) * 10) / 10);
		$( "#c_10290" ).val(String(amount));
		$( "#c_10290_slider" ).slider( "value", String(amount) );
		$( "#c_10290_show" ).val( "" + String(amount) + " %" );
	});
	$( "#c_10290_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10290" ).val())+0.1) * 10) / 10;
		$( "#c_10290" ).val(String(amount));
		$( "#c_10290_slider" ).slider( "value", String(amount) );
		$( "#c_10290_show" ).val( "" + String(amount) + " %" );
	});

	$( "#c_10059_slider" ).slider({
		range: "min",
		value: 160,
		min: 100, max: 200, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10059_show" ).val( "" + ui.value + " cm");
			$( "#c_10059" ).val(ui.value);
		}
	});
	$( "#c_10059_show" ).val( "" + $( "#c_10059_slider" ).slider( "value" ) + " cm" );
	$( "#c_10059" ).val( $( "#c_10059_slider" ).slider( "value" ) );
	$( "#c_10059_minus" ).click(function(){
		amount = Math.max(100, Math.round((parseFloat($( "#c_10059" ).val())-0.5) * 10) / 10);
		$( "#c_10059" ).val(String(amount));
		$( "#c_10059_slider" ).slider( "value", String(amount) );
		$( "#c_10059_show" ).val( "" + String(amount) + " cm" );
	});
	$( "#c_10059_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10059" ).val())+0.5) * 10) / 10;
		$( "#c_10059" ).val(String(amount));
		$( "#c_10059_slider" ).slider( "value", String(amount) );
		$( "#c_10059_show" ).val( "" + String(amount) + " cm" );
	});

	//レイヤーに対する記述

	//設問に対する記述
	$( "#c_10060_slider" ).slider({
		range: "min",
		value: 60,
		min: 30, max: 150, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10060_show" ).val( "" + ui.value + " kg");
			$( "#c_10060" ).val(ui.value);
		}
	});
	$( "#c_10060_show" ).val( "" + $( "#c_10060_slider" ).slider( "value" ) + " kg" );
	$( "#c_10060" ).val( $( "#c_10060_slider" ).slider( "value" ) );
	$( "#c_10060_minus" ).click(function(){
		amount = Math.max(30, Math.round((parseFloat($( "#c_10060" ).val())-0.5) * 10) / 10);
		$( "#c_10060" ).val(String(amount));
		$( "#c_10060_slider" ).slider( "value", String(amount) );
		$( "#c_10060_show" ).val( "" + String(amount) + " kg" );
	});
	$( "#c_10060_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10060" ).val())+0.5) * 10) / 10;
		$( "#c_10060" ).val(String(amount));
		$( "#c_10060_slider" ).slider( "value", String(amount) );
		$( "#c_10060_show" ).val( "" + String(amount) + " kg" );
	});

	$( "#c_10061_slider" ).slider({
		range: "min",
		value: 60,
		min: 30, max: 150, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10061_show" ).val( "" + ui.value + " kg");
			$( "#c_10061" ).val(ui.value);
		}
	});
	$( "#c_10061_show" ).val( "" + $( "#c_10061_slider" ).slider( "value" ) + " kg" );
	$( "#c_10061" ).val( $( "#c_10061_slider" ).slider( "value" ) );
	$( "#c_10061_minus" ).click(function(){
		amount = Math.max(30, Math.round((parseFloat($( "#c_10061" ).val())-0.5) * 10) / 10);
		$( "#c_10061" ).val(String(amount));
		$( "#c_10061_slider" ).slider( "value", String(amount) );
		$( "#c_10061_show" ).val( "" + String(amount) + " kg" );
	});
	$( "#c_10061_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10061" ).val())+0.5) * 10) / 10;
		$( "#c_10061" ).val(String(amount));
		$( "#c_10061_slider" ).slider( "value", String(amount) );
		$( "#c_10061_show" ).val( "" + String(amount) + " kg" );
	});

	$( "#c_10062_slider" ).slider({
		range: "min",
		value: 60,
		min: 30, max: 150, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10062_show" ).val( "" + ui.value + " kg");
			$( "#c_10062" ).val(ui.value);
		}
	});
	$( "#c_10062_show" ).val( "" + $( "#c_10062_slider" ).slider( "value" ) + " kg" );
	$( "#c_10062" ).val( $( "#c_10062_slider" ).slider( "value" ) );
	$( "#c_10062_minus" ).click(function(){
		amount = Math.max(30, Math.round((parseFloat($( "#c_10062" ).val())-0.5) * 10) / 10);
		$( "#c_10062" ).val(String(amount));
		$( "#c_10062_slider" ).slider( "value", String(amount) );
		$( "#c_10062_show" ).val( "" + String(amount) + " kg" );
	});
	$( "#c_10062_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10062" ).val())+0.5) * 10) / 10;
		$( "#c_10062" ).val(String(amount));
		$( "#c_10062_slider" ).slider( "value", String(amount) );
		$( "#c_10062_show" ).val( "" + String(amount) + " kg" );
	});

	$( "#c_10063_slider" ).slider({
		range: "min",
		value: 60,
		min: 30, max: 150, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10063_show" ).val( "" + ui.value + " kg");
			$( "#c_10063" ).val(ui.value);
		}
	});
	$( "#c_10063_show" ).val( "" + $( "#c_10063_slider" ).slider( "value" ) + " kg" );
	$( "#c_10063" ).val( $( "#c_10063_slider" ).slider( "value" ) );
	$( "#c_10063_minus" ).click(function(){
		amount = Math.max(30, Math.round((parseFloat($( "#c_10063" ).val())-0.5) * 10) / 10);
		$( "#c_10063" ).val(String(amount));
		$( "#c_10063_slider" ).slider( "value", String(amount) );
		$( "#c_10063_show" ).val( "" + String(amount) + " kg" );
	});
	$( "#c_10063_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10063" ).val())+0.5) * 10) / 10;
		$( "#c_10063" ).val(String(amount));
		$( "#c_10063_slider" ).slider( "value", String(amount) );
		$( "#c_10063_show" ).val( "" + String(amount) + " kg" );
	});

	$( "#c_10064_slider" ).slider({
		range: "min",
		value: 60,
		min: 30, max: 150, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10064_show" ).val( "" + ui.value + " kg");
			$( "#c_10064" ).val(ui.value);
		}
	});
	$( "#c_10064_show" ).val( "" + $( "#c_10064_slider" ).slider( "value" ) + " kg" );
	$( "#c_10064" ).val( $( "#c_10064_slider" ).slider( "value" ) );
	$( "#c_10064_minus" ).click(function(){
		amount = Math.max(30, Math.round((parseFloat($( "#c_10064" ).val())-0.5) * 10) / 10);
		$( "#c_10064" ).val(String(amount));
		$( "#c_10064_slider" ).slider( "value", String(amount) );
		$( "#c_10064_show" ).val( "" + String(amount) + " kg" );
	});
	$( "#c_10064_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10064" ).val())+0.5) * 10) / 10;
		$( "#c_10064" ).val(String(amount));
		$( "#c_10064_slider" ).slider( "value", String(amount) );
		$( "#c_10064_show" ).val( "" + String(amount) + " kg" );
	});

	$( "#c_10065_slider" ).slider({
		range: "min",
		value: 60,
		min: 30, max: 150, step: 0.5,
		slide: function( event, ui ) {
			$( "#c_10065_show" ).val( "" + ui.value + " kg");
			$( "#c_10065" ).val(ui.value);
		}
	});
	$( "#c_10065_show" ).val( "" + $( "#c_10065_slider" ).slider( "value" ) + " kg" );
	$( "#c_10065" ).val( $( "#c_10065_slider" ).slider( "value" ) );
	$( "#c_10065_minus" ).click(function(){
		amount = Math.max(30, Math.round((parseFloat($( "#c_10065" ).val())-0.5) * 10) / 10);
		$( "#c_10065" ).val(String(amount));
		$( "#c_10065_slider" ).slider( "value", String(amount) );
		$( "#c_10065_show" ).val( "" + String(amount) + " kg" );
	});
	$( "#c_10065_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10065" ).val())+0.5) * 10) / 10;
		$( "#c_10065" ).val(String(amount));
		$( "#c_10065_slider" ).slider( "value", String(amount) );
		$( "#c_10065_show" ).val( "" + String(amount) + " kg" );
	});

	$( "#c_10198_slider" ).slider({
		range: "min",
		value: 3000,
		min: 1000, max: 4500, step: 10,
		slide: function( event, ui ) {
			$( "#c_10198_show" ).val( "" + ui.value + " g");
			$( "#c_10198" ).val(ui.value);
		}
	});
	$( "#c_10198_show" ).val( "" + $( "#c_10198_slider" ).slider( "value" ) + " g" );
	$( "#c_10198" ).val( $( "#c_10198_slider" ).slider( "value" ) );
	$( "#c_10198_minus" ).click(function(){
		amount = Math.max(1000, Math.round((parseFloat($( "#c_10198" ).val())-10) * 10) / 10);
		$( "#c_10198" ).val(String(amount));
		$( "#c_10198_slider" ).slider( "value", String(amount) );
		$( "#c_10198_show" ).val( "" + String(amount) + " g" );
	});
	$( "#c_10198_plus" ).click(function(){
		amount = Math.round((parseFloat($( "#c_10198" ).val())+10) * 10) / 10;
		$( "#c_10198" ).val(String(amount));
		$( "#c_10198_slider" ).slider( "value", String(amount) );
		$( "#c_10198_show" ).val( "" + String(amount) + " g" );
	});


	});