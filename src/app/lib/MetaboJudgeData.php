<?php

class MetaboJudgeData  {
	
	public static function globals()
	{
		//通常版 判定出力結果
		//簡易版 判定出力結果
		//[判定出力値] = 判定出力結果

		/* 修正 201304改修 20130425 Straight Ozeki */

		$METABO_JUDGE_RESULT[1] = "現在おおむね健康な生活をおくっていらっしゃるようですが、検査をしてみないとわからない異常が潜んでいることもあります。
		ご自分の健康管理のためにも定期的な健康診断の受診をこころがけてください。
		規則正しい生活で暴飲、暴食を避けるようにしてください。";

		$METABO_JUDGE_RESULT[2] = "現在おおむね健康な生活をおくっていらっしゃるようですが、検査をしてみないとわからない異常が潜んでいることもあります。
		ご自分の健康管理のためにも定期的な健康診断の受診をこころがけてください。
		体調に気になるところがある時は早めに医療機関に受診してください。";

		$METABO_JUDGE_RESULT[3] = "メタボリック症候群の兆候は検査をしてみないとわからないことも多くあります。
		ご自分の健康管理のためにも定期的な健康診断の受診をこころがけてください。
		これまでに健診で異常を指摘されている方は指導されている注意を守った生活を心がけてください。
		体調に気になるところがある時は早めに医療機関に受診してください。";

		$METABO_JUDGE_RESULT[4] = "現在症状がなかったとしても、何らかのメタボリック症候群のリスクが潜んでいる可能性があります。
		ご自分の健康管理のためにも定期的な健康診断の受診を心がけてください。
		これまでに健診で異常を指摘されている方は指導されている注意を守った生活を心がけてください。
		体調に気になるところがある時は早めに医療機関に受診してください。";
		
		$METABO_JUDGE_RESULT[5] = "現在の状態で検査をしてみると、何らかのメタボリック症候群のリスクが潜んでいる可能性が高いです。
		ご自分の健康管理のためにも定期的な健康診断の受診を心がけてください。
		これまでに健診で異常を指摘されている方は指導されている注意を守った生活を心がけてください。
		体調に気になるところがある時は早めに医療機関に受診してください。";

		/*
		$METABO_JUDGE_RESULT[0] = "現在大きな問題はないようです。現在の健康状態を維持できるように食事、運動に気をつけてください。";
		$METABO_JUDGE_RESULT[1] = "概ね健康的な生活を送っているようですが、油断は禁物です。現在の健康状態を維持できるように食事、運動に気をつけてください。これまであまり意識したことがなかった人はこの機会に自分の食習慣、運動習慣を見直して見ましょう。";
		$METABO_JUDGE_RESULT[2] = "現時点では大きな問題はありません。が、一歩気を許すとメタボリック症候群予備軍に仲間入りです。今の時点で改善できる点を改善していきましょう。";
		$METABO_JUDGE_RESULT[3] = "そろそろメタボリック症候群予備軍になりかけていませんか？ほんのちょっとの運動、食事の調整で今より健康な生活に仲間入りです。できるところからはじめてみましょう。";
		$METABO_JUDGE_RESULT[4] = "メタボリック症候群予備軍です。今なら間に合います。食事、運動習慣を改善すれば健康な生活に戻れます。できるところからはじめてみましょう。";
		$METABO_JUDGE_RESULT[5] = "かなりメタボリック症候群に近いところに来ています。自分が気が付かない間に病気が潜んでいる可能性も出てきます。現在の自分の食事、運動習慣を見直して見ましょう。心配な方は早めに医者に受診しておきましょう。";
		$METABO_JUDGE_RESULT[6] = "体調が悪いところはないですか？メタボリック症候群の怖いところは自分の気づかないところで病気になっていることです。このままの生活ではメタボリック症候群の可能性が高いです。自分の食事、運動習慣を見直して見ましょう。心配な方は早めに医師の診察を受けて隠れた病気がないかチェックしましょう。";
		$METABO_JUDGE_RESULT[7] = "体調が悪いところはないですか？メタボリック症候群の怖いところは自分の気づかないところで病気になっていることです。このままの生活ではメタボリック症候群の可能性がかなり高いです。自分の食事、運動習慣を見直して見ましょう。心配な方は早めに医師の診察を受けて隠れた病気がないかチェックしましょう。";
		$METABO_JUDGE_RESULT[8] = "現在の状態を続けていると病気になるリスクが極めて高いです。自分の食事、運動習慣を見直して見ましょう。心配な方は早めに医師の診察を受けて隠れた病気がないかチェックしましょう。";
		$METABO_JUDGE_RESULT[9] = "メタボリック症候群の可能性があります。医師の診察を受けるようにしてください。まずは食事の見直しから始めましょう。運動については医師の指示に従ってください。";
		$METABO_JUDGE_RESULT[10] = "メタボリック症候群の可能性が高いです。医師の診察を受けるようにしてください。まずは食事の見直しから始めましょう。運動については医師の指示に従ってください。";
		*/
		
		$GLOBALS['METABO_JUDGE_RESULT'] = $METABO_JUDGE_RESULT;

		//超簡易版 結果
		//[BMI判定値][カロリー判定値][運動判定値] = 判定出力結果

		/* 修正 201304改修 20130423 Straight Ozeki */
		/*CASE1*/
		$METABO_JUDGE_RESULT_LIGHT[0][0][0] = array("result" => 0,"outputtext" => "医療機関への受診、相談をおすすめします。すでにかかりつけ医がある場合はその指示に従ってください。");
		/*CASE2*/
		$METABO_JUDGE_RESULT_LIGHT[1][1][1] = array("result" => 1,"outputtext" => "現在非常によいバランスを保っているようです。このままの生活を続けましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][1][2] = array("result" => 2,"outputtext" => "現在非常によいバランスを保っているようです。このままの生活を続けましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][1][3] = array("result" => 3,"outputtext" => "現在非常によいバランスを保っているようです。気を抜かずに１日１０分程度の運動を取り入れて見ましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][2][1] = array("result" => 4,"outputtext" => "現在よいバランスを保っているようです。食事を腹８分目にし将来のメタボリック症候群を予防しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][2][2] = array("result" => 5,"outputtext" => "現在よいバランスを保っているようです。食事を腹８分目にし将来のメタボリック症候群を予防しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][2][3] = array("result" => 6,"outputtext" => "現在よいバランスを保っているようです。食事を腹８分目にし１日１０分程度の運動を取り入れて将来のメタボリック症候群を予防しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][3][1] = array("result" => 7,"outputtext" => "現在よい体重を保っていますが、食事が多くなりがちのようです。腹８分目の食事を心がけて将来のメタボリック症候群を予防しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][3][2] = array("result" => 8,"outputtext" => "現在よい体重を保っていますが、食事が多くなりがちのようです。腹８分目の食事を心がけて将来のメタボリック症候群を予防しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[1][3][3] = array("result" => 9,"outputtext" => "現在よい体重を保っていますが、食事が多くなりがちのようです。腹８分目の食事を心がけ、１日１０分程度の運動を取り入れましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][1][1] = array("result" => 10,"outputtext" => "現在適正な体重を保っています。運動も行っているようなので、このままの生活を続けましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][1][2] = array("result" => 11,"outputtext" => "現在適正な体重を保っています。適度な運動もできているようなので、このままの生活を続けましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][1][3] = array("result" => 12,"outputtext" => "現在適正な体重を保っています。気を抜かずに１日１０分程度の運動を取り入れて見ましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][2][1] = array("result" => 13,"outputtext" => "現在適正な体重を保っています。やや食事が多くなることがあるようなので、気をつけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][2][2] = array("result" => 14,"outputtext" => "現在適正な体重を保っています。やや食事が多くなることがあるようなので、気をつけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][2][3] = array("result" => 15,"outputtext" => "現在適正な体重を保っています。やや食事が多くなることがあるようなので、気をつけて１日１０分程度の運動を取り入れて見ましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][3][1] = array("result" => 16,"outputtext" => "現在適正な体重ですが、食事摂取が多く、将来メタボリック症候群になる可能性があります。腹８分目の食事を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][3][2] = array("result" => 17,"outputtext" => "現在適正な体重ですが、食事摂取が多く、将来メタボリック症候群になる可能性があります。腹８分目の食事を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[2][3][3] = array("result" => 18,"outputtext" => "現在適正な体重ですが、食事摂取が多く、将来メタボリック症候群になる可能性があります。腹８分目の食事、１日１０分程度の運動を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][1][1] = array("result" => 19,"outputtext" => "メタボリック症候群の可能性があります。腹８分目の食事を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][1][2] = array("result" => 20,"outputtext" => "メタボリック症候群の可能性があります。腹８分目の食事を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][1][3] = array("result" => 21,"outputtext" => "メタボリック症候群の可能性があります。腹８分目の食事を心がけ、１日１０分程度の運動をするようにしましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][2][1] = array("result" => 22,"outputtext" => "メタボリック症候群の可能性があります。毎食腹８分目の食事を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][2][2] = array("result" => 23,"outputtext" => "メタボリック症候群の可能性があります。毎食腹８分目の食事を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][2][3] = array("result" => 24,"outputtext" => "メタボリック症候群の可能性があります。毎食腹８分目の食事を心がけ、１日１０分程度の運動を心がけましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][3][1] = array("result" => 25,"outputtext" => "メタボリック症候群の可能性があります。食事をとりすぎているようなので、食事を抜本的に見直しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][3][2] = array("result" => 26,"outputtext" => "メタボリック症候群の可能性があります。食事をとりすぎているようなので、食事を抜本的に見直しましょう。");
		$METABO_JUDGE_RESULT_LIGHT[3][3][3] = array("result" => 27,"outputtext" => "メタボリック症候群の可能性があります。食事をとりすぎているようなので、食事を抜本的に見直し、１日１０分程度の運動を心がけましょう。");


		/*
		$METABO_JUDGE_RESULT_LIGHT[1][1][1] = "現在非常によいバランスを保っているようです。気を抜かずに1日10分程度の運動を取り入れて見ましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][1][2] = "現在非常によいバランスを保っているようです。このままの生活を続けましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][1][3] = "現在非常によいバランスを保っているようです。このままの生活を続けましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][2][1] = "現在よいバランスを保っているようです。食事を腹8分目にし1日10分程度の運動を取り入れて将来のメタボリック症候群を予防しましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][2][2] = "現在よいバランスを保っているようです。食事を腹8分目にし将来のメタボリック症候群を予防しましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][2][3] = "現在よいバランスを保っているようです。食事を腹8分目にし将来のメタボリック症候群を予防しましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][3][1] = "現在よい体重を保っていますが、食事が多くなりがちのようです。腹8分目の食事を心がけ、1日10分程度の運動を取り入れましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][3][2] = "現在よい体重を保っていますが、食事が多くなりがちのようです。腹8分目の食事を心がけて将来のメタボリック症候群を予防しましょう。";
		$METABO_JUDGE_RESULT_LIGHT[1][3][3] = "現在よい体重を保っていますが、食事が多くなりがちのようです。腹8分目の食事を心がけて将来のメタボリック症候群を予防しましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][1][1] = "現在適正な体重を保っています。気を抜かずに1日10分程度の運動を取り入れて見ましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][1][2] = "現在適正な体重を保っています。適度な運動もできているようなので、このままの生活を続けましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][1][3] = "現在適正な体重を保っています。運動も行っているようなので、このままの生活を続けましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][2][1] = "現在適正な体重を保っています。やや食事が多くなることがあるようなので、気をつけて1日10分程度の運動を取り入れて見ましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][2][2] = "現在適正な体重を保っています。やや食事が多くなることがあるようなので、気をつけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][2][3] = "現在適正な体重を保っています。やや食事が多くなることがあるようなので、気をつけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][3][1] = "現在適正な体重ですが、食事摂取が多く、将来メタボリック症候群になる可能性があります。腹8分目の食事、1日10分程度の運動を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][3][2] = "現在適正な体重ですが、食事摂取が多く、将来メタボリック症候群になる可能性があります。腹8分目の食事を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[2][3][3] = "現在適正な体重ですが、食事摂取が多く、将来メタボリック症候群になる可能性があります。腹8分目の食事を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][1][1] = "メタボリック症候群の可能性があります。腹8分目の食事を心がけ、1日10分程度の運動をするようにしましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][1][2] = "メタボリック症候群の可能性があります。腹8分目の食事を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][1][3] = "メタボリック症候群の可能性があります。腹8分目の食事を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][2][1] = "メタボリック症候群の可能性があります。毎食腹8分目の食事を心がけ、1日10分程度の運動を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][2][2] = "メタボリック症候群の可能性があります。毎食腹8分目の食事を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][2][3] = "メタボリック症候群の可能性があります。毎食腹8分目の食事を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][3][1] = "メタボリック症候群の可能性があります。食事をとりすぎているようなので、食事を抜本的に見直し、1日10分程度の運動を心がけましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][3][2] = "メタボリック症候群の可能性があります。食事をとりすぎているようなので、食事を抜本的に見直しましょう。";
		$METABO_JUDGE_RESULT_LIGHT[3][3][3] = "メタボリック症候群の可能性があります。食事をとりすぎているようなので、食事を抜本的に見直しましょう。";
		*/
		$GLOBALS['METABO_JUDGE_RESULT_LIGHT'] = $METABO_JUDGE_RESULT_LIGHT;
	}

	public static function MetaboJudgeData18_20F()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		//18-20F
		$METABO_18_20F[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 2);
		$METABO_18_20F[2][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[2][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[2][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 2);
		$METABO_18_20F[2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[2][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[3][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[3][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[4][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20F[4][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[4][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[4][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20F[4][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20F[4][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20F[4][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[4][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20F[4][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$GLOBALS['METABO_18_20F'] = $METABO_18_20F;
	}

	public static function MetaboJudgeData18_20M()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		$METABO_18_20M[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[4][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[4][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[4][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[4][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[4][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[4][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[4][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[4][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[4][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);

		$GLOBALS['METABO_18_20M'] = $METABO_18_20M;

		/*

		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//18-20M
		$METABO_18_20M[1][1][1][1][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][1][1][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][1][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][1][2][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][2][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][2][1][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][2][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][1][2][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][1][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][1][1][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][1][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][1][2][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][2][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][2][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][2][2][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][1][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][1][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][2][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][3][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[1][1][4][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][4][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[1][1][4][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][4][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[1][1][4][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][1][4][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[1][1][4][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[1][1][4][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][3][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][2][4][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][4][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][2][4][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][4][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][2][4][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][2][4][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][2][4][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][2][4][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][3][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][3][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][3][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][3][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][3][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][3][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][3][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][3][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][3][4][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][3][4][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[1][4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][3][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][3][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[1][4][4][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[1][4][4][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][3][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][3][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][3][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][3][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][3][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][3][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][3][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][3][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][4][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][4][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][4][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][4][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][4][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][1][4][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][4][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][1][4][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_18_20M[2][2][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][3][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][3][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][3][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][3][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][3][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][3][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][3][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][3][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][2][4][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][2][4][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][3][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[2][3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][3][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][3][4][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][4][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][4][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][4][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][3][4][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_18_20M[2][3][4][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][3][4][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][3][4][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][1][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][1][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[2][4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][3][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][3][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][3][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][3][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][3][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][3][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][3][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][3][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[2][4][4][1][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][1][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][2][2][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[2][4][4][2][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][1][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][3][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][3][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_18_20M[3][1][4][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[3][1][4][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][4][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][4][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][4][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][4][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][4][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][1][4][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[3][2][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_18_20M[3][2][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_18_20M[3][2][3][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][3][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][3][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][3][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][3][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][3][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_18_20M[3][2][3][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][3][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[3][2][4][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][4][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][4][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][4][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][4][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][4][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[3][2][4][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][2][4][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][1][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][1][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][2][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][2][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_18_20M[3][3][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][3][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][3][4][1][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][1][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][2][2][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][3][4][2][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][1][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_18_20M[3][4][1][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_18_20M[3][4][1][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_18_20M[3][4][1][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_18_20M[3][4][1][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_18_20M[3][4][1][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][1][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_18_20M[3][4][1][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][2][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][3][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][3][1][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][3][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][3][1][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][3][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][3][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][3][2][2][1] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][3][2][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][1][1][1] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_18_20M[3][4][4][1][1][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][1][2][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][1][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][2][1][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][2][1][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][2][2][1] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_18_20M[3][4][4][2][2][2] = array('outputValue' => 8, 'predictiveValue' => 3);

		$GLOBALS['METABO_18_20M'] = $METABO_18_20M;
		*/
	}

	public static function MetaboJudgeData21_30F()
	{
		//通常版 判定出力
		//簡易版 判定出力

		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//21-30F
		$METABO_21_30F[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30F[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[2][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 2);
		$METABO_21_30F[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30F[2][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30F[2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30F[2][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30F[3][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[3][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[4][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30F[4][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30F[4][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30F[4][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30F[4][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30F[4][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30F[4][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30F[4][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$GLOBALS['METABO_21_30F'] = $METABO_21_30F;
	}

	public static function MetaboJudgeData21_30M()
	{
		//通常版 判定出力
		//簡易版 判定出力


		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		$METABO_21_30M[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[4][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[4][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[4][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[4][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[4][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[4][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[4][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[4][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);

		$GLOBALS['METABO_21_30M'] = $METABO_21_30M;


		/*
		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//21-30M
		$METABO_21_30M[1][1][1][1][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][1][1][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][1][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][1][2][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][2][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][2][1][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][2][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][1][2][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][1][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][1][1][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][1][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][1][2][2] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][2][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][2][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][2][2][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][1][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][1][2][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][2][1][1] = array('outputValue' => 0, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][3][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[1][1][4][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][4][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[1][1][4][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][4][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[1][1][4][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][1][4][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][1][4][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[1][1][4][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][3][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][2][4][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][4][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][2][4][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][4][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][2][4][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][2][4][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][2][4][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][2][4][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][3][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][3][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][3][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][3][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][3][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][3][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][3][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][3][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][3][4][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][3][4][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[1][4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][3][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][3][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[1][4][4][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[1][4][4][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][1][2][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][2][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][3][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][3][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][3][1][2][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][3][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][3][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][3][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][3][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][3][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][4][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][4][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][4][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][4][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][4][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][1][4][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][4][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][1][4][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_21_30M[2][2][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][3][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][3][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][3][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][3][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][3][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][3][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][3][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][3][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][2][4][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][2][4][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][3][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[2][3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][3][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][3][4][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][4][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][4][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][4][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][3][4][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_21_30M[2][3][4][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][3][4][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][3][4][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][1][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][1][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[2][4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][3][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][3][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][3][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][3][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][3][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][3][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][3][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][3][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[2][4][4][1][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][1][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][2][2][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[2][4][4][2][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][1][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][3][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][3][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_21_30M[3][1][4][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][1][4][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][4][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][4][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][4][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][4][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][4][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][1][4][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[3][2][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_21_30M[3][2][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_21_30M[3][2][3][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][3][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][3][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][3][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][3][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][3][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 3);
		$METABO_21_30M[3][2][3][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][3][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[3][2][4][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][4][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][4][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][4][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][4][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][4][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[3][2][4][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][2][4][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][1][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][1][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][2][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][2][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_21_30M[3][3][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][1][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][3][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][3][4][1][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][1][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][2][2][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][3][4][2][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][1][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_21_30M[3][4][1][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_21_30M[3][4][1][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_21_30M[3][4][1][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_21_30M[3][4][1][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_21_30M[3][4][1][2][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][1][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_21_30M[3][4][1][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][1][1][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][2][2][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][3][1][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][3][1][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][3][1][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][3][1][2][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][3][2][1][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][3][2][1][2] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][3][2][2][1] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][3][2][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][1][1][1] = array('outputValue' => 6, 'predictiveValue' => 2);
		$METABO_21_30M[3][4][4][1][1][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][1][2][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][1][2][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][2][1][1] = array('outputValue' => 6, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][2][1][2] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][2][2][1] = array('outputValue' => 7, 'predictiveValue' => 3);
		$METABO_21_30M[3][4][4][2][2][2] = array('outputValue' => 8, 'predictiveValue' => 3);
		$GLOBALS['METABO_21_30M'] = $METABO_21_30M;
		*/
	}

	public static function MetaboJudgeData31_40F()
	{
		//通常版 判定出力
		//簡易版 判定出力

		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//31-40F
		$METABO_31_40F[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40F[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40F[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40F[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40F[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40F[2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40F[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_31_40F[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_31_40F[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40F[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_31_40F[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_31_40F[4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40F[4][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40F[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40F[4][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40F[4][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);

		$GLOBALS['METABO_31_40F'] = $METABO_31_40F;
	}

	public static function MetaboJudgeData31_40M()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		//31-40M
		$METABO_31_40M[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40M[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_31_40M[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40M[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40M[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40M[2][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_31_40M[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_31_40M[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_31_40M[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40M[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_31_40M[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_31_40M[4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40M[4][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_31_40M[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40M[4][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_31_40M[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$GLOBALS['METABO_31_40M'] = $METABO_31_40M;
	}

	public static function MetaboJudgeData41_50F()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		//41_50F
		$METABO_41_50F[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50F[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50F[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50F[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50F[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50F[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_41_50F[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_41_50F[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_41_50F[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50F[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_41_50F[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[4][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_41_50F[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50F[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_41_50F[4][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_41_50F[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$GLOBALS['METABO_41_50F'] = $METABO_41_50F;
	}

	public static function MetaboJudgeData41_50M()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）
		//41-50M
		$METABO_41_50M[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][2][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50M[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[2][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50M[2][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_41_50M[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50M[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[2][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50M[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_41_50M[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_41_50M[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 3);
		$METABO_41_50M[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_41_50M[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_41_50M[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_41_50M[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$GLOBALS['METABO_41_50M'] = $METABO_41_50M;
	}

	public static function MetaboJudgeData51_60F()
	{
		//通常版 判定出力
		//簡易版 判定出力

		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//51-60F
		$METABO_51_60F[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60F[2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_51_60F[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60F[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_51_60F[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60F[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60F[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60F[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60F[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60F[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_51_60F[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60F[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60F[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_51_60F[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$GLOBALS['METABO_51_60F'] = $METABO_51_60F;
	}

	public static function MetaboJudgeData51_60M()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		//51-60M
		$METABO_51_60M[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][1][1][2] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_51_60M[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60M[2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_51_60M[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60M[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_51_60M[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_51_60M[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 3);
		$METABO_51_60M[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60M[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60M[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_51_60M[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60M[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_51_60M[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_51_60M[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_51_60M[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_51_60M[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$GLOBALS['METABO_51_60M'] = $METABO_51_60M;
	}

	public static function MetaboJudgeData61_70F()
	{
		//通常版 判定出力
		//簡易版 判定出力

		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//61_70F
		$METABO_61_70F[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70F[2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_61_70F[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70F[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_61_70F[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70F[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70F[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_61_70F[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70F[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_61_70F[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_61_70F[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70F[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_61_70F[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_61_70F[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$GLOBALS['METABO_61_70F'] = $METABO_61_70F;

	}

	public static function MetaboJudgeData61_70M()
	{
		//通常版 判定出力
		//簡易版 判定出力

		//[腹囲判定値][BMI判定値][カロリー判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値

		//61-70M
		$METABO_61_70M[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70M[2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_61_70M[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70M[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_61_70M[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_61_70M[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_61_70M[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 1);
		$METABO_61_70M[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_61_70M[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_61_70M[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_61_70M[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$GLOBALS['METABO_61_70M'] = $METABO_61_70M;
	}

	public static function MetaboJudgeData71_xxF()
	{
		//通常版 判定出力
		//簡易版 判定出

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		//71_xxF
		$METABO_71_xxF[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxF[2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_71_xxF[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxF[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_71_xxF[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxF[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxF[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxF[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxF[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxF[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_71_xxF[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxF[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxF[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_71_xxF[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$GLOBALS['METABO_71_xxF'] = $METABO_71_xxF;
	}	

	public static function MetaboJudgeData71_xxM ()
	{
		//通常版 判定出力
		//簡易版 判定出力

		/* 修正 201304改修 20130425 Straight Ozeki */

		//[BMI判定値][食事判定値][夕食判定値][飲酒判定値][運動判定値] = 判定出力値, 予測値（後から削除予定）

		//71-xxM
		$METABO_71_xxM[1][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][2][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][1][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[1][2][2][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][1][1][1] = array('outputValue' => 1, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][1][2][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_71_xxM[2][1][2][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxM[2][2][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][2][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_71_xxM[2][2][1][2][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][2][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxM[2][2][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[2][2][2][1][2] = array('outputValue' => 2, 'predictiveValue' => 2);
		$METABO_71_xxM[2][2][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 2);
		$METABO_71_xxM[2][2][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 3);
		$METABO_71_xxM[3][1][1][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][1][1][2] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][1][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][2][1][1] = array('outputValue' => 2, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][2][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][1][2][2][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][1][2][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][2][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[3][2][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][1][1][2] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][1][2][2] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][2][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxM[4][1][2][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[4][1][2][2][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxM[4][2][1][1][1] = array('outputValue' => 3, 'predictiveValue' => 1);
		$METABO_71_xxM[4][2][1][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxM[4][2][1][2][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[4][2][1][2][2] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_71_xxM[4][2][2][1][1] = array('outputValue' => 4, 'predictiveValue' => 1);
		$METABO_71_xxM[4][2][2][1][2] = array('outputValue' => 4, 'predictiveValue' => 2);
		$METABO_71_xxM[4][2][2][2][1] = array('outputValue' => 5, 'predictiveValue' => 2);
		$METABO_71_xxM[4][2][2][2][2] = array('outputValue' => 5, 'predictiveValue' => 3);
		$GLOBALS['METABO_71_xxM'] = $METABO_71_xxM;
	}		

}	