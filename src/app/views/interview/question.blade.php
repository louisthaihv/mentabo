<?php if(isset($mas) == 1) { ?>
<form action="{{ URL::to('/') }}/interview" method="POST" id="form_main">
<?php } ?>

<?php $roop_section = 1 ?>
<?php foreach ($section_list as $key => $value) { ?>
	<?php $section_data = $value ?>
	<?php if(isset($section_data['layer_inclusion_tree'])) { ?>
		<?php $roop_1st = 0 ?>
		<?php foreach ($section_data['layer_inclusion_tree'] as $layer_id_1st => $lower_layer_id_key_list) { ?>
			<?php if(isset($section_data['layer_choice_list'][$layer_id_1st])) $layer_choice_data = $section_data['layer_choice_list'][$layer_id_1st] ?>
			<?php if($layer_choice_data['choice_list'][0]) { ?>
				<?php $layer_type_1st = $layer_choice_data['choice_list'][0]['choice_type'] ?>
			<?php }else{ ?>	
				<?php $layer_type_1st = '' ?>
			<?php } ?>		
			<?php if($roop_1st == 0){ ?>
				<div id="l_<?php echo $layer_id_1st ?>">
				<h3 id="s_<?php echo $section_data['section_id'] ?>" <?php if($roop_section == 1){ ?> class="top" <?php } ?>><?php echo $section_data['section_string'] ?></h3>
			<?php } ?>
			<?php if($layer_choice_data['layer_data']['annotation'] || $section_data['flg_must_input'] == 1 || $section_data['flg_must_input'] == 2) { ?>
				<p class="lead">
					<?php if($roop_1st == 0 && ($section_data['flg_must_input'] == 1 || $section_data['flg_must_input'] == 2)) { ?><span><img src="{{ URL::to('/') }}/img/icon_required.png" alt="必須" /></span><?php }?>
					<?php echo $layer_choice_data['layer_data']['annotation'] ?>
				</p>
			<?php } ?>		

			<?php if(isset($layer_choice_data['layer_data']['help_title']) && $layer_choice_data['layer_data']['help_text']) { ?>
				<div class="annotation-overlay">
					<a href="#" rel="#overlay-<?php echo $section_data['section_id'] ?>" class="trigger"><?php echo $layer_choice_data['layer_data']['help_title'] ?><!--{$layer_choice_data.layer_data.help_title}--></a>
					<div id="overlay-<?php echo $section_data['section_id'] ?>" class="overlayBox">
						<h4><?php echo $layer_choice_data['layer_data']['help_title'] ?><!--{$layer_choice_data.layer_data.help_title}--></h4>
						<p><?php echo $layer_choice_data['layer_data']['help_text'] ?><!--{$app_ne.layer_list[$layer_choice_data.layer_data.layer_id].help_text}--></p>
						<p class="close">×</p>
					</div>
				</div>
			<?php } ?>

			<!--{*DIV開始*}-->
			<?php if($layer_type_1st == 'tab') { ?>
				<div id="tab_<?php echo $layer_id_1st ?>" class="tabs">
			<?php }elseif($layer_type_1st == 'accordion') { ?>
				<div id="acc_<?php echo $layer_id_1st ?>" class="accordion">
			<?php } ?>	

			<!--{*タブの見出し*}-->
			<?php if($layer_type_1st == 'tab') { ?>
					<?php $last = count($layer_choice_data['choice_list'])-1;  ?>
					<?php foreach ($layer_choice_data['choice_list'] as $idx_c => $choice_data) {?>
					<?php if($idx_c == 0){ ?>
						<ul>
					<?php } ?>
						<li class="tablabel tablabel<?php echo $idx_c ?>" id="tablabel_<?php echo $choice_data['choice_id'] ?>"><a href="#tab_<?php echo $choice_data['choice_id'] ?>"><?php echo $choice_data['choice_string'] ?></a></li>
					<?php if($idx_c == $last ){ ?>
						</ul>
					<?php } ?>	
					<?php } ?>	
			<?php } ?>	
			<?php if($lower_layer_id_key_list) { ?>
				<?php $roop_2nd = 0 ?>
				<?php foreach ($lower_layer_id_key_list as $layer_id_2nd => $lower_layer_id_key_list_2nd) { ?>	

					<?php if(isset($section_data['layer_choice_list'][$layer_id_1st]['choice_list'][$roop_2nd])) $choice_data_1st = $section_data['layer_choice_list'][$layer_id_1st]['choice_list'][$roop_2nd] ?>

					<?php $layer_choice_data = $section_data['layer_choice_list'][$layer_id_2nd] ?>
					<?php $layer_type_2nd = $layer_choice_data['choice_list'][0]['choice_type'] ?>
					<?php if($layer_type_1st == 'tab') { ?>
						<div id="tab_<?php echo $choice_data_1st['choice_id'] ?>" class="tabcontent<?php echo $roop_2nd ?>">
					<?php }elseif($layer_type_1st == 'accordion') { ?>
						<h4><a href="#"><?php echo $choice_data_1st['choice_string'] ?></a></h4>
					<?php } ?>
					<!--{*DIV開始*}-->
					<?php if($layer_type_2nd == 'tab') { ?>
						<div id="tab_<?php echo $layer_id_2nd ?>" class="tabs">
					<?php }elseif($layer_type_2nd == 'accordion') { ?>
						<div id="acc_<?php echo $layer_id_2nd ?>" class="accordion">
					<?php }else { ?>	
						<div id="l_<?php echo $layer_id_2nd ?>" <?php if($layer_type_1st != 'tab' && $layer_type_1st != 'accordion') {?>class="choice-lowlayer"<?php }?>>
					<?php } ?>

					<!--{*タブの見出し*}-->
					<?php if($layer_type_2nd == 'tab') { ?>
							<?php $last2 = count($layer_choice_data['choice_list'])-1;  ?>
							<?php foreach ($layer_choice_data['choice_list'] as $idx_c => $choice_data) {?>
							<?php if($idx_c == 0){ ?>
								<ul>
							<?php } ?>
								<li class="tablabel tablabel<?php echo $roop_2nd ?>" id="tablabel_<?php $choice_data['choice_id'] ?>"><a href="#tab_<?php $choice_data['choice_id'] ?>"><?php $choice_data['choice_string'] ?></a></li>
							<?php if($idx_c == $last2 ){ ?>
								</ul>
							<?php } ?>
							<?php } ?>
					<?php } ?>

					<?php if($lower_layer_id_key_list_2nd) {?>
						<?php $roop_3rd = 0 ?>
							<?php foreach ($lower_layer_id_key_list_2nd as $layer_id_3rd => $lower_layer_id_key_list_3rd) { ?>
							<?php $choice_data_2nd = $section_data['layer_choice_list'][$layer_id_2nd]['choice_list'][$roop_3rd] ?>
							<?php $layer_choice_data = $section_data['layer_choice_list'][$layer_id_3rd] ?>
							<?php if($layer_type_2nd == 'tab') { ?>
								<div id="tab_<?php echo $choice_data_2nd['choice_id'] ?>" class="tabcontent<?php echo $roop_3rd ?>">
							<?php }elseif($layer_type_2nd == 'accordion') { ?>
								<h4><a href="#"><?php echo $choice_data_2nd['choice_string'] ?></a></h4>
							<?php } ?>

							<div id="l_<?php echo $layer_id_3rd ?>"<?php if($layer_type_2nd == 'tab' && $layer_type_2nd == 'accordion') {?> class="choice-lowlayer"<?php }?>>
								<?php foreach ($layer_choice_data['choice_list'] as $idx_c => $choice_data) {?>
								@include('interview.choice_data')
							<?php } ?>
							</div>
							<?php if($layer_type_2nd == 'tab'){ ?>
								</div>
							<?php } ?>	
						<?php $roop_3rd++; ?>
						<?php } ?>
					<?php }else {?>	
						<!--{*選択肢の出力*}-->
						<?php if($layer_choice_data['layer_data']['choice_width_type']) { ?>
						<div class="choices-<?php echo $layer_choice_data['layer_data']['choice_width_type'] ?>">
						<?php }else {?>	
						<div class="choices">
						<?php }?>	
						<?php foreach ($layer_choice_data['choice_list'] as $idx_c => $choice_data) {?>
							@include('interview.choice_data')
						<?php } ?>
						</div>
					<?php } ?>		
					<!--{*DIV終了*}-->
					</div>
					<?php if($layer_type_1st == 'tab') { ?>
						</div>
					<?php } ?>		
				<?php $roop_2nd++; ?>	
				<?php } ?>	
			<?php }else{ ?>		
				<!--{*選択肢の出力*}-->
				<?php foreach ($layer_choice_data['choice_list'] as $idx_c => $choice_data) {?>
					@include('interview.choice_data')
				<?php } ?>	
			<?php } ?>	

			<!--{*DIV終了*}-->
			<?php if($layer_type_1st = 'tab'){ ?>
				</div>
			<?php }elseif($layer_type_1st = 'accordion'){ ?>
				</div>
				<div style="clear:left;"></div>
			<?php } ?>		

			<?php if($roop_1st > 0) { ?>
				</div>
			<?php } ?>	
		<?php $roop_1st++; ?>	
		<?php } ?>	
	<?php }else { ?>	
		<!--{*通常の選択肢表示*}-->
		<?php $roop_normal = 1 ?>
		<?php foreach ($section_data['layer_choice_list'] as $idx2 => $layer_choice_data) {?>
			<div id="l_<?php echo $layer_choice_data['layer_data']['layer_id'] ?>">
			<?php if($roop_normal == 1) { ?>
				<h3 id="s_<?php echo $section_data['section_id'] ?>" <?php if($roop_section == 1){ ?> class="top" <?php } ?>><?php echo $section_data['section_string'] ?></h3>
			<?php }else {?>
			<?php } ?>
			<?php if($layer_choice_data['layer_data']['annotation'] || $section_data['flg_must_input'] == 1 || $section_data['flg_must_input'] == 2){ ?>
				<p class="lead">
					<?php if($roop_normal == 1 && ($section_data['flg_must_input'] == 1 || $section_data['flg_must_input'] == 2)) {?>
						<span><img src="{{ URL::to('/') }}/img/icon_required.png" alt="必須" /></span>
					<?php } ?>
					<?php echo $layer_choice_data['layer_data']['annotation'] ?>
				</p>
			<?php } ?>	
			<?php  if(isset($layer_choice_data['help_title']) && $layer_choice_data['help_text']) {?>
				<div class="annotation-overlay">
					<a href="#" rel="#overlay-<?php echo $section_data['section_id'] ?>" class="trigger"><?php echo $layer_choice_data['layer_data']['help_title'] ?></a>
					<div id="overlay-<?php echo $section_data['section_id'] ?>" class="overlayBox">
						<h4><?php echo $layer_choice_data['layer_data']['help_title'] ?></h4>
						<p><?php echo $layer_choice_data['layer_data']['help_text'] ?></p>
						<p class="close">×</p>
					</div>
				</div>
			<?php } ?>		
			<?php if($layer_choice_data['layer_data']['choice_width_type']) { ?>
				<div class="choices-<?php echo $layer_choice_data['layer_data']['choice_width_type'] ?>">
			<?php }else{ ?>	
				<div class="choices">
			<?php } ?>	
			<?php foreach ($layer_choice_data['choice_list'] as $idx_c => $choice_data) {?>
				@include('interview.choice_data')
			<?php } ?>		
				</div>
			</div>
		<?php $roop_normal++; ?>	
		<?php } ?>	
	<?php } ?>	
<?php $roop_section ++ ?>
<?php } ?>
<?php if(isset($mas) == 1) { ?>
	<?php if(isset($type) != 'edit') {?>
	<br clear ="all" >
	<div class="btn-area">
		<input type="hidden" name="step_id" value="<?php echo $step_id ?>">
		<div>
		<ul class="action_interview_question">
			<li style="text-align: right"><?php if($step_id > 2) { ?><input type="button" value="戻る" onClick="location.href='?s=<?php echo ($step_id-2) ?>'" /><?php }else{ echo '&nbsp'; } ?></li>	
			<li style="text-align: left"><input type="submit" name="action_interview_question" value="次へ" /></li>
		</ul>
	</div>
	<?php } ?>
</form>	
<?php } ?>

