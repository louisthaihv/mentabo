@include('admin/header')

<hr />
<div id="wrapper">
<div id="accountEntry" class="contents">
	<h2 id="pageTitle">アカウント編集</h2>
	<hr />

	<div class="buttonArea">
		<ul>
			<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
		</ul>
	</div>

	
	<div id="main">
		
			<P><span class="required">＊</span>は必須項目です。</P>

			<form  action="../editaccount/{{ $account->account_id }}" method="post">
                @if($errors->all('message'))
                <ul class="valid_error">
                    @foreach($errors->all('<li>:message</li>') as $message)
                    {{ $message }}
                    @endforeach
                </ul>
                @endif
				<div class="table-wrap">
					<table summary="アカウント登録">
												<tr>
							<th><span class="arbitrary"></span>アカウントID</th>
							<td>{{ $account->account_id }}
								<input type="hidden" name="account_id" value="{{ $account->account_id }}"/></td>
						</tr>
												<tr>
							<th><span class="required">＊</span>ログインID<span class="caution">（半角英数）</span></th>
							<td><input type="text" name="login_name" value="{{ $account->login_name }}" /></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>氏名</th>
							<td><input type="text" name="name" value="{{ $account->name }}" /></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>氏名かな</th>
							<td><input type="text" name="name_kana" value="{{ $account->name_kana }}" /></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>パスワード<span class="caution">（半角英数）</span></th>
							<td><input type="password" name="password" value="" /></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>パスワード確認<span class="caution">（半角英数）</span></th>
							<td><input type="password" name="password_confirmation" value="" /></td>
						</tr>
						<tr>
							<th><span class="required">＊</span>役割</th>
							<td>
                                <select name="role_id">
                                <?php
                                    if(count($roles))foreach($roles as $role_id => $role_name){?>
                                        <?php if($role_id): ?>
                                        <option value="<?php echo $role_id;?>"<?php if($account->role_id == $role_id){echo " selected";}?>><?php echo $role_name->role_name;?></option>
                                        <?php endif; ?>
                                    <?php }
                                ?>
																	{{-- $roles[$account->role_id ]->role_name --}}
                                </select>
															</td>
						</tr>
						<tr>
							<th><span class="required">＊</span>有効／無効</th>
                                    <td><select name="valid">
<option value="1" <?php if ($account->valid=="1") echo 'selected="selected"';?> >有効</option>
<option value="0" <?php if ($account->valid=="0") echo 'selected="selected"';?> >無効</option>
</select></td>
						</tr>
						<tr>
							<th><span class="arbitrary"></span>登録日時</th>
							<td>{{ $account->create_datetime }}
							</td>
						</tr>
						<tr>
							<th><span class="arbitrary"></span>最終更新日時</th>
							<td>{{ $account->update_datetime }}
							</td> 
						</tr>
						<tr>
							<th><span class="arbitrary"></span>最終更新者アカウントID</th>
							<td>{{ $account->update_account_id }}
								</td>
						</tr>
						<tr>
							<th colspan="2"  class="btn"><input type="submit" name="action_admin_account_save" value="保存"></th>
						</tr>
					</table>
					<div class="btm"><img src="../../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
				</div>
			</form>

		
	</div><!-- /#main -->

</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
