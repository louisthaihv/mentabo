<?php

namespace Admin;

use Form;
use View;
use Input;
use Auth;
use Session;
use Interview;
use AdminInterview;

class InterviewController extends \BaseController {


	public function getIndex($id = null)
	{		
		if (!$this->loggedin1()) {
			return \Redirect::to('admin/login');
		}

		$input = Input::all();
		if(isset($input['ms'])){
			return $this->interviewUpdate();
		}else{

			if(isset($_GET['section_id'])) {

				if($_GET['section_id'] == 'email') {
					echo $this->interviewEmail();
				} else{
					echo $this->interviewEdit();	
				}
				
			}else{
				if(isset($id)) echo $this->interviewDetail($id);
				else echo $this->interviewList();
			}
		}
	}


	public function interviewList()
	{

		$special = $this->special();
		if ($special) {
			$tenant_id = \Session::get('temp_tenant_id');
		}else {
			$tenant_id = \Auth::user()->get()->tenant_id;
		} 

		$skip = 0;
		$take = 30;
		$input = Input::all();

		if($input) {
			// $interview_list = AdminInterview::getInterviewSearch($skip, $take, $tenant_id);
			// $interview_list->setBaseUrl($_SERVER['REQUEST_URI']);

			$interview_list = AdminInterview::getInterviewList($skip, $take, $tenant_id, $input);
			$interview_list->setBaseUrl('');
		} else {
			$input = array(
				'interview_y_f' => '',
				'interview_m_f' => '',
				'interview_d_f' => '',
				'interview_h_f' => '',
				'interview_i_f' => '',
				'interview_y_t' => '',
				'interview_m_t' => '',
				'interview_d_t' => '',
				'interview_h_t' => '',
				'interview_i_t' => '',
				'birthday_y_f' => '',
				'birthday_m_f' => '',
				'birthday_d_f' => '',
				'birthday_m_t' => '',
				'birthday_d_t' => '',
				'name' => '',
				'birthday_y_t' => '',
			); 
			$interview_list = AdminInterview::getInterviewList($skip, $take, $tenant_id);
			$interview_list->setBaseUrl('');
		}

        if(isset($input['gender'])){
            // '1:男性、2：女性
 			$data['gender_1'] = (in_array('1',$input['gender']))?true:false;
            $data['gender_2'] = (in_array('2',$input['gender']))?true:false;
        }else{
            $data['gender_1'] = true;
            $data['gender_2'] = true;
        }
        if(isset($input['section_set_id'])){
            $data['section_set_id_1'] = (in_array('34',$input['section_set_id']))?true:false;
            $data['section_set_id_2'] = (in_array('33',$input['section_set_id']))?true:false;
            $data['section_set_id_3'] = (in_array('32',$input['section_set_id']))?true:false;
            $data['section_set_id_4'] = (in_array('31',$input['section_set_id']))?true:false;
            $data['section_set_id_5'] = (in_array('35',$input['section_set_id']))?true:false;
        }else{
            $data['section_set_id_1'] = true;
            $data['section_set_id_2'] = true;
            $data['section_set_id_3'] = true;
            $data['section_set_id_4'] = true;
            $data['section_set_id_5'] = true;
        }

        $data['week'] = array();
		if(count($interview_list)){
			foreach($interview_list as $key => $val) {
				$res = AdminInterview::getFollowupDataById($val['interview_id'], true);
				if($res && @$res[0]['newest_week']){
                    $data['week'][$key]['newest_week'] = $res[0]['newest_week'] . ' 週';
					//$interview_list->$key->newest_week = $res[0]['newest_week'] . ' 週';
				} else {
                    $data['week'][$key]['newest_week'] = '--';
					//$interview_list->$key->newest_week = '--';
				}		
			}
		}

		$content = View::make('admin.interview.list', compact('interview_list', 'input'));
        View::share('data',$data);
		return View::make('admin.content', compact('content'));
	}

	public function interviewDetail($interview_id)
	{
		//問診情報取得
		$interview_data = AdminInterview::getInterviewDataById($interview_id);
		// if (Ethna::isError($interview_data)) {
		// 	$this->ae->add(null, $interview_data->getMessage());
		// 	return 'admin_trouble';
		// }
		$isExist = true;
		if (empty($interview_data)) {
			//$this->af->setApp('message', '問診IDが存在しません');
			$isExist = false;

		} else {

			//対象セクションセット取得（フォローアップ対象セットかを確認）
			//$section_set_data = $interview_manager->getSectionSet($interview_data['section_set_id']);	
			$section_set_data = '';
			/* 医師向け表示 */
			//$age = Ax_Util::calcAge($interview_data['start_datetime'], $interview_data['birth_date']);
			//$interview_data['age'] = $age;

			//設問取得
			$section_list = AdminInterview::getSectionSetContentsById($interview_data['section_set_id'], $interview_data);
			// if (Ethna::isError($section_list)) {
			// 	$this->ae->add('null', $section_list->getMessage());
			// 	return 'admin_trouble';
			// }

			//データ生成
			$data_doctorView = AdminInterview::getDocterViewData($interview_data, $section_list);
			// if (Ethna::isError($data_doctorView)) {
			// 	$this->ae->add(null, $data_doctorView->getMessage());
			// 	return 'admin_trouble';
			// }

			/* 患者向け表示 */
			$data_patientView = $this->judgeByInterviewId($interview_data, $interview_data['section_set_id']);
			// if (Ethna::isError($data_patientView)) {
			// 	$this->ae->add(null, $data_patientView->getMessage());
			// 	return 'admin_trouble';
			// }

			/* フォローアップ情報 */
			$data_followup = AdminInterview::getFollowupDataById($interview_id);
			// if (Ethna::isError($data_followup)) {
			// 	$this->ae->add(null, $data_followup->getMessage());
			// 	return 'admin_trouble';
			// }

			/* フォローアップがあったらURL作成 */
			if($data_followup || $interview_data['flg_followup'] == 1){
				$null_data_followup = AdminInterview::getFollowupNullDataById($interview_id);
				if($null_data_followup){
					$week = $null_data_followup[0]['week'];
				} else {
					$week = 2;
				}
				
				$email = $interview_data["email"];
				//$plain_text = 'week={$week}&interview_id={$interview_id}&email={$email}';
				//$encode_text = Ax_Util::getEncodeString($plain_text);
				//$followup_url = AX_ABSOLUTE_URL . "followup/index.php?section_set_id=5&" . $encode_text;
				$followup_url = '';
			}

			$is_followup = $interview_data['flg_followup'];
			$followup_list = $data_followup;
			$followup_url = $followup_url;
			$section_set_data = '';
			$interview_type = '';
			//$section_set_data = $section_set_data[$interview_data['section_set_id']];
			//$interview_type = $interview_type[$interview_data['section_set_id']];
			$result = $data_patientView;
			$absolute_url = \URL::to('/');
			$view_type = 'admin';

			
			// $interview_type = $this->config->get('interview_type');
			// $this->af->setApp('is_followup',$interview_data["flg_followup"]);
			// $this->af->setApp('followup_list', $data_followup);
			// $this->af->setApp('followup_url', $followup_url);
			
			// $this->af->setApp('section_set_data', $section_set_data[$interview_data['section_set_id']]);
			// $this->af->setApp('interview_data', $interview_data);
			// $this->af->setApp('interview_type',$interview_type[$interview_data["section_set_id"]]);
			// $this->af->setAppNE('data_doctorView', $data_doctorView);
			// $this->af->setApp('result', $data_patientView);

			// $this->af->setApp('absolute_url', AX_ABSOLUTE_URL);
			// $this->af->setApp('view_type', 'admin');
		}
		$iteration = 1;
		$content = View::make('admin.interview.detail', compact('interview_id', 'iteration', 'isExist', 'is_followup', 'followup_list',  'followup_url',  'section_set_data', 'interview_type', 'result', 'absolute_url',  'view_type', 'interview_data', 'data_doctorView'));
		return View::make('admin.content', compact('content'));
	}

	public function interviewEdit()
	{

		$input = Input::all();
		//$interview_data = $this->interviewData($input['interview_id'], 'user_id'); // BaseController
		$interview_data = AdminInterview::selectInterview($input['interview_id'], $input['user_id']);
		//$user_data = $this->userData($interview_data['user_id']); // BaseController
		$user_data = $this->selectUser($interview_data['user_id']); // BaseController
		
		// セクションIDからセクションデータを取得
		$param = array(
			'section_set_id' => $interview_data['section_set_id'],
			'section_id' => $input['section_id']
		);

		$section_data = $this->getCommonData($param);  // BaseController
		$section_data = $section_data[0];
		
		//セクションIDから関連する問題を取得する
		$section_list_one = $this->getSectionSetContentByBlockParty($interview_data['section_set_id'], $section_data, $user_data); // BaseController

		
		foreach ($section_list_one as $k => $v) {
			$section_list_id[] = $v['section_id'];
		}

		$choice_list = AdminInterview::getEditData($interview_data, $section_list_id);

		$base_choice_id_array = array();
		$base_choice_id_array['section_set_id'] = $param['section_set_id'];
		$base_choice_id_array['interview_id'] = $param['section_id'];

		$choice_data = array();
		if(is_array($choice_list)){
			foreach($choice_list as $key => $val){
				$base_choice_id_array['id'][] = $val['choice_id'];
				if($val['choice_type'] == 'checkbox'){
					$choice_data['il_' . $val['layer_id']][$val['choice_id']] = $val['choice_id'];
				} else {
					$choice_data['il_' . $val['layer_id']] = $val['choice_id'];
				}
				
				switch ($val['choice_type']) {
					case 'radio': case "checkbox":
						if ($val['selection']) $value = $val['choice_string'];
						break;
					case 'string':
						$value = $val['input_string'];
						break;
					case 'year':
						$value = date('Y', strtotime($val['input_datetime']));
						break;
					case 'month':
						$value = date('m', strtotime($val['input_datetime']));
						break;
					case 'day':
						$value = date('d', strtotime($val['input_datetime']));
						break;
					case 'date':
						$value = date('Y/m/d', strtotime($val['input_datetime']));
						break;
					case 'time':
						$value = date('H:i:s', strtotime($val['input_datetime']));
						break;
					case 'datetime':
						$value = $val['input_datetime'];
						break;
					case 'slider':
					case 'slider_hour':
					case 'slider_minute':
					case 'slider_show_by_step':
					case "numeric":
						$value = $val['input_float'];
						break;
					case 'accordion':
					case 'tab':
					default:
						break;
				}
				$choice_data['i_' . $val['choice_id']] = $value;
			}
		}
		
		foreach($section_list_one as $key => $val){
			$section_set_by_page[1][$key + 1] = $val['section_id'];
		}

		Session::put('section_id', $input['section_id']);
		Session::put('interview_id', $input['interview_id']);
		Session::put('section_set_id', $interview_data['section_set_id']);
		Session::put('input_data', $choice_data);
		Session::put('section_set_by_page', $section_set_by_page);
		Session::put('base_choice_id_array', $base_choice_id_array);
	
		$sectionSetByPage = $this->sectionSetByPage($section_set_by_page, 1);  // BaseController
		$jquery = array('jquery_display' => $sectionSetByPage['jquery_display'], 'jquery_ui' => $sectionSetByPage['jquery_ui'], 'jquery_requiredCheck' => $sectionSetByPage['jquery_requiredCheck'], 'jquery_regexpCheck' => $sectionSetByPage['jquery_regexpCheck']);	
		Session::put('session_choice_list', $sectionSetByPage['choice_list']);
		$content = View::make('interview.question', array('type'=> 'edit', 'input_data'=> $choice_data, 'section_list'=> $sectionSetByPage['section_list']));
		return View::make('admin.interview.edit', compact('content', 'jquery'));
	}	

	public function interviewUpdate()
	{	

		$input_data =  Input::all();
		$section_id = Session::get('section_id');
		$interview_id = Session::get('interview_id');
		$choice_list  = Session::get('base_choice_id_array');
		$session_choice_list = Session::get('session_choice_list');

		//t_choice_selection からデータを削除
		foreach($choice_list['id'] as $key => $choice_id){
			$res = AdminInterview::DeleteCommonData($interview_id, $choice_id);	
			// if (Ethna::isError($res)){
			// 	$db->rollback();//ロールバック
			// 	$this->ae->add( null, $res->getMessage());
			// 	return 'admin_trouble';
			// }
		}

		$select_data = array();
		$choice_id_sql_condition = "";
		
		//postデータを整形 （可変のため af では取得できない）
		foreach($input_data as $key => $val){
			if(preg_match("/^il_/",$key)){
				if(is_array($val)){
					foreach($val as $il_key => $il_val){
						$select_data[$il_val]["data"] = 1; 
						$select_data[$il_val]["type"] = $session_choice_list[$il_val]["type"];
						$select_data[$il_val]["layer_id"] = $session_choice_list[$il_val]["layer_id"];
						$choice_id_sql_condition .= ($choice_id_sql_condition != "") ? " , " : "";
						$choice_id_sql_condition .= $il_val;
					}
				} else {
					$select_data[$val]["data"] = 1; 
					$select_data[$val]["type"] = $session_choice_list[$val]["type"];
					$select_data[$val]["layer_id"] = $session_choice_list[$val]["layer_id"];
					$choice_id_sql_condition .= ($choice_id_sql_condition != "") ? " , " : "";
					$choice_id_sql_condition .= $val;
				
				}
			}
			if(preg_match("/^i_/",$key)){
				$val_choice_id = str_replace("i_","",$key);
				$select_data[$val_choice_id]["data"] = $val;
				$select_data[$val_choice_id]["type"] =  $session_choice_list[$val_choice_id]["type"];
				$select_data[$val_choice_id]["layer_id"] =  $session_choice_list[$val_choice_id]["layer_id"];
				$choice_id_sql_condition .= ($choice_id_sql_condition != "") ? " , " : "";
				$choice_id_sql_condition .= $val_choice_id;
			}

		}
		

		$choice_id_sql_condition = explode(', ', $choice_id_sql_condition);

		// 親子データの親を取得するための一連ルーチン   ********************
		//$layer_control_list = $interview_manager->getLayerControlByChoiceByChoiceIdList($choice_id_sql_condition);
		$layer_control_list = Interview::getLayerControlByChoiceBySectionIdList($choice_id_sql_condition); 
		// if(Ethna::isError($layer_control_list)){
		// 	$db->rollback();//ロールバック
		// 	$this->ae->add( null, $layer_control_list->getMessage());
		// 	return 'admin_trouble';
		// }

		//layer_id, group_noをキーに並び替え
		$upper_id_list = array();
		foreach ($layer_control_list as $value) {
			$upper_id_list[$value['layer_id']]['all_or_every'] = $value['all_or_every'];
			$upper_id_list[$value['layer_id']]['group'][$value['group_no']][] = $value;
			$upper_id_list[$value['layer_id']]['show_type'] =  $value['show_type'];
		}


		// 親子データの親を取得するための一連ルーチン   ********************
		//登録のためのInsertデータを作成 ***********************************
		foreach($select_data as $choice_id => $val){

			$insert_values = null;
			$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $select_data, $val, $upper_id_list);
			if (!empty($insert_values)) {
				$rows[] = $insert_values;
			}
		}

		//choiceデータ登録
		if (!empty($rows)) {
			//$result = $interview_manager->insertChoiceSelection($interview_id, $rows);
			$result = Interview::insertChoiceSelection($interview_id, $rows);
			// if (Ethna::isError($result)) {
			// 		$this->ae->add( null, $result->getMessage());
			// 	$db->rollback();//ロールバック
			// 	return "admin_trouble";
			// }
		}

		
		//t_guidance 登録
		//$interview_data = $interview_manager->getInterviewDataById($interview_id);
		$interview_data = AdminInterview::getInterviewDataById($interview_id);
		// if (Ethna::isError($interview_data)) {
		// 	$db->rollback();
		// 	$this->ae->add(null, $interview_data->getMessage());
		// 	return 'admin_trouble';
		// }
		if($interview_data['flg_followup'] != 0){
			
			//フォローアップデータが作成されていたら変更不可
			$param = array(
				'first_interview_id' => $interview_id
			);
			//$check_data = $interview_manager->getCommonData('t_followup' , $param);
			$check_data = AdminInterview::getFollowup($param);
			//print_r($check_data);die;
			if(empty($check_data)){
			
				//ガイダンス登録用の判定取得
				//$result = $interview_manager->judgeByInterviewId($interview_data);
				$result = $this->judgeByInterviewId($interview_data, $interview_data['section_set_id']);
				//print_r($result);die;
				// if (Ethna::isError($result)) {
				// 	$this->ae->add( null, $result->getMessage());
				// 	$db->rollback();
				// 	return 'admin_trouble';
				// }
				$guidance_data = $result['guidance'];
				$guidance_data['interview_id'] = $interview_id;
				$param = array(
					'interview_id' => $interview_id
				);
				
				//一度ガイダンスデータを削除
				//$res = $admininterview_manager->DeleteCommonData('t_guidance', $param);
				AdminInterview::DeleteGuidance($param);
				// if (Ethna::isError($res)){
				// 	$db->rollback();//ロールバック
				// 	$this->ae->add( null, $res->getMessage());
				// 	return 'admin_trouble';
				// }
				
				//$res = $interview_manager->insertCommonData('t_guidance' , $guidance_data);
				AdminInterview::insertGuidance($guidance_data);
				// if (Ethna::isError($res)){
				// 	$db->rollback();//ロールバック
				// 	$this->ae->add( null, $res->getMessage());
				// 	return 'admin_trouble';
				// }
			}
		}
		//●コミット
		// $db->commit();
		
		return View::make('admin.interview.update');
	}

	public function interviewEmail()
	{	

	}

	public function createChoiceSelectionInsertValue($choice_id, $choice_list , $choice_data, $upper_id_list)
	{
		//$choice_data = $choice_list[$choice_id]; //現在の選択肢の情報
		$choice_type = $choice_data['type']; //現在の選択肢のタイプ
		$upper = $upper_id_list[$choice_data['layer_id']];

		if (!empty($upper)) {

			$isParentSelected = $this->isSelectedUpper($choice_list, $upper);
			if ($upper['show_type'] ==  0) {
				if ($isParentSelected) {
					return null;
				}
			} else {
				if (!$isParentSelected) {
					return null;
				}
			}
			if ($choice_type !== "radio" && $choice_type !== "checkbox") {
				if ($choice_data["data"] === "" || is_null($choice_data["data"])) {
					return null;
				}
			}
		}

		$value = $choice_data["data"];

		$insert_values = array('choice_id' => $choice_id);
		switch ($choice_type) {
			case 'radio':
			case 'checkbox':
				$insert_values['selection'] = true;
				break;
			case 'string':
				$insert_values['input_string'] = $value;
				break;
			case 'numeric':
				$insert_values['input_float'] = $value;
				break;
			case 'date':
				$insert_values['input_datetime'] = $value . ' 00:00:00';
				break;
			case 'time':
				$insert_values['input_datetime'] = '1999-01-01 ' . $value;
				break;
			case 'datetime':
				$insert_values['input_datetime'] = $value;
				break;
			case 'year':
				$insert_values['input_datetime'] = $value . '-01-01 00:00:00';
				break;
			case 'month':
				$insert_values['input_datetime'] = '1999-' . $value . '-01 00:00:00';
				break;
			case 'day':
				$insert_values['input_datetime'] = '1999-01-' . $value . ' 00:00:00';
				break;
			case 'slider':
			case 'slider_hour':
			case 'slider_show_by_step':
			case 'slider_minute':
				$insert_values['input_float'] = $value;
				break;
			case 'tab':
			case 'accordion': //見出し
				return null;
		}

		return $insert_values;
	}
	
	/**
	 * 判断元の選択肢が選択されているかチェック
	 * Enter description here ...
	 * @param $layer_id
	 * @param $selected_choice_id_list
	 * @param $upper_id_list
	 */
	public function isSelectedUpper($selected_choice_id_list, $upper) {
		$all_or_every = $upper['all_or_every'];
		$group = $upper['group'];

		$isSatisfy = false;
		switch ($all_or_every) {
			case "all":
				$isSatisfy = true;
				foreach ($group as $group_no => $upper_list) {
					foreach ($upper_list as $upper_data) {
						if (!array_key_exists($upper_data['choice_id_upper'], $selected_choice_id_list)) {
							$isSatisfy = false;
							break;
						}
					}
				}
				break;
			case "every":
				$isSatisfy = false;
				foreach ($group as $group_no => $upper_list) {
					foreach ($upper_list as $upper_data) {
						if (array_key_exists($upper_data['choice_id_upper'], $selected_choice_id_list)) {
							$isSatisfy = true;
							break;
						}
					}
				}
				break;
			default:
				break;
		}

		return $isSatisfy;
	}	

	public function postSendMail($interview_id = null)
	{
		
		$input = Input::all();

		$special = $this->special();
		if ($special) {
			$tenant_id = \Session::get('temp_tenant_id');
		}else {
			$tenant_id = \Auth::user()->get()->tenant_id;
		} 

		$select_interview = AdminInterview::select_interview($interview_id, $tenant_id);

		$param['week'] = 2;
		$param['first_interview_id'] = $interview_id;
		$param['name'] = $select_interview['first_name'].$select_interview['last_name'];
		$param['email'] = (empty($input['sendfollowup_email'])) ? $select_interview['email'] : $input['sendfollowup_email'];
		$param['tenant_id'] = $tenant_id;

		$this->sendMail($param);
		$content  = View::make('admin.sendmail');
		return View::make('admin.content', compact('content'));
	}

	public function special() {

		$special = ((\Auth::user()->get()->role_id ==0)&&(\Session::has('temp_tenant_id')));
		return $special;
	}	
}
