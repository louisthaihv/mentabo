
<dt><span>食事に関する詳細</span></dt>
<dd class="result-food"> あなたの摂取総エネルギーは {{ $result['food']['kcal']['total'] }}<!--{$app.result.food.kcal.total|round:0}-->kcalです。

	<table>
		<tr>
			<th>タンパク質</th>
			<th>脂質</th>
			<th>炭水化物</th>
			<th>塩分</th>
			<th>食物繊維量</th>
		</tr>
		<tr>
			<td>{{ $result['food']['nutrient']['Col_003'] }}kcal</td>
			<td>{{ $result['food']['nutrient']['Col_004'] }}kcal</td>
			<td>{{ $result['food']['nutrient']['Col_005'] }}kcal</td>
			<td>{{ $result['food']['nutrient']['Col_059'] }}g</td>
			<td>{{ $result['food']['nutrient']['Col_058'] }}g</td>
		</tr>
		<tr>
			<th>鉄</th>
			<th>カルシウム</th>
			<th>ビタミンB1</th>
			<th>ビタミンB2</th>
			<th>ビタミンC</th>
		</tr>
		<tr>
			<td>{{ $result['food']['nutrient']['Col_023'] }}mg</td>
			<td>{{ $result['food']['nutrient']['Col_020'] }}mg</td>
			<td>{{ $result['food']['nutrient']['Col_043'] }}mg</td>
			<td>{{ $result['food']['nutrient']['Col_044'] }}mg</td>
			<td>{{ $result['food']['nutrient']['Col_051'] }}mg</td>
		</tr>
		<tr>
			<th>脂肪酸</th>
			<th class="empty" colspan="4" rowspan="2"></th>
		</tr>
		<tr>
			<td>{{ $result['food']['nutrient']['Col_070'] }}g</td>
		</tr>
	</table>

	<table>
		<tr>
			<th>主食</th>
			<th>主菜</th>
			<th>副菜・汁物</th>
			<th>果物･乳類･飲料</th>
			<th>おやつ</th>
		</tr>
		<tr>
			<td>{{ $result['food']['kcal']['group'][1] }}kcal</td>
			<td>{{ $result['food']['kcal']['group'][2] }}kcal</td>
			<td>{{ $result['food']['kcal']['group'][3] }}kcal</td>
			<td>{{ $result['food']['kcal']['group'][4] }}kcal</td>
			<td>{{ $result['food']['kcal']['group'][5] }}kcal</td>
		</tr>
	</table>
</dd>

<link class="include" rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/jquery.jqplot.min.css" />
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="{{ URL::to('/') }}/js/excanvas.min.js"></script><![endif]-->
<script class="include" type="text/javascript" src="{{ URL::to('/') }}/js/jquery.jqplot.min.js"></script>
<script class="include" language="javascript" type="text/javascript" src="{{ URL::to('/') }}/js/plugins/jqplot.pieRenderer.min.js"></script>
<div id="chart1" style="height:300px; width:500px;"><p>エネルギー％</p></div>

<script class="code" type="text/javascript">
$(document).ready(function(){
    var data = [
                ['たんぱく質', {{ $result['food']['psc']['p'] }} ],['脂質', {{ $result['food']['psc']['f'] }}], ['炭水化物', {{ $result['food']['psc']['c'] }}]
                ];
    var plot1 = jQuery.jqplot ('chart1', [data], {
        seriesColors: [ "#ffebcc", "#ff9900", "#ffc266"],
        seriesDefaults: {
            renderer: jQuery.jqplot.PieRenderer,
            rendererOptions: {
                showDataLabels: true,
                dataLabels: 'value',
                fill: true,
                sliceMargin: 0,
                lineWidth: 0,
                shadow: false,
                startAngle: -90,
                sliceMargin: 5,
                lineWidth: 0
            }
        },
        legend: { show:true, location: 'e' },
        grid: {
            borderWidth: 0,
            shadow: false,
            background: '#fdf9e9'
        }
    });
});
</script>


