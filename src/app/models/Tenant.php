<?php
	
class Tenant extends Eloquent {
	public $timestamps = false;
	public $table = 'm_tenant';
    public $primaryKey = 'tenant_id';
	
	
    public static $rulesreg= array(
  		'token' => 'alpha_num|required|halfwidth|min:4|max:12|unique:m_tenant,token',	
		'tenant_name' =>'required|max:50'
    );

}	

?>