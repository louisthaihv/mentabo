<?php

namespace Api;
use DB;

class ApiController extends \BaseController {

	public function postIndex()
	{
		$this->getIndex();
	}	

	public function getIndex()
	{
        
        if(isset($_REQUEST)) header('Content-Type: application/xml; charset=utf-8');
        
        if($this->checkServer()){
			
			
			if ( isset($_REQUEST['type']) && ($_REQUEST['type']=='DBErrorCheck')){
                print_r($this->_errorMsg('1005', '1005 msg'));
                exit;
			}
			
			if ( isset($_REQUEST['type']) && ($_REQUEST['type']=='IrregularError')){
                print_r($this->_errorMsg('1100', '1100 msg'));
                exit;
			}
			
			if ( isset($_REQUEST['type']) && (($_REQUEST['type']!='IrregularError')&&($_REQUEST['type']!='DBErrorCheck')&&($_REQUEST['type']!='interview'))){
				print_r($this->_OK());
				exit;
			}
			
			if ( isset($_REQUEST['type']) && ($_REQUEST['type']=='interview') && !isset($_REQUEST['xmldata']) ){
                print_r($this->_errorMsg('1003', '入力情報が充分ではありません'));
                exit;
			}
			
			if (!isset($_REQUEST['type']) || (empty($_REQUEST['type'])) || !isset($_REQUEST['xmldata']) ){
				print_r($this->_OK());
				exit;
			}
			
			if ( isset($_REQUEST['type']) && ($_REQUEST['type']=='interview')  && isset($_REQUEST['xmldata']) ){
				// echo $_REQUEST['xmldata'];exit;
				libxml_use_internal_errors(true);
				$result = simplexml_load_string($_REQUEST['xmldata'], 'SimpleXMLElement', LIBXML_NOCDATA );
				
				
				if (!$result) {
                	print_r($this->_errorMsg('1001', '1001 msg'));
                	exit;	
				}
				
				if ((!$result->birth_date) || (!$result->questions) ) {
                	print_r($this->_errorMsg('1003', '入力情報が充分ではありません'));
                	exit;
				}
				
				if ( ($result->birth_date)&& (!$this->validateDate($result->birth_date)) ){
                	print_r($this->_errorMsg('1004', '1004 msg'));
                	exit;	
				}
				
			}
			
			
			
	        if(isset($_REQUEST['type']) &&  ($_REQUEST['type'] == 'interview')) {

	            if(!isset($_REQUEST['input_time'])) {
	                print_r($this->_errorMsg('1003', '入力情報が充分ではありません'));
	                exit;
	            }
	            
	            if(isset($_REQUEST['input_time']) && ($_REQUEST['input_time'] == '')) {
	                print_r($this->_errorMsg('1004', '入力情報が正しくありません'));
	                exit;
	            }
				
				
					
				
				
	            preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/',trim($_REQUEST['input_time']), $matches);
	            if(isset($matches[0])){
		            if(strcmp((string) $matches[0], $_REQUEST['input_time'])){
		                print_r($this->_errorMsg('1004', '入力情報が正しくありません'));
		                exit;
		            }
	        	}else{
	        		print_r($this->_errorMsg('1004', '入力情報が正しくありません'));
	        		 exit;
	        	}

	            if(isset($_REQUEST['xmldata'])){
					$_REQUEST['xmldata'] = str_replace('\"', '"', $_REQUEST['xmldata']);
					print_r($this->judgeInterview($_REQUEST['xmldata']));
	            } else {
	            	 print_r($this->_errorMsg('1003', '入力情報が正しくありません'));
	            	 exit;
	            }	

	        } else {
	        	print_r($this->_errorMsg('1004', '入力情報が正しくありません'));
	        	exit;
	        }
    	}
	}

	/**
     * エラー時にxmlを返却する
     *
     * @param  int     $code       エラーコード
     * @param  string  $msg        エラーメッセージ
     * @param  string  $filename   エラーファイル名
     * @return string  $xml_output 出力XML
     */
    public function _errorMsg($code, $msg){      

		$xml_output  = '';
		$xml_output .='<?xml version="1.0" encoding="UTF-8"?><health_data><error><code>'.$code.'</code><message>'.$msg.'</message></error></health_data>';
		return $xml_output;
    }
	
	public function _OK(){
		$xml_output = '<?xml version="1.0" encoding="UTF-8" ?> <health_data> <status>OK</status></health_data>';
		return $xml_output;
	}
	
	function validateDate($date)
	{
	    $d = \DateTime::createFromFormat('Y-m-d', $date);
	    return $d && $d->format('Y-m-d') == $date;
	}

	/**
     * Check DB connection, use for health _api
     */
    function checkServer()
    {
        // Connect DB error
        if(!DB::connection()->getDatabaseName()) {
            return $this->_errorMsg('1005', 'データベースに接続できませんでした', 'health_api.php');
        }else{

			$xml_output = '<?xml version="1.0" encoding="UTF-8" ?>'."\n". "<health_data>\n<status>";
			$xml_output .= "OK</status>\n</health_data>";
			return $xml_output;	
        }
 
    }

   function judgeInterview($xmldata_request)
    {
        $interview_data = array();    // store interview data 
        $record_by_choice = array();  // store selected question and answer 
        $input_questions = array();   // store all input questions

        // XML input empty
        if(empty($xmldata_request)) {
            return $this->_errorMsg('1003', '入力情報が充分ではありません');
        }

        $xml = @simplexml_load_string($xmldata_request);
        if($xml == false) {
            return $this->_errorMsg('1001', 'XML構文エラーです');
        }

        $interview_data["card_number"] = $xml->card_number.'';
        $interview_data["gender"] = $xml->gender.'';
        $interview_data["birth_date"] = $xml->birth_date.'';
        $interview_data["section_set_id"] = 34;        // Simplest Judge section_set_id.

        // Validate input xml data
        // check valid birth_date
        preg_match('/\d{4}-\d{2}-\d{2}/',trim($xml->birth_date), $matches);
        if(strcmp((string)$matches[0],$xml->birth_date)){
            return $this->_errorMsg('1004', '入力情報が正しくありません');
        }
        $test_date  = explode('-', $xml->birth_date.'');
        // check date month day year
        if(count($test_date) !== 3){
            return $this->_errorMsg('1003', '入力情報が充分ではありません');
        }
        if (!checkdate($test_date[1], $test_date[2], $test_date[0])) {
            return $this->_errorMsg('1004', '入力情報が正しくありません');
        }

        // birth_date in future

        // $now = date('Y-m-d');
        // if(strtotime($xml->birth_date.'') >= strtotime($now)) {
        //     return $this->_errorMsg('1004', '入力情報が正しくありません');
        // }

        if(!($this->_valid_date($xml->birth_date.''))){
            return $this->_errorMsg('1004', '入力情報が正しくありません');
        }

        // check valid gender
        if(!isset($xml->gender)) {
            return $this->_errorMsg('1003', '入力情報が充分ではありません');
        }
        //if((strtolower($xml->gender.'') != 'male') && (strtolower($xml->gender.'') != 'female')) {
        if((strtolower($xml->gender.'') != '1') && (strtolower($xml->gender.'') != '2')) {    
            return $this->_errorMsg('1004', '入力情報が正しくありません');
        }

        // card_number length
        if(mb_strlen($xml->card_number.'') > 45) {
            return $this->_errorMsg('1004', '入力情報が正しくありません');
        }

        $mental_ouput_flag = $xml->mental_check_output_flag.'';
        if(trim($mental_ouput_flag) === "") {
            return $this->_errorMsg('1003', '入力情報が充分ではありません');
        }

        $questions = get_object_vars($xml->questions);   
        $questions = $questions['question'];

        // Empty questions tag
        if(!isset($questions)) {
            return $this->_errorMsg('1003', '入力情報が充分ではありません');
        }
        $number_questions = count($questions);      // number of questions


        // section_id refer to question_id
        // choice_id refer to answer_id
        for ($i = 0; $i < $number_questions; $i++) {
            $key = $xml->questions->question[$i]['answer_id'].'';

            $record_by_choice[$key] = array();
            $question_id =$xml->questions->question[$i]['question_id'].'';
            $choice_id = $xml->questions->question[$i]['answer_id'].'';
            $record_by_choice[$key]['section_id'] = $question_id;

            //qidがない
            if(!$question_id){
                return $this->_errorMsg('1004', '入力情報が正しくありません');
            }
            //qidが数値ではない
            if(!($question_id > 0)){
                return $this->_errorMsg('1004', '入力情報が正しくありません');
            }

            //超簡易版以外のqidはスルーする
            $light_type_questions = $this->_get_light_type_questions();
            if(!in_array($question_id,$light_type_questions)){
                continue;
            }

            $input_questions[] = $question_id;

            if(!$choice_id){//check empty answer_id
                return $this->_errorMsg('1004', '入力情報が正しくありません');
            }
            if(!($choice_id > 0)){//check int answer_id
                return $this->_errorMsg('1004', '入力情報が正しくありません');
            }
            //answer_idがquestion_idに紐づいたものかどうか
            $choice_ids = $this->_get_answer_id_by_question_id($question_id);
            if(!in_array($choice_id,$choice_ids)){
                return $this->_errorMsg('1004', '入力情報が正しくありません');
            }

            if($question_id == 2006) { // check height
                if(!$this->_valid_height($xml->questions->question[$i].''))
                    return $this->_errorMsg('1004', '入力情報が正しくありません');
            }
            if($question_id == 2007)  { // check weight
                if(!$this->_valid_weight($xml->questions->question[$i].''))
                    return $this->_errorMsg('1004', '入力情報が正しくありません');
            }

            // check answer is yes or no only with y/n questions
            $YN_questions = $this->_get_yes_no_question();
            if(in_array($question_id, $YN_questions)) {
                if (($xml->questions->question[$i].'' != '1') && ($xml->questions->question[$i].'' != '0'))
                    return $this->_errorMsg('1004', '入力情報が正しくありません');
            }
            // check answer is select questions
            $select_questions = $this->_get_select_question();
            if(in_array($question_id, $select_questions)) {
                if ($xml->questions->question[$i].'' != '1') {
                    return $this->_errorMsg('1004', '入力情報が正しくありません');
                }
            }

            $record_by_choice[$key]['choice_id'] = $choice_id;
            $record_by_choice[$key]['selection'] = 1;
            $record_by_choice[$key]['input_float'] = $xml->questions->question[$i].'';
        }

        // check lack question
        if($this->_lack_question($input_questions)) {
            return $this->_errorMsg('1003', '入力情報が充分ではありません');
        }

        //$interview_manager = $this->backend->getManager('Interview');

        //$interview_id = $this->session->get('current_interview_id');

        // compute judge result
        // $results = $interview_manager->judgeByRecord($interview_data, $record_by_choice);
        //print_r($record_by_choice);die;

        $results = $this->judgeByRecord($interview_data, $record_by_choice, 34);

        $data_patient = array();
        $data_patient['output_time'] = date("Y-m-d H:i:s");
        if($results['judge_metabo'][14] != 0) { $data_patient['metabolic_risk_score'] = $results['judge_metabo'][14]; };
        $data_patient['metabolic_risk'] = $results['judge_metabo'][13];
        $data_patient['mental_risk'] = implode(' ', $results['mental_result']);
        $xml_output = '<?xml version="1.0" encoding="UTF-8" ?>'."\n". "<data>";
        $xml_output .= "<output_time>". $data_patient['output_time']. "</output_time>". "\n";
        $xml_output .= "<metabolic_risk_score>". $data_patient['metabolic_risk_score']. "</metabolic_risk_score>" . "\n";
        $xml_output .= "<metabolic_risk>". $data_patient['metabolic_risk']. "</metabolic_risk>" . "\n";
        if($mental_ouput_flag == 1) {
            $xml_output .= "<mental_risk>". $data_patient['mental_risk']. "</mental_risk>". "\n";
        } 
        $request_log = print_r($xmldata_request, true);
        $xml_output .= "</data>";

        // file_put_contents($log_file, "#####################################\n". 
        //                              date('H:i:s') ."\nInput::\n". $request_log ."\n\n#####################################\n".
        //                              $xml_output . "\n", FILE_APPEND);

        return $xml_output;
    }

    /**
     * はい・いいえの選択肢の問診を抽出
     *
     * @return array question_idの配列
     */
    function _get_yes_no_question() {
        //$sql = "select  section_id from m_choice where choice_string = 'はい' or choice_string = 'いいえ' group by section_id;";
        $all_YN_questions = DB::table('m_choice')->where('choice_string',  'はい')->OrWhere('choice_string',  'いいえ')->groupBy('section_id')->get();
        $YN_questions = array();
        foreach($all_YN_questions as $quest) {
            $YN_questions[] = $quest['section_id'];
        }
        return $YN_questions;
    }

    /**
     * 問診IDに対応する回答IDを抽出
     *
     * @param  string $question_id 問診ID
     * @return array  $choice_ids  対応する回答IDの配列
     */
    function _get_answer_id_by_question_id($question_id) {
        //$sql = "select choice_id from m_choice where section_id = '{$question_id}';";
        $all_choice_ids = DB::table('m_choice')->where('section_id', $question_id)->get();
        $choice_ids = array();
        foreach($all_choice_ids as $quest) {
            $choice_ids[] = $quest['choice_id'];
        }
        return $choice_ids;
    }

    /**
     * 必須問診データの存在確認
     *
     * @param $question_arr 入力されたデータの問診ID
     * @return bool
     */
    function _lack_question($question_arr) {
        $require_quests = $this->_get_require_questions();
        if(count($question_arr) < count($require_quests)) {
            return true;
        }
        $tmp_array = array_intersect($require_quests, $question_arr);
        if(count($tmp_array) < count($require_quests)) {
            return true;
        }
        return false;
    }

    /**
     * 選択肢の回答の値が1になっているかどうか
     *
     * @param $question_id 問診ID
     * @param $value       回答値
     * @return bool
     */
    function _is_one_select_question($question_id,$value) {
        $require_quests = $this->_get_select_question();
        foreach($require_quests as $question){
            if(in_array($question,array(2006,2007,2113,2115,2117))){
                return true;
            }elseif($question == $question_id){
                if($value == 1){
                    return true;
                }else{
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 体重データバリデーション
     *
     * @param float $weight 体重
     * @return bool
     */
    function _valid_weight($weight) {
        $remainder = explode('.', (string)$weight);
        if(isset($remainder[1]) && ($remainder[1] != 5) && ($remainder[1] != '')) {
            return false;
        }
        if(($weight > 150) || ($weight < 30)) {
            return false;
        }
        return true;
    }

    /**
     * 身長データバリデーション
     *
     * @param float $height 身長
     * @return bool
     */
    function _valid_height($height) {
        $remainder = explode(".", (string)$height);
        if(isset($remainder[1]) && ($remainder[1] != 5) && ($remainder[1] != '')) {
            return false;
        }
        if($height > 200 || ($height < 100)) {
            return false;
        }
        return true;
    }

    /**
     * 日付データバリデーション
     *
     * @param $date 日付
     * @return bool
     */
    function _valid_date($date) {
        if($date < '1900-01-01' || $date > date('Y-m-d')) {
            return false;
        }
        return true;
    }

    /**
     * 必須問診取得
     *
     * @return array
     */
    function _get_require_questions() {
        $require_quests = array(2006, 2007, 2101, 2102, 2103, 2104, 2105, 2107, 2109, 2110, 2111, 2112, 2114, 2116, 2118, 2119, 2120, 2125, 2126, 2127, 2128);
        return $require_quests;
    }

    /**
     * 超簡易版で回答されうる問診ID取得
     *
     * @return array
     */
    function _get_light_type_questions() {
        return array(2006,2007,2101,2102,2103,2104,2105,2106,2107,2108,2109,2110,2111,2112,2113,2114,2115,2116,2117,2118,2119,2120,2125,2126,2127,2128);
    }

    /**
     * 超簡易版で回答されうる選択肢の問診ID取得
     *
     * @return array
     */
    function _get_select_question() {
        return array(2101,2102,2103,2104,2105,2106,2107,2108,2109,2110,2111,2112,2114,2116,2118,2119,2120,2125,2126,2127,2128);
    }	

}
