﻿@include('../../header')

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2>生活習慣診断TOP</h2>
	<div id="contentInner">

			
			<div id="loginBox">
				<div id="loginBox-inner">
					
					<?php
					if (isset($error_message)){
						echo $error_message." <br/><br/>";
					}
					?>
					<form id="form1" action="{{ url('user/login'."/".$token) }}" method="POST">
						
						<div>
							<h3 class="top">ログイン ID</h3>
							<input type="text" name="login_id" value = "{{ $login_id }}" size=60 > 
						</div>
						     &nbsp;
						<div>
							<h3 class="top">パスワード</h3>
							<input type="password" name="password" value = "{{ $password }}" size=60 onkeypress="submitOnEnter(this, event)" > 
						</div>
						
					
						<br/> <br/>
						<br/> <br/>
						<br/>
						<div class="choices-half">
		
							<center>
							
							<input type="radio" id="t_102" name = "submit_button" value="" onclick="submitFunction()" >
							<label for="t_102">ログイン</label>
							</center>
						</div>
					</form>

				</div><!-- /#loginBox-inner -->
			
			</div><!-- /#loginBox -->
			<script type="text/javascript" >
			
				function submitFunction()
				{
				  document.getElementById("form1").submit();
				}
			
		        function submitOnEnter(inputElement, event) {  
		           if (event.keyCode == 13) {  
		               submitFunction();  
		           }  
		        }  
			</script>

	</div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->



	<hr />
@include('../../footer')