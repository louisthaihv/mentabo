<?php

class FollowupJudgeData  {

	public static function globals()
	{
		
		// 減算する時間
		$GLOBALS['MAX_TIME_TO_SUBTRACT'] = '30';				// 最大値
		$GLOBALS['MIN_TIME_TO_SUBTRACT'] = '10';				// 最小値

		// 減算する回数
		$GLOBALS['MAX_NUMBER_OF_TIMES_TO_SUBTRACT'] = '0';		// 最大値（未使用）
		$GLOBALS['MIN_NUMBER_OF_TIMES_TO_SUBTRACT'] = '1';		// 最大値

		// 減算するMETS値
		$GLOBALS['MAX_METS_TO_SUBTRACT'] = '0';					// 最大値（未使用）
		$GLOBALS['MIN_METS_TO_SUBTRACT'] = '2.5';				// 最大値

		// 提示可能な最低METS値
		$GLOBALS['MIN_VALUE_OF_METS'] = '2.5';


		// 削減可能カロリーの上限
		$GLOBALS['UPPER_LIMIT_CALORIE_REDUCTION'] = '15';





		// 判定結果
		$GLOBALS['FOLLOWUP_JUDGE'] = array(
			0	=>	array( 'judgment_type'=>'0',	'judgment_detail'=>'このまま運動を継続してください。' ),
			1	=>	array( 'judgment_type'=>'1',	'judgment_detail'=>'運動の時間をCHOISE_TIMEからPROPOSE_TIMEに減らして取り組んで見てください。' ),
			2	=>	array( 'judgment_type'=>'2',	'judgment_detail'=>'週にPROPOSE_COUNT回、運動に取り組むようにしましょう。' ),
			3	=>	array( 'judgment_type'=>'3',	'judgment_detail'=>'現在の生活改善を目指す場合、PROPOSE_SPORTSのような運動に取り組んでみてください。' ),
			4	=>	array( 'judgment_type'=>'4',	'judgment_detail'=>'医療機関を受診して、このまま運動を続けていいか確認しましょう。' ),
			5	=>	array( 'judgment_type'=>'5',	'judgment_detail'=>'あわせて、食事のカロリーをPROPOSE_CALORIEkcalにするようにチャレンジしてみてください。' ),
		);

		$GLOBALS['FOLLOWUP_JUDGE_CALOROE'] = array(
			0	=>	array( 'judgment_type'=>'0',	'judgment_detail'=>'あわせて、食事のカロリーをPROPOSE_CALORIEkcalにするようにチャレンジしてみてください。' ),
		);



		// 今後の取り組み目標（8週目以降）
		$GLOBALS['FOLLOWUP_TARGET'] = array(
			0	=>	array( 'target_type'=>'0',		'target_detail'=>'このままの運動を推奨します。' ),
			1	=>	array( 'target_type'=>'1',		'target_detail'=>'現在の運動を継続してください。' ),
			2	=>	array( 'target_type'=>'2',		'target_detail'=>'がんばって運動と食事制限を行いましょう。' ),
		);

	}

}	