<?php

namespace Admin;
use AdminDownload;
use CsvHeaderFood;
use CsvHeaderInterview;
use View;
use Input;

class DownloadController extends \BaseController {

	public function getIndex($id = null)
	{
		if (!$this->loggedin()) {
			
			return false;
		}

		$csvHeaderFood = CsvHeaderFood::globals();
		$interviewData = AdminDownload::interviewData($id);
		$interviewFood = AdminDownload::interviewFood();

		$filename = $interviewData['first_name'].$interviewData['last_name'].'_'.date('Ymd').'.csv';
		//$gender = ($interviewData['gender'] == 'male') ? '男性': '女性';
		$gender = ($interviewData['gender'] == '1') ? '男性': '女性';
		//$type = $this->interviewType($interviewData['section_set_id']); //BaseController
		$type = $this->sectionSet($interviewData['section_set_id']);
		//print_r($type);die;
		//sectionSet($section_set_id)
	
		$tmp_header = array (
			'ファイル出力者の利用者ID', 'ファイル出力日時', '問診ID', '問診日時', '氏名', '生年月日', '性別', '問診種別', '朝食kcal合計',
			'昼食kcal合計', '夕食kcal合計', '間食kcal合計', '総計kcal合計', '主食kcal合計', '主菜kcal合計', '副菜・汁物kcal合計', '果物・乳類・飲料kcal合計', 'おやつkcal合計'
		);

		foreach ($interviewFood as $FoodId => $food_data) {
			if ($food_data['flg_csv']) {
				$array_push = array_push($tmp_header, $food_data['FoodName'].'kcal合計');
				$foodid_kcal[$food_data['FoodID']] = 0;
			}
		}

		// 共通
		$header = array();
		foreach ($tmp_header as $value) {
			$header[] = $value;
		}

		// 栄養素
		foreach ($GLOBALS['CSV_HEADER_FOOD'] as $value) {
			$header[] = $value['header_name'];
		}
		
		// 食事（カテゴリー）のkcal header
		$body = array();
		foreach ($GLOBALS['FOOD_CATEGORY'] as $value) {
			$category_kcal[$value['category']] = 0;
			$sub_category_kcal[$value['sub_category']] = 0;
			//$foodid_kcal[$value[food_id]] = 0;
		}

		// 食事の総kcal header
		$total_kcal = 0;
		// 食事（サブカテゴリー）のkcal header
		foreach ($GLOBALS['CSV_HEADER_FOOD'] as $csv_header_food) {
			$filed_name = $csv_header_food['filed_name'];
			$nutrient[$filed_name] = 0;
		}

		// 問診の回答データ（食事のみ）所得（t_choice_selection）
		$str_choice_id = '';
		foreach ($GLOBALS['FOOD_CATEGORY'] as $key3 => $row3) {
			$str_choice_id[] = $key3;
		}

		// データの計算
		// 問診の回答のループ
		$choice_selection_list = AdminDownload::choiceSelection($id, $str_choice_id);
		foreach ($choice_selection_list as $choice_id => $choice_selection_data) {
			// FoodIDの取得
			$food_id = $GLOBALS['FOOD_CATEGORY'][$choice_id]['food_id'];
			//gを単位に変換（各栄養素の計算は、単位で行う）
			$unit = $choice_selection_data['input_float'] / $interviewFood[$food_id]['UnitWeight'];
			//カロリー算出
			$kcal = $interviewFood[$food_id]['Col_001'] * $unit;
			//カロリー合計
			$category_kcal[$GLOBALS['FOOD_CATEGORY'][$choice_id]['category']] += $kcal; //カテゴリーの合計（朝食、昼食等）
			$sub_category_kcal[$GLOBALS['FOOD_CATEGORY'][$choice_id]['sub_category']] += $kcal; //カテゴリーの合計（主食、主菜等）
			$foodid_kcal[$GLOBALS['FOOD_CATEGORY'][$choice_id]['food_id']] += $kcal; //食事内容の合計（めし、和風めん等）
			$total_kcal += $kcal; //総計
			//栄養素のループ
			foreach ($GLOBALS['CSV_HEADER_FOOD'] as $csv_header_food) {
				$filed_name = $csv_header_food['filed_name']; //Col_xx
				$nutrient[$filed_name] += $interviewFood[$food_id][$filed_name] * $unit; //栄養素の総計
			}
		}

		$array = array('login_name', date('Y-m-d H:i:s'), $interviewData['interview_id'], $interviewData['create_datetime'], $interviewData['name'], $interviewData['birth_date'], $gender, $type['type']);
		$category_kcal = array_merge($array, $category_kcal);
		$total_kcal = array_merge($category_kcal, array($total_kcal));
		$sub_category_kcal = array_merge($total_kcal, $sub_category_kcal);
		$foodid_kcal = array_merge($sub_category_kcal, $foodid_kcal);
		$body = array_merge($foodid_kcal, $nutrient);
		//
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename='.$filename);
		header('Pragma: no-cache');
		header('Expires: 0');
		header('Content-Transfer-Encoding: binary');

		print_r(mb_convert_encoding($this->generateRow($header), 'SJIS', 'UTF-8'));
		print_r(mb_convert_encoding($this->generateRow($body), 'SJIS', 'UTF-8'));

		//$header = chr(255).chr(254).iconv('UTF-8', 'UTF-16LE//IGNORE', $this->generateRow($header));
	}
	
	public function getAll()
	{	
		$input = Input::all();
		$csv_max_cnt_interview = 2000;
		$filename = '問診'.date('Ymd').'.csv';
		$CsvHeaderInterview = CsvHeaderInterview::globals();

		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename='.$filename);
		header('Pragma: no-cache');
		header('Expires: 0');
		header('Content-Transfer-Encoding: binary');	
		
		$interview_f = $input['interview_y_f'].'-'.$input['interview_m_f'].'-'.$input['interview_d_f'].' 00:00:00'; //期間from
		$interview_t = $input['interview_y_t'].'-'.$input['interview_m_t'].'-'.$input['interview_d_t'].' 00:00:00'; //期間to
		$interview_data_list = AdminDownload::interviewDataList($interview_f, $interview_t, $csv_max_cnt_interview); //fromの値

		//print_r($interview_data_list);

		$choiceDataList = AdminDownload::choiceDataList(31); // 問診の設問情報
		foreach ($choiceDataList as $key => $row) {
			$section_data_list[$row2['section_id']] = $row2;
			$choice_data_list[$row2['section_id']][$row2['choice_id']] = $row2;
		}  
		//echo '<meta charset="utf-8">';
		$header = array('ファイル出力者の利用者ID', 'ファイル出力日時', '問診ID', '問診日', '問診種別', 'ID（診察券番号、受付番号）', '生年月日', '性別');
		print_r(mb_convert_encoding($this->generateRow($header), 'SJIS', 'UTF-8'));
		// 問診内容
		foreach ($GLOBALS['CSV_HEADER_INTERVIEW'] as $value) {
			if ($value['choice_id'] == '') {
				// 質問内容がヘッダー
				//echo $section_data_list[$value['section_id']]['section_string'];
				print_r(mb_convert_encoding($section_data_list[$value['section_id']]['section_string'], 'SJIS', 'UTF-8'));
			} else {
				// 質問内容+選択内容がヘッダー（choiceで出力する問診）
				//echo $section_data_list[$value['section_id']]['section_string'].''.$value['choice_string'];
				$str = $section_data_list[$value['section_id']]['section_string'].''.$value['choice_string'];
				print_r(mb_convert_encoding($str, 'SJIS', 'UTF-8'));
				//echo mb_convert_encoding($str,"SJIS","UTF-8").",";
			}
		}
		// 対象の問診リストのループ
		foreach ($interview_data_list as $key => $interviewData) {
			// 問診の回答データ所得（t_choice_selection）
			$choice_list = AdminDownload::getInterviewChoice($interviewData['interview_id']);
			//$gender = ($interviewData['gender'] == 'male') ? '男性': '女性';
			$gender = ($interviewData['gender'] == '1') ? '男性': '女性';
			$type = $this->sectionSet($interviewData['section_set_id']);
			$array = array('login_name', date('Y-m-d H:i:s'), $interviewData['interview_id'], $interviewData['create_datetime'], $interviewData['name'], $interviewData['birth_date'], $gender, $type['type']);
			//echo $this->generateRow($array);
			print_r(mb_convert_encoding($this->generateRow($array), 'SJIS', 'UTF-8'));
			// 問診の回答のループ
			foreach ($GLOBALS['CSV_HEADER_INTERVIEW'] as $value) {
				$section_id = $value['section_id']; //section_id
				echo "\""; //フィールドの囲みダブルコーテーション（頭）
				$answer = ""; //初期化
				if ($value['choice_id'] == "") {
					// header行が質問の場合
					foreach ($choice_list[$section_id] as $choice_data) {
						$answer .= $this->getAnswer($choice_data)." ";
					}
				} else if(is_array($value['choice_id'])) {
					// header行が質問と選択肢の場合
					foreach ($value['choice_id'] as $val_choice_id) {
						$answer .= $this->getAnswer($choice_list[$section_id][$val_choice_id])." ";
					}
				} else {
					// header行が、選択肢(choice)の場合
					$answer = $this->getAnswer($choice_list[$section_id][$value['choice_id']]);
				}
				//echo $answer; //1カラムのデータ
				print_r(mb_convert_encoding($answer, 'SJIS', 'UTF-8'));
				echo "\","; //フィールドの囲みダブルコーテーション（後）
			}
			//改行 （次のデータへ）
			echo "\n";
		}
	}

	public function generateRow($row) {
		foreach ($row as $key => $value) {
			//$value = mb_convert_encoding($value,"SJIS","UTF-8");
			$row[$key] = '"'. str_replace('"', '\"', $value) .'"';
		}
		return implode(",", $row) . "\n";
	}

	public function getAnswer($choice_data) {
		$value = "";
		$choice_type = $choice_data['choice_type'];
		$choice_string = $choice_data['choice_string']." ";
		$unit = $choice_data['choice_string_unit'];
		switch ($choice_type) {
			case "radio":
			case "checkbox":
				if ($choice_data['selection']) {
					$value = $choice_data['choice_string'];
					$choice_string = "";
				}
				break;
			case "string":
				$value = $choice_data['input_string'];
				break;
			case "numeric":
				$value = $choice_data['input_float'];
				break;
			case "year":
				$value = date("Y", strtotime($choice_data['input_datetime']));
				break;
			case "month":
				$value = date("m", strtotime($choice_data['input_datetime']));
				break;
			case "day":
				$value = date("d", strtotime($choice_data['input_datetime']));
				break;
			case "date":
				$value = date("Y/m/d", strtotime($choice_data['input_datetime']));
				break;
			case "time":
				$value = date("H:i:s", strtotime($choice_data['input_datetime']));
				break;
			case "datetime":
				$value = $choice_data['input_datetime'];
				break;
			case 'slider':
				$value = $choice_data['input_float'];
				break;
			case 'slider_hour':
				$value = $choice_data['input_float'];
//				if (!is_null($value) && $value != "") {
//					if ($choice_data['ui_type'] === "2") {
//						$value .= "時間";
//					} else {
//						$value .= "時";
//					}
//				}
				break;
			case 'slider_minute':
				$value = $choice_data['input_float'];
//				if (!is_null($value) && $value != "") {
//					$value .= "分";
//				}
//				break;
			case 'slider_show_by_step':
				$value = $choice_data['input_float'];
//				if (!empty($value)) {
//					$value .= "g";
//				}
//				break;
			case 'accordion':
			case 'tab':
				break;
			default:
				break;
		}

		if (!empty($value)) {
			$operator = "";
			if (in_array($section_id, $in_section_id)) {
				$operator = "\",\"";
			} else {
				switch ($choice_type) {
					case "month":
					case "day":
						$operator = "";
						break;
					default:
						$operator = " ";
					break;
				}
			}
			$answer .= $this->setOperator($answer, $operator);
			//$answer .= $choice_string.$value.$unit;
			$answer .= $value;
		}

		return $answer;
	}

	public function setOperator($argStr, $operator) {
		if ($argStr == "") {
			return "";
		}
		return $operator;
	}
	
}
