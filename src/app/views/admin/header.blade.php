<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head>
<title>[管理]生活習慣診断</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/admin_common/format.css" media="all" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/admin_common/base.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/admin_common/color.css" media="scereen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/admin_style.css" media="screen" />

<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/admin_addition.css" media="screen" />
<link type="text/css" href="{{ URL::to('/') }}/css/sunny/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/admin_edit.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/common_style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/print_style.css" media="print" />

<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-1.6.3.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.alerts.js"></script>

<link   type="text/css" href="{{ URL::to('/') }}/css/jquery.alerts.css" media="screen" />
<script type="text/javascript" src="{{ URL::to('/') }}/js/admin_style.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/admin_addition.js"></script>

<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.alerts.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/style.js"></script>

</head>

<body>

@include('admin/menubar')