<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

  use UserTrait, RemindableTrait;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  //protected $table = 'users';
  protected $table = 't_account';
  protected $primaryKey = 'account_id';
  public $timestamps = false;
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = array('password', 'remember_token');
  //protected $hidden = array('password');

  public static $rules= array(
		//"username" => 'required|alpha_num|min:3',
		//"email" => "required|email|unique:t_account,email",
		'login_name' => "required|alphanum_under|min:4|unique:t_account,login_name"	,
		'password' => 'required|confirmed|min:4|max:12'
			
  );
  
	
  public function getRememberToken()
  {
    return null; // not supported
  }

  public function setRememberToken($value)
  {
    // not supported
  }

  public function getRememberTokenName()
  {
    return null; // not supported
  }

  /**
   * Overrides the method to ignore the remember token.
   */
  public function setAttribute($key, $value)
  {
    $isRememberTokenAttribute = $key == $this->getRememberTokenName();
    if (!$isRememberTokenAttribute)
    {
      parent::setAttribute($key, $value);
    }
  }

}
