<script type="text/javascript">
$(function(){
	$(document).ready(function(){ control_inputs(); });
	$("input").change(function(){ control_inputs(); });
	$("#l_5009").hide("slow");
	function cal_hour( minute ){ return Math.floor(minute/60); }
	function cal_min( minute ){ return ("0"+String(minute-60*Math.floor(minute/60))).slice(-2); }
	function control_inputs(){
		
		{{ $jqueryArray['jquery_display'] }}

	}

	{{ $jqueryArray['jquery_ui'] }}

	$("#form_main").submit(function() {
		var tmp_error_text = '';
		ok_section_ids = new Array();
		error_section_ids = new Array();

		{{ $jqueryArray['jquery_requiredCheck'] }}
		{{ $jqueryArray['jquery_regexpCheck'] }}

		if (tmp_error_text.length >= 1) {
			var error_text = "<ul>" + tmp_error_text + "</ul>";
			$("div.compulsoryInputError").html(error_text).show();
			$("div.compulsoryInputError").css("display", "block");
			while (section_id = ok_section_ids.pop()) {
				$("#s_" + section_id).css("background-color", "#FFFFFF");
			}
			while (section_id = error_section_ids.pop()) {
				$("#s_" + section_id).css("background-color", "#FFA0A2");
			}
			scrollTo(0,0);
			return false;
		}
		return true;
	});
});

</script>

<div id="interviewDetail" class="contents">
	<h2 id="pageTitle">フォローアップ詳細編集</h2>
	<hr />

	<!--{include file="admin/error.tpl"}-->

	<div id="main">

		<div id="content">
			<div class="embossLine">
				<dl class="interviewData">
					<dt>問診ID</dt>
					<dd>{{ $followup_data['first_interview_id'] }}</dd>
					<dt>経過週</dt>
					<dd>{{ $followup_data['week'] }}週</dd>
					<dt>問診タイプ</dt>
					<dd>{{ $followup_data['type'] }}W</dd>
					<dt>問診開始日時</dt>
					<dd>{{ $interview_data['start_datetime'] }}～{{ $interview_data['finished_datetime'] }}</dd>
				</dl>
			</div>
			<div class="embossLine">
				<dl class="interviewData">
					<dt>氏名</dt>
					<dd>{{ $interview_data['first_name'] }}{{ $interview_data['last_name'] }}</dd>
					<dt>生年月日</dt>
					<dd>{{ $interview_data['birth_date'] }}（問診時年齢 {{ $interview_data['years_old'] }}歳）</dd>
					<dt>性別</dt>
					<dd>{{ ($interview_data['gender'] == '1') ? '男性' : '女性' }}</dd>
				</dl>
			</div>
			<br />

			<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->

			<div id="content">
				<div id="contentInner">

					<div class="compulsoryInputError"></div>

					<form id="form_main" action="{{ URL::to('/') }}/admin/followup/update/{{ $followup_data['first_interview_id'] }}/{{ $type }}/{{ $followup_data['followup_interview_id'] }}/{{ $page }}" method="POST">

						<!--{foreach from=$app.followup_sports key=key item=item}-->
						@foreach ($followup_sports as $key => $item)
							<input type="hidden" name="{{ $key }}" id="{{ $key }}" value="{{ $item }}"/>
						@endforeach
						<!--{/foreach}-->
						
						<!--{include file="admin/followup/include_display_questions.tpl"}-->
						{{ $question }}

						<input type="hidden" name="pagecount" value="{{ $page }}">
						<input type="hidden" name="first_interview_id" value="{{ $followup_data['first_interview_id'] }}">
						<input type="hidden" name="followup_interview_id" value="{{ $followup_data['followup_interview_id'] }}">

						<div class="btn-area" style="text-align: center;">
							<input type="submit" name="action_admin_followup_do" value="編集">
						</div>
					</form>

				</div><!-- /#contentInner -->

			</div><!-- /#content -->
			<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->
			<script>
			document.getElementById("content").style.display = "block";
			</script>

		</div><!-- /#content -->

		<div class="buttonArea">
			<ul>
				<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
			</ul>
		</div>

	</div><!-- /#main -->
</div><!-- /.contents -->
