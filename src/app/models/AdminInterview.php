<?php

class AdminInterview extends Eloquent {

	public static function selectInterview($interview_id, $user_id) {
		return DB::table('t_interview')->Where('user_id', $user_id)->Where('interview_id', $interview_id)->orderBy('interview_id', 'desc')->first();
	}

	public static function select_interview($interview_id, $tenant_id) {
		return DB::table('t_user')->join('t_interview', 't_user.user_id', '=', 't_interview.user_id')->where('t_interview.interview_id', $interview_id)->where('t_user.tenant_id', $tenant_id)->first(array('*', 't_user.email'));
	}

	public static function getInterviewList($skip, $take, $tenant_id, $input=[]) {

		$query = DB::table('t_interview')
				->join('m_section_set', function($join)
		        {
		            $join->on('m_section_set.section_set_id', '=', 't_interview.section_set_id');
		        })
				->join('t_user', function($join)
		        {
		            $join->on('t_user.user_id', '=', 't_interview.user_id');
		        })
				->where('t_interview.finished_datetime', '!=', 'NULL');

                if(isset($input["interview_y_f"]) && isset($input["interview_m_f"]) && isset($input["interview_d_f"])){
                    //問診日時from
                    $query->where('t_interview.finished_datetime', '>=', $input["interview_y_f"].'-'.$input["interview_m_f"].'-'.$input["interview_d_f"].' '.$input["interview_h_f"].':'.$input["interview_i_f"]);
                }
                if(isset($input["interview_y_t"]) && isset($input["interview_m_t"]) && isset($input["interview_d_t"])){
                    //問診日時to
                    $query->where('t_interview.finished_datetime', '<=', $input["interview_y_t"].'-'.$input["interview_m_t"].'-'.$input["interview_d_t"].' '.$input["interview_h_t"].':'.$input["interview_i_t"].':59');
                }
                if(isset($input["birthday_y_f"]) && isset($input["birthday_m_f"]) && isset($input["birthday_d_f"])){
                    //生年月日from
                    $query->where('t_user.birth_date', '>=', $input["birthday_y_f"].'-'.$input["birthday_m_f"].'-'.$input["birthday_d_f"].' 00:00:00');
                }
                if(isset($input["birthday_y_t"]) && isset($input["birthday_m_t"]) && isset($input["birthday_d_t"])){
                    //生年月日to
                    $query->where('t_user.birth_date', '<=', $input["birthday_y_t"].'-'.$input["birthday_m_t"].'-'.$input["birthday_d_t"].' 23:59:59');
                }
                if(isset($input["gender"])){
                    //性別
                    $query->whereIn('t_user.gender', $input["gender"]);
                }
                if(isset($input["name"])){
                    //ニックネーム
                    $query->where('t_user.nick_name', 'like', '%'.$input["name"].'%');
                }
                if(isset($input["section_set_id"])){
                    //問診タイプ
                    $query->whereIn('t_interview.section_set_id', $input["section_set_id"]);
                }
        /*
                if(isset($input["interview_y_f"])){
                    //フォローアップ経過週from
                    $query->where('t_interview.finished_datetime', '>=', $input["interview_y_f"]);
                }
                if(isset($input["interview_y_f"])){
                    //フォローアップ経過週to
                    $query->where('t_interview.finished_datetime', '>=', $input["interview_y_f"]);
                }
                if(isset($input["interview_y_f"])){
                    //フォローアップ診断日時from
                    $query->where('t_interview.finished_datetime', '>=', $input["interview_y_f"]);
                }
                if(isset($input["interview_y_f"])){
                    //フォローアップ診断日時to
                    $query->where('t_interview.finished_datetime', '>=', $input["interview_y_f"]);
                }
        */
                return $query->skip($skip)->take($take)
                     ->orderBy('t_interview.interview_id', 'desc')
                     ->paginate($take);
                    //->get();
	}

	public static function judgeByInterviewId($interview_id)
	{
		return DB::table('t_choice_selection')
        ->join('m_choice', function($join) use ($interview_id)
        {
            $join->on('t_choice_selection.choice_id', '=', 'm_choice.choice_id')->where('t_choice_selection.interview_id', '=', $interview_id);
        })
        ->get();
	}

	public static function getFollowupNullDataById($interview_id){

		$result = DB::table('t_followup')
            ->join('t_interview', 't_interview.interview_id', '=', 't_followup.first_interview_id')
            ->where('t_followup.first_interview_id', $interview_id)
            ->where('t_followup.first_interview_id', $interview_id)
            ->whereNull('t_followup.input_date')
            ->orWhere( 't_followup.input_date', '')
            ->get();

		$list = array();
		foreach ($result as $key => $row) {
			$list[] = $row;
		}
		return $list;
	}

	public static function sectionSet() {
		return DB::table('m_section_set')->get();
	}


	public static function getInterviewSearch($skip, $take) {

		$section_set_id = $_GET['section_set_id']; 

		$finished_datetime1 = $_GET['interview_y_f'].'-'.$_GET['interview_m_f'].'-'.$_GET['interview_d_f'].' '.$_GET['interview_h_f'].':'.$_GET['interview_i_f'].':'.'00'; 
		$finished_datetime2 = $_GET['interview_y_t'].'-'.$_GET['interview_m_t'].'-'.$_GET['interview_d_t'].' '.$_GET['interview_h_t'].':'.$_GET['interview_i_t'].':'.'00'; 
		
		$birth_date1 = $_GET['birthday_y_f'].'-'.$_GET['birthday_m_f'].'-'.$_GET['birthday_d_f'];
		$birth_date2 = $_GET['birthday_y_t'].'-'.$_GET['birthday_m_t'].'-'.$_GET['birthday_d_t'];

		$gender = $_GET['gender'];
		$name = $_GET['name'];

		$week[] = $_GET['followup_w_f'];
		$week[] = $_GET['followup_w_t'];

		$return = DB::table('t_interview')
			->where( function ($query) use ($section_set_id) {
			    if (isset($section_set_id[0])) { $query->orwhere('t_interview.section_set_id', 31);}	
			    if (isset($section_set_id[1])) { $query->orwhere('t_interview.section_set_id', 32);}	
			    if (isset($section_set_id[2])) { $query->orwhere('t_interview.section_set_id', 33);}	
			    if (isset($section_set_id[3])) { $query->orwhere('t_interview.section_set_id', 34);}
			    if (isset($section_set_id[4])) { $query->orwhere('t_interview.section_set_id', 35);}
			})
			->where( function ($query) use ($finished_datetime1) {
			    if (isset($finished_datetime1)) { $query->where('t_interview.finished_datetime', '>=', $finished_datetime1);}	
			})
			->where( function ($query) use ($finished_datetime2) {
			    if (isset($finished_datetime2)) { $query->where('t_interview.finished_datetime', '<=', $finished_datetime2);}	
			})
			->where( function ($query) use ($birth_date1) {
			    if (isset($birth_date1)) { $query->where('t_interview.birth_date', '>=', $birth_date1);}	
			})
			->where( function ($query) use ($birth_date2) {
			    if (isset($birth_date2)) { $query->where('t_interview.birth_date', '<=', $birth_date2);}	
			})
			->where( function ($query) use ($gender) {
			    //if (isset($gender[0])) { $query->orwhere('t_interview.gender', '=', 'male');}
			    //if (isset($gender[1])) { $query->orwhere('t_interview.gender', '=', 'female');}	
			    if (isset($gender[0])) { $query->orwhere('t_interview.gender', '=', '1');}
			    if (isset($gender[1])) { $query->orwhere('t_interview.gender', '=', '2');}	
			})
			->where( function ($query) use ($name) {
			    if (isset($name)) { 
			    	$query->orwhere('t_interview.name', 'LIKE', $name );
			    }	
			})
			// ->where( function ($query) use ($week) {
			//     if (isset($week[0])) { $query->where('t_followup.week', '>=',$week[0] );}	
			// })
			// ->where( function ($query) use ($week) {
			//     if (isset($week[0])) { $query->where('t_followup.week', '<=', $week[1] );}	
			// })
			// ->where( function ($query) use ($week) {
			//     if (isset($week)) { $query->join('t_followup', 't_followup.first_interview_id', '=', 't_interview.interview_id');}	
			// })
			->select('t_interview.interview_id', 't_interview.section_set_id', 't_interview.name', 't_interview.birth_date', 't_interview.gender', 't_interview.interview_type', 't_interview.finished_datetime', 't_interview.flg_followup')
		    ->orderBy('t_interview.finished_datetime', 'DESC')
		    ->skip($skip)->take($take)
		    //->get();
		    ->paginate($take);
		    //->toSql();

		    if($return) {
		    	return $return;
		    }else {
		    	return false;
		    }


	}

	public static function getInterviewDataById($interview_id) {

		$data = DB::table('t_interview')
				->join('t_user', 't_user.user_id', '=', 't_interview.user_id')
		        ->join('m_section_set', 'm_section_set.section_set_id', '=', 't_interview.section_set_id')
		        ->where('interview_id', '=', $interview_id)->first();
        $finished_dt = new DateTime($data["finished_datetime"]);
        $birth_dt = new DateTime($data["birth_date"]);
        $data["years_old"] = $finished_dt->diff($birth_dt)->y;
        return $data;
	}

	public static function m_section_set() {

		return DB::table('m_section_set')->get();

	}

	public static function getSectionSetContentsById($section_set_id, $interview_data) {

		return DB::table('m_section_set_contents')
            ->join('m_section', 'm_section_set_contents.section_id', '=', 'm_section.section_id')
            ->where('m_section_set_contents.section_set_id', $section_set_id)
            ->where('m_section_set_contents.flg_display', 1)
			->where( function ($query) use ($interview_data) { $query->whereNull('target_gender')->orWhere( 'target_gender', '=', $interview_data['gender']); }) 
			->where( function ($query) use ($interview_data) { $query->whereNull('target_age_from')->orWhere( 'target_age_from', '<=', $interview_data['years_old']); })
			->where( function ($query) use ($interview_data) { $query->whereNull('target_age_to')->orWhere( 'target_age_to', '=>', $interview_data['years_old']); })
            ->get();
            //->toSql();
	}

	public static function getDocterViewData($interview_data, $section_list) {
		foreach ($section_list as $key_section => $section) {
			$select = DB::table('m_choice')
		            ->join('m_layer', 'm_choice.layer_id', '=', 'm_layer.layer_id')
		            ->join('t_choice_selection', 'm_choice.choice_id', '=', 't_choice_selection.choice_id')
		            ->select('t_choice_selection.choice_id', 
							'm_choice.layer_id', 
							'm_choice.choice_type', 
							'm_choice.choice_string',
							'm_choice.choice_string_unit',
							'm_layer.order_in_section',
							'm_choice.order_in_layer',
							'm_choice.ui_type',
							't_choice_selection.selection',
							't_choice_selection.input_int',
							't_choice_selection.input_float',
							't_choice_selection.input_int',
							't_choice_selection.input_datetime',
							't_choice_selection.input_string')

		            ->where('m_choice.section_id', '=', $section['section_id'])
		            ->where('t_choice_selection.interview_id',  $interview_data['interview_id'])
		            ->orderBy('m_layer.order_in_section', 'asc')
		            ->orderBy('m_choice.order_in_layer', 'asc')
		            ->get();
		           	// ->toSql();
           	$answer = "";
          	foreach ($select as $key => $row) {
				$value = "";
				$choice_type = $row['choice_type'];
				$choice_string = $row['choice_string']." ";
				$unit = $row['choice_string_unit'];
				switch ($choice_type) {
					case "radio":
					case "checkbox":
						if ($row['selection']) {
							$value = $row['choice_string'];
							$choice_string = "";
						}
						break;
					case "string":
						$value = $row['input_string'];
						break;
					case "numeric":
						$value = $row['input_float'];
						break;
					case "year":
						$value = date("Y", strtotime($row['input_datetime']));
						break;
					case "month":
						$value = date("m", strtotime($row['input_datetime']));
						break;
					case "day":
						$value = date("d", strtotime($row['input_datetime']));
						break;
					case "date":
						$value = date("Y/m/d", strtotime($row['input_datetime']));
						break;
					case "time":
						$value = date("H:i:s", strtotime($row['input_datetime']));
						break;
					case "datetime":
						$value = $row['input_datetime'];
						break;
					case 'slider':
						$value = $row['input_float'].$unit;
						$unit = "";
						break;
					case 'slider_hour':
						$value = $row['input_float'];
						if (!is_null($value) && $value != "") {
							if ($row['ui_type'] === "2") {
								$value .= "時間";
							} else {
								$value .= "時";
							}
						}
						break;
					case 'slider_minute':
						$value = $row['input_float'];
						if (!is_null($value) && $value != "") {
							$value .= "分";
						}
						break;
					case 'slider_show_by_step':
						$value = $row['input_float'];
						if (!empty($value)) {
							$value .= "g";
							$unit = "";
						}
						break;
					case 'accordion':
					case 'tab':
						break;
					default:
						break;
            	} 
				if (!empty($value)) {
					$operator = "";
					switch ($choice_type) {
						case "month":
						case "day":
							$operator = "";
							break;
						default:
							$operator = "<br>";
						break;
					}	
					$answer .= \AdminInterview::setOperator($answer, $operator);
					$answer .= $choice_string.$value.$unit;
				}
			}

			$ret[$key_section]['section_string'] = $section['section_string'];
			$ret[$key_section]['section_id'] =$section['section_id'];
			$ret[$key_section]['answer'] = $answer;
		}	
		return $ret;
	}

	public static function setOperator($argStr, $operator) {
		if ($argStr == "") {
			return "";
		}
		return $operator;
	}

	public static function getEditData($interview_data, $section_list) {

		$select = DB::table('m_choice')
	            ->join('m_layer', 'm_choice.layer_id', '=', 'm_layer.layer_id')
	            ->join('t_choice_selection', 'm_choice.choice_id', '=', 't_choice_selection.choice_id')
	            ->select('t_choice_selection.choice_id', 
						'm_choice.layer_id', 
						'm_choice.choice_type', 
						'm_choice.choice_string',
						'm_choice.choice_string_unit',
						'm_layer.order_in_section',
						'm_choice.order_in_layer',
						'm_choice.ui_type',
						't_choice_selection.selection',
						't_choice_selection.input_int',
						't_choice_selection.input_float',
						't_choice_selection.input_int',
						't_choice_selection.input_datetime',
						't_choice_selection.input_string')
	            //->where('m_choice.section_id', '=', $section_list[0]['section_id'])
	            ->where('t_choice_selection.interview_id',  $interview_data['interview_id'])
	            ->WhereIn('m_choice.section_id', $section_list)
	            ->orderBy('m_layer.order_in_section', 'asc')
	            ->orderBy('m_choice.order_in_layer', 'asc')
	            ->get();

	    return $select; 
	
	}

	public static function getFollowupDataById($interview_id, $is_newest_week = false){

		$db = DB::table('t_followup')
	            ->join('t_interview', 't_followup.first_interview_id', '=', 't_interview.interview_id')
	            ->select('t_followup.*')
				->where('t_followup.first_interview_id', '=',  $interview_id)
				->where('t_followup.input_date', '<>',  'NULL')
	            ->get();

	    if($db) {
		    foreach ($db as $key => $row) {
		        $list[] = $row;
		    }
		    return $list;  
	    }else {
	    	return false;
	    }    
	

	}

	public static function DeleteCommonData($interview_id, $choice_id) {
		return DB::table('t_choice_selection')->where('interview_id', $interview_id)->where('choice_id', $choice_id)->delete();
	}

	public static function getFollowup($param = null) {

		$list = array();
		$db = DB::table('t_followup')->where($param)->get();
		foreach ($db as $key => $row) {
			$list[] = $row;
		}
		return $list;
	}	

	public static function DeleteGuidance($param) {
		return DB::table('t_guidance')->where($param)->delete();
	}

	public static function insertGuidance($guidance_data) {
		return DB::table('t_guidance')->insert($guidance_data);
	}

}

?>