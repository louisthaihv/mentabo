<?php

namespace Interview;
use View;
use Input;
use Redirect;
use Session;
use Auth;
use Interview;

class InterviewController extends \BaseController {
   
	public function getIndex()
	{
		
		Session::put('followup_code', '');
		if (!$this->loggedin()) {
			
			if (\Session::has('tenant_token')) {
				$token = \Session::get('tenant_token');
			} else {
				 $token = 'default';
			}	
			return \Redirect::to('top/'.$token);
		}

		$user_id = Auth::tuser()->get()->user_id;
		$selectUser = $this->selectUser($user_id);
		$action = Input::get('action');
		$step_id = Input::get('step_id');

		$selectTenant = Interview::selectTenant(array('tenant_id' => $selectUser['tenant_id']));
		$section_set_id = $selectTenant['interview_type'];

		if($selectUser['followup_complete_flag'] == 1) {
			return \Redirect::to('followup');
			exit;
		}

		$sectionSet = $this->sectionSet($section_set_id);
		$setTitle = $sectionSet['section_set_name'];

		if($selectUser['interview_complete_flag'] == 1 OR $action == 'result') {

			return $this->getResult($user_id, $section_set_id, $setTitle);
		
		} else {

			if($selectUser['interview_step'] OR $action == 'question') {

				return $this->getQuestion($user_id, $selectUser, $step_id, $sectionSet, $section_set_id);
			
			}else {
				
				$age = floor((time() - strtotime($selectUser['birth_date'])) / (60*60*24*365));
				$content = View::make('interview.confirm', array('array' => array('step_id' => 1, 'section_set_id' => $section_set_id, 'age' => $age, 'gender' => ($selectUser['gender']=='1') ? '男性': '女性')));
				return View::make('interview.content', array('content' => $content, 'setTitle' => $setTitle));
			}

		}

	}

	public function getQuestion($user_id, $selectUser, $step_id, $sectionSet, $section_set_id)
	{		
		return $this->postQuestion($user_id, $selectUser, $step_id, $sectionSet, $section_set_id);
	}

	public function postQuestion($user_id, $selectUser, $step_id, $sectionSet, $section_set_id)
	{	
		$input = Input::all();
		$step = Input::get('s');

		$interview_type = 1;
		$setTitle = $sectionSet['section_set_name'];

		if($selectUser['interview_step'] == 0) {

			$update_step = 1;
			$interview_id = $this->insertInterview($sectionSet['section_set_id'], $user_id, $interview_type);
			$section_set_by_page = $this->_sectionSetByPage($sectionSet, $selectUser);
			Interview::update_sectionSetByPage($interview_id, $user_id, serialize($section_set_by_page));

		}else {

			$nstep = ($step_id) ? $step_id : $selectUser['interview_step'];
			$update_step = ($step) ? $step: $nstep;

			$selectInterview = $this->selectInterview($user_id);
			$section_set_by_page = unserialize($selectInterview['section_set_by_page']);
			$interview_id = $selectInterview['interview_id'];

		}	
		
		if(isset($selectInterview['input_raw_data'])){
			$array_merge = unserialize($selectInterview['input_raw_data']);
		}else{
			$array_merge = array();
		}

		$input_raw_data = array_merge($array_merge, $input);
		Interview::update_inputRawData($interview_id, $user_id, serialize($input_raw_data));
		$input_data = $input_raw_data;

		//print_r($input_data);

		$stepCount = count($section_set_by_page);
		if($selectUser['interview_step'] >= $stepCount) {

			//print_r($selectUser['interview_step']);

			return $this->getJudge($user_id, $stepCount, $section_set_id, $setTitle, $section_set_id);

		}else {

			$updateStep = Interview::updateStep($user_id, $update_step);
			$sectionSetByPage = $this->sectionSetByPage($section_set_by_page, $update_step, $input_raw_data); // Base controller

			$section_list = $sectionSetByPage['section_list'];
			$layer_list = $sectionSetByPage['layer_list'];
			$choice_list = $sectionSetByPage['choice_list'];

			$jqueryArray['jquery_display'] = $jquery_display = $sectionSetByPage['jquery_display'];
			$jqueryArray['jquery_ui'] = $sectionSetByPage['jquery_ui'];
			$jqueryArray['jquery_requiredCheck'] = $sectionSetByPage['jquery_requiredCheck'];
			$jqueryArray['jquery_regexpCheck'] = $sectionSetByPage['jquery_regexpCheck'];

			$interviewStep = View::make('interview.step', array('step' => $update_step, 'stepCount' => $stepCount));
			$headerScript = View::make('interview.header_script', array('jqueryArray' => $jqueryArray));
			$content = View::make('interview.question', array('section_list' => $section_list, 'step_id' => ($update_step+1), 'input_data' => $input_data,  'section_set_id' => $section_set_id, 'mas' => 1));
			return View::make('interview.content', array('content' => $content, 'headerScript' => $headerScript, 'interviewStep' => $interviewStep, 'setTitle' => $setTitle));

		}	
	}

	public function getJudge($user_id, $stepCount, $section_set_id, $setTitle)
	{		
		$interview_step = $stepCount;
		$interview_complete_flag = 0;
		Interview::update_interviewCompleteFlag($user_id, $interview_step, $interview_complete_flag);
		$content = View::make('interview.judge', array('section_set_id' => $section_set_id));
		return View::make('interview.content', array('content' => $content, 'setTitle' => $setTitle));
	}

	public function getResult($user_id, $section_set_id, $setTitle)
	{	
    
	    //set_error_handler("w3r_notice", E_NOTICE);  
	    //set_error_handler("w3r_error", E_ERROR);  
	    //echo $demo; 

		$input = Input::all();
		$sectionSet = $this->sectionSet($section_set_id);
		$selectUser = $this->selectUser($user_id);
		$start_datetime = date('Y-m-d H:i:s');

		$selectInterview = $this->selectInterview($user_id);
		//print_r($selectInterview);
		$interview_id = $selectInterview['interview_id'];
		$check = \DB::table('t_choice_selection')->where('interview_id', $interview_id)->first();	

		if(!isset($check)){

			$section_set_by_page = unserialize($selectInterview['section_set_by_page']);
			$section_id_sql_condition = '';
			foreach ($section_set_by_page as $page => $page_section_set_list) {
				foreach ($page_section_set_list as $section_id) {
					if (!empty($section_id_sql_condition)) {
						$section_id_sql_condition .= ', ';
					}
					$section_id_sql_condition .= $section_id;
				}
			}
			$section_id_sql_condition = explode(', ', $section_id_sql_condition);
			// if (empty($section_id_sql_condition)) {
			// 	$this->af->setApp('message', "この問診は判定が終了しています。");
			// 	return 'interview_trouble';
			// }

			//対象設問の全選択肢取得
			$choice_list = Interview::getChoiceBySectionId($section_id_sql_condition);
			// if(Ethna::isError($choice_list)){
			// 	$this->ae->add( null, $choice_list->getMessage());
			// 	return 'interview_trouble';
			// }

			//判断元選択肢のID一覧取得
			$layer_control_list = Interview::getLayerControlByChoiceBySectionIdList($section_id_sql_condition);
			// if(Ethna::isError($layer_control_list)){
			// 	$this->ae->add( null, $layer_control_list->getMessage());
			// 	return 'interview_trouble';
			// }
			//layer_id, group_noをキーに並び替え
			$upper_id_list = array();
			foreach ($layer_control_list as $value) {
				$upper_id_list[$value['layer_id']]['all_or_every'] = $value['all_or_every'];
				$upper_id_list[$value['layer_id']]['group'][$value['group_no']][] = $value;
				$upper_id_list[$value['layer_id']]['show_type'] =  $value['show_type'];
			}

			//登録
			$input_raw_data = unserialize($selectInterview['input_raw_data']);
			$this->saveInput($user_id, $interview_id, $selectUser, $section_set_id, 1, $start_datetime, $input_raw_data, $choice_list, $upper_id_list);

		} 

		$result = $this->judgeByInterviewId($selectInterview, $section_set_id);
		return View::make('interview.result', array('result' => $result, 'setTitle' => $setTitle, 'sectionSet' => $sectionSet, 'selectInterview' => $selectInterview));
	
	
	}
	public function saveInput($user_id, $interview_id, $selectUser, $section_set_id, $interview_type, $start_datetime, $input_raw_data, $choice_list, $upper_id_list)
	{
		/* 回答登録 */
		//判断元選択肢の選択有無確認用ID一覧作成（※判断元となる選択肢は、現状ではラジオボタン・チェックボックスのみ）
		$selected_choice_id_list = array();
		foreach ($input_raw_data as $input_name => $value) {
			if (strpos($input_name, 'il_') === 0) {
				if (is_array($value)) {
					foreach ($value as $choice_id) {
						$selected_choice_id_list[] = $choice_id;
					}
				} else {
					$selected_choice_id_list[] = $value;
				}
			}
		}
		
		//一括insertデータ作成
		$rows = array();
		foreach ($input_raw_data as $input_name => $value) {
			$insert_values = null;
			//チェックボックス・ラジオボタン
			if (strpos($input_name, 'il_') === 0) {
				if (!is_array($value)) {
					$choice_id_list = array($value);
				} else {
					$choice_id_list = $value;
				}
				foreach ($choice_id_list as $choice_id) {
					$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list);
					if (!empty($insert_values)) {
						$rows[] = $insert_values;
					}
				}
			//入力値
			} elseif (strpos($input_name, 'i_') === 0) {
				$choice_id = substr($input_name, 2);
				$insert_values = $this->createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list, $value);
				if (!empty($insert_values)) {
					$rows[] = $insert_values;
				}
			}
		}


		//一括insert
		if (!empty($rows)) {
			$interview_step = 1;
			$interview_complete_flag = 1;
			Interview::update_interviewCompleteFlag($user_id, $interview_step, $interview_complete_flag);
			Interview::update_interview($interview_id, $user_id);
			Interview::insertChoiceSelection($interview_id, $rows);
			// if (Ethna::isError($result)) {
			// 	return $result;
			// }
		}
	}

	// 回答データ登録用データ作成
	public function createChoiceSelectionInsertValue($choice_id, $choice_list, $upper_id_list, $selected_choice_id_list, $value = '')
	{
		$choice_data = $choice_list[$choice_id]; //現在の選択肢の情報
		$choice_type = $choice_data['choice_type']; //現在の選択肢のタイプ
		$upper = $upper_id_list[$choice_data['layer_id']];

		if (!empty($upper)) { 

			//子選択肢
			$isParentSelected = $this->isSelectedUpper($selected_choice_id_list, $upper);
			if ($upper['show_type'] ==  0) {
				if ($isParentSelected) {
					return null;
				}
			} else {
				if (!$isParentSelected) {
					return null;
				}
			}
			if ($choice_type !== "radio" && $choice_type !== "checkbox") {
				if ($value === "" || is_null($value)) {
					return null;
				}
			}
		}

		$insert_values = array('choice_id' => $choice_id);
		switch ($choice_type) {
			case 'radio':
			case 'checkbox':
				$insert_values['selection'] = true;
				break;
			case 'string':
				$insert_values['input_string'] = $value;
				break;
			case 'numeric':
				$insert_values['input_float'] = $value;
				break;
			case 'date':
				$insert_values['input_datetime'] = $value . ' 00:00:00';
				break;
			case 'time':
				$insert_values['input_datetime'] = '1999-01-01 ' . $value;
				break;
			case 'datetime':
				$insert_values['input_datetime'] = $value;
				break;
			case 'year':
				$insert_values['input_datetime'] = $value . '-01-01 00:00:00';
				break;
			case 'month':
				$insert_values['input_datetime'] = '1999-' . $value . '-01 00:00:00';
				break;
			case 'day':
				$insert_values['input_datetime'] = '1999-01-' . $value . ' 00:00:00';
				break;
			case 'slider':
			case 'slider_hour':
			case 'slider_show_by_step':
			case 'slider_minute':
				$insert_values['input_float'] = $value;
				break;
			case 'tab':
			case 'accordion': //見出し
				return null;
		}
		return $insert_values;
	}

	// 判断元の選択肢が選択されているかチェック
	public function isSelectedUpper($selected_choice_id_list, $upper) {
		$all_or_every = $upper['all_or_every'];
		$group = $upper['group'];

		$isSatisfy = false;
		switch ($all_or_every) {
			case "all":
				$isSatisfy = true;
				foreach ($group as $group_no => $upper_list) {
					foreach ($upper_list as $upper_data) {
						if (!in_array($upper_data['choice_id_upper'], $selected_choice_id_list)) {
							$isSatisfy = false;
							break;
						}
					}
				}
				break;
			case "every":
				$isSatisfy = false;
				foreach ($group as $group_no => $upper_list) {
					foreach ($upper_list as $upper_data) {
						if (in_array($upper_data['choice_id_upper'], $selected_choice_id_list)) {
							$isSatisfy = true;
							break;
						}
					}
				}
				break;
			default:
				break;
		}

		return $isSatisfy;
	}			

	public function _sectionSetByPage($sectionSet, $selectUser)
	{
		//ランダムフラグを確認
		if($sectionSet['flg_followup'] != '1'){

			// 1以外は通常表示
			//選択した問診タイプの設問ID一覧取得
			$section_list = \Interview::getSectionSetContentsById($sectionSet['section_set_id'], $selectUser);
			
			// if (Ethna::isError($section_list)) {
			// 	$this->ae->add('null', $section_list->getMessage());
			// 	return 'interview_trouble';
			// }
			//ページごとに並び替え
			$section_set_by_page = array();
			foreach ($section_list as $section) {
				$section_set_by_page[$section['page_num']][$section['order_section']] = $section['section_id'];
			}
		} else {

			// ランダム表示
			//固定ページを取得
			$param = array();
			$param['flg_display'] = 1;
			$param['section_type'] = "metabo";
			$param['display_position'] = 1;
			$page = 1;
			$order = 1;
			$section_set_by_page = array();
			//$section_list = \Interview::getSectionSetContentByBlockParty($sectionSetId, $param, $userData);
			$section_list = $this->getSectionSetContentByBlockParty($sectionSet['section_set_id'], $param, $selectUser);
			
			if($section_list){
				foreach($section_list as $section){
					$section_set_by_page[$page][$order] = $section['section_id'];
					$order++;
				}
				$page++;
			}

			//メンタル質問を取得
			$param = array();
			$param['flg_display'] = 1;
			$param['section_type'] = 'mental';
			//$mental_block_list = \Interview::getSectionSetContentBlockParty($sectionSetId, $param);
			$mental_block_list = $this->getSectionSetContentBlockParty($sectionSet['section_set_id'], $param);
			
			//ブロックIDをシャッフル
			//shuffle($mental_block_list["id"]);
			//シャッフルしたメンタルのブロックIDのパーティIDを取得
			$mental_party_list = array();
			foreach($mental_block_list["id"] as $block_key => $block_val){
				$param['block_id'] = $block_val;
				//$temp_mental_party_list = \Interview::getSectionSetContentBlockParty($sectionSetId, $param);
				$temp_mental_party_list = $this->getSectionSetContentBlockParty($sectionSet['section_set_id'], $param);
				$mental_party_list[$block_val] = $temp_mental_party_list["id"];
				shuffle($mental_party_list[$block_val]);
			}
			
			//シャッフル結果を用いて、必要データを検索する
			$mental_list = array();
			foreach($mental_party_list as $party_key => $party_val){
				foreach($party_val as $unit_key => $unit_val){
					$param['block_id'] = $party_key;
					$param['party_id'] = $unit_val;
					//$mental_section_list = \Interview::getSectionSetContentByBlockParty($sectionSetId, $param, $userData);
					$mental_section_list = $this->getSectionSetContentByBlockParty($sectionSet['section_set_id'], $param, $selectUser);
					$mental_list[] = $mental_section_list;
				}
			}
			
			// 1ページに表示するブロックを取得
			$param = array();
			$param['flg_display'] = 1;
			$param['section_type'] = "metabo";
			$param['display_position'] = 2;
			//$block_list = \Interview::getSectionSetContentBlockParty($sectionSetId, $param);
			$block_list = $this->getSectionSetContentBlockParty($sectionSet['section_set_id'], $param);
			
			//ブロックIDをシャッフル
			shuffle($block_list["id"]);
			
			//シャッフルしたブロックIDのパーティIDを取得
			$party_list = array();
			foreach($block_list["id"] as $block_key => $block_val){
				$param['block_id'] = $block_val;
				//$temp_party_list = \Interview::getSectionSetContentBlockParty($sectionSetId, $param);
				$temp_party_list = $this->getSectionSetContentBlockParty($sectionSet['section_set_id'], $param);
				$party_list[$block_val] = $temp_party_list["id"];
				shuffle($party_list[$block_val]);
			}

			//シャッフル結果を用いて、必要データを検索し、ページを割り振る
			foreach($party_list as $party_key => $party_val){
				foreach($party_val as $unit_key => $unit_val){
					$param['block_id'] = $party_key;
					$param['party_id'] = $unit_val;
					//$section_list = \Interview::getSectionSetContentByBlockParty($sectionSetId, $param, $userData);
					$section_list = $this->getSectionSetContentByBlockParty($sectionSet['section_set_id'], $param, $selectUser);
					foreach($section_list as $section){
						$section_set_by_page[$page][$order] = $section['section_id'];
						$order++;
					}
				}
				$page++;
				$order = 1;
			}

			//この時点でメンタル質問を後ろにつけていく
			$mental_page = max(1,$page -1);
			$mental_backup = $mental_list;
			
			for($i=1;$i<=$mental_page;$i++){
				$order = max(array_keys($section_set_by_page[$i])) + 1;
				//$random_for = rand($getSectionSet[$sectionSetId]["mental_band_min"], $getSectionSet[$sectionSetId]["mental_band_max"]);
				$random_for = rand($sectionSet["mental_band_min"], $sectionSet["mental_band_max"]);
				$temp_mental = $mental_list;
				$cnt = 1;
				$ques_count = 0;
				foreach($temp_mental as $temp_key => $temp_val){
					$ques_count += count($temp_val);
					if($ques_count > $random_for && $ques_count != count($temp_val)){
						break;
					}
					foreach($temp_val as $mental_key => $mental_val){
						$section_set_by_page[$i][$order] = $mental_val['section_id'];
						$order++;
					}
					unset($mental_list[$temp_key]);
					if($cnt >= $random_for){
						break;
					}
					$cnt++;
				}
				if(empty($mental_list)){
					break;
				}
			}
			if(!empty($mental_list)){
				foreach($temp_mental as $temp_key => $temp_val){
					foreach($temp_val as $mental_val){
						$section_set_by_page[$mental_page][$order] = $mental_val['section_id'];
						$order++;
					}
				}
			}
				
			//固定ページ（後ろ）取得
			//固定ページを取得
			$param = array();
			$param['flg_display'] = 1;
			$param['section_type'] = "metabo";
			$param['display_position'] = 3;
			$order = 1;
			//$section_list = \Interview::getSectionSetContentByBlockParty($sectionSetId, $param, $userData);
			$section_list = $this->getSectionSetContentByBlockParty($sectionSet['section_set_id'], $param, $selectUser);
			if($section_list){
				foreach($section_list as $section){
					$section_set_by_page[$page][$order] = $section['section_id'];
					$order++;
				}
				$page++;
			}
		}
		return $section_set_by_page;
	}
}
