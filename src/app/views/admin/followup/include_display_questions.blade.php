
<div class="editView">
<div class="edit-table-wrap">
<table summary="問診結果詳細">
<!--{foreach name="roop_section" from=$app.section_list key=idx item=section_data}-->
{{-- */$roop_section = 1;/* --}}
@foreach ($section_list as $idx => $section_data)
	<!--{if $section_data.layer_inclusion_tree}-->
	@if($section_data['layer_inclusion_tree'])
		<!--{foreach name="roop_1st" from=$section_data.layer_inclusion_tree key=layer_id_1st item=lower_layer_id_key_list}-->
		{{-- */ $roop_1st = 1; /* --}}
		@foreach($section_data['layer_inclusion_tree'] as $layer_id_1st => $lower_layer_id_key_list)
			<!--{assign var="layer_choice_data" value=$section_data.layer_choice_list[$layer_id_1st]}-->
			{{-- */$layer_choice_data = $section_data['layer_choice_list'][$layer_id_1st] ;/* --}}
			<!--{if $layer_choice_data.choice_list[0]}-->
			@if($layer_choice_data['choice_list'][0])
				<!--{assign var="layer_type_1st" value=$layer_choice_data.choice_list[0].choice_type}-->
			@else	
			<!--{else}-->
				<!--{assign var="layer_type_1st" value=""}-->
				{{-- */$layer_type_1st = '';/* --}}
			@endif	
			<!--{/if}-->

			<!--{if $smarty.foreach.roop_1st.first}-->
			@if($roop_1st == 1)
				<div id="l_{{ $layer_id_1st }}">
				<h3 id="s_{{ $section_data.section_id }}" @if($roop_section == 1) class="top" @endif >{{ $section_data['section_string'] }}</h3>
			@endif
			<!--{/if}-->

			<!--{if $layer_choice_data.layer_data.annotation || $section_data.flg_must_input eq 1 || $section_data.flg_must_input eq 2}-->
			@if($layer_choice_data['layer_data']['annotation'] || $section_data['flg_must_input'] == 1 || $section_data['flg_must_input'] == 2 )
				<p class="lead">
					<!--{if $smarty.foreach.roop_1st.first && ($section_data.flg_must_input eq 1 || $section_data.flg_must_input eq 2)}-->
					@if($roop_1st == 1 && ($section_data['flg_must_input'] == 1 || $section_data['flg_must_input'] == 2))
						<span><img src="../img/icon_required.png" alt="必須" /></span>
					@endif	
					<!--{/if}-->
					<!--{$app_ne.layer_list[$layer_choice_data.layer_data.layer_id].annotation}-->
					{{ $layer_choice_data['layer_data']['annotation'] }}
				</p>
			@endif	
			<!--{/if}-->
			<!--{if $layer_choice_data.layer_data.help_title && $layer_choice_data.layer_data.help_text}-->
			@if($layer_choice_data['layer_data']['help_title'] && $layer_choice_data['layer_data']['help_text'])
				<div class="annotation-overlay">
					<a href="#" rel="#overlay-{{ $section_data['section_id'] }}" class="trigger">{{ $layer_choice_data['layer_data']['help_title'] }}</a>
					<div id="overlay-{{ $section_data['section_id'] }}" class="overlayBox">
						<h4>{{ $layer_choice_data['layer_data']['help_title'] }}</h4>
						<p>{{ $layer_choice_data['layer_data']['help_text'] }}</p>
						<p class="close">×</p>
					</div>
				</div>
			@endif	
			<!--{/if}-->

			<!--{*DIV開始*}-->
			<!--{if $layer_type_1st eq "tab"}-->
			@if($layer_type_1st == 'tab')
				<div id="tab_{{ $layer_id_1st }}" class="tabs">
			@elseif($layer_type_1st == 'accordion')	
			<!--{elseif $layer_type_1st eq "accordion"}-->
				<div id="acc_{{ $layer_id_1st }}" class="accordion">
			@endif	
			<!--{/if}-->

			<!--{*タブの見出し*}-->
			<!--{if $layer_type_1st eq "tab"}-->
			@if($layer_type_1st == 'tab')
				<!--{foreach name="choice_roop_1st" from=$layer_choice_data.choice_list key=idx_c item=choice_data}-->
				{{-- */$choice_roop_1st = 1;/* --}}
				@foreach($layer_choice_data['choice_list'] as $idx_c => $choice_data)
					<!--{if $smarty.foreach.choice_roop_1st.first}-->
					@if($choice_roop_1st == 1)
						<ul>
					@endif	
					<!--{/if}-->
						<li class="tablabel tablabel<!--{$smarty.foreach.choice_roop_1st.index}-->" id="tablabel_<!--{$choice_data.choice_id}-->"><a href="#tab_<!--{$choice_data.choice_id}-->"><!--{$choice_data.choice_string}--></a></li>
					<!--{if $smarty.foreach.choice_roop_1st.last}-->
					{{-- */ $last = count($layer_choice_data['choice_list']); /* --}}
					@if($choice_roop_1st == $idx_c)	
						</ul>
					@endif	
					<!--{/if}-->
				{{-- */ $choice_roop_1st ++; /* --	
				@endforeach	
				<!--{/foreach}-->
			@endif		
			<!--{/if}-->

			<!--{if $lower_layer_id_key_list}-->
			@if($lower_layer_id_key_list)
				<!--{foreach name="roop_2nd" from=$lower_layer_id_key_list key=layer_id_2nd item=lower_layer_id_key_list_2nd}-->
				@foreach($lower_layer_id_key_list as $layer_id_2nd => $lower_layer_id_key_list_2nd)
					<!--{assign var="choice_data_1st" value=`$section_data.layer_choice_list[$layer_id_1st].choice_list[$smarty.foreach.roop_2nd.index]`}-->
					{{-- */ $choice_data_1st = $section_data['layer_choice_list'][$layer_id_1st]['choice_list'][$roop_2nd]; /* --}}
					<!--{assign var="layer_choice_data" value=$section_data.layer_choice_list[$layer_id_2nd]}-->
					{{-- */ $choice_roop_1st = $section_data['layer_choice_list'][$layer_id_2nd]; /* --}}
					<!--{assign var="layer_type_2nd" value=$layer_choice_data.choice_list[0].choice_type}-->
					{{-- */ $choice_roop_1st = $layer_choice_data['choice_list'][0]['choice_type']; /* --}}

					<!--{if $layer_type_1st eq "tab"}-->
					@if($layer_type_1st == 'tab')
						<div id="tab_{{ $choice_data_1st['choice_id'] }}" class="tabcontent{{  $roop_2nd }}">
					@elseif($layer_type_1st =='accordion')	
					<!--{elseif $layer_type_1st eq "accordion"}-->
						<h4><a href="#">{{ $choice_data_1st['choice_string'] }}</a></h4>
					@endif	
					<!--{/if}-->

					<!--{*DIV開始*}-->
					<!--{if $layer_type_2nd eq "tab"}-->
					@if($layer_type_2nd == 'tab')
						<div id="tab_{{ $layer_id_2nd }}" class="tabs">
					@elseif ($layer_type_2nd == 'accordion')	
					<!--{elseif $layer_type_2nd eq "accordion"}-->
						<div id="acc_{{ $layer_id_2nd }}" class="accordion">
					@else	
					<!--{else}-->
						<div id="l_{{ $layer_id_2nd }}" @if($layer_type_1st == 'tab' && $layer_type_1st == 'accordion') class="choice-lowlayer" @endif>
					@endif
					<!--{/if}-->

					<!--{*タブの見出し*}-->
					<!--{if $layer_type_2nd eq "tab"}-->
					@if($layer_type_2nd == 'tab')
						<!--{foreach name="choice_roop_2nd" from=$layer_choice_data.choice_list key=idx_c item=choice_data}-->
						{{-- */ $choice_roop_2nd = 1; /* --}}
						@foreach($layer_choice_data['choice_list'] as $idx_c => $choice_data)	
							<!--{if $smarty.foreach.choice_roop_2nd.first}-->
							@if($choice_roop_2nd == 1)
									<ul>
							@endif			
							<!--{/if}-->
								<li class="tablabel tablabel{{ $roop_2nd }}" id="tablabel_{{ $choice_data['choice_id'] }}"><a href="#tab_{{ $choice_data ['choice_id'] }}">{{ $choice_data['choice_string'] }}></a></li>
							<!--{if $smarty.foreach.choice_roop_2nd.last}-->
							{{-- */ $last2 = count($layer_choice_data['choice_list']); /* --}}
							@if($idx_c == $last2)
								</ul>
							@endif	
							<!--{/if}-->
						{{-- */ $choice_roop_2nd ++; /* --}}	
						@endforeach
						<!--{/foreach}-->
					@endif	
					<!--{/if}-->

					<!--{if $lower_layer_id_key_list_2nd}-->
					@if($lower_layer_id_key_list_2nd)
						<!--{foreach name="roop_3rd" from=$lower_layer_id_key_list_2nd key=layer_id_3rd item=lower_layer_id_key_list_3rd}-->
						{{-- */ $roop_3rd = 1; /* --}}
						@foreach($lower_layer_id_key_list_2nd as $layer_id_3rd => $lower_layer_id_key_list_3rd)
							<!--{assign var="choice_data_2nd" value=`$section_data.layer_choice_list[$layer_id_2nd].choice_list[$smarty.foreach.roop_3rd.index]`}-->
							{{-- */ $choice_data_2nd = $section_data['layer_choice_list'][$layer_id_2nd]['choice_list'][$roop_3rd]; /* --}}
							<!--{assign var="layer_choice_data" value=$section_data.layer_choice_list[$layer_id_3rd]}-->
							{{-- */ $layer_choice_data = $section_data['layer_choice_list'][$layer_id_3rd]; /* --}}

							<!--{if $layer_type_2nd eq "tab"}-->
							@if($layer_type_2nd == 'tab')
								<div id="tab_{{ $choice_data_2nd['choice_id'] }}" class="tabcontent{{ $roop_3rd }}">
							@elseif($layer_type_2nd == 'accordion')	
							<!--{elseif $layer_type_2nd eq "accordion"}-->
								<h4><a href="#">{{ $choice_data_2nd['choice_string'] }}></a></h4>
							@endif	
							<!--{/if}-->

							<div id="l_{{ $layer_id_3rd }}" @if($layer_type_2nd == 'tab' && $layer_type_2nd == 'accordion') class="choice-lowlayer" @endif>
							<!--{foreach from=$layer_choice_data.choice_list key=idx_c item=choice_data}-->
							@foreach($layer_choice_data['choice_list'] as $idx_c => $choice_data)
								<!--{include file="followup/include_display_choice_data.tpl"}-->	<!--{*選択肢ごとの出力*}-->
								@include('admin.followup.include_display_choice_data')
							@endforeach	
							<!--{/foreach}-->
							</div>
							<!--{if $layer_type_2nd eq "tab"}-->
							@if($layer_type_2nd == 'tab')	
								</div>
							@endif	
							<!--{/if}-->
						{{-- */ $roop_3rd ++; /* --}}
						@endforeach	
						<!--{/foreach}-->
					@else	
					<!--{else}-->
						<!--{*選択肢の出力*}-->
						<!--{if $layer_choice_data.layer_data.choice_width_type}-->
						@if($layer_choice_data['layer_data']['choice_width_type'])
						<div class="choices-{{ $layer_choice_data['layer_data.choice_width_type'] }}">
						@else
						<!--{else}-->
						<div class="choices">
						@endif
						<!--{/if}-->
						<!--{foreach from=$layer_choice_data.choice_list key=idx_c item=choice_data}-->
						@foreach($layer_choice_data['choice_list'] as $idx_c => $choice_data)
							<!--{include file="followup/include_display_choice_data.tpl"}-->
							@include('admin.followup.include_display_choice_data')
						@endforeach	
						<!--{/foreach}-->
						</div>
					@endif	
					<!--{/if}-->
					<!--{*DIV終了*}-->
					</div>
					<!--{if $layer_type_1st eq "tab"}-->
					@if($layer_type_1st == 'tab')
						</div>
					@endif	
					<!--{/if}-->
				@endforeach	
				<!--{/foreach}-->
			@else	
			<!--{else}-->
				<!--{*選択肢の出力*}-->
				<!--{foreach from=$layer_choice_data.choice_list key=idx_c item=choice_data}-->
					@foreach($layer_choice_data['choice_list'] as $idx_c => $choice_data)
					<!--{include file="followup/include_display_choice_data.tpl"}-->
					@include('admin.followup.include_display_choice_data')
					@endforeach
				<!--{/foreach}-->
			@endif
			<!--{/if}-->

			<!--{*DIV終了*}-->
			<!--{if $layer_type_1st eq "tab"}-->
			@if($layer_type_1st == 'tab')
				</div>
			@elseif($layer_type_1st == 'accordion')	
			<!--{elseif $layer_type_1st eq "accordion"}-->
				</div>
			@else	
			<!--{else}-->
				<div style="clear:left;"></div>
			@endif
			<!--{/if}-->

			<!--{if $smarty.foreach.roop_1st.last}-->
			@if($roop_1st > 0)	
				</div>
			@endif	
			<!--{/if}-->
		{{-- */ $roop_1st ++; /* --}}	
		@endforeach	
		<!--{/foreach}-->
	@else	
	<!--{else}-->
		<!--{*通常の選択肢表示*}-->


		<!--{foreach name="roop_normal" from=$section_data.layer_choice_list key=idx2 item=layer_choice_data}-->
		{{-- */ $roop_normal = 1; /* --}}
		@foreach($section_data['layer_choice_list'] as $idx2 => $layer_choice_data)


				<tr id="l_{{ $layer_choice_data['layer_data']['layer_id'] }}">
					<th>
						<!--{* 質問内容 *}-->
						<!--{if $smarty.foreach.roop_normal.first}-->
						@if($roop_normal == 1)

								<div id="s_{{ $section_data['section_id'] }}" @if($roop_normal == 1) class="top" @endif>{{ $section_data['section_string'] }}</div>
						@else		
						<!--{else}-->
						@endif
						<!--{/if}-->

						<!--{* 注釈 *}-->

					</th>

					<td>
						<!--{if $layer_choice_data.layer_data.help_title && $layer_choice_data.layer_data.help_text}-->
						@if($layer_choice_data['layer_data']['help_title'] && $layer_choice_data['layer_data']['help_text'])
								<div class="annotation-overlay">
									<a href="#" rel="#overlay-{{ $section_data['section_id'] }}" class="trigger">[{ $layer_choice_data['layer_data']['help_title'] }}</a>
									<div id="overlay-{{ $section_data['section_id'] }}" class="overlayBox">
										<h4>{{ $layer_choice_data['layer_data']['help_title'] }}</h4>
										<p>{{ $layer_choice_data['layer_data']['help_text'] }}</p>
										<p class="close">×</p>
									</div>
								</div>
						@endif		
						<!--{/if}-->

						<!--{if $layer_choice_data.layer_data.choice_width_type}-->
						@if($layer_choice_data['layer_data']['choice_width_type'])
								<div class="choices-{{ $layer_choice_data['layer_data']['choice_width_type'] }}">
						@else		
						<!--{else}-->
								<div class="choices">
						@endif		
						<!--{/if}-->

									<!--{foreach from=$layer_choice_data.choice_list key=idx_c item=choice_data}-->
									@foreach($layer_choice_data['choice_list'] as $idx_c => $choice_data)
										<!--{include file="followup/include_display_choice_data.tpl"}-->	<!--{*選択肢の出力*}-->
										@include('admin.followup.include_display_choice_data')
									@endforeach	
									<!--{/foreach}-->

								</div>
					</td>
				</tr>

		{{-- */ $roop_normal ++; /* --}}
		@endforeach
		<!--{/foreach}-->
	@endif	
	<!--{/if}-->
@endforeach	
<!--{/foreach}-->
</table>
	<div class="btm"><img src="{{ URL::to('/') }}/img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
</div>
</div>

