<!--{if $app.result.past_history == "1"}-->
<?php if($result['past_history'] == 1) { ?>
<dl>
<dd>現在かかりつけの主治医に指導されている内容に従ってください。</dd>
</dl>
<?php } ?>
<!--{else}-->

	<dl>
		<dt><span>メタボ前置</span></dt>
		<dd>メタボリック症候群は腹囲を内臓脂肪蓄積の目安とし、糖尿病・高血圧・高脂血症それぞれの予備軍の状況からも、
			重なり合って蓄積することによって動脈硬化性疾患（心筋梗塞・脳卒中など）のリスクが高くなることから誕生した概念です。
			メタボリック症候群あるいは糖尿病・高血圧・高脂血症の各疾患を予防することは最終的に動脈硬化を予防し、
			これによって発生する各種疾患にかからないようにするために最善の方法です。
		</dd>

		<dt><span>基礎情報</span></dt>
		<dd>あなたの　身長<!--{$app.result.tall}--><?php echo $result['tall'] ?>cm　体重<!--{$app.result.weight}--><?php echo $result['weight'] ?>kg　腹囲<!--{$app.result.abdominal_girth}--><?php echo $result['abdominal_girth'] ?>cm　
			から計算すると、あなたのBMIは、<?php echo $result['output'][3] ?><!--{$app.result.output.3}-->です。あなたの理想体重は、<?php echo $result['output'][1] ?><!--{$app.result.output.1}-->kgです。<br><br>
			<!--{if $app.result.output.3 < 13}-->
			<?php if ($result['output'][3] < 13) { ?>
				<?php $bmi = 13 ?>
				<!--{assign var="bmi" value="13"}-->
			<!--{elseif $app.result.output.3 > 38}-->
			<?php }elseif ($result['output'][3] > 38) { ?>
				<!--{assign var="bmi" value="38"}-->
				<?php $bmi = 38 ?>
			<?php }else { ?>	
			<!--{else}-->
				<!--{assign var="bmi" value=$app.result.output.3}-->
				<?php $bmi = $result['output'][3] ?>
			<?php } ?>	
			<!--{/if}-->
			<img src="{{ URL::to('/') }}/img/graph_bmi_<?php echo $bmi ?>.gif" alt="BMIグラフ">
		</dd>

		<dt><span>判定値</span></dt>
		<dd>以上の結果から、現在のあなたのメタボリック症候群リスクは、 <?php echo $result['judge_metabo'][19] ?><!--{$app.result.judge_metabo.19}--> です。<br /><br />
			<img src="{{ URL::to('/') }}/img/graph_risk_<?php echo $result['judge_metabo'][19] ?>.gif" alt="メタボリック症候群リスクグラフ">
		</dd>

		<!--<dt><span>判定文</span></dt>-->
        <br />
		<dd><?php echo $result['judge_metabo'][20] ?><!--{$app.result.judge_metabo.20}--></dd>

		<dt><span>予測値</span></dt>
		<dd>あなたが現在の生活を継続した場合に今後何らかの動脈硬化性疾患を発症するリスクは、<?php
            switch($result['judge_metabo'][24]){
                case "1":echo "低い";break;
                case "2":echo "やや低い";break;
                case "3":echo "中等度";break;
                case "4":echo "高い";break;
                case "5":echo "きわめて高い";break;
                default:echo "何らかの原因により判定失敗";
            }?><!--{$app.result.judge_metabo.24}--> です。<br /><br />
			<img src="{{ URL::to('/') }}/img/graph_risk_<?php echo $result['judge_metabo'][24]*2 ?>.gif" alt="動脈硬化性疾患を発症するリスクグラフ">
		</dd>

		<dt><span>指導運動</span></dt>
		<dd>まずは、週に3回、1回10分以上の運動をするように心がけてください。</dd>

		<dt><span>指導食事</span></dt>
		<dd>あわせて食事の総カロリーを、<?php echo $result['output'][8] ?><!--{$app.result.output.8}-->kcalにするようにチャレンジしてみてください。</dd>

		<!--{if $app.result.show_smoking_leading}-->
		<?php if(isset($result['show_smoking_leading'])) { ?>
		<dt><span>喫煙指導</span></dt>
		<dd>喫煙は動脈硬化をすすめる原因になります。またその他多くの病気(肺気腫、がんなど）のもとになります。
			周囲の方の健康を害するものでもあり、できる限り禁煙しましょう。
			禁煙指導が必要な場合は専門医療機関にご相談ください。</dd>
		<?php } ?>	
		<!--{/if}-->

		<dt><span>メンタル判定</span></dt>
		<dd>
			<!--{foreach from=$app.result.mental_result key=key item=textext}-->
			<?php foreach ($result['mental_result'] as $key => $textext) { ?>
				<?php echo $textext ?><!--{$textext}--><br>
			<?php } ?>		
			<!--{/foreach}-->
		</dd>
		@include('interview.result_food')
		<!--{include file="result_food.tpl"}-->
	</dl>

<!--{/if}-->