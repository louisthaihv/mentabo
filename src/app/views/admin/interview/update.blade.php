<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-1.6.3.min.js"></script>
<script>
$(function(){
	$(window).load(function() {
		parent.$("body").css("overflow","auto");
	    parent.$("#popup-dialog").fadeOut("normal", function() {
	        parent.$("iframe", this).remove();
	    });
	    parent.location.reload();
	    return false;
	});
});
</script>