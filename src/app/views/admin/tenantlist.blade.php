@include('admin/header')

<hr />
<div id="wrapper">
<div id="accountList" class="contents">
	<h2 id="pageTitle">テナント一覧</h2>
	<hr />

	
	<div id="main">

		<div class="table-wrap">
			<table summary="アカウント一覧">
				<thead>
					<tr>
						<th></th>
						<th>テナントID</th>
						<th>テナント名</th>
						<th>ロゴ画像URL</th>

						<!-- <th>token</th> -->
						<th>有効／無効</th>
						<th>テナントログイン</th>
					</tr>
				</thead>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th>テナントID</th>
						<th>テナント名</th>
						<th>ロゴ画像URL</th>
						<!-- <th>token</th> -->
						<th>有効／無効</th>
						<th>テナントログイン</th>
					</tr>
				</tfoot>
									<tbody>
					@foreach ($tenants as $tenant)
											<tr>
							<td class="column1">
									<input type="button" value="編集" onClick="location.href='edittenantaccount/{{$tenant->token}}'" class="input-btn">
														</td>
							<td class="column2">{{ $tenant->tenant_id }}</td>
							<td class="column3">{{ $tenant->tenant_name }}</td>
							<td class="column4">{{ $tenant->logo_url }}</td>
							
							<!-- <td class="column6 center">{{ $tenant->token }}</td> -->
							<td class="column7">@if ($tenant->valid ==1) 
												    有効
												@else 
												    無効
											    @endif
											</td>
								<td class="column8 center"><button onclick="window.location.href='../admin/speciallogin/{{$tenant->tenant_id}}';">ログイン</button></td>			
						</tr>
					@endforeach
												</table>

			<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>

	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
