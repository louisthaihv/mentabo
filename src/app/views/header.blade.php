
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
<title>生活習慣診断</title>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link type="text/css" href="{{ URL::to('/') }}/css/sunny/jquery-ui-1.8.4.custom.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/common/format.css" media="all" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/common/base.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/common_style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/print_style.css" media="print" />
<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/addition.css" rel="stylesheet" />
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery-ui-1.8.12.custom.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.alerts.js"></script>
<script type="text/javascript" src="{{ URL::to('/') }}/js/style.js"></script>
<!--script type="text/javascript" src="{{ URL::to('/') }}/js/jquery.format.1.05.js"></script-->
<script type="text/javascript">
// $(document).ready(function () {
	
// 	$('.slider_input').keyup(function() {
// 	  var inputVal = $(this).val()
// 	  var inputLastChar = inputVal.substr(-1);
// 	  var mapping = {"１": "1", "２": "2", "３": "3", "４": "4", "５": "5", "６": "6", "７": "7", "８": "8", "９": "9", "０": "0", "。": ".", ",": "."}

// 	  matchedChar = null
// 	  $.each(mapping, function(k, v) {
// 	    if (inputLastChar === k) {
// 	      matchedChar = v;
// 	    }
// 	  });

// 	  if (matchedChar) {
// 	    $(this).val(inputVal.substr(0, inputVal.length - 1) + matchedChar);
// 	  }
// 	});
// 	$('.slider_input').format({precision: 1, allow_negative:false, autofix:true});
// });
</script>
</head>

<body id="interview">
<div id="wrap">

<div id="noscript" style="display:none">
	<p>このページは、Javascriptを使用しています。<br>
	Javascriptを有効にしてご利用ください。</p>
</div>
<script>
	document.getElementById("noscript").style.display = "none";
</script>

<!-- ↓↓↓ header  ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="header">
<div id="headerInner">
	<h1 style="background-size: contain;"><!--{$config.service_name}--></h1>
	<ul>
		<li class="btn-faq"><a href="#">FAQ</a>&nbsp;&nbsp;|</li>
		<li class="btn-contact"><a href="#">お問い合わせ</a></li>
        <?php if(! \Auth::tuser()->check()): ?>
        <li class="btn-mypage" style="visibility: hidden;"><a href="{{ URL::to('/') }}/mypage">マイページ</a></li>
        <?php else: ?>
        <li class="btn-mypage"><a href="{{ URL::to('/') }}/mypage">マイページ</a></li>
        <?php endif; ?>
		<!--{assign var="url" value="`$config.interview_absolute_url`?action_interview_logout_do=true"}-->
        <?php if(\Auth::tuser()->check()): ?>
			<li class="btn-logout"><a href="{{ URL::to('/') }}/logout">ログアウト</a></li>
        <?php else: ?>
			{{--
            // can just use # as the link in this case because currently the login page is the only page that has the 'not logged in' status.
			// but to make sure in the case the system add more pages with 'not logged in' status, should use a link
            --}}
			@if(Session::has('tenant_token'))
                <li class="btn-login"><a href="{{ URL::to('/')."/top/".Session::get('tenant_token') }}">ログイン</a></li>
			@else
                <li class="btn-login"><a href="#">ログイン</a></li>
            @endif

        <?php endif; ?>
	</ul>
</div>
</div><!-- /#header -->
								<?php
									 if ( \Auth::tuser()->check() )
										 $tenant_id = \Auth::tuser()->get()->tenant_id;
									 else 
									 if (!isset($tenant_id)) $tenant_id = 'abc'; // $tenant_id was set in TUserController action login   
								?>
 								<script type="text/javascript">
								function Logo()
								{
								   
									$("#header h1").css("background-image","url('{{ URL::to('/') }}/logo/{{ $tenant_id }}')");
								    
								}
 								Logo();
 								</script>
<!-- ↑↑↑ header  ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->

