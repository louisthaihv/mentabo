@include('admin/header')

<hr /><link rel="stylesheet" type="text/css" href="../css/admin_mail.css" media="screen" />

	<div id="noscript">
		<p>このページは、Javascriptを使用しています。<br>
		Javaascriptを有効にしてご利用ください。</p>
	</div>
	<script>
	document.getElementById("noscript").style.display = "none";
	</script>


<div id="wrapper">
<div id="interviewDetail" class="contents">
	<h2 id="pageTitle">メール内容設定</h2>
	<hr />

	
	<div id="main">

		<div id="content">

			<form action="mailedit" method="POST">
                @if($errors->all('message'))
                <ul class="valid_error">
                    @foreach($errors->all('<li>:message</li>') as $message)
                    {{ $message }}
                    @endforeach
                </ul>
                @endif
				<div id="contentInner">

					<div class="compulsoryInputError"></div>

														<p><font color="#ff0000">※</font>は必須項目です</p>
								<h3>フォローアップ通知メール</h3>
								<div class="mail-input-area">

									<div class="table-wrap">
										<table summary="フォローアップ通知メール">
											<tr>
												<th><font color="#ff0000">※</font>差出人メールアドレス（半角英数）</th>
												<td><input value="{{ $followup['fromaddress'] }}" type="text" name="followup_sender_address" /></td>
											</tr>
											<tr>
												<th><font color="#ff0000">※</font>差出人名</th>
												<td><input value="{{ $followup['fromname'] }}" type="text" name="followup_sender_name" /></td>
											</tr>
											<tr>
												<th><font color="#ff0000">※</font>件名</th>
												<td><input value="{{ $followup['subject'] }}" type="text" name="followup_subject" /></td>
											</tr>
											<tr>
												<th><font color="#ff0000">※</font>本文
													<div class="mail-comment">
														{$name}<br />　患者様のお名前<br />{$week}<br />　フォローアップの対象週<br />{$followup_url}<br />　フォローアップ問診画面URL<br />{$mailedit_url}<br />　メールアドレス変更画面URL
													</div>
												</th>
											    <td><textarea name="followup_body" >{{ htmlspecialchars($followup['body'],ENT_QUOTES) }}</textarea></td>
											</tr>
										</table>
										<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
									</div>

								</div>
						
														<h3>リマインドメール</h3>
								<div class="mail-input-area">

									<div class="table-wrap">
										<table summary="フォローアップ通知メール">
											<tr>
												<th><font color="#ff0000">※</font>差出人メールアドレス（半角英数）</th>
												<td><input value="{{ $remind['fromaddress'] }}" type="text" name="remind_sender_address" /></td>
											</tr>
											<tr>
												<th><font color="#ff0000">※</font>差出人名</th>
												<td><input value="{{ $remind['fromname'] }}" type="text" name="remind_sender_name" /></td>
											</tr>
											<tr>
												<th><font color="#ff0000">※</font>件名</th>
												<td><input value="{{ $remind['subject'] }}" type="text" name="remind_subject" /></td>
											</tr>
											<tr>
												<th><font color="#ff0000">※</font>本文
													<div class="mail-comment">
														{$name}<br />　患者様のお名前<br />{$week}<br />　フォローアップの対象週<br />{$followup_url}<br />　フォローアップ問診画面URL<br />{$mailedit_url}<br />　メールアドレス変更画面URL
													</div>
												</th>
												<td><textarea name="remind_body" >{{ htmlspecialchars($remind['body'],ENT_QUOTES) }}</textarea></td>
											</tr>
										</table>
										<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
									</div>

								</div>
								
								<?php if (!empty($change_last)) { ?>
										<div class="mail-display-area">

											<div class="table-wrap">
												<table summary="更新情報">
													<tr>
														<th>登録日時</th>
														<td>{{ $change_last['create_datetime'] }}</td>
													</tr>
													<tr>
														<th>最終更新日時</th>
														<td>{{ $change_last['update_datetime'] }}</td>
													</tr>
													<tr>
														<th>最終更新者アカウントID</th>
														<td>{{ $change_last['update_user_id'] }}</td>
													</tr>
												</table>
												<div class="btm"><img src="../img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
											</div>

										</div>
								<?php } ?>
						
												<div style="text-align: center;">
							<input type="submit" name="action_admin_mail_do" value="保存">
						</div>
				</div><!-- /#contentInner -->
			</form>
			<br />
		</div><!-- /#content -->

	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
