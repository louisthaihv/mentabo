<!--{assign var="input_name" value="i_`$choice_data.choice_id`"}-->
{{-- */ $input_name = 'i_'.$choice_data['choice_id']; /* --}}
<!--{assign var="input_layer_name" value="il_`$layer_choice_data.layer_data.layer_id`"}-->
{{-- */ $input_layer_name = 'il_'.$layer_choice_data['layer_data']['layer_id']; /* --}}
<!--{assign var="choice_data_choice_id" value="`$choice_data.choice_id`"}-->
{{-- */ $choice_data_choice_id = $choice_data['choice_id']; /* --}}

<!--{if $choice_data.choice_type eq "radio"}-->
@if($choice_data['choice_type'] == 'radio')
		<input type="radio" id="c_{{ $choice_data['choice_id'] }}" name="{{ $input_layer_name }}" value="{{ $choice_data['choice_id'] }}" @if($input_data[$choice_data['choice_id']] == $choice_data['choice_id']) checked="checked" @endif /><label for="c_{{ $choice_data['choice_id'] }}">{{ $choice_data['choice_string'] }}</label>
@elseif($choice_data['choice_type'] == 'checkbox')
<!--{elseif $choice_data.choice_type eq "checkbox"}-->
		<!--{* フォローアップ問診には当項目は無し *}-->
@elseif($choice_data['choice_type'] == 'date')		
<!--{elseif $choice_data.choice_type eq "date"}-->
		<!--{* フォローアップ問診には当項目は無し *}-->
@elseif($choice_data['choice_type'] == 'time')		
<!--{elseif $choice_data.choice_type eq "time"}-->
		<!--{* フォローアップ問診には当項目は無し *}-->
@elseif($choice_data['choice_type'] == 'datetime')		
<!--{elseif $choice_data.choice_type eq "datetime"}-->
		<!--{* フォローアップ問診には当項目は無し *}-->
@elseif($choice_data['choice_type'] == 'string')		
<!--{elseif $choice_data.choice_type eq "string"}-->
		<!--{if $choice_data.ui_type eq "3"}-->
		@if($choice_data['ui_type'] == 3)
		<input type="text" id="c_{{ $choice_data['choice_id'] }} " name="{{ $input_name }}" value="{{ $input_data[$input_name] }}" class="slider_input" autocomplete="off" readonly="readonly" />
		@else
		<!--{else}-->
		<input type="text" id="c_{{ $choice_data['choice_id'] }} " name="{{ $input_name }}" value="{{ $input_data[$input_name] }}" class="text" autocomplete="off" />
		@endif
		<!--{/if}-->
@elseif($choice_data['choice_type'] == 'numeric')
<!--{elseif $choice_data.choice_type eq "numeric"}-->
		<input type="text" id="c_{{ $choice_data['choice_id'] }}" name="{{ $input_name }}" value="{{ $input_data[$input_name] }}" class="text" onBlur="convert(this)" autocomplete="off" />
@elseif($choice_data.choice_type == 'slider' || $choice_data.choice_type == 'slider_hour' || $choice_data.choice_type == 'slider_minute' || $choice_data.choice_type == 'slider_show_by_step')
<!--{elseif $choice_data.choice_type eq "slider" | $choice_data.choice_type eq "slider_hour" | $choice_data.choice_type eq "slider_minute" | $choice_data.choice_type eq "slider_show_by_step"}-->
		<div id="c_{{ $choice_data['choice_id'] }}_div" class="slider-wrapper">
			<!--{if $choice_data.show_image}-->
			<div class="sliderImage"><img src="{{ URL::to('/') }}/img/choice/{{ $choice_data['choice_id'] }}.png"></div>
			<!--{/if}-->
			<div class="sliderMain">
				<label for="c_{{ $choice_data['choice_id'] }}">{{ $choice_data['choice_string'] }}</label>
			<!--{if $choice_data.lead}-->
			@if( $choice_data.lead)
				<p class="lead">{{ $choice_data['lead'] }}</p>
			@endif	
			<!--{/if}-->
				<img id="c_{{ $choice_data['choice_id'] }}_minus" class="minus" src="{{ URL::to('/') }}/img/metabo_minus.gif">
				<input type="text" id="c_{{ $choice_data['choice_id'] }}_show" class="slider_input" readonly="readonly" />
				<input type="hidden" id="c_{{ $choice_data['choice_id'] }}" name="{{ $input_name }}" />
				<img id="c_{{ $choice_data['choice_id'] }}_plus" class="plus" src="{{ URL::to('/') }}/img/metabo_plus.gif">
				<div id="c_{{ $choice_data['choice_id'] }}_slider"></div>
			</div>
		</div>
@elseif($choice_data.choice_type == 'year' || $choice_data.choice_type == 'month' || $choice_data.choice_type == 'day')
<!--{elseif $choice_data.choice_type eq "year" | $choice_data.choice_type eq "month" | $choice_data.choice_type eq "day"}-->
		<select id="c_{{ $choice_data['choice_id'] }}" name="{{ $input_name }} ">
		<!--{assign var="number" value=`$choice_data.value_min`}-->
		<!--{if $app.input_data[$input_name]}-->
			<!--{assign var="selected_number" value=`$app.input_data[$input_name]`}-->
		<!--{else}-->
			<!--{assign var="selected_number" value=`$choice_data.value_default`}-->
		<!--{/if}-->

		<!--{assign var="loopNum" value=$choice_data.value_max+1}-->
		<!--{section name=number_section start=`$choice_data.value_min` loop=$loopNum}-->
			<option value="<!--{$number}-->" <!--{if $selected_number==$number}-->selected="selected"<!--{/if}-->><!--{$number}--></option>
			<!--{assign var="number" value=$number+1}-->
		<!--{/section}-->
		</select>
		{{ $choice_data['choice_string_unit'] }}
@elseif($choice_data.choice_type == 'tab')
<!--{elseif $choice_data.choice_type eq "tab"}-->
@elseif($choice_data.choice_type == 'accordion')
<!--{elseif $choice_data.choice_type eq "accordion"}-->
@else
<!--{else}-->
@endif
<!--{/if}-->
