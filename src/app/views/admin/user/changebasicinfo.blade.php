@include('../../header')

<!-- ↓↓↓ content ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
<div id="content">
	<h2>基本情報修正</h2>
	<div id="contentInner">
		
	
	<div id="main">
		
		
		<!-- <img src="{{ URL::to('/') }}/img/icon_required.png"/> <font color=red>*</font> は必須項目です。-->
		
		<form  action="basic_info" method="post">
            @if($errors->all('message'))
            <ul class="valid_error">
                @foreach($errors->all('<li>:message</li>') as $message)
                {{ $message }}
                @endforeach
            </ul>
            @endif
		
		
					<div>
						<h3 class="top">ニックネーム</h3>
						<img src="{{ URL::to('/') }}/img/icon_required.png"/> ニックネームを入力してください。<br/><br/>
						<dd><input type="text" name="nick_name" value="{{ $theuser->nick_name }}" size="60" maxlength="45" /></dd><br/>
					</div>
			
					<div>
						<h3 class="top">姓</h3>
						姓を入力してください。<br/><br/>
						<dd><input type="text" name="last_name" value="{{ $theuser->last_name }}" size="60" maxlength="45" /></dd><br/>
					</div>
			
					<div>
						<h3 class="top">名</h3>
						名を入力してください。<br/><br/>
						<dd><input type="text" name="first_name" value="{{ $theuser->first_name }}" size="60" maxlength="45" /></dd><br/>
					</div>
			
					<div>
						<h3 class="top">姓カナ</h3>
						姓をカタカナで入力してください。<br/><br/>
						<dd><input type="text" name="last_name_kana" value="{{ $theuser->last_name_kana }}" size="60" maxlength="45" /></dd><br/>
					</div>
			
					<div>
						<h3 class="top">名カナ</h3>
						名をカタカナで入力してください。<br/><br/>
						<dd><input type="text" name="first_name_kana" value="{{ $theuser->first_name_kana }}" size="60"  maxlength="45" /></dd><br/>
					</div>
					
					<div>
						<h3 class="top">生年月日</h3>
						<img src="{{ URL::to('/') }}/img/icon_required.png"/>（例）1974/3/20 <br/><br/>
						<dd>	<?php
								$current_year = date("Y");
								$c1 = $current_year - 100;
								$c2 = $current_year - 18;
								
								
								
								?>
								<script type="text/javascript">
								   function resetdays(){
									   // find the number of days in the month
									   year =  document.getElementById("year").value;
									   month =  document.getElementById("month").value - 1;
									   var monthStart = new Date(year, month, 1);
									   var monthEnd = new Date(year, month + 1, 1);
									   var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
									   // change the select box day
									   var select = document.getElementById("day");
									   select.options.length = 0;
									  
									   for(i=1; i<=monthLength; i++) {
									   	    select.options[select.options.length] = new Option(i, i);
									   	}
								   }
								</script>
								<select id="year" name="year" onchange="resetdays();" >
									<?php 
									for ($i=$c1; $i<=$c2; $i++){
									?>
									<option value="{{ $i }}" <?php if ($year==$i) echo 'selected="selected"';?> >{{ $i }}</option>
									<?php
									}
									?>
								</select>
								<select id="month" name="month"  onchange="resetdays();" >
									<?php 
									for ($i=1; $i<=12; $i++){
									?>
									<option value="{{ $i }}" <?php if ($month==$i) echo 'selected="selected"';?> >{{ $i }}</option>
									<?php
									}
									?>
								</select>
								<select id="day" name="day">
									<?php 
									// can use cal_days_in_month , but on the server , does not enable calendar , so dont use this way
									//$d=cal_days_in_month(CAL_GREGORIAN,$month,$year);
									?>
								</select>
								<script type="text/javascript">
								    resetdays();
									var select = document.getElementById("day");
									select.options[{{ ($day -1) }}].selected = true;
								</script>
						</dd><br/>
					</div>
					
					<div>
						<h3 class="top">性別</h3>
						<img src="{{ URL::to('/') }}/img/icon_required.png"/> 性別を選択してください。<br/><br/>
						<dd><div class="choices">
								<input type="radio" id="c_10005" name="gender" value="1" <?php if ($theuser->gender=="1") echo 'checked="checked"';?> >
								<label for="c_10005">男性</label>
								<input type="radio" id="c_10006" name = "gender" value="2" <?php if ($theuser->gender=="2") echo 'checked="checked"';?> >
								<label for="c_10006">女性</label>
							</div>
						</dd><br/>
					</div>
					

				<br/>
				
				
				
					<div>
						<center><input type="submit" name="action_user_change" value="保存"></center>
					</div>
			
		</form>
		
		
	</div><!-- /#main -->
	

  </div><!-- /#contentInner -->
</div><!-- /#content -->
<!-- ↑↑↑ content ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ -->



	<hr />
@include('../../footer')
