<div id="wrapper">
<div id="interviewList" class="contents">
	<h2 id="pageTitle">問診結果一覧</h2>
	<hr />

	<!--{include file="admin/error.tpl"}-->
	<div id="main">

		<div class="embossLine search-condition">
			<form  action="" method="post">
			<dl>
				<dt>問診日時</dt>
				<dd>
                    {{ Form::selectRange('interview_y_f', 2011, date('Y'), $input['interview_y_f']) }}月
                    {{ Form::selectRange('interview_m_f', 1, 12, $input['interview_m_f']) }}月
					{{ Form::selectRange('interview_d_f', 1, 31, $input['interview_d_f']) }}日
					{{ Form::selectRange('interview_h_f', 0, 24, $input['interview_h_f']) }}時
					{{ Form::selectRange('interview_i_f', 0, 59, $input['interview_i_f']) }}分
					～
					{{ Form::selectRange('interview_y_t', 2011, date('Y'), $input['interview_y_t']?:date('Y')) }}年
					{{ Form::selectRange('interview_m_t', 1, 12, $input['interview_m_t']?:date('m')) }}月
					{{ Form::selectRange('interview_d_t', 1, 31, $input['interview_d_t']?:date('d')) }}日
					{{ Form::selectRange('interview_h_t', 0, 24, $input['interview_h_t']?:23) }}時
					{{ Form::selectRange('interview_i_t', 0, 59, $input['interview_i_t']?:59) }}分
				</dd>
				<dt>生年月日</dt>
				<dd>
					{{ Form::selectRange('birthday_y_f', 1900, date('Y'), $input['birthday_y_f']?:date('Y')-50) }}年
					{{ Form::selectRange('birthday_m_f', 1, 12, $input['birthday_m_f']) }}月
					{{ Form::selectRange('birthday_d_f', 1, 31, $input['birthday_d_f']) }}日
					～
					{{ Form::selectRange('birthday_y_t', 1900, date('Y'), $input['birthday_y_t']?:date('Y')-18) }}年
					{{ Form::selectRange('birthday_m_t', 1, 12, $input['birthday_m_t']?:date('m')) }}月
					{{ Form::selectRange('birthday_d_t', 1, 31, $input['birthday_d_t']?:date('d')) }}日
				</dd>
				<dt>性別</dt>
				<dd>
					<!-- '1:男性、2：女性 -->
					<label for="gender_1">{{ Form::checkbox('gender[]', '1', $data['gender_1'], array('id'=>'gender_1')) }} 男性</label>
					<label for="gender_2">{{ Form::checkbox('gender[]', '2', $data['gender_2'], array('id'=>'gender_2')) }} 女性</label>
				</dd>
				<dt>ニックネーム</dt>
				<dd>{{ Form::text('name', $input['name']) }}(部分一致)</dd>
				<dt>問診タイプ</dt>
				<dd>
                    <label for="section_set_id_1">{{ Form::checkbox('section_set_id[]', '34', $data['section_set_id_1'], array('id'=>'section_set_id_1')) }} 生活習慣診断 超簡易版</label>
                    <label for="section_set_id_2">{{ Form::checkbox('section_set_id[]', '33', $data['section_set_id_2'], array('id'=>'section_set_id_2')) }} 生活習慣診断 簡易版 </label>
                    <label for="section_set_id_3">{{ Form::checkbox('section_set_id[]', '32', $data['section_set_id_3'], array('id'=>'section_set_id_3')) }} 生活習慣診断 通常版</label><br>
 					<label for="section_set_id_4">{{ Form::checkbox('section_set_id[]', '31', $data['section_set_id_4'], array('id'=>'section_set_id_4')) }} 生活習慣診断 PRO版</label>
                    <label for="section_set_id_5">{{ Form::checkbox('section_set_id[]', '35', $data['section_set_id_5'], array('id'=>'section_set_id_5')) }} 生活習慣診断 メンタル版</label>
				</dd>
				
				{{--
				<dt>フォローアップ経過週</dt>
				<dd>
					{{ Form::select('followup_w_f', array('' => '---', '2' => '2', '4' => '4', '6' => '6', '8' => '8', '12' => '12', '24' => '24', '36' => '36', '48' => '48')) }} 週
					～
					{{ Form::select('followup_w_t', array('' => '---', '2' => '2', '4' => '4', '6' => '6', '8' => '8', '12' => '12', '24' => '24', '36' => '36', '48' => '48')) }} 週 
				</dd>
				<dt>フォローアップ診断日時</dt>
				<dd>
					{{ Form::selectRange('followup_y_f', 2011, date('Y'), $input['followup_y_f']) }}年
					{{ Form::selectRange('followup_m_f', 1, 12, $input['followup_m_f']) }}月
					{{ Form::selectRange('followup_d_f', 1, 31, $input['followup_d_f']) }}日
					{{ Form::selectRange('followup_h_f', 0, 24, $input['followup_h_f']) }}時
					{{ Form::selectRange('followup_i_f', 0, 59, $input['followup_i_f']) }}分
					～
					{{ Form::selectRange('followup_y_t', 2011, date('Y'), $input['followup_y_t']?:date('Y')) }}年
					{{ Form::selectRange('followup_m_t', 1, 12, $input['followup_m_t']?:date('m')) }}月
					{{ Form::selectRange('followup_d_t', 1, 31, $input['followup_d_t']?:date('d')) }}日
					{{ Form::selectRange('followup_h_t', 0, 24, $input['followup_h_t']?:23) }}時
					{{ Form::selectRange('followup_i_t', 0, 59, $input['followup_i_t']?:59) }}分
				</dd>
                --}}
				<p></p>
			</dl>
			<input type="hidden" name="type" value="search" style="clear:both;">
			<p><input type="submit" name="action_admin_interview_list" value="検索" class="input-btn"></p>
			</form>
		</div><!-- /.embossLine -->

		<div>データ数：{{ count($interview_list) }}</div>
		<div class="page">{{ $interview_list->links() }}</div>
		<br>
		<form  action="" method="post">
		<div class="table-wrap">
			<table summary="問診結果一覧">
				<thead>
					<tr>
						<th class="column1"></th>
						<th class="column2">問診ID</th>
						<th class="column3">問診タイプ</th>
						<th class="column4">問診日時</th>
						<th class="column5">ニックネーム</th>
						<th class="column6">生年月日</th>
						<th class="column7">性別</th>
						<th class="column7">経過週</th>
						<!--{if $app.foodCsvFlg != false}-->
						<th class="column8"></th>
						<!--{/if}-->
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="column1"></th>
						<th class="column2">問診ID</th>
						<th class="column3">問診タイプ</th>
						<th class="column4">問診日時</th>
						<th class="column5">ニックネーム</th>
						<th class="column6">生年月日</th>
						<th class="column7">性別</th>
						<th class="column7">経過週</th>
						<!--{if $app.foodCsvFlg != false}-->
						<th class="column8"></th>
						<!--{/if}-->
					</tr>
				</tfoot>
				<tbody>
					@if($interview_list)
						
						@forelse($interview_list as $key=>$value)
							<tr>
								<!--{assign var="url" value="`$config.admin_absolute_url`?action_admin_interview_detail=true&interview_id=`$value.interview_id`"}-->
								<td class="column1 center"><input type="button" value="詳細" onClick="location.href='{{ URL::to('/') }}/admin/interview/{{ $value['interview_id'] }}'" class="input-btn"></td>
								<td class="column2">{{ $value['interview_id'] }}<!--{$value.interview_id}--></td>
								<td class="column3">{{ $value['section_set_name'] }}<!--{$session.section_set_list[$value.section_set_id].section_set_name}--></td>
								<td class="column4 center">{{ $value['finished_datetime'] }}<!--{$value.finished_datetime|escape|strtotime|date_format:"%Y/%m/%d %H:%M:%S"}--></td>
								<td class="column5">{{ $value['nick_name'] }}<!--{$value.name}--></td>
								<td class="column6 center">{{ $value['birth_date'] }}<!--{$value.birth_date|escape|strtotime|date_format:"%Y/%m/%d"}--></td>
								<td class="column7 center">{{{ isset($value['gender']) ? '男性' : '女性' }}}<!--{$config.gender_j[$value.gender]}--></td>
								<td class="column7 center">{{ @$data['week'][$key]["newest_week"] }}<!--{$value.newest_week}--></td>
								<!--{assign var="url" value="`$config.admin_absolute_url`?action_admin_interview_csv_food=true&interview_id=`$value.interview_id`"}-->
								<!--{if $app.foodCsvFlg != false}-->
								<td class="column8 center"><input type="button" value="食事DL" onClick="location.href='{{ URL::to('/') }}/admin/interview/download/{{ $value['interview_id'] }}'" class="input-btn"></td>
								<!--{/if}-->
							</tr>
						@empty	
							<tr>
								<td colspan="8">データがありません</td>
							</tr>
						@endforelse	

					@else	
						<tr>
							<td colspan="8">データがありません</td>
						</tr>
					@endif	
				</tbody>
			</table>

			<div class="btm"><img src="{{ URL::to('/') }}/img/admin/common/box904_bg_btm.gif" width="904" height="5" alt="" /></div>
		</div>
		</form>
		<div>データ数：{{ count($interview_list) }}</div>
		<div class="page">{{ $interview_list->links() }}</div>

	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<style type="text/css">
.page {
	text-align: left;
}
.pagination li {
	float: left;
	padding: 10px;
}
</style>
