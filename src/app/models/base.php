<?php

class Base extends Eloquent {
	
	public static function selectUser($user_id) {
		return DB::table('t_user')->where('user_id', $user_id)->first();
	}

	public static function selectAccount($user_id) {
		return DB::table('t_account')->Where('user_id', $user_id)->first();
	}

	public static function selectInterview($user_id) {
		return DB::table('t_user')->join('t_interview', 't_user.user_id', '=', 't_interview.user_id')->where('t_user.user_id', $user_id)->orderBy('interview_id', 'desc')->first(array('*', 't_user.tenant_id', 't_user.email', 't_user.gender', 't_user.birth_date'));
		//return DB::table('t_interview')->Where('user_id', $user_id)->orderBy('interview_id', 'desc')->first();
	}

	public static function insertInterview($section_set_id, $user_id, $interview_type) {
		return DB::table('t_interview')->insertGetId(array('user_id' => $user_id, 'section_set_id' => $section_set_id, 'interview_type' => $interview_type, 'create_datetime' => date('Y-m-d H:i:s')));
	}

	public static function sectionSet($sectionSetId) {
		return DB::table('m_section_set')->where('section_set_id', $sectionSetId)->first();
	}

	public static function getCommonData($param = null) {
		$list = array();
		$db = DB::table('m_section_set_contents')->where('section_set_id', '=', $param['section_set_id'])->where('section_id', '=',  $param['section_id'])->get();
		foreach ($db as $key => $row) {
			$list[] = $row;
		}
		return $list;
	}	

	public static function getSectionSetContentByBlockParty($section_set_id = null, $param = array(), $selectUser = array())
	{	
		return DB::table('m_section_set_contents')
		    ->join('m_section', 'm_section_set_contents.section_id', '=', 'm_section.section_id')
			->where('m_section_set_contents.section_set_id', $section_set_id)
			->where( function ($query) use ($selectUser) { $query->whereNull('target_gender')->orWhere( 'target_gender', '=', $selectUser['gender']); }) 
			->where( function ($query) use ($selectUser) { $query->whereNull('target_age_from')->orWhere( 'target_age_from', '<=', $selectUser['years_old']); })
			->where( function ($query) use ($selectUser) { $query->whereNull('target_age_to')->orWhere( 'target_age_to', '>=', $selectUser['years_old']); })
			->where( function ($query) use ($param) {

			    if (isset($param['flg_display'])) $query->where('flg_display', $param['flg_display']);
			    if (isset($param['section_type'])) $query->where('section_type', $param['section_type']);
			    if (isset($param['display_position'])) $query->where('display_position', $param['display_position']);
			    if (isset($param['block_id'])) $query->where('block_id', $param['block_id']);
			    if (isset($param['party_id'])) $query->where('party_id', $param['party_id']);

			})
			->select('m_section_set_contents.section_set_id', 'm_section_set_contents.section_id', 'm_section_set_contents.page_num', 'm_section_set_contents.order_section', 'm_section.section_string', 'm_section_set_contents.block_id', 'm_section_set_contents.party_id')
		   // ->orderBy('order_section', 'asc')
		    ->get();
		    //->toSql();
	}

	public static function getSectionSetContentBlockParty($section_set_id = null, $param = array())
	{
		$select_col = (isset($param['block_id']) != '') ? 'party_id' : 'block_id';
		$return = DB::table('m_section_set_contents')->select($select_col)
				->where('section_set_id', $section_set_id)
				->where('flg_display', $param['flg_display'])
				->where('section_type', $param['section_type'])
				->where( function ($query) use ($param) {
				    if (isset($param['display_position'])) $query->where('display_position', $param['display_position']);
				    if (isset($param['block_id'])) $query->where('block_id', $param['block_id']);
				})
				->select(DB::raw('count(*) as cnt, '.$select_col))
				->groupBy($select_col)
				->get();
		
		foreach ($return as $key => $value) {
			$list['id'][] = $value[$select_col];
			$list['count'][$value[$select_col]] = $value['cnt'];
		}
		return $list;
	}

	public static function getSectionRow($getRowId)
	{	
		return DB::table('m_section')->where('section_id', $getRowId)->first();
	}

	public static function getLayerList($getRowId)
	{	
		return DB::table('m_layer')->where('section_id', $getRowId)->get();
	}

	public static function getLayerInclusion($layerId)
	{	
		return DB::table('m_layer_inclusion')->where('layer_id', $layerId)->get();
	}

	public static function getLayerControlByChoice($layerId)
	{	
		return DB::table('m_layer_control_by_choice')->where('layer_id', $layerId)->where('control_type', 'show')->orderBy('group_no', 'asc')->get();
	}

	public static function getChoiceList($getRowId)
	{	
		return DB::table('m_choice')->where('section_id', $getRowId)->orderBy('order_in_layer', 'asc')->get();
	}

	public static function judgeByInterviewId($interview_id)
	{
		return DB::table('t_choice_selection')
        ->join('m_choice', function($join) use ($interview_id)
        {
            $join->on('t_choice_selection.choice_id', '=', 'm_choice.choice_id')->where('t_choice_selection.interview_id', '=', $interview_id);
        })
        ->get();
	}

	public static function insertChoiceSelection($interview_id, $rows)
	{
		foreach ($rows as $data) {
		
			$string['interview_id'] = $interview_id;

			if(isset($data['choice_id'])) {
				$string['choice_id'] = $data['choice_id'];
			} else {
				$string['choice_id'] = '0';	
			}
				
			if (isset($data['selection'])) {
				$string['selection'] = 1;
			} else {
				$string['selection'] =  0;
			}	

			if (isset($data['input_float'])) {
				$string['input_float'] = $data['input_float'];
			} else {
				$string['input_float'] = 'null';
			}

			if (isset($data['input_datetime'])) {
				$string['input_datetime'] = $data['input_datetime'];
			} else {
				$string['input_datetime'] = 'null';
			}

			if (isset($data['input_string'])) {
				$string['input_string'] = $data['input_string'];
			} else {
				$string['input_string'] = 'null';
			}

			DB::table('t_choice_selection')->insert($string);
		}	
	}

	public static function update_interview($where, $update) {

		return DB::table('t_interview')->where($where)->update($update);
	}

}	
