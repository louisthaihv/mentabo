@include('admin/header')
<hr />
<div id="wrapper">
<div id="uploadLogo" class="contents">
	
	<div class="buttonArea">
		<ul>
			<li class="btn_back"><a href="Javascript:void(0)" onclick="history.back();return false;">一覧へ戻る</a></li>
		</ul>
	</div>
	
	<h2 id="pageTitle">Upload logo for tenant {{ $tenant->tenant_name }} </h2>
	<hr />
    
	<div id="main">
    Current logo: 
	<?php if ($exist) { ?>
	  <img src="../showfiletenant/{{ $tenant->token }}" />
	<?php } else 
	  echo "No current logo."
	?>
	  <br/> <br/>
        <form action="{{ $tenant->token }}" method="POST" enctype="multipart/form-data">
            Choose an image file <input type="file" name="uploaded_file" accept="image/*" ><br>
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="submit" value="Upload logo">
        </form>


	</div><!-- /#main -->
</div><!-- /.contents -->
</div><!-- /#wrapper-->

<hr />

@include('admin/footer')
