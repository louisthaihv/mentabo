$(function(){
	
	if($(".doctorView").size() > 0){
	
		$('.choice').hover( function() {
			$(this).css("cursor","pointer");
		});
	
		$('.choice').click( function() {
			
			//窓サイズ取得
			$('body').css('overflow',"hidden");
			var window_w = $(window).width();
			var window_half = window_w / 2 ;
			var dialog_half = $('#popup-dialog').css("width") / 2;
			var dialog_pos = window_half - dialog_half;
			$('#popup-dialog').css("left" , dialog_half + "px");
			
			//高さもなんとかする
			var scroll_h = $(window).scrollTop();
			var window_h = $(window).height();
			$('#popup-dialog').css("top" , scroll_h + 50 + "px");
			
			$('#loading').css("display" , "block");
			
			var param = $(this).attr("id").split("_");
			
			if(param[2]=='email') {
				var loc = "../interview?interview_id=" + param[1] + "&section_id=" + param[2];
			}else{
				var loc = "../interview?interview_id=" + param[1] + "&section_id=" + param[2] + "&user_id=" + param[3];
			}
			

			$('#popup-dialog').show().find('#iframeContainer').html('<iframe id="popup-iframe" src="' + loc + '"></iframe>');
			$('#loading').css("height" , window_h - 246 + "px");
			/*$('#popup-iframe').css("display" , "none");*/
			$('#popup-iframe').css("height" ,  "1px");
			//オーバーレイ
			$('#dialog-overlay').fadeTo(400, 0.8);
			
			return false; 
		});
	
		//クリック
		//閉じるイベント
		$('#popup-dialog .close').click(function() {
				$('body').css('overflow',"auto");
		    //追加したiframeのDOMを削除
		    $('#popup-dialog').fadeOut('normal', function() {
		        $('iframe', this).remove();
		    });
		    //オーバーレイを削除
		    $('#dialog-overlay').hide();
		    return false;
		});
		
		
		
		
	}
	
	if($("#detailedit").size() > 0){
			$(window).load(function() {
				var h = parent.$('#loading').css("height");
				parent.$('#loading').fadeOut('fast', function() {
					parent.$("#popup-iframe").fadeIn('normal');
					parent.$("#popup-iframe").css('height',h);
			 });
			});
	
			$("#btn_cancel").click(function() {
				parent.$('body').css('overflow',"auto");
		    //追加したiframeのDOMを削除
		    parent.$('#popup-dialog').fadeOut('normal', function() {
		        parent.$('iframe', this).remove();
		    });
		    //オーバーレイを削除
		    parent.$('#dialog-overlay').hide();
		    return false;
			});
	}
	
	if($("#detailcompletion").size() > 0){
		$(window).load(function() {
				parent.$('body').css('overflow',"auto");
		    //追加したiframeのDOMを削除
		    parent.$('#popup-dialog').fadeOut('normal', function() {
		        parent.$('iframe', this).remove();
		    });
		    parent.location.reload();
		    return false;
		});
		
	}
	
});

