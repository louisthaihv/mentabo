<?php

class BaseController extends Controller {


	public function loggedin(){

		if ( \Auth::tuser()->check() ) {
			return true;
		} else {
			return false; 
		}
		
    }

	public function loggedin1(){
	  if ( \Auth::user()->check() ) {
		  return true;
	  }
	  else return false; 
    }

	public function selectUser($user_id)
	{	
		return Base::selectUser($user_id);
	}

	public function selectAccount($user_id)
	{	
		return Base::selectAccount($user_id);
	}

	public function selectInterview($user_id)
	{	
		return Base::selectInterview($user_id);
	}

	public function insertInterview($section_set_id, $user_id, $interview_type)
	{	
		return Base::insertInterview($section_set_id, $user_id, $interview_type);
	}

	public function getCommonData($param = null)
	{	
		return Base::getCommonData($param);
	}

	public function insertGuidance($user_id)
	{	
		//ガイダンス登録用の判定取得
		$InterviewUser = Followup::InterviewUser($user_id);
		$guidanceData = $this->judgeByInterviewId($InterviewUser, '');
		$guidanceData = $guidanceData['guidance'];
		$guidanceData['interview_id'] = $InterviewUser['interview_id'];

		$deleteGuidance = Followup::deleteGuidance($InterviewUser['interview_id']);
		$insertGuidance = Followup::insertGuidance($guidanceData);
	}

	protected function sectionSet($section_set_id)
	{
		switch ($section_set_id) {
		    case 4:
		    case 31:
		       $type['type'] = 4;
		       $type['file'] = 'full';
		       $type['sectionSetId'] = 31;
		       // 生活習慣診断 PRO版
		       break;
		    case 3:
		    case 32:
		       $type['type'] = 3;
		       $type['file'] = 'basic';
		       $type['sectionSetId'] = 32;
		       // 生活習慣診断 通常版
		       break;
		    case 2:
		    case 33:
		       $type['type'] = 2;
		       $type['file'] = 'simple';
		       $type['sectionSetId'] = 33;
		       // 生活習慣診断 簡易版
		       break;
		    case 1:
		    case 34:
		       $type['type'] = 1;
		       $type['file'] = 'light';
		       $type['sectionSetId'] = 34;
		       // 生活習慣診断 超簡易版
		       break;
		    case 5:
		    case 35:
		       $type['type'] = 5; 
		       $type['file'] = 'light';
		       $type['sectionSetId'] = 35;
		       // 生活習慣診断 メンタル版
		       break;
		}
		
		$sectionSet = Base::sectionSet($type['sectionSetId']);
		return array_merge($type, $sectionSet);
	}

	public function getSectionSetContentByBlockParty($section_set_id = null, $param, $selectUser)
	{
		return Base::getSectionSetContentByBlockParty($section_set_id, $param, $selectUser);
	}

	public function getSectionSetContentBlockParty($section_set_id = null, $param = array())
	{
		return Base::getSectionSetContentBlockParty($section_set_id, $param);
	}

	public function sectionSetByPage($section_set_by_page, $step_id, $input_raw_data = null)
	{
		$count = count($section_set_by_page);
		$step_id = empty($step_id)? 1: $step_id;

		if (!empty($section_set_by_page[$step_id])) {

			//表示用の設問情報を取得
			$section_list = array();
			$layer_list = array();
			$choice_list = array();
			foreach ($section_set_by_page[$step_id] as $section_id) {
				list($section_data, $layer_list_show) = $this->getSectionDataToShowById($section_id);
				$section_list[] = $section_data;
				$layer_list += $layer_list_show;
				$choice_list = $choice_list +  $this->getChoiceType($section_data);
			}

			$jquery_display = $this->createJqueryDisplay($section_list, $input_raw_data); // 表示非表示のJquery生成
			$jquery_ui = $this->createJqueryUI($section_list, $input_raw_data); // UIのjQuery生成		
			list($jquery_requiredCheck, $jquery_regexpCheck) = $this->createJqueryInputCheck($section_list); // //入力チェックのjQuery生成
			
			return array(
						'section_list' => $section_list,
						'layer_list' => $layer_list,
						'choice_list' => $choice_list,
						'jquery_display' => $jquery_display,
						'jquery_ui' => str_replace('\t', '', $jquery_ui),
						'jquery_requiredCheck' => $jquery_requiredCheck,
						'jquery_regexpCheck' => $jquery_regexpCheck
			);
		}
	}	

	public function getSectionDataToShowById($section_id)
	{	
		
		//設問データ取得
		$section_data = Base::getSectionRow($section_id);
		// if (Ethna::isError($section_data)) {
		// 	return $section_data;
		// }

		//設問に対するレイヤー情報を一気に取得
		$layer_list = Base::getLayerList($section_id);
		// if (Ethna::isError($layer_list)) {
		// 	return $layer_list;
		// }

		$layer_inclusion_list = array();
		$layer_ctrl_list = array();
		$layer_list_show = array(); //setAppNE用
		foreach ($layer_list as $layer_data) {
			//レイヤー包括情報を取得
			$layer_inclusion_list_temp = Base::getLayerInclusion($layer_data['layer_id']);
			// if (Ethna::isError($layer_inclusion_list_temp)) {
			// 	return $layer_inclusion_list_temp;
			// }
			if (!empty($layer_inclusion_list_temp)) {
				//$layer_inclusion_list[$layer_data['layer_id']] = $layer_inclusion_list_temp;	//レイヤーIDをキーに保存
				foreach ($layer_inclusion_list_temp as $layer_inclusion_data) {
					$layer_inclusion_list[] = $layer_inclusion_data;	//統合
				}
			}

			//レイヤー表示条件情報を取得
			$layer_ctrl_list_temp = Base::getLayerControlByChoice($layer_data['layer_id']);
			// if (Ethna::isError($layer_ctrl_list_temp)) {
			// 	return $layer_ctrl_list_temp;
			// }
			if (!empty($layer_ctrl_list_temp)) {
				//レイヤー表示条件情報をグループ番号に分類
				$layer_ctrl_list_key_group_no = array();
				foreach ($layer_ctrl_list_temp as $layer_ctrl_data) {
					$layer_ctrl_list_key_group_no[$layer_ctrl_data['group_no']][] = $layer_ctrl_data;	//グループ番号ごとのレイヤー表示条件一覧
				}
				$layer_ctrl_list[$layer_data['layer_id']] = $layer_ctrl_list_key_group_no;	//レイヤーIDをキーに保存
			}

			$layer_list_show[$layer_data['layer_id']]['annotation'] = $layer_data['annotation'];
			$layer_list_show[$layer_data['layer_id']]['help_text'] = $layer_data['help_text'];
		}

		//レイヤー包括情報があれば、木構造に変換
		if (!empty($layer_inclusion_list)) {
			$layer_inclusion_tree = $this->createInclusionTree($layer_inclusion_list);
		}

		//設問に対する選択肢情報を一気に取得
		$choice_list = Base::getChoiceList($section_id);
		// if (Ethna::isError($choice_list)) {
		// 	return $choice_list;
		// }
		//画像が存在するならばshow_imageフラグを立て
		foreach ($choice_list as $idx => $choice_data) {
			if (file_exists($_SERVER['DOCUMENT_ROOT'].'/img/choice/'.$choice_data['choice_id'] . '.png')) {
				$choice_list[$idx]['show_image'] = 1;
			}
		}

		//選択肢をレイヤーIDに分類
		$choice_list_key_layer_id = array();
		foreach ($choice_list as $choice_data) {
			$choice_list_key_layer_id[$choice_data['layer_id']][] = $choice_data;	//レイヤーIDごとの選択肢一覧
		}

		//まとめ
		$layer_choice_list = array();
		foreach ($layer_list as $idx => $layer_data) {

			//if($layer_data['layer_id'] != 238 ) {
				$layer_choice_list[$idx]['layer_data'] = $layer_data;

				if(isset($choice_list_key_layer_id[$layer_data['layer_id']])) $layer_choice_list[$idx]['choice_list'] = $choice_list_key_layer_id[$layer_data['layer_id']];

				if (!empty($layer_ctrl_list[$layer_data['layer_id']])) {
					$layer_choice_list[$idx]['layer_control_by_choice_list'] = $layer_ctrl_list[$layer_data['layer_id']];
				}
			//}
		}

		if (!empty($layer_inclusion_tree)) {
			$section_data['layer_inclusion_tree'] = $layer_inclusion_tree;
			//layer_choice_listをレイヤーIDベースの配列に詰め代え
			foreach ($layer_choice_list as $data){
				$layer_choice_list_swapped[$data['layer_data']['layer_id']] = $data;
			}
			$layer_choice_list = $layer_choice_list_swapped;
		}

		$section_data['layer_choice_list'] = $layer_choice_list;
		return array($section_data, $layer_list_show);

	}

	//レイヤーの包括関係の配列を作成する
	public function createInclusionTree($layer_inclusion_list)
	{
		$node = array();
		//詰め替え
		foreach ($layer_inclusion_list as $layer_inclusion_data) {
			$node[$layer_inclusion_data['layer_id_upper']][$layer_inclusion_data['layer_id']] =& $node[$layer_inclusion_data['layer_id']];
			$joined_node[] = $layer_inclusion_data['layer_id'];
		}
		//不要データをトリミングして値渡しコピー
		foreach ($node as $layer_id => $data_arr) {
			if (!in_array($layer_id, $joined_node)) {
				$inclusion_tree[$layer_id] = $data_arr;
			}
		}
		return $inclusion_tree;
	}

	//  choice_type の タグ用リスト取得
	public function getChoiceType($section_data){
		$res = array();
		foreach($section_data["layer_choice_list"] as $choice_list){
			foreach($choice_list["choice_list"] as $key => $val){
				$res[$val["choice_id"]]["type"] = $val["choice_type"];
				$res[$val["choice_id"]]["layer_id"] = $val["layer_id"];
			}
		}
		return $res;
	}

	//  表示非表示のJquery生成
	public function createJqueryDisplay($section_list, $input_raw_data) {
		
		$jquery ='';
		foreach ($section_list as $key1 => $section_data) {
			foreach ($section_data['layer_choice_list'] as $key2 => $layer_choice_data) {

				if(isset($layer_choice_data['layer_control_by_choice_list'])) $layer_control_by_choice_list = $layer_choice_data['layer_control_by_choice_list'];
				else $layer_control_by_choice_list = '';

				if ($layer_control_by_choice_list) {
					$i = 0;
					foreach ($layer_control_by_choice_list as $group_no => $lcbc_list) {
						$count = count($layer_control_by_choice_list[$group_no]);
						$i++;
						if ($i == 1) {
							$jquery .= "if(";
						} else {
							$jquery .= " & ";
						}

						$j = 0;
						foreach ($lcbc_list as $idx_lcbc => $layer_control_by_choice_data) {
							$j++;
							if ($j == 1) {
							} elseif ($layer_control_by_choice_data['all_or_every'] === "all") {
								$jquery .= " & ";
							} elseif ($layer_control_by_choice_data['all_or_every'] === "every") {
								$jquery .= " | ";
							}
							$jquery .= "$(\"#c_".$layer_control_by_choice_data['choice_id_upper'].":checked\").val() == '".$layer_control_by_choice_data['choice_id_upper']."'";
						}
					}
					$reset = "";
					foreach ($layer_choice_data['choice_list'] as $input_name => $choice_data) {
						$choice_id = $choice_data['choice_id'];
						$choice_type = $choice_data['choice_type'];
						$value_default = $choice_data['value_default'];

						if(isset($input_raw_data["i_" . $choice_data['choice_id']]) && count($input_raw_data["i_" . $choice_data['choice_id']]) > 0 ){
							$value_default = $input_raw_data["i_" . $choice_data['choice_id']];
						}
						
						$unit = $choice_data['choice_string_unit'];
						switch ($choice_type) {
							case "radio":
							case "checkbox":
							case "date":
							case "time":
							case "datetime":
							case "tab":
							case "accordion":
								break;
							case "string":
							case "numeric":
								if (empty($value_default)) {
									$value = "";
								} else {
									$value = $value_default;
								}
								$reset .= "$(\"#c_".$choice_id."\").val(\"".$value."\");";
								break;
							case "slider":
								//$unit = '';
								if(isset($input_raw_data[$input_name])) $value_default = $input_raw_data[$input_name];
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"".$value_default.$unit."\");";
								$reset .= "$(\"#c_".$choice_id."\").val(\"".$value_default."\");";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\",String(".$value_default."));";
								break;
							case "slider_hour":
								if ($choice_data['ui_type'] == "2") {
									$unit = "時間";
								} else {
									$unit = "時";
								}

								$floor_value = floor($value_default);
								if ($floor_value != $value_default) {
									$unit .= "半";
								}
								//$unit = '';
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"".$floor_value.$unit."\"); ";
								$reset .= "$(\"#c_".$choice_id."\" ).val(".$value_default."); ";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(".$value_default.")); ";
								break;
							case "slider_minute":
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(".$value_default.") + \"時間\" + cal_min(".$value_default.") + \"分\");";
								$reset .= "$(\"#c_".$choice_id."\" ).val(".$value_default."); ";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(".$value_default.")); ";
								break;
							case "slider_show_by_step";
								$value = round($value_default / $choice_data['value_step']) / 10;
								$reset .= "$(\"#c_".$choice_id."_show\").val(\"".$value.$unit."(".$value_default."g)\"); ";
								$reset .= "$(\"#c_".$choice_id."\").val(".$value_default."); ";
								$reset .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(".$value_default.")); ";
								break;
							case "year":
							case "month":
							case "day":
								if (empty($value_default)) {
									$value = "";
								} else {
									$value = $value_default;
								}
								$reset .= "$(\"#c_".$choice_id."\").val(\"".$value."\");";
								break;
							default:
								break;
						}
					}
					$jquery .= ") { ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']."\").show(\"slow\"); ";
					$jquery .= "} else { ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." input\").attr(\"checked\", false); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." input\").button(\"refresh\"); ";
					$jquery .= $reset;
					$jquery .= "$(\"#s_".$section_data['section_id']."\").css(\"background-color\", \"#FFFFFF\"); ";
					$jquery .= "$(\"#s_".$section_data['section_id']."\").css(\"background\", 'url(\"".url()."/img/h3_bg.gif\") no-repeat scroll 0 100% transparent'); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." h3\").css(\"background-color\", \"#FFFFFF\"); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']." h3\").css(\"background\", 'url(\"".url()."/img/h3_bg.gif\") no-repeat scroll 0 100% transparent'); ";
					$jquery .= "$(\"#l_".$layer_choice_data['layer_data']['layer_id']."\").hide(\"slow\"); ";
					$jquery .= "}"."\n";
				}
			}
		}
		return $jquery;
	}
		/**
	 * UIのJquery生成
	 */
	function createJqueryUI($section_list) {
		//通勤時間の合計処理
		$s2034_choice_id_list = array(10164, 10165, 10166, 10423);

        $jquery = "";
		foreach ($section_list as $idx => $section_data) {
			foreach ($section_data['layer_choice_list'] as $idx2 => $layer_choice_data) {
				$layer_type = "";
				foreach ($layer_choice_data['choice_list'] as $idx3 => $choice_data) {
					$input_name = "i_".$choice_data['choice_id'];
					$input_layer_name = "il_".$layer_choice_data['layer_data']['layer_id'];
					$choice_id = $choice_data['choice_id'];
					$choice_type = $choice_data['choice_type'];
					$ui_type = $choice_data['ui_type'];
					switch ($choice_type) {
						case "radio":
						case "checkbox":
						case "date":
						case "time":
						case "datetime":
						case "string":
						case "year":
						case "month":
						case "day":
						case "tab":
						case "accordion":
							break;
						case "slider":
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							$jquery .= "	value: ".$choice_data['value_default'].",";
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	slide: function( event, ui ) {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + ui.value + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(ui.value);";
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"".$choice_data['choice_string_unit']."\" );";
							$jquery .= "$(\"#c_".$choice_id."\").val($( \"#c_".$choice_id."_slider\").slider(\"value\"));";

							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\",String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\",String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

/*
							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
							$jquery .= "amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider(\"value\",String(amount));";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
							$jquery .= "amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + String(amount) + \"".$choice_data['choice_string_unit']."\");";
							$jquery .= "});";
*/
							$jquery .= "\n";
							break;
						case "slider_hour":
							if ($ui_type == "2") {
								$unit = "時間";
							} else {
								$unit = "時";
							}
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							$jquery .= "	value: ".$choice_data['value_default'].",";
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	slide: function( event, ui ) {";
							$jquery .= "		if(Math.floor(ui.value)==ui.value){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + Math.floor(ui.value) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + Math.floor(ui.value) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(ui.value);";
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\" ).val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"".$unit."\");";
							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";

							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		if(Math.floor(amount)==amount){";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "		} else {";
							$jquery .= "			$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "		}";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

/*
							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
							$jquery .= "	amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "	$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	if(Math.floor(amount)==amount){";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "	} else {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
							$jquery .= "	amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "	$(\"#c_".$choice_id."\").val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	if(Math.floor(amount)==amount){";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."\");";
							$jquery .= "	} else {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + String(Math.floor(amount)) + \"".$unit."半\");";
							$jquery .= "	}";
							$jquery .= "});";
*/
							$jquery .= "\n";
							break;
						case "slider_minute":
							//通勤時間の１日合計算出
							if ($choice_id == 10423) {
								$jquery .= "function sum_s_2034() {";
								$jquery .= "	amount = Math.max(0, Math.round((parseFloat($(\"#c_10164\").val()) + parseFloat($(\"#c_10165\").val()) + parseFloat($(\"#c_10166\").val()) + parseFloat($(\"#c_10423\").val())) * 10) / 10); ";
								$jquery .= "	$(\"#c_10448\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
								$jquery .= "};";
								$jquery .= "\n";
								$jquery .= "$(\"#c_10448\").val(\"2時間00分\");";
								$jquery .= "\n";
							}
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							$jquery .= "	value: ".$choice_data['value_default'].",";
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	slide: function( event, ui ) {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(ui.value) + \"時間\" + cal_min(ui.value) + \"分\");";
							$jquery .= "		$(\"#c_".$choice_id."\").val(ui.value);";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour($(\"#c_".$choice_id."_slider\").slider(\"value\")) + \"時間\" + cal_min($(\"#c_".$choice_id."_slider\").slider(\"value\")) + \"分\");";
							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";

							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
								$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

/*
							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
							$jquery .= "	amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount) );";
							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
							$jquery .= "	amount = Math.min(".$choice_data['value_max'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) + ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + cal_hour(amount) + \"時間\" + cal_min(amount) + \"分\" );";
							if (in_array($choice_id, $s2034_choice_id_list)) {
									$jquery .= "	sum_s_2034();";
							}
							$jquery .= "});";
*/
							$jquery .= "\n";
							break;
						case "slider_show_by_step":
							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
							$jquery .= "	range: \"min\",";
							$jquery .= "	value: ".$choice_data['value_default'].",";
							$jquery .= "	min: ".$choice_data['value_min'].",";
							$jquery .= "	max: ".$choice_data['value_max'].",";
							$jquery .= "	step: ".$choice_data['value_step'].",";
							$jquery .= "	slide: function(event, ui) {";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(ui.value / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + ui.value + \"g)\");";
							$jquery .= "		$(\"#c_".$choice_id."\").val(ui.value);";
							$jquery .= "	}";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") / ".$choice_data['value_step']." / 10 + \"".$choice_data['choice_string_unit']."(\"  + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"g)\");";
							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";

							$jquery .= "new long_action( $(\"#c_".$choice_id."_minus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "		if (" . $choice_data['value_min'] . " > amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;

							$jquery .= "new long_action( $(\"#c_".$choice_id."_plus\"), {" ;
							$jquery .= "	onClick: function( ) {" ;
							$jquery .= "		amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "	},";
							$jquery .= "	onLong: function( ) {" ;
							$jquery .= "		amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "		if (" . $choice_data['value_max'] . " < amount) return ; " ;
							$jquery .= "		$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "	}" ;
							$jquery .= "}) ;" ;
/*
							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
							$jquery .= "	amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "});";
							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
							$jquery .= "	amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
							$jquery .= "});";
*/
							$jquery .= "\n";
//							$jquery .= "$(\"#c_".$choice_id."_slider\").slider({";
//							$jquery .= "	range: \"min\",";
//							$jquery .= "	value: ".$choice_data['value_default'].",";
//							$jquery .= "	min: ".$choice_data['value_min'].",";
//							$jquery .= "	max: ".$choice_data['value_max'].",";
//							$jquery .= "	step: ".$choice_data['value_step'].",";
//							$jquery .= "	slide: function(event, ui) {";
//							$jquery .= "		$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(ui.value / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + ui.value + \"g)\");";
//							$jquery .= "		$(\"#c_".$choice_id."\").val(ui.value);";
//							$jquery .= "	}";
//							$jquery .= "});";
//							$jquery .= "$(\"#c_".$choice_id."_show\").val(\"\" + $(\"#c_".$choice_id."_slider\").slider(\"value\") / ".$choice_data['value_step']." + \"".$choice_data['choice_string_unit']."(\"  + $(\"#c_".$choice_id."_slider\").slider(\"value\") + \"g)\");";
//							$jquery .= "$(\"#c_".$choice_id."\").val($(\"#c_".$choice_id."_slider\").slider(\"value\"));";
//							$jquery .= "$(\"#c_".$choice_id."_minus\").click(function(){";
//							$jquery .= "	amount = Math.max(".$choice_data['value_min'].", Math.round((parseFloat($(\"#c_".$choice_id."\").val()) - ".$choice_data['value_step'].") * 10) / 10);";
//							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
//							$jquery .= "});";
//							$jquery .= "$(\"#c_".$choice_id."_plus\").click(function(){";
//							$jquery .= "	amount = Math.round((parseFloat($(\"#c_".$choice_id."\" ).val()) + ".$choice_data['value_step'].") * 10) / 10;";
//							$jquery .= "	$(\"#c_".$choice_id."\" ).val(String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_slider\").slider(\"value\", String(amount));";
//							$jquery .= "	$(\"#c_".$choice_id."_show\").val(\"\" + Math.round(amount / ".$choice_data['value_step'].") / 10 + \"".$choice_data['choice_string_unit']."(\" + amount + \"g)\" );";
//							$jquery .= "});";
//							$jquery .= "\n";
							break;
						default:
							break;
					}

				}
			}
		}
		return $jquery;
	}


	// 入力チェックのJquery生成
	public function createJqueryInputCheck($section_list) {
		
		$jquery_required = "";
		$jquery_regexp = "";
		foreach ($section_list as $key1 => $section_data) {
			if ($section_data['flg_must_input'] == 1) {
				foreach ($section_data['layer_choice_list'] as $key2 => $layer_choice_list) {
					if (!empty($layer_choice_list['choice_list'])) {
						$i = 0;
						foreach ($layer_choice_list['choice_list'] as $key3 => $choice_list) {
							$i++;
							$choice_type = $choice_list['choice_type'];
							$choice_id = $choice_list['choice_id'];

							$prefix = "";
							if ($i == 1) {
								$prefix = "if (";
							} else {
								$prefix = " && ";
							}

							$blPostfix = false;
							switch ($choice_type) {
								case "radio":
								case "checkbox":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id.":checked\").val() == undefined ";
									$blPostfix = true;
									break;
								case "string":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"\" ";
									$blPostfix = true;
									break;
								case "numeric":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && ($(\"#c_".$choice_id."\").val() == \"0\" || $(\"#c_".$choice_id."\").val() == \"\") ";
									$blPostfix = true;
									$jquery_regexp .= "if($(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id."\").val().length > 0) {";
									$jquery_regexp .= "	if($(\"#c_".$choice_id."\").val().match(/^(?:[1-9]\d*|0)(?:\.\d*[0-9])?$/g)) {";
									$jquery_regexp .= "		ok_section_ids.push(".$section_data['section_id'].");";
									$jquery_regexp .= "	} else {";
									$jquery_regexp .= "		tmp_error_text += \"<li>設問「".$section_data['section_string']."」には数値を入力してください</li>\";";
									$jquery_regexp .= "		error_section_ids.push(".$section_data['section_id'].");";
									$jquery_regexp .= "	}";
									$jquery_regexp .= "}"."\n";
									break;
								case "year":
								case "month":
								case "day":
									$jquery_required .= $prefix."$(\"#c_".$choice_id.":visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"\" ";
									$blPostfix = true;
									break;
								case "slider":
								case "slider_minute":
								case "slider_show_by_step":
									$jquery_required .= $prefix."$(\"#c_".$choice_id."_show:visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"0\" ";
									$blPostfix = true;
									break;
								case "slider_hour":
									if ($choice_list['ui_type'] == "2") {
										$jquery_required .= $prefix."$(\"#c_".$choice_id."_show:visible\").val() != undefined && $(\"#c_".$choice_id."\").val() == \"0\" ";
										$blPostfix = true;
									}
									break;
								case "date":
								case "time":
								case "datetime":
									//未使用
									break;
								case "tab":
								case "accordion":
									//見出し
									break;
								default:
									break;
							}
						}
						if ($blPostfix) {
							$jquery_required .= ") {";
							$jquery_required .= "	tmp_error_text += \"<li>設問「".$section_data['section_string']."」を入力してください</li>\";";
							$jquery_required .= "	error_section_ids.push(".$section_data['section_id'].");";
							$jquery_required .= "} else { ";
							$jquery_required .= "	ok_section_ids.push(".$section_data['section_id'].");";
							$jquery_required .= "}"."\n";
						}
					}
				}
			}
		}

		return array($jquery_required, $jquery_regexp);
	}

	public function judgeByInterviewId($selectInterview, $section_set_id)
	{
		$array_output = array();
		$record_by_choice = \Base::judgeByInterviewId($selectInterview['interview_id']);
		foreach ($record_by_choice as $key => $value) {
			$array_output[$value['choice_id']] =  $value;
		} 

		$return = $this->judgeByRecord($selectInterview, $array_output, $section_set_id);
		return $return;
	}

	public function judgeByRecord($interview_data, $record_by_choice, $section_set_id)
	{

		$judge = array();
		$judge['user_name'] = $interview_data['name'];
		$judge['seinengappi'] = $interview_data['birth_date'];
        if($interview_data["finished_datetime"]){
            $today = date('Ymd',strtotime($interview_data["finished_datetime"]));
        }else{
            $today = date("Ymd");
        }
		$judge['age'] = (int)(($today-date("Ymd", strtotime($judge['seinengappi']))) / 10000);
		//$judge['age'] = $interview_data['years_old'];
		//$judge['sex'] = $interview_data['gender'] === "male" ? 1: 2;
		$judge['sex'] = $interview_data['gender'];
		$judge['tall'] = $record_by_choice[10059]['input_float'];
		$judge['weight'] = $record_by_choice[10060]['input_float'];
		$judge['abdominal_girth'] = $record_by_choice[10066]['input_float'];
		$judge['sports_viable_count'] = $record_by_choice[10194]['input_float'];
		$judge['sports_viable_time'] = $record_by_choice[10195]['input_float'];

		//print_r($judge['age']);

		//喫煙指導フラグ
		if (!empty($record_by_choice[10125]['selection']) || !empty($record_by_choice[10128]['selection'])) {
			$judge['show_smoking_leading'] = 1;
		}

		//BMI
		$judge['bmi'] = $judge['weight'] / (($judge['tall'] / 100) * ($judge['tall'] / 100));

		//生活活動強度・係数
		\MetaboMasterData::globals();
		foreach ($GLOBALS['JOB_MASTER_BY_CHOICE_ID'] as $choice_id => $job_data) {
			
			if (!empty($record_by_choice[$choice_id]['selection'])) {
				$judge['job_data'] = $job_data;
				//break;
			}
		}

		if (empty($judge['job_data'])) {
			//return Ethna::raiseError('職業が特定できません', E_USER_ERROR);  
		}

		if ($judge['age'] >= 45 && isset($judge['job_data']['over45_discount'])) {
			$judge['job_data']['strength'] = 1; //特定職業では年齢45以上の場合、生活活動強度は1とする
		}

		//基礎代謝量基礎値
		foreach ($GLOBALS['BASE_METABOLISM'] as $age_bottom => $bm_data) {
			if ($judge['age'] < $age_bottom) {
				continue;
			}
			$judge['baseValue_base_metabolism'] = $bm_data[$judge['sex']];
		}

		if (empty($judge['baseValue_base_metabolism'])) {
			//return Ethna::raiseError('基礎代謝量基礎値が特定できません', E_USER_ERROR); 
		}

		switch ($interview_data['section_set_id']) {
			case '31':
				\MetaboJudgeDataExerciseIntensity::globals();
				\MetaboJudgeDataFull::globals();
				\FollowupMasterData::globals();
				$judge = $this->judgeMetaboFull($judge, $record_by_choice);
				$judge_type = 8;
				break;
			case '32':
				\MetaboJudgeDataExerciseIntensity::globals();
				\MetaboJudgeData::globals();
				\FollowupMasterData::globals();
				$judge = $this->judgeMetaboBasic($judge, $record_by_choice);
				$judge_type = 19;
				break;
			case '33':
				\MetaboJudgeData::globals();
				$judge = $this->judgeMetaboSimple($judge, $record_by_choice);
				$judge_type = 0;
				break;
			case '34':
				\MetaboJudgeData::globals();
				$judge = $this->judgeMetaboLight($judge, $record_by_choice);
				$judge_type = 0;
				break;
			case '35':
				\MetaboJudgeData::globals();
				$judge = $this->judgeMetaboLight($judge, $record_by_choice);
				$judge_type = 0;
				break;
			default:
				break;
		}

		//print_r($judge_type);

		//食事のデータべース関連
		$food = $this->foodByInterviewId($record_by_choice);
		//print_r($food);

		// if (Ethna::isError($food)){ 
		// 	return $food;
		// }
		$judge['food'] = $food;
	
		//Straight 20130716 guidance 用データ収集
		$judge = $this->setGuidanceDataList($judge, $record_by_choice , $judge_type);

		return $judge;
	}

	public function judgeMetaboFull($judge, $record_by_choice)
	{
		/*
		// 20120327 add 暫定対応(健康診断 結果入力削除による修正)　⇒20120622 検査値入力復活のため削除
		$record_by_choice[10196]['input_float'] = 120;   //収縮期血圧(最高血圧)
		$record_by_choice[10197]['input_float'] = 80;   //拡張期血圧(最低血圧)
		$record_by_choice[10280]['input_float'] = 90;   //空腹時血糖
		$record_by_choice[10281]['input_float'] = 120;   //随時血糖
		$record_by_choice[10282]['input_float'] = 120;   //中性脂肪
		$record_by_choice[10283]['input_float'] = 160;   //総コレステロール
		$record_by_choice[10284]['input_float'] = 110;   //LDLコレステロール
		$record_by_choice[10285]['input_float'] = 50;   //HDLコレステロール
		$record_by_choice[10286]['input_float'] = 6;   //尿酸
		$record_by_choice[10287]['input_float'] = 20;   //AST
		$record_by_choice[10288]['input_float'] = 30;   //ALT
		$record_by_choice[10289]['input_float'] = 30;   //GGTP
		$record_by_choice[10290]['input_float'] = 5;   //HbA1c
		*/

		// 05.判定算出 ====================================================================================================
		// 01 腹囲判定値FULL用
		if ($judge['sex'] == 1) {
			if (100 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][1] = 1;
			} elseif (85 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][1] = 2;
			} else {
				$judge['judge_metabo'][1] = 3;
			}
		} else {
			if (100 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][1] = 1;
			} elseif (90 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][1] = 2;
			} elseif (75 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][1] = 3;
			} else {
				$judge['judge_metabo'][1] = 4;
			}
		}

		// 02 血圧判定値
		if (140 <= $record_by_choice[10196]['input_float'] | 90 <= @$record_by_choice[10197]['input_float']) {
			$judge['judge_metabo'][2] = 1;
		} elseif (130 <= $record_by_choice[10196]['input_float'] | 85 <= @$record_by_choice[10197]['input_float']) {
			$judge['judge_metabo'][2] = 2;
		} else {
			$judge['judge_metabo'][2] = 3;
		}

		// 03 血糖判定値
		if (126 <= $record_by_choice[10280]['input_float'] | 200 <= @$record_by_choice[10281]['input_float']) {
			$judge['judge_metabo'][3] = 1;
		} elseif (110 <= $record_by_choice[10280]['input_float']) {
			$judge['judge_metabo'][3] = 2;
		} else {
			$judge['judge_metabo'][3] = 3;
		}

		// 04 脂判定値
		if (220 <= $record_by_choice[10283]['input_float'] | 140 <= @$record_by_choice[10284]['input_float']) {
			$judge['judge_metabo'][4] = 1;
		} elseif (150 <= $record_by_choice[10282]['input_float'] | @$record_by_choice[10285]['input_float'] < 40) {
			$judge['judge_metabo'][4] = 2;
		} else {
			$judge['judge_metabo'][4] = 3;
		}

		// kiem tra lai globals
		// 05 メタボ判定値補助
		//変更 201305 Straight Ozeki ON/NotBlank に変更
		if ($judge['sex']==1) {
			$judge['judge_metabo'][5] = $GLOBALS['MTBxxM'][$judge['judge_metabo'][1]][$judge['judge_metabo'][2]][$judge['judge_metabo'][3]][$judge['judge_metabo'][4]];
		} else {
			$judge['judge_metabo'][5] = $GLOBALS['MTBxxF'][$judge['judge_metabo'][1]][$judge['judge_metabo'][2]][$judge['judge_metabo'][3]][$judge['judge_metabo'][4]];
		}
		
		// 追加 201305 Straight Ozeki
		//06 腹囲判定値FULL用3
		if ($judge['sex']==1) {
			if (85 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][6] = 2;
			} else {
				$judge['judge_metabo'][6] = 1;
			}
		} else {
			if (90 <= $record_by_choice[10066]['input_float']) {
				$judge['judge_metabo'][6] = 2;
			} else {
				$judge['judge_metabo'][6] = 1;
			}
		}

		// /*
		// // 削除 201305 Straight Ozeki
		// // 06 腹囲判定値FULL用2
		// if ($judge['sex']==1) {
		// 	if (85 <= $record_by_choice[10066]['input_float']) {
		// 		$judge['judge_metabo'][6] = 1;
		// 	} else {
		// 		$judge['judge_metabo'][6] = 2;
		// 	}
		// } else {
		// 	if (90 <= $record_by_choice[10066]['input_float']) {
		// 		$judge['judge_metabo'][6] = 1;
		// 	} else {
		// 		$judge['judge_metabo'][6] = 2;
		// 	}
		// }
		// */
		
		// 07 脂判定値2
		if (140 <= $record_by_choice[10284]['input_float']) {
			$judge['judge_metabo'][7] = 1;
		} elseif (150 <= $record_by_choice[10282]['input_float'] | @$record_by_choice[10285]['input_float'] < 40) {
			$judge['judge_metabo'][7] = 2;
		} else {
			$judge['judge_metabo'][7] = 3;
		}

		// 08 メタボ判定値FULL2
		if ($judge['sex']==1) {
			$judge['judge_metabo'][8] = $GLOBALS['MTBxxM_NEW'][$judge['judge_metabo'][6]][$judge['judge_metabo'][2]][$judge['judge_metabo'][3]][$judge['judge_metabo'][7]];
		} else {
			$judge['judge_metabo'][8] = $GLOBALS['MTBxxF_NEW'][$judge['judge_metabo'][6]][$judge['judge_metabo'][2]][$judge['judge_metabo'][3]][$judge['judge_metabo'][7]];
		}

		//26.リスク算出3 =================================================================================================
		//01～08
		$result_commonlogicCalculate1 = $this->commonlogicCalculate1($judge, $record_by_choice);
		foreach ($result_commonlogicCalculate1 as $key => $value) {
			$judge['judge_risk'][$key + 1] = $value;
		}

		//09～13
		$result_commonlogicCalculate2 = $this->commonlogicCalculate2($judge, $record_by_choice, $judge['judge_risk'][8]);
		foreach ($result_commonlogicCalculate2 as $key => $value) {
			$judge['judge_risk'][$key + 9] = $value;
		}

		//14～16
		$result_commonlogicCalculateRisk = $this->commonlogicCalculateRiskFull($judge, $record_by_choice, $judge['judge_risk'][9], $judge['judge_risk'][10], $judge['judge_risk'][13], $judge['judge_risk'][12]);
		foreach ($result_commonlogicCalculateRisk as $key => $value) {
			$judge['judge_risk'][$key + 14] = $value;
		}

		// 14.運動強度算出 ================================================================================================
		// 01～07
		$judge = $this->commonlogicCalculateExerciseIntensity($judge, $record_by_choice);

		// 23.判定出力結果 ================================================================================================
		$judge['metabo_judge_text'] = $GLOBALS['METABO_JUDGE_FULL'][$judge['judge_metabo'][8]];

		// 25.指導算出 ====================================================================================================
		// 01～07
		$judge = $this->commonlogicCalculateGuidance1($judge, $judge['judge_risk'][2]);
		// 08～15
		$judge = $this->commonlogicCalculateGuidance2($judge, $record_by_choice, $judge['judge_risk'][2], $judge['judge_risk'][5]);

		// 24.指導内容 ====================================================================================================
		// 指導判定値
		if (!empty($record_by_choice[10089]['selection'])) {
			$judge['output']['anamnesis'][] = '高血圧';
		} elseif ($judge['judge_metabo'][2] == 1) {
			$judge['output']['disease'][] = '高血圧';
		}

		if (!empty($record_by_choice[10090]['selection'])) {
			$judge['output']['anamnesis'][] = '糖尿病';
		} elseif ($judge['judge_metabo'][3] == 1) {
			$judge['output']['disease'][] = '糖尿病';
		}

		if (!empty($record_by_choice[10091]['selection'])) {
			$judge['output']['anamnesis'][] = '高脂血症';
		} elseif ($judge['judge_metabo'][4]==1) {
			$judge['output']['disease'][] = '高脂血症';
		}

		if (empty($judge['output']['anamnesis']) & empty($judge['output']['disease']) & $judge['judge_metabo'][5]=="ON") {
		//if (empty($judge['output']['anamnesis']) & empty($judge['output']['disease']) & $judge['judge_metabo'][5]['sub']==3) {
			$judge['output']['disease'][] = 'メタボリック症候群';
		}

		if (empty($judge['output']['anamnesis']) & empty($judge['output']['disease'])) {
			$judge['output']['enable'] = false;
		} else {
			$judge['output']['enable'] = true;
		}

		// 動脈硬化性疾患(既往歴で心筋梗塞OR狭心症OR脳卒中（脳梗塞・脳出血）)があった場合
		$judge = $this->commonlogicJudgeEnable($judge, $record_by_choice);

		//メンタル診断 ===================================================================================================
		$judge = $this->judgeMental($judge, $record_by_choice);

		return $judge;

	}

	public function judgeMetaboBasic($judge, $record_by_choice)
	{
		

		//05.判定算出 ====================================================================================================
		//01～03
		$judge = $this->commonlogicCalculateJudgement($judge);

		//04～11
		$result_commonlogicCalculate1 = $this->commonlogicCalculate1($judge, $record_by_choice);
		foreach ($result_commonlogicCalculate1 as $key => $value) {
			$judge['judge_metabo'][$key + 4] = $value;
		}

		//12～16
		$result_commonlogicCalculate2 = $this->commonlogicCalculate2($judge, $record_by_choice, $judge['judge_metabo'][11]);
		foreach ($result_commonlogicCalculate2 as $key => $value) {
			$judge['judge_metabo'][$key + 12] = $value;
		}
		
		// 修正 201304改修 20130426 Straight Ozeki
		//04 食事判定値（設定しなおし
		if (!empty($record_by_choice[10486]['selection'])) { // 少ない
			$judge['judge_metabo'][4] = 1;
		} elseif (!empty($record_by_choice[10487]['selection'])) { // 普通
			$judge['judge_metabo'][4] = 1;
		} elseif (!empty($record_by_choice[10488]['selection'])) { // 多い
			$judge['judge_metabo'][4] = 2;
		}
		
		//17 判定出力
		//3:BMI判定値 4：食事判定値 13:夕食判定値 15:飲酒判定値 16:運動判定値
		$judge = $this->commonlogicOutputJudgement($judge);

		//18 喫煙判定による判定出力値調整
		$outputValue = $judge['judge_metabo'][17]['outputValue']; //判定出力値
		if ($record_by_choice[10129]['input_float'] >= 20) {
			$outputValue += 1;
		} else {
			$outputValue += 0.5;
		}
		$judge['judge_metabo'][18] = $outputValue;

		//19 既往歴による判定出力値調整
		$adjustedValue = 0;
		if ($record_by_choice[10072]['selection']) { //心筋梗塞
			$adjustedValue = max($adjustedValue, 2);
		}
		if ($record_by_choice[10073]['selection']) { //狭心症
			$adjustedValue = max($adjustedValue, 2);
		}
		if ($record_by_choice[10074]['selection']) { //脳卒中（脳梗塞・脳出血）
			$adjustedValue = max($adjustedValue, 2);
		}
		if ($record_by_choice[10067]['selection']) { //高血圧
			$adjustedValue = max($adjustedValue, 2);
		}
		if ($record_by_choice[10068]['selection']) { //糖尿病
			$adjustedValue = max($adjustedValue, 1);
		}
		if ($record_by_choice[10069]['selection']) { //高脂血症
			$adjustedValue = max($adjustedValue, 2);
		}
		if ($record_by_choice[10070]['selection']) { //痛風（高尿酸血症）
			$adjustedValue = max($adjustedValue, 1);
		}
		if ($record_by_choice[10071]['selection']) { //脂肪肝
			$adjustedValue = max($adjustedValue, 1);
		}

		// メタボリスク判定値
		$judge['judge_metabo'][19] = min(10, floor($judge['judge_metabo'][18] + $adjustedValue));
		// 20 判定出力結果
		$judge['judge_metabo'][20] = $GLOBALS['METABO_JUDGE_RESULT'][$judge['judge_metabo'][19]];

		// 21.運動強度算出 ================================================================================================
		// 01～07
		$judge = $this->commonlogicCalculateExerciseIntensity($judge, $record_by_choice);

		// 31.指導算出 ====================================================================================================
		// 01～07
		$judge = $this->commonlogicCalculateGuidance1($judge, $judge['judge_metabo'][5]);
		// 08～15
		$judge = $this->commonlogicCalculateGuidance2($judge, $record_by_choice, $judge['judge_metabo'][5], $judge['judge_metabo'][8]);

		// 32.リスク算出 ==================================================================================================
		// 01～03
		// 3:BMI判定値 4：食事判定値 13:夕食判定値 15:飲酒判定値 16:運動判定値
		$result_commonlogicCalculateRisk = $this->commonlogicCalculateRiskBasic($judge, $record_by_choice, $judge['judge_metabo'][2], $judge['judge_metabo'][4], $judge['judge_metabo'][13], $judge['judge_metabo'][15], $judge['judge_metabo'][16]);
		foreach ($result_commonlogicCalculateRisk as $key => $value) {
			$judge['judge_risk'][$key + 1] = $value;
		}

		//30.指導内容 ====================================================================================================
		// 動脈硬化性疾患(既往歴で心筋梗塞OR狭心症OR脳卒中（脳梗塞・脳出血）)があった場合
		$judge = $this->commonlogicJudgeEnable($judge, $record_by_choice);

		//メンタル診断 ===================================================================================================
		$judge = $this->judgeMental($judge, $record_by_choice);

		return $judge;
	}

	public function judgeMetaboSimple($judge, $record_by_choice)
	{
		//05.判定算出 ====================================================================================================
		
		/*$judge['judge_metabo'][17] = $GLOBALS["METABO_".$target_sheet]
		[$judge['judge_metabo'][1]]
		[$judge['judge_metabo'][3]]
		[$judge['judge_metabo'][12]]
		[$judge['judge_metabo'][13]]
		[$judge['judge_metabo'][15]]
		[$judge['judge_metabo'][16]];
		*/
		
		//01～03
		$judge = $this->commonlogicCalculateJudgement($judge);
		
		/* 修正 201304改修 20130424 Straight Ozeki */
		//3:BMI判定値 4：食事判定値 13:夕食判定値 15:飲酒判定値 16:運動判定値
		
		//04～11
		$result_commonlogicCalculate1 = $this->commonlogicCalculate1($judge, $record_by_choice);
		foreach ($result_commonlogicCalculate1 as $key => $value) {
			$judge['judge_metabo'][$key + 4] = $value;
		}
		//04 食事判定値（設定しなおし
		if (!empty($record_by_choice[10486]['selection'])) { // 少ない
			$judge['judge_metabo'][4] = 1;
		} elseif (!empty($record_by_choice[10487]['selection'])) { // 普通
			$judge['judge_metabo'][4] = 1;
		} elseif (!empty($record_by_choice[10488]['selection'])) { // 多い
			$judge['judge_metabo'][4] = 2;
		}
		
		//12～16
		$result_commonlogicCalculate2 = $this->commonlogicCalculate2($judge, $record_by_choice, $judge['judge_metabo'][11]);
		foreach ($result_commonlogicCalculate2 as $key => $value) {
			$judge['judge_metabo'][$key + 12] = $value;
		}
		
		//16 運動判定値 （設定しなおし
		/* 修正 201304改修 20130424 Straight Ozeki */
		if (!empty($record_by_choice[10177]['selection'])) { // いいえ
			$judge['judge_metabo'][16] = 2;
		} elseif (!empty($record_by_choice[10178]['selection'])) { // １～２回
			$judge['judge_metabo'][16] = 1;
		} elseif (!empty($record_by_choice[10179]['selection'])) { // 多め
			$judge['judge_metabo'][16] = 1;
		}
		
		
		
		//17 判定出力
		$judge = $this->commonlogicOutputJudgement($judge);
		
		//18 喫煙判定による予測値調整
		/* 削除 201304改修 20130425 Straight Ozeki */
		/*
		$predictiveValue = $judge['judge_metabo'][17]['predictiveValue']; //予測値
		if ($record_by_choice[10129]['input_float'] >= 20) {
			$predictiveValue += 1;
		} else {
			$predictiveValue += 0.5;
		}
		
		$judge['judge_metabo'][18] = min(3, floor($predictiveValue));
		*/
		
		//19 喫煙判定による判定出力値調整
		/* 修正 201304改修 20130425 Straight Ozeki */
		$judge['judge_metabo'][19] = $judge['judge_metabo'][17]['outputValue'];
		if ($record_by_choice[10125]['selection']){ //はい
			if ($record_by_choice[10129]['int_float'] * $record_by_choice[10130]['int_float'] >= 400){
				$judge['judge_metabo'][19] = min(10, $judge['judge_metabo'][17]['outputValue'] + 0.5);
			}
		} else if ($record_by_choice[10128]['selection']){ //以前吸っていた
			if ($record_by_choice[10131]['int_float'] * $record_by_choice[10132]['int_float'] >= 400){
				$judge['judge_metabo'][19] = min(10, $judge['judge_metabo'][17]['outputValue'] + 0.5);
			}
		}
		/*
		if ($judge['show_smoking_leading']) {
			$judge['judge_metabo'][19] = min(10, $judge['judge_metabo'][17]['outputValue'] + 1);
		} else {
			$judge['judge_metabo'][19] = min(10, $judge['judge_metabo'][17]['outputValue']);
		}
		*/
		
		//健診既往指摘
		//Straight 20130710
		//既往歴に関係するIDを設定
		$check_id_array = array(10067,10068,10069,10070,10071,10072,10073,10074,10075,10076,10077,10078,10079,10080,10081,10082,10083,10084,10085,10086,10087,10088);
		$judge['past_history'] = 0;
		foreach($check_id_array as $value){
			if(!empty($record_by_choice[$value]['selection'])){
				$judge['past_history'] = 1;
				break;
			}
		}
		//20 判定出力結果
		$judge['judge_metabo'][20] = $GLOBALS['METABO_JUDGE_RESULT'][floor($judge['judge_metabo'][19])];
		
		
		
		//21 予測結果
		/*
		switch ($judge['judge_metabo'][18]) {
			case 1:
				$judge['judge_metabo'][21] = "軽";
				break;
			case 2:
				$judge['judge_metabo'][21] = "中";
				break;
			case 3:
				$judge['judge_metabo'][21] = "重";
				break;
			default:
				break;
		}*/
		
		//15（22） リスク判定基礎値
		$risk_basic = 0;
		//性別が男性の場合、又は性別が女性で且つ年齢>=55
		if ($judge['sex'] == 1 || ($judge['sex'] != 1 && $judge['age'] >= 55)){
			$risk_basic += 1;
		}
		//家族歴で6/7/8が含まれる場合 ?
		//Straight 20130710 簡易版では質問項目がないので削除
		//if ($judge['sex'] == 1 || ($judge['sex'] != 1 && $judge['age'] >= 55)){
		//}
		
		//喫煙傾向=1 OR 喫煙傾向=4の場合
		if ($record_by_choice[10125]['selection'] || $record_by_choice[10128]['selection']){
			$risk_basic += 1;
		}
		//年齢>=55 AND 年齢<60の場合
		if ($judge['age'] >= 55 && $judge['age'] < 60){
			$risk_basic += 1;
		}
		//年齢>=60 AND 年齢<65の場合
		if ($judge['age'] >= 60 && $judge['age'] < 65){
			$risk_basic += 2;
		}
		//年齢>=65の場合
		if ($judge['age'] >= 65){
			$risk_basic += 3;
		}
		$judge['judge_metabo'][22] = $risk_basic;
		
		//16（23） リスク判定習慣値
		$judge['judge_metabo'][23] = ($judge['judge_metabo'][4] + $judge['judge_metabo'][13] + $judge['judge_metabo'][15] + $judge['judge_metabo'][16]) * 0.5;
		
		//17（24） 予測結果2
		$judge['judge_metabo'][24] = ceil(($judge['judge_metabo'][22] + $judge['judge_metabo'][23]) / 2);
		if ($judge['judge_metabo'][24] < 1){
			$judge['judge_metabo'][24] = 1;
		}
		if ($judge['judge_metabo'][24] > 5){
			$judge['judge_metabo'][24] = 5;
		}
		

		//31.指導算出 ====================================================================================================
		//01～07
		$judge = $this->commonlogicCalculateGuidance1($judge, $judge['judge_metabo'][5]);
		//08 理想カロリー調整値簡易
		if ($judge['output'][2] >= $judge['output'][7]) {
			$judge['output'][8] = round($judge['output'][2], 0);
		} else {
			if ($judge['output'][2] > $judge['output'][6]) {
				$judge['output'][8] = round($judge['output'][2], 0)."～".round($judge['output'][7], 0);
			} else {
				$judge['output'][8] = round($judge['output'][6], 0)."～".round($judge['output'][7], 0);
			}
		}
		

		//メンタル診断 ===================================================================================================
		$judge = $this->judgeMental($judge, $record_by_choice);

		return $judge;
	}

	public function judgeMetaboLight($judge, $record_by_choice)
	{
		//05.判定算出 ====================================================================================================
		//01～08
		$result = $this->commonlogicCalculate1($judge, $record_by_choice);
		foreach ($result as $key => $value) {
			$judge['judge_metabo'][$key + 1] = $value;
		}
		//09 BMI
		$judge['judge_metabo'][9] = (float) $judge['bmi'];
        /* 修正 20140617 浮動小数点の計算は正確ではない */
        $epsilon = pow(0.1,10); // 小数点数の丸め誤差上限

		/* 修正 201304改修 20130423 Straight Ozeki */
		//10 BMI判定値
		if ($judge['judge_metabo'][9] >= 25-$epsilon) {
			$judge['judge_metabo'][10] = 3;
		} elseif ($judge['judge_metabo'][9] >= 22-$epsilon) {
			$judge['judge_metabo'][10] = 2;
		} else {
			$judge['judge_metabo'][10] = 1;
		}
		//03 食事比較値
		if (!empty($record_by_choice[10449]['selection'])) {
			$judge['judge_metabo'][11] = 1;
		} elseif (!empty($record_by_choice[10450]['selection'])) {
			$judge['judge_metabo'][11] = 2;
		} elseif (!empty($record_by_choice[10451]['selection'])) {
			$judge['judge_metabo'][11] = 3;
		}
		
		//12 運動判定値
		if (!empty($record_by_choice[10461]['selection'])) {
			$judge['judge_metabo'][12] = 1;
		} elseif (!empty($record_by_choice[10462]['selection'])) {
			$judge['judge_metabo'][12] = 2;
		} elseif (!empty($record_by_choice[10463]['selection'])) {
			$judge['judge_metabo'][12] = 3;
		}
		//健診既往指摘
		if (!empty($record_by_choice[10452]['selection'])) {
			$judge['judge_metabo'][10] = 0;
			$judge['judge_metabo'][11] = 0;
			$judge['judge_metabo'][12] = 0;
		
		}
		
		/*
		//10 BMI判定値
		if ($judge['judge_metabo'][9] >= 25) {
			$judge['judge_metabo'][10] = 3;
		} elseif ($judge['judge_metabo'][9] >= 22) {
			$judge['judge_metabo'][10] = 2;
		} else {
			$judge['judge_metabo'][10] = 1;
		}
		//11 カロリー判定値
		if ($judge['judge_metabo'][8] >= 800) {
			$judge['judge_metabo'][11] = 3;
		} elseif ($judge['judge_metabo'][8] >= 400) {
			$judge['judge_metabo'][11] = 2;
		} else {
			$judge['judge_metabo'][11] = 1;
		}
		//12 運動判定値
		if (!empty($record_by_choice[10177]['selection'])) {
			$judge['judge_metabo'][12] = 1;
		} elseif (!empty($record_by_choice[10178]['selection'])) {
			$judge['judge_metabo'][12] = 2;
		} elseif (!empty($record_by_choice[10179]['selection'])) {
			$judge['judge_metabo'][12] = 3;
		}
		*/
		//13 判定出力結果
		$judge_array = $GLOBALS['METABO_JUDGE_RESULT_LIGHT'][$judge['judge_metabo'][10]][$judge['judge_metabo'][11]][$judge['judge_metabo'][12]];
		$judge['judge_metabo'][13] = $judge_array["outputtext"];
		$judge['judge_metabo'][14] = $judge_array["result"];

		//メンタル診断 ===================================================================================================
		$judge = $this->judgeMental($judge, $record_by_choice);

		return $judge;
	}

	public function commonlogicCalculate1($judge, $record_by_choice)
	{
		

		$retArr = array();
		//食事平均比較係数
		if (!empty($record_by_choice[10140]['selection'])) {
			$retArr[0] = 0.8;
		} elseif (!empty($record_by_choice[10141]['selection'])) {
			$retArr[0] = 1.0;
		} elseif (!empty($record_by_choice[10142]['selection'])) {
			$retArr[0] = 1.2;
		}

		//摂取カロリー調整済
		/* $kcal_table = array(
		0 => array('unit_weight'=>140, 'kcal'=>235.2),
		1 => array('unit_weight'=>215, 'kcal'=>252.75),
		2 => array('unit_weight'=>214, 'kcal'=>318.86),
		3 => array('unit_weight'=>60, 'kcal'=>177.25),
		4 => array('unit_weight'=>150, 'kcal'=>138.5),
		5 => array('unit_weight'=>70, 'kcal'=>186.6),
		6 => array('unit_weight'=>70, 'kcal'=>111.28),
		7 => array('unit_weight'=>50, 'kcal'=>78.75),
		8 => array('unit_weight'=>90, 'kcal'=>127.64),
		9 => array('unit_weight'=>150, 'kcal'=>46.35),
		10 => array('unit_weight'=>150, 'kcal'=>45.93),
		11 => array('unit_weight'=>150, 'kcal'=>30.96),
		12 => array('unit_weight'=>150, 'kcal'=>31),
		13 => array('unit_weight'=>200, 'kcal'=>458.5),
		14 => array('unit_weight'=>100, 'kcal'=>48.31),
		15 => array('unit_weight'=>200, 'kcal'=>164.87),
		16 => array('unit_weight'=>200, 'kcal'=>2.33),
		17 => array('unit_weight'=>200, 'kcal'=>99.77),
		18 => array('unit_weight'=>200, 'kcal'=>82),
		19 => array('unit_weight'=>50, 'kcal'=>242.35),
		20 => array('unit_weight'=>50, 'kcal'=>192.41),
		21 => array('unit_weight'=>50, 'kcal'=>130.37),
		22 => array('unit_weight'=>50, 'kcal'=>131.7),
		);
		$total_kcal = 0;
		$total_kcal += $kcal_table[0]['kcal'] * $record_by_choice[10311]['input_float'] / $kcal_table[0]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10312]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10313]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10314]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10315]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10316]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10317]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10318]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10319]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10320]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10321]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10322]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10323]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10324]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10325]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10326]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10327]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10328]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10329]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10330]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10331]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10332]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10333]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[0]['kcal'] * $record_by_choice[10334]['input_float'] / $kcal_table[0]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10335]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10336]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10337]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10338]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10339]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10340]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10341]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10342]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10343]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10344]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10345]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10346]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10347]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10348]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10349]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10350]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10351]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10352]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10353]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10354]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10355]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10356]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[0]['kcal'] * $record_by_choice[10357]['input_float'] / $kcal_table[0]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10358]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10359]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10360]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10361]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10362]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10363]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10364]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10365]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10366]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10367]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10368]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10369]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10370]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10371]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10372]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10373]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10374]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10375]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10376]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10377]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10378]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10379]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[0]['kcal'] * $record_by_choice[10380]['input_float'] / $kcal_table[0]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10381]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10382]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10383]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10384]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10385]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10386]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10387]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10388]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10389]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10390]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10391]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10392]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10393]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10394]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10395]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10396]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10397]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10398]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10399]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10400]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10401]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10402]['input_float'] / $kcal_table[22]['unit_weight'];
		*/

		$kcal_table = array (
			1   => array('unit_weight' => 50.0, 'kcal' => 84),	//めし
			2   => array('unit_weight' => 50.0, 'kcal' => 59),	//和風めん
			3   => array('unit_weight' => 50.0, 'kcal' => 75),	//洋風中華めん
			4   => array('unit_weight' => 50.0, 'kcal' => 148),	//パン
			5   => array('unit_weight' => 50.0, 'kcal' => 46),	//いも類（主食）
			6   => array('unit_weight' => 50.0, 'kcal' => 127),	//肉類
			7   => array('unit_weight' => 50.0, 'kcal' => 79),	//魚介類
			8   => array('unit_weight' => 50.0, 'kcal' => 79),	//卵
			9   => array('unit_weight' => 50.0, 'kcal' => 85),	//大豆
			10  => array('unit_weight' => 50.0, 'kcal' => 8),	//汁
			11  => array('unit_weight' => 50.0, 'kcal' => 15),	//緑黄色野菜
			12  => array('unit_weight' => 50.0, 'kcal' => 15),	//淡色野菜
			13  => array('unit_weight' => 50.0, 'kcal' => 35),	//海藻類
			14  => array('unit_weight' => 50.0, 'kcal' => 10),	//きのこ類
			15  => array('unit_weight' => 50.0, 'kcal' => 115),	//いも類（副菜）
			16  => array('unit_weight' => 50.0, 'kcal' => 41),	//乳類
			17  => array('unit_weight' => 50.0, 'kcal' => 24),	//果実類
			18  => array('unit_weight' => 50.0, 'kcal' => 242),	//菓子（水分少、脂質多）
			19  => array('unit_weight' => 50.0, 'kcal' => 192),	//菓子（水分少、脂質少）
			20  => array('unit_weight' => 50.0, 'kcal' => 130),	//菓子（水分多、脂質多）
			21  => array('unit_weight' => 50.0, 'kcal' => 132),	//菓子（水分多、脂質少）
			22  => array('unit_weight' => 50.0, 'kcal' => 0),	//水、白湯
			23  => array('unit_weight' => 50.0, 'kcal' => 1),	//茶類
			24  => array('unit_weight' => 50.0, 'kcal' => 25),	//ジュース類
			25  => array('unit_weight' => 50.0, 'kcal' => 20),	//酒（ビール）
			26  => array('unit_weight' => 50.0, 'kcal' => 55),	//酒（日本酒）
			27  => array('unit_weight' => 50.0, 'kcal' => 37),	//酒（ワイン）
			28  => array('unit_weight' => 50.0, 'kcal' => 108),	//酒（焼酎、ｳｨｽｷｰ、ﾌﾞﾗﾝﾃﾞｰ）
		);

        $total_kcal = 0;
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10311]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10312]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10313]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10314]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10315]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10316]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10317]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10318]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10319]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10320]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10321]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10322]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10323]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10324]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10325]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10326]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[23]['kcal'] * $record_by_choice[10327]['input_float'] / $kcal_table[23]['unit_weight'];
		$total_kcal += $kcal_table[24]['kcal'] * $record_by_choice[10328]['input_float'] / $kcal_table[24]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10330]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10331]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10332]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10333]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10334]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10335]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10336]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10337]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10338]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10339]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10340]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10341]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10342]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10343]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10344]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10345]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10346]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10347]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10348]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10349]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[23]['kcal'] * $record_by_choice[10350]['input_float'] / $kcal_table[23]['unit_weight'];
		$total_kcal += $kcal_table[24]['kcal'] * $record_by_choice[10351]['input_float'] / $kcal_table[24]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10353]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10354]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10355]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10356]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10357]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10358]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10359]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10360]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10361]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10362]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10363]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10364]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10365]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10366]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10367]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10368]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10369]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10370]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10371]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10372]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[23]['kcal'] * $record_by_choice[10373]['input_float'] / $kcal_table[23]['unit_weight'];
		$total_kcal += $kcal_table[24]['kcal'] * $record_by_choice[10374]['input_float'] / $kcal_table[24]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10376]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10377]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10378]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10379]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[1]['kcal'] * $record_by_choice[10380]['input_float'] / $kcal_table[1]['unit_weight'];
		$total_kcal += $kcal_table[2]['kcal'] * $record_by_choice[10381]['input_float'] / $kcal_table[2]['unit_weight'];
		$total_kcal += $kcal_table[3]['kcal'] * $record_by_choice[10382]['input_float'] / $kcal_table[3]['unit_weight'];
		$total_kcal += $kcal_table[4]['kcal'] * $record_by_choice[10383]['input_float'] / $kcal_table[4]['unit_weight'];
		$total_kcal += $kcal_table[5]['kcal'] * $record_by_choice[10384]['input_float'] / $kcal_table[5]['unit_weight'];
		$total_kcal += $kcal_table[6]['kcal'] * $record_by_choice[10385]['input_float'] / $kcal_table[6]['unit_weight'];
		$total_kcal += $kcal_table[7]['kcal'] * $record_by_choice[10386]['input_float'] / $kcal_table[7]['unit_weight'];
		$total_kcal += $kcal_table[8]['kcal'] * $record_by_choice[10387]['input_float'] / $kcal_table[8]['unit_weight'];
		$total_kcal += $kcal_table[9]['kcal'] * $record_by_choice[10388]['input_float'] / $kcal_table[9]['unit_weight'];
		$total_kcal += $kcal_table[11]['kcal'] * $record_by_choice[10389]['input_float'] / $kcal_table[11]['unit_weight'];
		$total_kcal += $kcal_table[12]['kcal'] * $record_by_choice[10390]['input_float'] / $kcal_table[12]['unit_weight'];
		$total_kcal += $kcal_table[13]['kcal'] * $record_by_choice[10391]['input_float'] / $kcal_table[13]['unit_weight'];
		$total_kcal += $kcal_table[14]['kcal'] * $record_by_choice[10392]['input_float'] / $kcal_table[14]['unit_weight'];
		$total_kcal += $kcal_table[15]['kcal'] * $record_by_choice[10393]['input_float'] / $kcal_table[15]['unit_weight'];
		$total_kcal += $kcal_table[17]['kcal'] * $record_by_choice[10394]['input_float'] / $kcal_table[17]['unit_weight'];
		$total_kcal += $kcal_table[16]['kcal'] * $record_by_choice[10395]['input_float'] / $kcal_table[16]['unit_weight'];
		$total_kcal += $kcal_table[23]['kcal'] * $record_by_choice[10396]['input_float'] / $kcal_table[23]['unit_weight'];
		$total_kcal += $kcal_table[24]['kcal'] * $record_by_choice[10397]['input_float'] / $kcal_table[24]['unit_weight'];
		$total_kcal += $kcal_table[18]['kcal'] * $record_by_choice[10399]['input_float'] / $kcal_table[18]['unit_weight'];
		$total_kcal += $kcal_table[19]['kcal'] * $record_by_choice[10400]['input_float'] / $kcal_table[19]['unit_weight'];
		$total_kcal += $kcal_table[20]['kcal'] * $record_by_choice[10401]['input_float'] / $kcal_table[20]['unit_weight'];
		$total_kcal += $kcal_table[21]['kcal'] * $record_by_choice[10402]['input_float'] / $kcal_table[21]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10424]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10425]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10426]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[10]['kcal'] * $record_by_choice[10427]['input_float'] / $kcal_table[10]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10428]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10429]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10430]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[22]['kcal'] * $record_by_choice[10431]['input_float'] / $kcal_table[22]['unit_weight'];
		$total_kcal += $kcal_table[25]['kcal'] * $record_by_choice[10432]['input_float'] / $kcal_table[25]['unit_weight'];
		$total_kcal += $kcal_table[26]['kcal'] * $record_by_choice[10433]['input_float'] / $kcal_table[26]['unit_weight'];
		$total_kcal += $kcal_table[27]['kcal'] * $record_by_choice[10434]['input_float'] / $kcal_table[27]['unit_weight'];
		$total_kcal += $kcal_table[28]['kcal'] * $record_by_choice[10435]['input_float'] / $kcal_table[28]['unit_weight'];
		$total_kcal += $kcal_table[25]['kcal'] * $record_by_choice[10436]['input_float'] / $kcal_table[25]['unit_weight'];
		$total_kcal += $kcal_table[26]['kcal'] * $record_by_choice[10437]['input_float'] / $kcal_table[26]['unit_weight'];
		$total_kcal += $kcal_table[27]['kcal'] * $record_by_choice[10438]['input_float'] / $kcal_table[27]['unit_weight'];
		$total_kcal += $kcal_table[28]['kcal'] * $record_by_choice[10439]['input_float'] / $kcal_table[28]['unit_weight'];
		$total_kcal += $kcal_table[25]['kcal'] * $record_by_choice[10440]['input_float'] / $kcal_table[25]['unit_weight'];
		$total_kcal += $kcal_table[26]['kcal'] * $record_by_choice[10441]['input_float'] / $kcal_table[26]['unit_weight'];
		$total_kcal += $kcal_table[27]['kcal'] * $record_by_choice[10442]['input_float'] / $kcal_table[27]['unit_weight'];
		$total_kcal += $kcal_table[28]['kcal'] * $record_by_choice[10443]['input_float'] / $kcal_table[28]['unit_weight'];
		$total_kcal += $kcal_table[25]['kcal'] * $record_by_choice[10444]['input_float'] / $kcal_table[25]['unit_weight'];
		$total_kcal += $kcal_table[26]['kcal'] * $record_by_choice[10445]['input_float'] / $kcal_table[26]['unit_weight'];
		$total_kcal += $kcal_table[27]['kcal'] * $record_by_choice[10446]['input_float'] / $kcal_table[27]['unit_weight'];
		$total_kcal += $kcal_table[28]['kcal'] * $record_by_choice[10447]['input_float'] / $kcal_table[28]['unit_weight'];

		$retArr[1] = @$retArr[0] * $total_kcal;
		//年齢
		$retArr[2] = $judge['age'];
		//基礎代謝量基礎値
		$retArr[3] = $judge['baseValue_base_metabolism'];
		//基礎代謝量
		$retArr[4] = $retArr[3] * $judge['weight'];
		//生活活動強度
		$retArr[5] = $judge['job_data']['strength'];
		//栄養所要量
		$retArr[6] = $retArr[4] * $retArr[5];
		//カロリー差分
		$retArr[7] = $retArr[1] - $retArr[6];

		return $retArr;
	}

	public function commonlogicCalculate2($judge, $record_by_choice, $clorie_difference)
	{
		$retArr = array();

		//カロリー判定値
		if ($clorie_difference < 0) {
			$retArr[0] = 1;
		} elseif ($clorie_difference < 400) {
			$retArr[0] = 2;
		} elseif ($clorie_difference < 800) {
			$retArr[0] = 3;
		} else {
			$retArr[0] = 4;
		}

		//夕食判定値
		if (3 < $record_by_choice[10143]['input_float'] && $record_by_choice[10143]['input_float'] < 22) {
			$retArr[1] = 1;
		} else {
			$retArr[1] = 2;
		}

		//エタノール換算					

		$sub_total = 0;
		$sub_total += $record_by_choice[10150]['input_float'] * 0.05;	//ビール
		$sub_total += $record_by_choice[10151]['input_float'] * 0.1;	//日本酒
		$sub_total += $record_by_choice[10152]['input_float'] * 0.25;	//焼酎
		$sub_total += $record_by_choice[10153]['input_float'] * 0.1;	//ワイン
		$sub_total += $record_by_choice[10154]['input_float'] * 0.4;	//ウィスキー
		$sub_total += $record_by_choice[10155]['input_float'] * 0.4;	//ブランデー

		if (!empty($record_by_choice[10144]['selection'])) {
			$retArr[2] = $sub_total * 1;								//毎日
		} elseif (!empty($record_by_choice[10145]['selection'])) {
			$retArr[2] = $sub_total * 5 / 7;							//週に1～2回休肝日を設けている
		} elseif (!empty($record_by_choice[10146]['selection'])) {
			$retArr[2] = $sub_total * 4 / 7;							//週の半分くらい
		} elseif (!empty($record_by_choice[10147]['selection'])) {
			$retArr[2] = $sub_total * 2 / 7;							//週に1～2回
		} elseif (!empty($record_by_choice[10148]['selection'])) {
			$retArr[2] = $sub_total * 4 / 30;							//月に数回
		} elseif (!empty($record_by_choice[10149]['selection'])) {
			$retArr[2] = $sub_total * 0;								//飲まない
		}

		//飲酒判定値
		if ($judge['sex'] == 1) {
			if (30 <= $retArr[2]) {
				$retArr[3] = 2;
			} else {
				$retArr[3] = 1;
			}
		} else {
			if (20 <= $retArr[2]) {
				$retArr[3] = 2;
			} else {
				$retArr[3] = 1;
			}
		}

		//運動判定値
		if (!empty($record_by_choice[10179]['selection'])) {
			$retArr[4] = 1;
		} else {
			$retArr[4] = 2;
		}

		return $retArr;
	}

	public function commonlogicCalculateRiskFull($judge, $record_by_choice, $value1, $value2, $value3, $value4) {
		
		$retArr = array();
		//リスク判定基礎値
		$tmp_value = 0;
		if ($judge['sex'] == 1 || $judge['sex'] == 2 && 55 <= $judge['age']) {
			$tmp_value++;
		}
		if (!empty($record_by_choice[10121]['selection']) || !empty($record_by_choice[10122]['selection']) || !empty($record_by_choice[10123]['selection'])) {
			$tmp_value++;
		}
		if (isset($judge['show_smoking_leading'])) {
			$tmp_value++;
		}
		if (65 <= $judge['age']) {
			$tmp_value += 3;
		} elseif (60 <= $judge['age']) {
			$tmp_value += 2;
		} elseif (55 <= $judge['age']) {
			$tmp_value += 1;
		}
		$retArr[4] = $tmp_value;
		
		//BMI判定値2
		$retArr[5] = ($judge['bmi'] >= 22) ? 2 : 1;
		
		//食事平均比較判定値2
		//10488 : 多い 10487 : 普通 10486 ： 少ない
		if (isset($record_by_choice[10488]['selection'])){
			$retArr[6] = 2;
		} else {
			$retArr[6] = 1;
		}
		
		//夕食判定値2
		if (3 < $record_by_choice[10143]['input_float'] && $record_by_choice[10143]['input_float'] < 22) {
			$retArr[7] = 1;
		} else {
			$retArr[7] = 2;
		}

		// 運動判定値2
		if (!empty($record_by_choice[10179]['selection'])) {
			$retArr[8] = 1;
		} else {
			$retArr[8] = 2;
		}

		//飲酒判定値2
		$retArr[9] = $judge["judge_risk"][12];
		
		//Straight 201308 参照項目追加
		$retArr[0] = $retArr[4];
		
		//リスク判定習慣値
		$retArr[1] = ($retArr[5] + $retArr[6] + $retArr[7] + $retArr[8] + $retArr[9]) * 0.5;

		//予測結果2
		$retArr[2] = min(5, max(1, round(($retArr[0] + $retArr[1]) / 2)));

		return $retArr;
	}

	public function commonlogicCalculateExerciseIntensity($judge, $record_by_choice) {
		
		

		//01 運動強度P疾患
		$judge_01 = null;
		if (!empty($record_by_choice[10072]['selection']) || !empty($record_by_choice[10073]['selection']) ||
			!empty($record_by_choice[10074]['selection']) || !empty($record_by_choice[10075]['selection'])) {
			$judge_01 = 1;
		} elseif (!empty($record_by_choice[10110]['selection'])) {
			$judge_01 = 1;
		} elseif (!empty($record_by_choice[10067]['selection']) || !empty($record_by_choice[10068]['selection']) ||
				!empty($record_by_choice[10069]['selection']) || !empty($record_by_choice[10070]['selection'])) {
			$judge_01 = 2;
		} else {
			$judge_01 = 3;
		}
		$judge['judge_sports'][1] = $judge_01;


		//02 運動強度P整形
		$judge_02 = null;
		if (!empty($record_by_choice[10077]['selection']) || !empty($record_by_choice[10078]['selection']) ||
			!empty($record_by_choice[10079]['selection']) || !empty($record_by_choice[10080]['selection']) ||
			!empty($record_by_choice[10082]['selection'])) {
			$judge_02 = 1;
		} elseif (!empty($record_by_choice[10111]['selection'])) {
			$judge_02 = 1;
		} elseif (!empty($record_by_choice[10081]['selection']) || !empty($record_by_choice[10083]['selection']) ||
				!empty($record_by_choice[10084]['selection']) || !empty($record_by_choice[10085]['selection'])) {
			$judge_02 = 2;
		} elseif (!empty($record_by_choice[10112]['selection'])) {
			$judge_02 = 2;
		} else {
			$judge_02 = 3;
		}
		$judge['judge_sports'][2] = $judge_02;
		//print_r($record_by_choice[10177]['selection']);die;
		//03 運動強度P運動
		if (!empty($record_by_choice[10177]['selection'])) {
			$judge['judge_sports'][3] = 1;
		} elseif (!empty($record_by_choice[10178]['selection'])) {
			$judge['judge_sports'][3] = 2;
		} elseif (!empty($record_by_choice[10179]['selection'])) {
			$judge['judge_sports'][3] = 3;
		} else {
			//return Ethna::raiseError('運動強度P運動を特定できません', E_USER_ERROR);
			$judge['judge_sports'][3] = '';
			//return '運動強度P運動を特定できません';
		}
		//print_r($judge);die;
		//04 運動強度P腹囲
		if (30 <= $judge['bmi']) {
			$judge['judge_sports'][4] = 3;
		} elseif (26 <= $judge['bmi']) {
			$judge['judge_sports'][4] = 2;
		} else {
			$judge['judge_sports'][4] = 1;
		}
		//05 運動強度P過去
		if (!empty($record_by_choice[10183]['selection'])) {
			$judge['judge_sports'][5] = 1;
		} elseif (!empty($record_by_choice[10184]['selection'])) {
			$judge['judge_sports'][5] = 2;
		} elseif (!empty($record_by_choice[10185]['selection'])) {
			$judge['judge_sports'][5] = 3;
		} else {
			$judge['judge_sports'][5] = '';	
			//return Ethna::raiseError('運動強度P過去を特定できません', E_USER_ERROR);
			//return '運動強度P運動を特定できません';
		}
		//06 運動強度判定値
		if ($judge['sex']==1) {
			if (76 <= $judge['age']) {
				$target_sheet = 'WS76_xxM';
			} elseif (66 <= $judge['age']) {
				$target_sheet = 'WS66_75M';
			} elseif (46 <= $judge['age']) {
				$target_sheet = 'WS46_65M';
			} else {
				$target_sheet = 'WS18_45M';
			}
		} else {
			if (71 <= $judge['age']) {
				$target_sheet = 'WS71_xxF';
			} elseif (61 <= $judge['age']) {
				$target_sheet = 'WS61_70F';
			} elseif (51 <= $judge['age']) {
				$target_sheet = 'WS51_60F';
			} else {
				$target_sheet = 'WS18_50F';
			}
		}
		$judge['judge_sports'][6] = $GLOBALS[$target_sheet][$judge['judge_sports'][1]][$judge['judge_sports'][2]][$judge['judge_sports'][3]][$judge['judge_sports'][4]][$judge['judge_sports'][5]];
		//$this->logger->log(LOG_NOTICE, "参照先 [" . print_r($target_sheet, true) . "]" . " [" . print_r($judge['judge_sports'][1], true) . "]" . " [" . print_r($judge['judge_sports'][2], true) . "]" . " [" . print_r($judge['judge_sports'][3], true) . "]" . " [" . print_r($judge['judge_sports'][4], true) . "]" . " [" . print_r($judge['judge_sports'][5], true) . "]" . "");
		//$this->logger->log(LOG_NOTICE, "judge_sports [" . print_r($judge['judge_sports'][6], true) . "]");
		
		//運動強度指導例の配列を並べ替え
		//
		$sports = $GLOBALS['FOLLOWUP_SPORTS_BY_CHOICE_ID'];
		$sports_array = array();
		if(is_array($sports)){
			foreach($sports as $key => $value){
				if($value["route"] != 1){ continue;}
				$sports_array[$value['exercise_value']][] = $value['sports_name'];
			}
		}
		//07 運動強度指導例
		$judge['judge_sports'][7] = $sports_array[$judge['judge_sports'][6]];
		/*
		switch($judge['judge_sports'][6]) {
			case 'A':
				$judge['judge_sports'][7] = array('ランニング', '柔道', 'キックボクシング', 'ラグビー', '水泳（バタフライ）',);
				break;
			case 'B':
				$judge['judge_sports'][7] = array('軽いジョギング', 'テニス', '水泳', 'ランニング', 'サッカー', 'スケート', 'スキー', 'サイクリング',);
				break;
			case 'C':
				$judge['judge_sports'][7] = array('早歩き', '自転車こぎ', 'アクアビクス', '太極拳', '卓球', 'バドミントン', 'バレエ', '野球', 'バスケットボール', 'エアロビクス',);
				break;
			case 'D':
				$judge['judge_sports'][7] = array('ウェイトトレーニング', 'フリスビー', 'ボーリング', 'バレーボール', '体操', 'ゴルフ',);
				break;
			case 'E':
			default:
				$judge['judge_sports'][7] = null;
				break;
		}*/

		return $judge;
	}

	public function commonlogicCalculateGuidance1($judge, $value1) {

		//print_r($judge);die;

		//01 理想体重
		$judge['output'][1] = round($judge['tall'] * $judge['tall'] * 22 / 10000, 0);

		//02 減算下限カロリー
		$judge['output'][2] = $value1 * 0.85;

		//03 BMI
		$judge['output'][3] = floor($judge['bmi']);

		//04 生活活動強度判定値MIN
		if (30 <= $judge['output'][3]) {
			$judge['output'][4] = 20;
		} else {
			switch($judge['job_data']['strength']) {
				case 1:	$judge['output'][4] = 25;	break;
				case 2:	$judge['output'][4] = 30;	break;
				case 3:	$judge['output'][4] = 35;	break;
			}
		}

		//05 生活活動強度判定値MAX
		if (30 <= $judge['output'][3]) {
			$judge['output'][5] = 25;
		} else {
			switch($judge['job_data']['strength']) {
				case 1:	$judge['output'][5] = 30;	break;
				case 2:	$judge['output'][5] = 35;	break;
				case 3:	$judge['output'][5] = 40;	break;
			}
		}

		//06 理想カロリーMIN
		$judge['output'][6] = $judge['output'][1] * $judge['output'][4];

		//07 理想カロリーMAX
		$judge['output'][7] = $judge['output'][1] * $judge['output'][5];

		return $judge;
	}

	public function commonlogicCalculateGuidance2($judge, $record_by_choice, $value1, $value2) {
		
		//08 運動強度可能係数
		switch($judge['judge_sports'][6]) {
			case 'A':	$judge['output'][8] = 10;	break;
			case 'B':	$judge['output'][8] = 8;	break;
			case 'C':	$judge['output'][8] = 5;	break;
			case 'D':	$judge['output'][8] = 3;	break;
			case 'E':	$judge['output'][8] = 0;	break;
			default:	$judge['output'][8] = 0;	break;
		}

		//09 運動消費想定カロリー
		$judge['output'][9] = ($judge['sports_viable_count'] / 7) *  $judge['sports_viable_time'] * $judge['output'][8] * 1.05 * $judge['weight'] * 0.8;
		
		//10 食事削減カロリーMIN
		$judge['output'][10] = $value1 - $judge['output'][7] - $judge['output'][9];
		
		//11 食事削減カロリーMAX
		$judge['output'][11] = $value1 - $judge['output'][6] - $judge['output'][9];
		
		//12 理想削減カロリー
		$judge['output'][12] = $value1 * 0.15;
		
		//13 理想カロリー調整値詳細
		if ($judge['output'][11] >= $judge['output'][12]) {
			$judge['output'][13] = round($judge['output'][2], 0);
		} else {
			$judge['output'][13] = round($judge['output'][6], 0).'～'.round($judge['output'][7], 0);
		}

		//14 日常生活消費カロリー
		$calories = 0;
		$calories += ($record_by_choice[10164]['input_float'] / 60) * 3 * 1.05 * $judge['weight']; //歩き
		$calories += ($record_by_choice[10165]['input_float'] / 60) * 2 * 1.05 * $judge['weight']; //バスまたは電車
		$calories += ($record_by_choice[10166]['input_float'] / 60) * 1 * 1.05 * $judge['weight']; //車
		$calories += ($record_by_choice[10423]['input_float'] / 60) * 4 * 1.05 * $judge['weight']; //自転車 20120626 add
		$calories += ($record_by_choice[10167]['input_float'] / 60) * 2 * 1.05 * $judge['weight']; //たっている時間
		$calories += ($record_by_choice[10171]['input_float'] / 60) * 2 * 1.05 * $judge['weight']; //たっている時間
		$calories += ($record_by_choice[10168]['input_float'] / 60) * 1.5 * 1.05 * $judge['weight']; //座って（作業して）いる時間
		$calories += ($record_by_choice[10172]['input_float'] / 60) * 1.5 * 1.05 * $judge['weight']; //座って（作業して）いる時間
		$calories += ($record_by_choice[10169]['input_float'] / 60) * 3 * 1.05 * $judge['weight']; //歩いている時間
		$calories += ($record_by_choice[10173]['input_float'] / 60) * 3 * 1.05 * $judge['weight']; //歩いている時間
		$calories += ($record_by_choice[10170]['input_float'] / 60) * 4 * 1.05 * $judge['weight']; //軽く息切れするくらいに走っている時間
		$calories += ($record_by_choice[10174]['input_float'] / 60) * 4 * 1.05 * $judge['weight']; //軽く息切れするくらいに走っている時間
		$judge['output'][14] = $calories;

		//15 栄養所要量2（現版では未使用・・・将来的に移行）
		$judge['output'][15] = $value2 + $judge['output'][9] + $judge['output'][14];

		return $judge;
	}

	public function commonlogicJudgeEnable($judge, $record_by_choice)
	{
		if (!empty($record_by_choice[10072]['selection']) | !empty($record_by_choice[10073]['selection']) | !empty($record_by_choice[10074]['selection'])) {
			$judge['judge_enable'] = false;
		} else {
			$judge['judge_enable'] = true;
		}

		return $judge;
	}

	public function judgeMental($judge, $record_by_choice)
	{
		//共通判定ロジック
		
		// sec_id:2102 物事に集中したり、何かを始める気力がありますか？ 10240 or 10241
		// sec_id:2103 週末は気分転換に出かけたり、趣味の時間を取れていますか？ 10242 or 10243
		// sec_id:2104 今までは楽しかった事、趣味は出来ていますか？ 10244 or 10245
		if (!empty($record_by_choice[10241]['selection']) & !empty($record_by_choice[10243]['selection']) & !empty($record_by_choice[10245]['selection'])) {
			$flg_AB = 'B';
		} else {
			$flg_AB = 'A';
		}
		
		// sec_id:2107 睡眠は十分に取れていますか？ はい：10251 いいえ：10252
		// sec_id:2108 どのように睡眠が不足していますか？ いそがしい：10253 熟睡感がない：10254 寝つきが悪い：10255
		if (!empty($record_by_choice[10251]['selection']) | (!empty($record_by_choice[10252]['selection']) & !empty($record_by_choice[10253]['selection']))) {
			$flg_CD = 'C';
		} else {
			$flg_CD = 'D';
		}
		
		//sec_id:2109 食欲はありますか？ はい：10256 いいえ：10257
		//sec_id:2110 いつもに比べて、ここ2週間の食事の量はどうですか？ 普段：10258 多い：10259 少ない：10260
		//sec_id:2111 季節・天候・体調などで食欲が変わりやすい時期ですか？ はい：10261 いいえ：10262
		//sec_id:2112 この2週間で大幅な（±5%以上）の体重変動がありましたか？ はい：10263 いいえ：10264
		//sec_id:2113 体重はどの程度変わりましたか？ 自由入力

		if (!empty($record_by_choice[10256]['selection'])) {
			$flg_EFG = 'E';
		} else {
			//全パターン分解
			if(!empty($record_by_choice[10258]['selection']) & !empty($record_by_choice[10261]['selection']) & !empty($record_by_choice[10263]['selection'])){
				$flg_EFG = 'F';
			}
			if(!empty($record_by_choice[10258]['selection']) & !empty($record_by_choice[10261]['selection']) & !empty($record_by_choice[10264]['selection'])){
				$flg_EFG = 'G';
			}
			if(!empty($record_by_choice[10258]['selection']) & !empty($record_by_choice[10262]['selection']) & !empty($record_by_choice[10263]['selection'])){
				$flg_EFG = 'F';
			}
			if(!empty($record_by_choice[10258]['selection']) & !empty($record_by_choice[10262]['selection']) & !empty($record_by_choice[10264]['selection'])){
				$flg_EFG = 'G';
			}
			
			if(!empty($record_by_choice[10259]['selection']) & !empty($record_by_choice[10261]['selection']) & !empty($record_by_choice[10263]['selection'])){
				$flg_EFG = 'F';
			}
			if(!empty($record_by_choice[10259]['selection']) & !empty($record_by_choice[10261]['selection']) & !empty($record_by_choice[10264]['selection'])){
				$flg_EFG = 'G';
			}
			if(!empty($record_by_choice[10259]['selection']) & !empty($record_by_choice[10262]['selection']) & !empty($record_by_choice[10263]['selection'])){
				$flg_EFG = 'F';
			}
			if(!empty($record_by_choice[10259]['selection']) & !empty($record_by_choice[10262]['selection']) & !empty($record_by_choice[10264]['selection'])){
				$flg_EFG = 'G';
			}
			
			if(!empty($record_by_choice[10260]['selection']) & !empty($record_by_choice[10261]['selection']) & !empty($record_by_choice[10263]['selection'])){
				$flg_EFG = 'F';
			}
			if(!empty($record_by_choice[10260]['selection']) & !empty($record_by_choice[10261]['selection']) & !empty($record_by_choice[10264]['selection'])){
				$flg_EFG = 'G';
			}
			if(!empty($record_by_choice[10260]['selection']) & !empty($record_by_choice[10262]['selection']) & !empty($record_by_choice[10263]['selection'])){
				$flg_EFG = 'F';
			}
			if(!empty($record_by_choice[10260]['selection']) & !empty($record_by_choice[10262]['selection']) & !empty($record_by_choice[10264]['selection'])){
				$flg_EFG = 'F';
			}
		}

		//ルート分岐
		if (!empty($record_by_choice[10238]['selection'])) {
			$route = 'right';
		} else {
			$route = 'left';
		}

		$result = false;
		//ルート別判定ロジック
		switch($route) {
			case 'left':
				$judge_table['A']['C']['E'] = 'R01';
				$judge_table['A']['C']['F'] = 'R03';
				$judge_table['A']['C']['G'] = 'R02';
				$judge_table['A']['D']['E'] = 'R05';
				$judge_table['A']['D']['F'] = 'R07';
				$judge_table['A']['D']['G'] = 'R06';
				$judge_table['B']['C']['E'] = 'R09';
				$judge_table['B']['C']['F'] = 'R11';
				$judge_table['B']['C']['G'] = 'R10';
				$judge_table['B']['D']['E'] = 'R12';
				$judge_table['B']['D']['F'] = 'R14';
				$judge_table['B']['D']['G'] = 'R13';
				$result = $judge_table[$flg_AB][$flg_CD][$flg_EFG];
				break;
			case 'right':
				if ($flg_AB == 'A') {
					$judge_table['C']['E'] = 'R15';
					$judge_table['C']['F'] = 'R17';
					$judge_table['C']['G'] = 'R16';
					$judge_table['D']['E'] = 'R19';
					$judge_table['D']['F'] = 'R21';
					$judge_table['D']['G'] = 'R20';
					$result = $judge_table[$flg_CD][$flg_EFG];
				} else {
					if (!empty($record_by_choice[10272]['selection'])) {
						$input[18] = 1;
					} else {
						$input[18] = 2;
					}
					if (!empty($record_by_choice[10274]['selection'])) {
						$input[19] = 1;
					} else {
						$input[19] = 2;
					}
					if (!empty($record_by_choice[10276]['selection'])) {
						$input[20] = 1;
					} else {
						$input[20] = 2;
					}
					$judge_small[1][1][1] = 'f';
					$judge_small[1][1][2] = 'e';
					$judge_small[1][2][1] = 'd';
					$judge_small[1][2][2] = 'c';
					$judge_small[2][1][1] = 'b';
					$judge_small[2][1][2] = 'a';
					$judge_small[2][2][1] = 'b';
					$judge_small[2][2][2] = 'a';
					$flg_small = $judge_small[$input[18]][$input[19]][$input[20]];

					$judge_table['C']['E']['a'] = 'R23';
					$judge_table['C']['E']['b'] = 'R23';
					$judge_table['C']['E']['c'] = 'R23';
					$judge_table['C']['E']['d'] = 'R23';
					$judge_table['C']['E']['e'] = 'R23';
					$judge_table['C']['E']['f'] = 'R23';
					
					$judge_table['C']['F']['a'] = 'S07';
					$judge_table['C']['F']['b'] = 'S08';
					$judge_table['C']['F']['c'] = 'S09';
					$judge_table['C']['F']['d'] = 'S10';
					$judge_table['C']['F']['e'] = 'S11';
					$judge_table['C']['F']['f'] = 'S12';
					
					$judge_table['C']['G']['a'] = 'S01';
					$judge_table['C']['G']['b'] = 'S02';
					$judge_table['C']['G']['c'] = 'S03';
					$judge_table['C']['G']['d'] = 'S04';
					$judge_table['C']['G']['e'] = 'S05';
					$judge_table['C']['G']['f'] = 'S06';
					
					$judge_table['D']['E']['a'] = 'R24';
					$judge_table['D']['E']['b'] = 'R24';
					$judge_table['D']['E']['c'] = 'R24';
					$judge_table['D']['E']['d'] = 'R24';
					$judge_table['D']['E']['e'] = 'R24';
					$judge_table['D']['E']['f'] = 'R24';
					
					$judge_table['D']['F']['a'] = 'S19';
					$judge_table['D']['F']['b'] = 'S20';
					$judge_table['D']['F']['c'] = 'S21';
					$judge_table['D']['F']['d'] = 'S22';
					$judge_table['D']['F']['e'] = 'S23';
					$judge_table['D']['F']['f'] = 'S24';
					
					$judge_table['D']['G']['a'] = 'S13';
					$judge_table['D']['G']['b'] = 'S14';
					$judge_table['D']['G']['c'] = 'S15';
					$judge_table['D']['G']['d'] = 'S16';
					$judge_table['D']['G']['e'] = 'S17';
					$judge_table['D']['G']['f'] = 'S18';
					$result = $judge_table[$flg_CD][$flg_EFG][$flg_small];

				}
		}

		$result_text = array (
		'R01'=> array('うつ病の可能性は極めて低いです。',
									'精神科コンサルト不要です。'),
		'R02'=> array('うつ病の可能性は極めて低いです。',
									'今後の体重変動に注意してフォローアップしてください。変化があれば精神科コンサルトをしましょう。'),
		'R03'=> array('うつ病の可能性は極めて低いです。',
									'体重変動の有無、器質的疾患の鑑別を行い、注意深くフォローしてください。必要に応じて精神科コンサルトをしましょう。'),
		'R04'=> array('うつ病の可能性は低いです。',
									'体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。必要に応じて精神科コンサルトをしましょう。'),
		'R05'=> array('うつ病の可能性は低いです。',
									'必要に応じて、不眠症について専門家に相談してください。'),
		'R06'=> array('うつ病の可能性は低いです。',
									'今後の体重変動に注意してフォローアップしてください。変化があれば精神科コンサルトをしましょう。'),
		'R07'=> array('うつ病の可能性は低いです。',
									'体重変動の有無、器質的疾患の鑑別を行い、注意深くフォローしてください。必要に応じて精神科コンサルトをしましょう。'),
		'R08'=> array('うつ病の可能性は否定できません。',
									'うつ病の可能性があるため、必要に応じて専門家に相談してください。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'R09'=> array('うつ病の可能性は低いです。',
									'必要に応じて意欲の低下について専門家に相談してください。定期的にフォローアップしてください。'),
		'R10'=> array('うつ病の可能性は低いです。',
									'体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。必要に応じて精神科コンサルトをしましょう。'),
		'R11'=> array('うつ病の可能性は否定できません。',
									'うつ病の可能性があるため、必要に応じて専門家に相談してください。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'R12'=> array('うつ病の可能性は否定できません。',
									'意欲の低下、睡眠障害もあり、うつ病の可能性があるため、必要に応じて専門家に相談してください。注意深くフォローしてください。'),
		'R13'=> array('うつ病の可能性は否定できません。',
									'体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。意欲の低下、睡眠障害があり必要に応じて専門家に相談してください。'),
		'R14'=> array('うつ病の可能性があります。',
									'意欲の低下、睡眠障害、食欲低下があり、うつ病の可能性があるため、専門家に相談してください。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'R15'=> array('うつ病の可能性は低いです。',
									'精神科コンサルト不要です。気分の落ち込みについてフォローしましょう。'),
		'R16'=> array('うつ病の可能性は低いです。',
									'気分の変動、今後の体重変動に注意してフォローアップしましょう。変化があれば精神科コンサルトしましょう。'),
		'R17'=> array('うつ病の可能性は否定できません。',
									'体重変動の有無、器質的疾患の鑑別を行い注意深くフォローしてください。変化があれば精神科コンサルトしましょう。'),
		'R18'=> array('うつ病の可能性があります。',
									'器質的疾患の鑑別を行い、うつ病の可能性について精神科コンサルトしましょう。'),
		'R19'=> array('うつ病の可能性は否定できません。',
									'気分の落ち込み、不眠症について必要に応じて専門家に相談してください。注意深くフォローしてください。'),
		'R20'=> array('うつ病の可能性は否定できません。',
									'今後の体重変動に注意してフォローアップしましょう。変化があれば精神科コンサルトしましょう。'),
		'R21'=> array('うつ病の可能性があります。',
									'体重変動の有無、器質的疾患の鑑別を行い、精神科コンサルトしましょう。'),
		'R22'=> array('うつ病の可能性があります。',
									'うつ病の可能性があるため、専門家に相談してください。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'R23'=> array('うつ病の可能性があります。',
									'意欲の低下について専門家に相談してください。定期的にフォローアップしてください。'),
		'R24'=> array('うつ病の可能性が高いです。',
									'希死念慮について確認し、近日中に精神科に相談しましょう。'),
		'S01'=> array('うつ病の可能性があります。',
									'体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。精神科コンサルトしましょう。'),
		'S02'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S03'=> array('うつ病の可能性があります。',
									'自責の念がありうつ病が疑われます。体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。精神科コンサルトしましょう。'),
		'S04'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S05'=> array('うつ病の可能性が高いです。',
									'自責の念が強くうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S06'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S07'=> array('うつ病の可能性があります。',
									'体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。精神科コンサルトしましょう。'),
		'S08'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S09'=> array('うつ病の可能性が高いです。',
									'自責の念がありうつ病が疑われます。体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。精神科コンサルトしましょう。'),
		'S10'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S11'=> array('うつ病の可能性が高いです。',
									'自責の念が強くうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S12'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S13'=> array('うつ病の可能性が高いです。',
									'体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。睡眠障害も含めて精神科コンサルトしましょう。'),
		'S14'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S15'=> array('うつ病の可能性が高いです。',
									'自責の念がありうつ病が疑われます。体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。精神科コンサルトしましょう。'),
		'S16'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S17'=> array('うつ病の可能性が高いです。',
									'自責の念が強くうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S18'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S19'=> array('うつ病の可能性が高いです。',
									'体重減少の精査、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。睡眠障害も含めて精神科コンサルトしましょう。'),
		'S20'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S21'=> array('うつ病の可能性が高いです。',
									'自責の念がありうつ病が疑われます。体重変動の有無、器質的疾患の鑑別を行い、意欲の低下を含めて注意深くフォローしてください。精神科コンサルトしましょう。'),
		'S22'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S23'=> array('うつ病の可能性が高いです。',
									'自責の念が強くうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		'S24'=> array('うつ病の可能性が高いです。',
									'希死念慮がありうつ病の可能性があるため、近日中に精神科に相談しましょう。体重変動を含め器質的疾患の鑑別を行い、注意深くフォローしてください。'),
		);

		$judge['mental_result'] = $result_text[$result];

		return $judge;
	}


	public function foodByInterviewId($record_by_choice) {

		// 食事の栄養素情報取得
		$food_data = \Interview::selectFoodDataList();

		// if (Ethna::isError($food_data)){
		// 	return $food_data;
		// }

		$food_choice_list[1]  = array('group' => 1,'choice' => array(10311, 10334, 10357, 10380)); //めし
		$food_choice_list[2]  = array('group' => 1,'choice' => array(10312, 10335, 10358, 10381)); //和風めん
		$food_choice_list[3]  = array('group' => 1,'choice' => array(10313, 10336, 10359, 10382)); //洋風めん/中華めん
		$food_choice_list[4]  = array('group' => 1,'choice' => array(10314, 10337, 10360, 10383)); //パン（甘い菓子パンは菓子類へ）
		$food_choice_list[5]  = array('group' => 1,'choice' => array(10315, 10338, 10361, 10384)); //いも類（主食）
		$food_choice_list[6]  = array('group' => 2,'choice' => array(10316, 10339, 10362, 10385)); //肉類
		$food_choice_list[7]  = array('group' => 2,'choice' => array(10317, 10340, 10363, 10386)); //魚介類
		$food_choice_list[8]  = array('group' => 2,'choice' => array(10318, 10341, 10364, 10387)); //卵
		$food_choice_list[9]  = array('group' => 2,'choice' => array(10319, 10342, 10365, 10388)); //大豆（とうふ・納豆・厚揚げ）
		$food_choice_list[10] = array('group' => 3,'choice' => array(10424, 10425, 10426, 10427)); //汁
		$food_choice_list[11] = array('group' => 3,'choice' => array(10320, 10343, 10366, 10389)); //緑黄色野菜
		$food_choice_list[12] = array('group' => 3,'choice' => array(10321, 10344, 10367, 10390)); //淡色野菜
		$food_choice_list[13] = array('group' => 3,'choice' => array(10322, 10345, 10368, 10391)); //海藻類
		$food_choice_list[14] = array('group' => 3,'choice' => array(10323, 10346, 10369, 10392)); //きのこ類
		$food_choice_list[15] = array('group' => 3,'choice' => array(10324, 10347, 10370, 10393)); //いも類（副菜）
		$food_choice_list[16] = array('group' => 4,'choice' => array(10326, 10349, 10372, 10395)); //乳類
		$food_choice_list[17] = array('group' => 4,'choice' => array(10325, 10348, 10371, 10394)); //果実類
		$food_choice_list[18] = array('group' => 5,'choice' => array(10330, 10353, 10376, 10399)); //菓子（水分少、脂質多）
		$food_choice_list[19] = array('group' => 5,'choice' => array(10331, 10354, 10377, 10400)); //菓子（水分少、脂質少）
		$food_choice_list[20] = array('group' => 5,'choice' => array(10332, 10355, 10378, 10401)); //菓子（水分多、脂質多）
		$food_choice_list[21] = array('group' => 5,'choice' => array(10333, 10356, 10379, 10402)); //菓子（水分多、脂質少）
		$food_choice_list[22] = array('group' => 4,'choice' => array(10428, 10429, 10430, 10431)); //水、白湯
		$food_choice_list[23] = array('group' => 4,'choice' => array(10327, 10350, 10373, 10396)); //茶類
		$food_choice_list[24] = array('group' => 4,'choice' => array(10328, 10351, 10374, 10397)); //ジュース類
		$food_choice_list[25] = array('group' => 4,'choice' => array(10432, 10436, 10440, 10444)); //酒（ビール）
		$food_choice_list[26] = array('group' => 4,'choice' => array(10433, 10437, 10441, 10445)); //酒（日本酒）
		$food_choice_list[27] = array('group' => 4,'choice' => array(10434, 10438, 10442, 10446)); //酒（ワイン）
		$food_choice_list[28] = array('group' => 4,'choice' => array(10435, 10439, 10443, 10447)); //酒（焼酎、ウィスキー、ブランデー）

		$nutrient['Col_003'] = 0; //たんぱく質
		$nutrient['Col_004'] = 0; //脂質
		$nutrient['Col_005'] = 0; //炭水化物
		$nutrient['Col_020'] = 0; //カルシウム
		$nutrient['Col_023'] = 0; //鉄
		$nutrient['Col_043'] = 0; //ビタミンB1
		$nutrient['Col_044'] = 0; //ビタミンB2
		$nutrient['Col_051'] = 0; //ビタミンC
		$nutrient['Col_058'] = 0; //食物繊維
		$nutrient['Col_059'] = 0; //塩分
		$nutrient['Col_070'] = 0; //脂肪酸

		$food_kcal[1] = 0; //主食
		$food_kcal[2] = 0; //主菜
		$food_kcal[3] = 0; //副菜・汁物
		$food_kcal[4] = 0; //果物･乳類･飲料
		$food_kcal[5] = 0; //おやつ

		
		$total_kcal = 0;
		if(isset($food_data) && is_array($food_data) && $food_data)foreach ($food_data as $food_id => $food_value) {
			if(isset($food_choice_list[$food_id])&&is_array($food_choice_list[$food_id]['choice'])&&$food_choice_list[$food_id]['choice'])foreach ($food_choice_list[$food_id]['choice'] as $choice_id) {
				$unit = $record_by_choice[$choice_id]['input_float'] / $food_value['UnitWeight']; //単位 = 入力値 / 単位重量

				$nutrient['Col_003'] += $food_value['Col_003'] * $unit;
				$nutrient['Col_004'] += $food_value['Col_004'] * $unit;
				$nutrient['Col_005'] += $food_value['Col_005'] * $unit;
				$nutrient['Col_020'] += $food_value['Col_020'] * $unit;
				$nutrient['Col_023'] += $food_value['Col_023'] * $unit;
				$nutrient['Col_043'] += $food_value['Col_043'] * $unit;
				$nutrient['Col_044'] += $food_value['Col_044'] * $unit;
				$nutrient['Col_051'] += $food_value['Col_051'] * $unit;
				$nutrient['Col_058'] += $food_value['Col_058'] * $unit;
				$nutrient['Col_059'] += $food_value['Col_059'] * $unit;
				$nutrient['Col_070'] += $food_value['Col_070'] * $unit;

				$kcal = $food_value['Col_001'] * $unit;
				$food_kcal[$food_choice_list[$food_id]['group']] += $kcal;
				$total_kcal += $kcal;
			}
		}

		// PFCバランス
        $psc = array();
        if($total_kcal){
    		$psc['p'] = $nutrient['Col_003'] / $total_kcal * 100; //たんぱく質(%) =  たんぱく質(kcal) /  食品および食事全体のエネルギー量(kcal)  ×  100
	    	$psc['f'] = $nutrient['Col_004'] / $total_kcal * 100; //脂質(%)       =  脂質(kcal)       /  食品および食事全体のエネルギー量(kcal)  ×  100
		    $psc['c'] = $nutrient['Col_005'] / $total_kcal * 100; //炭水化物(%)   =  炭水化物(kcal)   /  食品および食事全体のエネルギー量(kcal)  ×  100
        }

		//表示桁数
		$nutrient['Col_003'] = number_format($nutrient['Col_003'] , 0);
		$nutrient['Col_004'] = number_format($nutrient['Col_004'] , 0);
		$nutrient['Col_005'] = number_format($nutrient['Col_005'] , 1);
		$nutrient['Col_020'] = number_format($nutrient['Col_020'] , 2);
		$nutrient['Col_023'] = number_format($nutrient['Col_023'] , 0);
		$nutrient['Col_043'] = number_format($nutrient['Col_043'] , 2);
		$nutrient['Col_044'] = number_format($nutrient['Col_044'] , 2);
		$nutrient['Col_051'] = number_format($nutrient['Col_051'] , 0);
		$nutrient['Col_058'] = number_format($nutrient['Col_058'] , 1);
		$nutrient['Col_059'] = number_format($nutrient['Col_059'] , 1);
		$nutrient['Col_070'] = number_format($nutrient['Col_070'] , 0);
		$food_kcal[1] = number_format($food_kcal[1] , 0);
		$food_kcal[2] = number_format($food_kcal[2] , 0);
		$food_kcal[3] = number_format($food_kcal[3] , 0);
		$food_kcal[4] = number_format($food_kcal[4] , 0);
		$food_kcal[5] = number_format($food_kcal[5] , 0);

		$data['nutrient'] = $nutrient;
		$data['kcal']['group'] = $food_kcal;
		$data['kcal']['total'] = $total_kcal;
		$data['psc'] = $psc;

		return $data;

	}

	public function setGuidanceDataList($judge,$record_by_choice,$judge_type){
	
		$judge["guidance"]["bmi"] = $judge['bmi'];
		$judge["guidance"]["judge_id"] = $judge['judge_metabo'][$judge_type];
		$judge["guidance"]["ideal_weight"] = $judge["output"][1];
		$judge["guidance"]["high_blood_flg"] = (@$record_by_choice[10067]['selection']) ? true : false;
		$judge["guidance"]["diabetes_flg"] = (@$record_by_choice[10068]['selection']) ? true : false;
		$judge["guidance"]["hyperlipidemia_flg"] = (@$record_by_choice[10069]['selection']) ? true : false;
		$judge["guidance"]["metabo_decision_aids"] = ""; //現在未使用
		$judge["guidance"]["metabo_decision_value"] = $judge['judge_metabo'][$judge_type];
		$judge["guidance"]["forecast"] = @$judge['judge_metabo'][24];
		$judge["guidance"]["exercise_example_type"] = "";
		$judge["guidance"]["exercise_cnt"] = $judge["sports_viable_count"];
		$judge["guidance"]["exercise_time"] = $judge["sports_viable_time"];
		$judge["guidance"]["ideal_calorie_min"] = $judge['output'][6];
		$judge["guidance"]["ideal_calorie_max"] = $judge['output'][7];
		$judge["guidance"]["exercise_value"] = $judge['judge_sports'][6];
		$judge["guidance"]["current_calorie"] = $judge['food']['kcal']['total'];
		
		
		switch($judge["guidance"]["exercise_value"]){
			CASE "A": 
				$judge["guidance"]["mets_min"] = 10;
				$judge["guidance"]["mets_max"] = 99;
				break;
			CASE "B":
				$judge["guidance"]["mets_min"] = 7;
				$judge["guidance"]["mets_max"] = 9.9;
				break;
			CASE "C":
				$judge["guidance"]["mets_min"] = 4;
				$judge["guidance"]["mets_max"] = 6.9;
				break;
			CASE "D":
				$judge["guidance"]["mets_min"] = 2.5;
				$judge["guidance"]["mets_max"] = 3.9;
				break;
			CASE "E":
				$judge["guidance"]["mets_min"] = 0;
				$judge["guidance"]["mets_max"] = 0;
				break;
		}
		
		$judge["guidance"]["current_height"] = $judge['tall'];
		$judge["guidance"]["current_weight"] = $judge['weight'];
		
		return $judge;
		
	}

	public function commonlogicCalculateJudgement($judge) {
		
		//01 腹囲判定値
		if ($judge['sex'] == 1) {
			if ($judge['abdominal_girth'] >= 100) {
				$judge['judge_metabo'][1] = 3;
			} elseif ($judge['abdominal_girth'] >= 85) {
				$judge['judge_metabo'][1] = 2;
			} else {
				$judge['judge_metabo'][1] = 1;
			}
		} else {
			if ($judge['abdominal_girth'] >= 100) {
				$judge['judge_metabo'][1] = 4;
			} elseif ($judge['abdominal_girth'] >= 90) {
				$judge['judge_metabo'][1] = 3;
			} elseif ($judge['abdominal_girth'] >= 75) {
				$judge['judge_metabo'][1] = 2;
			} else {
				$judge['judge_metabo'][1] = 1;
			}
		}

		//02 BMI
		$judge['judge_metabo'][2] = floor($judge['bmi']);

		//03 BMI判定値
		if ($judge['judge_metabo'][2] >= 25) {
			$judge['judge_metabo'][3] = 4;
		} elseif ($judge['judge_metabo'][2] >= 22) {
			$judge['judge_metabo'][3] = 3;
		} elseif ($judge['judge_metabo'][2] >= 18) {
			$judge['judge_metabo'][3] = 2;
		} else {
			$judge['judge_metabo'][3] = 1;
		}

		return $judge;
	}

	public function commonlogicOutputJudgement($judge) {

		$retArr = array();
		$age = $judge['age'];
		$target_sheet = "";
		if ($judge['sex'] == 1) {
			if ($age >= 71) {
				$target_sheet = "71_xxM";
			} elseif ($age >= 61) {
				$target_sheet = "61_70M";
			} elseif ($age >= 51) {
				$target_sheet = "51_60M";
			} elseif ($age >= 41) {
				$target_sheet = "41_50M";
			} elseif ($age >= 31) {
				$target_sheet = "31_40M";
			} elseif ($age >= 21) {
				$target_sheet = "21_30M";
			} else {
				$target_sheet = "18_20M";
			}
		} else {
			if ($age >= 71) {
				$target_sheet = "71_xxF";
			} elseif ($age >= 61) {
				$target_sheet = "61_70F";
			} elseif ($age >= 51) {
				$target_sheet = "51_60F";
			} elseif ($age >= 41) {
				$target_sheet = "41_50F";
			} elseif ($age >= 31) {
				$target_sheet = "31_40F";
			} elseif ($age >= 21) {
				$target_sheet = "21_30F";
			} else {
				$target_sheet = "18_20F";
			}
		}

		/*
		1:腹囲 3:BMI判定値 12:カロリー判定値 13:夕食判定値 15:飲酒判定値 16:運動判定値
		から
		3:BMI判定値 4：食事判定値 13:夕食判定値 15:飲酒判定値 16:運動判定値
		に変更
		*/
		
		// require_once dirname(dirname(__FILE__))."/data/MetaboJudgeData".$target_sheet.".php";
		$function = 'MetaboJudgeData'.$target_sheet;
		\MetaboJudgeData::{$function}();
		$judge['judge_metabo'][17] = $GLOBALS["METABO_".$target_sheet][$judge['judge_metabo'][3]][$judge['judge_metabo'][4]][$judge['judge_metabo'][13]][$judge['judge_metabo'][15]][$judge['judge_metabo'][16]];

		return $judge;
	}

	public function commonlogicCalculateRiskBasic($judge, $record_by_choice, $value1, $value2, $value3, $value4 ,$value5) {

		$retArr = array();
		//リスク判定基礎値
		$tmp_value = 0;
		if ($judge['sex'] == 1 || $judge['sex'] == 2 && 55 <= $judge['age']) {
			$tmp_value++;
		}
		if (!empty($record_by_choice[10121]['selection']) || !empty($record_by_choice[10122]['selection']) || !empty($record_by_choice[10123]['selection'])) {
			$tmp_value++;
		}
		if ($judge['show_smoking_leading']) {
			$tmp_value++;
		}
		if (65 <= $judge['age']) {
			$tmp_value += 3;
		} elseif (60 <= $judge['age']) {
			$tmp_value += 2;
		} elseif (55 <= $judge['age']) {
			$tmp_value += 1;
		}
		$retArr[0] = $tmp_value;
		
		//BMI判定値2
		//22>BMI>0
		if (22 > $value1 && $value1 > 0){
			$value_bmi = 1;
		} else if ($value1 >= 22){
			$value_bmi = 2;
		}
		
		//リスク判定習慣値
		$retArr[1] = ($value_bmi + $value2 + $value3 + $value4 + $value5) * 0.5;
		//予測結果2
		$retArr[2] = min(5, max(1, round(($retArr[0] + $retArr[1]) / 2)));

		return $retArr;
	}

	public function sendMail($param, $type = null)
	{
		//$param['validation_key'] = $this->randString();
        $interview_data = \DB::table("t_interview")->where("interview_id",$param["first_interview_id"])->get();
        $param['validation_key'] = $interview_data[0]["validation_key"];

		//if($type == 'user_register_mail') {
		//	$update_interview = Base::update_interview(array('interview_id' => $param['first_interview_id']), array('user_register_mail' => $param['validation_key']));
		//}else {
		//	$update_interview = Base::update_interview(array('interview_id' => $param['first_interview_id']), array(/*'validation_key' => $param['validation_key'], */'user_change_mail' => $param['validation_key']));
		//}
		
		if(isset($param['type'])) {

			$file = app_path() . '/data/followup/entrymail.xml';

		} else {

			$file_exists = app_path() . '/data/followup/followup_'.$param['tenant_id'].'.xml';
			if (file_exists($file_exists)) {
				$file = $file_exists;
			} else {
				$file = app_path() . '/data/followup/followup.xml';
			}

		}

        $followup_url = $this->get_followup_url($param);
		$mailedit_url = $this->get_email_url($param);
		$entry_url = $this->get_entry_url($param);

		$SimpleXmlElement = new \SimpleXmlElement(file_get_contents($file));
		$xml = json_decode(json_encode($SimpleXmlElement), true);

		$detail = str_replace("\n", '<br/>', $xml['content']['body']);
		$detail = str_replace('{$week}', $param['week'], $detail);
		$detail = str_replace('{$followup_url}', $followup_url, $detail);
		$detail = str_replace('{$mailedit_url}', $mailedit_url, $detail);
		$detail = str_replace('{$entry_url}', $entry_url, $detail);

		$user = array('email' => $param['email'], 'name' => $param['name'], 'fromaddress' => $xml['content']['fromaddress'], 'fromname' => $xml['content']['fromname'], 'subject' => $xml['content']['subject']);
		$data = array('subject' =>  $xml['content']['subject'], 'detail' => $detail);

		Mail::send('notice.index', $data, function($message) use ($user)
		{
			$message->from($user['fromaddress'], $user['fromname']);
			$message->to($user['email'], $user['name'])->subject($user['subject']);
			//$message->cc('more@addresses.com');
			//$messages->bcc(array('evenmore@address.com' => 'Another name', 'onelast@address.com'));

		});

	}

    /**
     * @param $param require param => week , first_interview_id , email , validation_key
     * @return string
     */
    public function get_followup_url($param){
        if(!isset($param['validation_key']) || !isset($param['email'])){
            return URL::to('/followup');
        }
        $plain_text = "week={$param['week']}&interview_id={$param['first_interview_id']}&email={$param['email']}&validation_key={$param['validation_key']}";
        $encode_text_followup = base64_encode($plain_text);
        $followup_url = URL::to('/followup?section_set_id=5&').$encode_text_followup;
        return $followup_url;
    }

    public function get_email_url($param){
        $plain_text = "week={$param['week']}&interview_id={$param['first_interview_id']}&email={$param['email']}&validation_key={$param['validation_key']}";
        $encode_text_followup = base64_encode($plain_text);
        $followup_url = URL::to('/followup?address_mod=1&').$encode_text_followup;
        return $followup_url;
    }

    public function get_entry_url($param){
        $plain_text = "week={$param['week']}&interview_id={$param['first_interview_id']}&email={$param['email']}&validation_key={$param['validation_key']}";
        $encode_text_entry = base64_encode($plain_text);
        $entry_url = URL::to('/followup?entry=1&').$encode_text_entry;
        return $entry_url;
    }

	public function randString() {

		$str = '';
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$size = strlen($chars);
		for( $i = 0; $i < 15; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
        $is_exist = \DB::table("t_interview")->where("validation_key",$str)->get();
        if(count($is_exist)){
            return $this->randString();
        }
		return $str;
	}		

}
