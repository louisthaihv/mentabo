
function changeDateOption(o){
	var f = o.form;
	if(!(f.birth_date_y.value == "0") && !(f.birth_date_m.value == "0")) {
		var d = new Date(f.birth_date_y.value, f.birth_date_m.value, 1);
		d.setTime(d.getTime() - (24 * 3600 * 1000));
		lastDay = d.getDate();
		f.birth_date_d.options.length = lastDay+1;
		for(i = 28; i <= lastDay; i++){
			f.birth_date_d.options[i].text = i;
			f.birth_date_d.options[i].value = i;
		}
	}
}

window.document.keydown = keydown;
function keydown(e) {
	if (window.event.keyCode == 8 && window.event.srcElement.readOnly == true){
		return false;
	}
}

function convert(t) {
	var str = t.value;
	for (i = 0; i < str.length; i++) {
		var chr = str.charAt(i);
		if (chr.match(/^[０-９]$/)) {
			str = str.replace(str[i], String.fromCharCode(str[i].charCodeAt(0) - 0xFEE0)); //全角数字の文字コードから65248個前
		}
	}
	t.value = str;
}


$(function(){
	$("div.compulsoryInputError").css("display", "none");
	$(".choices input:radio").button(); $(".choices input:checkbox").button();
	$(".choices-full input:radio").button(); $(".choices-full input:checkbox").button();
	$(".choices-half input:radio").button(); $(".choices-half input:checkbox").button();
	$(".choices-third input:radio").button(); $(".choices-third input:checkbox").button();
	$(".choices-quarter input:radio").button(); $(".choices-quarter input:checkbox").button();
	$(".tabs").tabs();
	$(".accordion").accordion({autoHeight: false, navigation: true});

	$(".annotation-overlay a[rel]").overlay({
		top: 'center',
		left: 'center',
		fixed: false
	});

	$("#instruction").keyup(function(){
		var count = $(this).val().length;
		var max = 250;
		$("#rest").text(max - count);

	});

	$("#print-btn").click( function() {
/*		var name = $('#name').val();
		if (name == false) {
			jAlert('入力者名を入力してください', 'エラー');
			return false;
		}
*/
		var length = $("#instruction").val().length;
		if (length > 250) {
			jAlert('特記事項は250文字以内で入力してください', '入力エラー');
			return false;
		}
		jConfirm('印刷を開始しますか？', '印刷', function(r) {
			if (r) {
				printHistory();
				return false;
			} else {
				return false;
			}
		});
	});

	$("#followup-print-btn").click( function() {
		jConfirm('フォローアップ結果の印刷を開始しますか？', '印刷', function(r) {
			if (r) {
				printFollowupHistory();
				return false;
			} else {
				return false;
			}
		});
	});

	function printHistory() {
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: $('#absolute_url').val() + 'history',
			data: {
				'view_type' : $('#view_type').val(),
				'interview_id' : $('#interview_id').val(),
				'instruction' : $('#instruction').val(),
				'name' : $('#name').val()
			},
			success: function(data, dataType){
				if (data.result == true) {
					print();
				} else {
					jAlert('エラー：印刷できません');
				}
			},
			error: function(){
				jAlert('エラー：印刷できません');
			}
		});
	}

	function printFollowupHistory() {
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: $('#absolute_url').val() + 'history',
			data: {
				'view_type' : $('#view_type').val(),
				'interview_id' : $('#interview_id').val(),
				'name' : $('#name').val()
			},
			success: function(data, dataType){
				if (data.result == true) {
					print();
				} else {
					jAlert('エラー：フォローアップ結果を印刷できません');
				}
			},
			error: function(){
				jAlert('エラー：フォローアップ結果を印刷できません');
			}
		});
	}
	
});


	/** ロングクリック感知 */
	var admin_long_action = function( aElement, aEvent ) {
		this.action = false ;
		this.onEvent = aEvent ;
		var self = this ;
		this.long_time2 = new lib_timer(
				lib_timer.loop,
				function ( ) {
					self.call( self.onEvent.onLong ) ;
				},50
			) ;
		this.long_time = new lib_timer(
				lib_timer.one,
				function ( ) {
					self.action = true ;
					self.long_time2.start( ) ;
				},500
				
			) ;
			
		aElement.bind( 'mousedown', function( aEvent ) {			// マウスダウン
				self.long_time.start( ) ;
			}) ;
		aElement.bind( 'mouseup', function( ) {			// マウスアップ
				self.long_time2.stop( ) ;
				self.long_time.stop( ) ;
				if ( !self.action ) self.call( self.onEvent.onClick ) ;
				self.action = false ;
			}) ;
		aElement.bind( 'mouseout', function( ) {			// マウスキャンセル
				self.long_time2.stop( ) ;
				self.long_time.stop( ) ;
				self.action = false ;
			}) ;
		// おまじないイベント
		aElement.hover( function () { },
				function () {
					self.long_time2.stop( ) ;
					self.long_time.stop( ) ;
					self.action = false ;
				}) ;
	}

	admin_long_action.prototype = {
		/** [private] コールバック実行
		 * @param aFunction - (function) コールバックファンクション
		 * @param aStatus - (int) ステータス
		 */
		call: function( aFunction ) {
			aFunction.apply( this ) ;
		}
	}


	/** ロングクリック感知 */
	var long_action = function( aElement, aEvent ) {
		this.action = false ;
		this.onEvent = aEvent ;

		var self = this ;
		this.long_time2 = new lib_timer(
				lib_timer.loop,
				function ( ) {
					self.call( self.onEvent.onLong ) ;
				},
				50
			) ;
		this.long_time = new lib_timer(
				lib_timer.one,
				function ( ) {
					self.action = true ;
					self.long_time2.start( ) ;
				},
				500
				
			) ;
			
		if ( window.ontouchstart === null ) {
            /*
			aElement.bind('dblTap', function() {
				});
			aElement.bind('touchstart', function() {
					self.long_time.start( ) ;
				});
			aElement.bind('touchend', function() {
					self.long_time2.stop( ) ;
					self.long_time.stop( ) ;
					if ( !self.action ) self.call( self.onEvent.onClick ) ;
					self.action = false ;
				});
			aElement.bind('touchcancel', function() {
					self.long_time2.stop( ) ;
					self.long_time.stop( ) ;
					self.action = false ;
				});
				*/
            aElement.bind( 'mousedown', function( aEvent ) {
                // マウスダウン
                self.long_time.start( ) ;
            }) ;
            aElement.bind( 'mouseup', function( ) {
                // マウスアップ
                self.long_time2.stop( ) ;
                self.long_time.stop( ) ;
                if ( !self.action ) self.call( self.onEvent.onClick ) ;
                self.action = false ;
            }) ;
            aElement.bind( 'mouseout', function( ) {
                // マウスキャンセル
                self.long_time2.stop( ) ;
                self.long_time.stop( ) ;
                self.action = false ;
            }) ;
            // おまじないイベント
            aElement.hover( function () { },
                function () {
                    self.long_time2.stop( ) ;
                    self.long_time.stop( ) ;
                    self.action = false ;
                }) ;
		} else {
			aElement.bind( 'mousedown', function( aEvent ) {
						// マウスダウン
					self.long_time.start( ) ;
				}) ;
			aElement.bind( 'mouseup', function( ) {
					// マウスアップ
					self.long_time2.stop( ) ;
					self.long_time.stop( ) ;
					if ( !self.action ) self.call( self.onEvent.onClick ) ;
					self.action = false ;
				}) ;
			aElement.bind( 'mouseout', function( ) {
						// マウスキャンセル
					self.long_time2.stop( ) ;
					self.long_time.stop( ) ;
					self.action = false ;
				}) ;
			// おまじないイベント
			aElement.hover( function () { },
					function () {
						self.long_time2.stop( ) ;
						self.long_time.stop( ) ;
						self.action = false ;
					}) ;
		}

	}

	long_action.prototype = {
		/** [private] コールバック実行
		 * @param aFunction - (function) コールバックファンクション
		 * @param aStatus - (int) ステータス
		 */
		call: function( aFunction ) {
			aFunction.apply( this ) ;
		}
	}
	/** タイマー
	 * @param aType - (int) タイマータイプ 0/通常タイマー 1/ループタイマー
	 * @param aCallBack - (function) コールバックファンクション
	 * @param aTime - (long) タイマー時間(ms)
	 */
	var lib_timer = function( aType, aCallBack, aTime ) {
		this.type		= aType ;			// (int) タイマータイプ 0/タイマー 1/ループタイマー
		this.timer_id	= -1 ;				// (int) タイマーID
		this.callback	= aCallBack ;	// (function) コールバック
		this.time		= aTime ;			// (long) タイマー時間ms
		this.proc 		= false ;			// (bookean) 処理状態 true/処理中 false/待機
	}
	lib_timer.prototype = {
		/** タイマー時間(ms)設定
		 * @param aTime - (long) 時間(ms)
		 */
		setTime: function( aTime ) {
			this.time = aTime ;
		},
		/** タイマー起動 */
		start: function( ) {
			this.stop( ) ;

			var self = this ;
			switch ( this.type ) {
				case 0 : {
					this.timer_id = setTimeout(
								function( ) {
									self.stop( ) ;
									self.exec( ) ;
								},
								this.time
							) ;
				} break ;
				case 1 : {
					this.timer_id = setInterval(
								function( ) { self.exec( ) },
								this.time
							) ;
				} break ;
			}
			return this ;
		},
		/** コールバックファンクション実行 */
		exec: function( ) {
			if ( this.proc ) return ;
			this.proc = true ;
			this.callback( this ) ;
			this.proc = false ;
		},
		/** タイマー停止 */
		stop: function( ) {
			this.proc = false ;
			if ( !this.timer_id ) return ;

			switch ( this.type ) {
				case 0 : clearTimeout( this.timer_id ) ; break ;
				case 1 : clearInterval( this.timer_id ) ; break ;
			}
		}
	}
	lib_timer.one = 0 ;
	lib_timer.loop = 1 ;
 