<div class="change-input-area">

	<?php if(isset($checkmail) && $checkmail === false) { ?>
		
		<p>メールアドレスを受け付けました。<br />次回のメール配信より変更されます。</p>
	
	<?php } else { ?>
        @if(Input::get("followup_changerequest_do"))
		    <p>このメールアドレスはすでに登録されています。</p>
        @endif
		<form action="" method="POST">
			<input type="hidden" value="followup_changerequest_do" name="followup_changerequest_do">
			<p class="coution">変更を行うメールアドレスを入力して下さい。	</p>
			<p>変更メールアドレス：<input value="{{ Input::get("mailaddress") }}" name="mailaddress"></p>
			<div class="btn-area">
				<p><input type="submit" value="メールアドレスを登録する" class="input-btn"/></p>
			</div>
		</form>	

	<?php } ?>

</div>