<?php

class Notice extends Eloquent {

    const TEMPORARY_INTERVIEW_TABLE_NAME = "temp_interview";
    const TEMPORARY_SENDMAIL_TABLE_NAME = "temp_sendmail";
    const TEMPORARY_SENDMAIL_FOLLOWUP_TABLE_NAME = "t_send_followup_mail";

    public static function selectUser($user_id) {
        return DB::table('t_user')->where('user_id', $user_id)->first();
    }

    public static function createTemporaryInterviewTable($date = '')
	{	
        $sql  = " CREATE TEMPORARY TABLE " . self::TEMPORARY_INTERVIEW_TABLE_NAME ;
        $sql .= " (PRIMARY KEY (interview_id)) ";
        $sql .= " SELECT i.* FROM t_interview i";
        //$sql .= " SELECT i.* FROM t_interview i where date(finished_datetime) >= date('{$date}') and flg_followup = 1 ";
        return DB::statement($sql);
    }

	public static function createTemporarySendTable($data)
	{	
        $sql  = " CREATE TEMPORARY TABLE " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $sql .= " (PRIMARY KEY (interview_id)) ";
        $sql .= " SELECT 'default' as mail_type, i.*, 0 as first_interview_id , 0 as week,0 as plan_date FROM " . self::TEMPORARY_INTERVIEW_TABLE_NAME . " i where flg_followup = $data ";
        //$sql .= " SELECT 'default' as mail_type, i.*, 0 as first_interview_id , 0 as week,0 as plan_date FROM " . self::TEMPORARY_INTERVIEW_TABLE_NAME . " i where flg_followup = $data limit 0,1 ";
        return DB::statement($sql);
    }

	public static function insertInterviewNoticeDataByDate($date = '', $is_temporary = true)
	{	

        $sql  = " INSERT IGNORE INTO " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $sql .= " SELECT 'notice' as mail_type , i.* , f.* FROM " . self::TEMPORARY_INTERVIEW_TABLE_NAME . " i ";
        $sql .= " LEFT JOIN ";
        $sql .= "   (SELECT first_interview_id , min(week) as week , min(plan_date) as plan_date FROM t_followup f WHERE input_date is null and (finish_flg = 0 or finish_flg is null) GROUP BY first_interview_id) as f ";
        $sql .= "   ON i.interview_id = f.first_interview_id ";
        $sql .= " WHERE i.flg_followup = 1 ";
        $sql .= "   AND date(i.finished_datetime) = date('{$date}') ";
        $sql .= " HAVING first_interview_id is null ";
        DB::insert( $sql);

        $sql = " select * from " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $return = DB::select($sql);

        $list = array();
        foreach ($return as $key => $row) {
           $list[] = $row;
        }       
        return $list;
    }

	public static function insertInterviewRemindDataByDate($date = '', $is_temporary = true)
	{

        $sql  = " INSERT IGNORE INTO " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $sql .= " SELECT 'remind' as mail_type , i.* , f.*  FROM " . self::TEMPORARY_INTERVIEW_TABLE_NAME . " i ";
        $sql .= " LEFT JOIN ";
        $sql .= "   (SELECT first_interview_id , min(week) week, min(plan_date) plan_date FROM t_followup f WHERE input_date is null and (finish_flg = 0 or finish_flg is null) GROUP BY first_interview_id) as f ";
        $sql .= "   ON i.interview_id = f.first_interview_id ";
        $sql .= " WHERE i.flg_followup = 1 ";
        $sql .= "   AND date(i.finished_datetime) < date('{$date}') ";
        $sql .= "   AND DAYOFWEEK(i.finished_datetime) = DAYOFWEEK('{$date}') ";
        $sql .= " HAVING first_interview_id is null ";
		DB::insert($sql);

        $sql = " select * from " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $return = DB::select($sql);

        $list = array();
        foreach ($return as $key => $row) {
           $list[] = $row;
        }       
        return $list;
	}

	public static function insertFollowupRemindDataByDate($date = '', $is_temporary = true)
	{

        $sql  = " INSERT IGNORE INTO " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $sql .= " SELECT 'remind' as mail_type , i.* , f.*  FROM " . self::TEMPORARY_INTERVIEW_TABLE_NAME . " i ";
        $sql .= " INNER JOIN ";
        $sql .= "   (SELECT first_interview_id , min(week) week, min(plan_date) plan_date FROM t_followup f WHERE input_date is null and (finish_flg = 0 or finish_flg is null) GROUP BY first_interview_id) as f ";
        $sql .= "   ON  i.interview_id = f.first_interview_id ";
        $sql .= "   AND DAYOFWEEK(plan_date) = DAYOFWEEK('2014-08-19 02:59:20') ";
        $sql .= " WHERE i.flg_followup = 1 ";
        $sql .= " AND date(f.plan_date) < date('{$date}') ";
        DB::insert($sql);

        $sql = " select * from " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $return = DB::select($sql);

        $list = array();
        foreach ($return as $key => $row) {
           $list[] = $row;
        }       
        return $list;

	}

	public static function insertFollowupNoticeDataByDate($date = '', $is_temporary = true)
	{	

        $sql  = " INSERT IGNORE INTO " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $sql .= " SELECT 'notice' as mail_type , i.* , f.*  FROM " . self::TEMPORARY_INTERVIEW_TABLE_NAME . " i ";
        $sql .= " INNER JOIN ";
        $sql .= "   (SELECT first_interview_id , min(week) week, min(plan_date) plan_date FROM t_followup f WHERE input_date is null and  (finish_flg = 0 or finish_flg is null) GROUP BY first_interview_id) as f ";
        $sql .= "   ON  i.interview_id = f.first_interview_id ";
        $sql .= " WHERE i.flg_followup = 1 ";
        $sql .= " AND date(f.plan_date) = date('{$date}') ";
        DB::insert($sql);

        $sql = " select * from " . self::TEMPORARY_SENDMAIL_TABLE_NAME ;
        $return = DB::select($sql);

        $list = array();
        foreach ($return as $key => $row) {
           $list[] = $row;
        }       
        return $list;

    }

    public static function setSendLog($first_interview_id, $mail_type, $week, $validation_key, $email)
    {   
       $array = array('first_interview_id' => $first_interview_id, 'mail_type' => $mail_type, 'week' => $week, 'validation_key' => $validation_key, 'email' => $email, 'send_datetime' => date('Y-m-d H:i:s'), 'send_flg' => 1);
       return DB::table(self::TEMPORARY_SENDMAIL_FOLLOWUP_TABLE_NAME)->insert($array);
    }

}